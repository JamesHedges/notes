# FROM docker.repo1.uhc.com/python:3.12.3-alpine3.20
FROM python:3.12.3-alpine3.20

RUN pip install mkdocs
RUN pip install mkdocs-mermaid2-plugin
RUN pip install mkdocs-material
RUN pip install mkdocs-material-extensions
RUN pip install mkdocs-open-in-new-tab
RUN pip install mkdocs-table-reader-plugin
RUN pip install pymdown-extensions
RUN pip install Pygments
RUN pip install openpyxl
RUN pip install mkdocs-drawio-url

WORKDIR /mnt/research-docs

CMD ["mkdocs", "serve"]
