# AI

## Definitions

- **Generative:** In this context, generative AI algorithms can be used to create new content or generate code in response to training datasets.
- **Predictive:** Teams can use predictive AI algorithms to make decisions or improve processes based on previous outcomes.

- **Augmented Intelligence:** This AI technology has the capacity to accelerate decision-making processes that typically require human intervention.
- **Transformer:** A model architecture that eschews recurrence and instead relies entirely on an attention mechanism to draw global dependencies between input and output.

## Automation

### Five practical applications

From [Five Practical Applications of AI in Automation](https://www.forbes.com/sites/forbestechcouncil/2023/10/02/five-practical-applications-of-ai-in-automation/)

#### 1. Continuously improving processes

It's possible to improve end-to-end processes by combining process orchestration with AI. For example, a process orchestration system gathers data on process execution and performance across a variety of endpoints (e.g., the people, systems and devices that make up a process). This process execution data can be fed into a predictive AI model to make predictions about how certain processes will perform (e.g., process duration), based on the performance of similar processes in the past.

The holy grail of self-healing processes isn’t far away with the addition of emerging augmented intelligence models. This technology has the potential to detect bottlenecks in processes, provide suggestions for enhancement and either automatically update models or update them following human validation of the decision.

#### 2. Generating process testing data

One compelling use of generative AI involves the generation of testing data for processes. For example, there are many ways that forms can produce errors, based on typical human errors—think about the myriad ways of representing dates (1.15.23 versus 15.1.23 versus January 15, 2023, etc.).

Using past human behavior as a reference, the system can create tests that have the potential to "challenge" a form, including scenarios like inputting incorrect numeric values, surpassing character limits and so on.

#### 3. Accelerating decision-making

Many organizations already use automation to accelerate certain decisions, such as accepting or rejecting a mortgage application based on an applicant’s credit score and perceived risk profile. Predictive AI can be used alongside decision models to accelerate certain decision-making, such as predicting instances of fraud based on previous user data in a fraud-prevention scenario.

Companies with advanced AI capabilities can harness machine learning-ready datasets from a process orchestration system, seamlessly integrating them with internal datasets to anticipate patterns and extract valuable insights for informed decision-making.

#### 4. Streamlining human tasks

Generative AI can play an important role in streamlining certain human tasks. Take, for example, a community market that wants to process applications for new vendors. By incorporating the ChatGPT API into a process model, the market could automate the extraction of pertinent data from forms to produce either acceptance or rejection emails—and generate descriptions for each approved vendor to be featured on the market's website.

#### 5. Developing process models

The business and enterprise software architects or developers who write code for process models could benefit from generative AI, as well. Teams can explore the use of open-source code generators like GitHub Copilot to build and code a process model, drawing inspiration from existing models established within the organization.

Rather than beginning from square one, these tools can effectively optimize limited development resources and enhance the level of automation across the entire organization.

## Transformers

ToDo: add basic info on transformers

<object data="../attention-is-all-you-nee-v7.pdf" type="application/pdf" width="1200px" height="1000px">
    <embed src="../attention-is-all-you-nee-v7.pdf" type="application/pdf" />
</object>

## Prompting

### Prompt attributes

- Length
- Format
- Audience
- Domain
- Perspective
- Tone
- Purpose

### Prompt chaining

- Token Limit: limits how much context returns in response
- Context Limit: limitations on how much of previous conversation a chat can remember

### Shot based prompting

## References

- [Five Practical Applications of AI in Automation](https://www.forbes.com/sites/forbestechcouncil/2023/10/02/five-practical-applications-of-ai-in-automation/)
- [How Tranformers Work](https://www.datacamp.com/tutorial/how-transformers-work)
- [Tranformer - Wiki](https://en.wikipedia.org/wiki/Transformer_%28deep_learning_architecture%29)
- [Attention is All You Need](https://paperswithcode.com/method/transformer)