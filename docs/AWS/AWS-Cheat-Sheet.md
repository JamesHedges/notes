<!-- omit in toc -->
# AWS Cheat Sheet

## Access Keys

### Managing Access Keys

- [Managing access keys (AWS CLI)](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey_CLIAPI)

### Rotating Access Keys

- [Rotating access keys](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_RotateAccessKey)

## DocumentDb

From the DocumentDb page, select the **Connectivity & Security** tab

Get rds-combined-ca-bundle.pem file or get it from here [RDS Download PRM](https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem)

From the **Configuration** tab, get the cluster endpoint (host name in Robo 3T) and port

Create a connection in Robo 3T

## Extract Binary from Secret

```powershell
# Using AWS CLI

$rawInfo = aws secretsmanager get-secret-value --secret-id arn:aws:secretsmanager:us-east-1:524668999053:secret:commonwell/dev/certificate/info/ClientCert-7H632G --query SecretString --output text
$certInfo = $rawInfo | ConvertFrom-Json
$certString = aws secretsmanager get-secret-value --secret-id $certInfo.CertificateArn --query SecretBinary --output text
$certBytes = [System.Convert]::FromBase64String($certString)
$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2 $certBytes, "Relay123", "ephemeralkeyset"

# Using AWS PowerShell

Function Get-PfxSecret {
    param (
        [Parameter(Mandatory)] $SecretInfoArn
    )

    $secretInfo = Get-SECSecretValue -SecretId $SecretInfoArn -Select SecretString | ConvertFrom-Json
    $certString = Get-SECSecretValue -SecretId $secretInfo.CertificateArn -Select SecretBinary
    $certBytes = [System.Byte[]]::CreateInstance([System.Byte],$certString.Length)
    $certString.ReadAsync($certBytes, 0, $certBytes.Length)
    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2 $certBytes, $secretInfo.Password, "ephemeralkeyset"
    
    return $cert
}
```

- [Setting the AWS CLI output format](https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-output-format.html#text-output)
- [PowerShell Encoding & Decoding (Base64)](https://adsecurity.org/?p=478)
- [get-secret-value](https://docs.aws.amazon.com/cli/latest/reference/secretsmanager/get-secret-value.html)
- [Get-SECSecretValue Cmdlet](https://docs.aws.amazon.com/powershell/latest/reference/index.html?page=Get-SECSecretValue.html&tocid=Get-SECSecretValue)

## Parsing JSON

Use `ConvertFrom-Json`

- [Parsing JSON with PowerShell](https://techcommunity.microsoft.com/t5/core-infrastructure-and-security/parsing-json-with-powershell/ba-p/2768721)
- [ConvertFrom-Json](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/convertfrom-json?view=powershell-7.2#examples)

## Query EC2 Instances

```powershell
# Get instance with tag Name=windows-runner-2
aws ec2 describe-instance --filters 'Name=tag:Name, Values=windows-runner-2'

# Project the results with a query
aws ec2 describe-instance --query 'Reservations[].Instances[].[InstanceId, InstanceType]'

# Mix it up
aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId, InstanceType][]' --filters 'Name=tag:Name, Values=windows-runner-1'
```

## Filtered Search with PowerShell

```powershell
$search = @(
  @{
     key = 'Name'
     values = 'commonwell/dev'
   }
)

Get-SECSecretList -Filter $search  | Select-Object -Property Name, ARN
```

## AWS PowerShell Modules

See [AWS Tools for PowerShell](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-welcome.html)

### Installing

See [Installing the AWS Tools for PowerShell on Windows](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-getting-set-up-windows.html)

```powershell
# Install the installer
Install-Module -Name AWS.Tools.Installer

# Install modules w/ cleanup (removes old versions)
Install-AWSToolsModule AWS.Tools.EC2, AWS.Tools.S3 -CleanUp
```

### Module Cmdlets

See documentation [AWS Tools for PowerShell Cmdlet Reference](https://docs.aws.amazon.com/powershell/latest/reference/Index.html)

#### SSM Parameter

##### SSM Parameter List

```powershell
Import-Module AWS.Tool.SimpleSystemsManagement

# Filtered
$filter = new-object -typename Amazon.SimpleSystemsManagement.Model.ParameterStringFilter -property @{ Key="Name"; Option="Contains"; Values="test"}
Get-SSMParameterList -ParameterFilter @($filter) -ProfileName MyProfile
```

## ECS

### Update Task's ECR Image

```powershell
$ecsService = "arn...service"
$ecsCluster = "arc...cluster"
$ecsRegion = "us-east-1"

# AWS CLI
aws ecs update-service --service ${ecsService}  --cluster ${ecsCluster} --region ${ecsRegion} --force-new-deployment

# AWS.Tools.ECS

Update-ECSService -Service $ecsService -Cluster $ecsCluster -Region $ecsRegion
```
