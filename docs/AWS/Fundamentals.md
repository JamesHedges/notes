<!-- omin in toc -->
# AWS Fundamentals

- [AWS Fundamentals](#aws-fundamentals)
  - [AWS Live Lessons](#aws-live-lessons)
    - [Overview](#overview)
    - [What is Cloud Computing](#what-is-cloud-computing)
    - [Regions](#regions)
    - [Availability Zones](#availability-zones)
    - [Edge Locations](#edge-locations)
    - [Scope of services](#scope-of-services)
  - [Identity and Access Management](#identity-and-access-management)
    - [Policies](#policies)
    - [Managing Access Keys](#managing-access-keys)
    - [Roles](#roles)
      - [Roles for EC2](#roles-for-ec2)
      - [Roles for Cross-Account Access](#roles-for-cross-account-access)
      - [Federated Users](#federated-users)
    - [First Steps and Best Practices](#first-steps-and-best-practices)
    - [Networking in AWS](#networking-in-aws)
      - [Virtual Private Cloud (VPC)](#virtual-private-cloud-vpc)
      - [Routing](#routing)
      - [Public, Private and Hybrid Subnets](#public-private-and-hybrid-subnets)
      - [Create and Configure Internet Gateway](#create-and-configure-internet-gateway)
      - [Network Access Control Lists (NACLs)](#network-access-control-lists-nacls)
    - [Security Groups](#security-groups)
    - [VPC Peering](#vpc-peering)
      - [VPC Peering Scenarios](#vpc-peering-scenarios)
    - [VPN Access](#vpn-access)
    - [AWS Direct Connect](#aws-direct-connect)
  - [Computing in AWS](#computing-in-aws)
    - [Introduction to Amazon Elastic Compute Cloud (EC2)](#introduction-to-amazon-elastic-compute-cloud-ec2)
    - [Amazon Machine Image (AMI)](#amazon-machine-image-ami)
    - [Key Pairs](#key-pairs)
    - [Launching EC2 Instance](#launching-ec2-instance)
    - [Instance Metadata Service](#instance-metadata-service)
    - [Bootstrapping with Userdata](#bootstrapping-with-userdata)
      - [Linux](#linux)
      - [Windows](#windows)
    - [Single vs Multi-Tenant Instances](#single-vs-multi-tenant-instances)
    - [Amazon EC2 Billing Options](#amazon-ec2-billing-options)
    - [Introduction to AWS Lambda](#introduction-to-aws-lambda)
    - [Introduction to Amazon Elastic Container Service (ECS)](#introduction-to-amazon-elastic-container-service-ecs)
    - [Introduction to Amazon ECS for Kubernetes (EKS)](#introduction-to-amazon-ecs-for-kubernetes-eks)
    - [Use Case: Running Container-Based Microservices](#use-case-running-container-based-microservices)
    - [VMware Cloud on AWS](#vmware-cloud-on-aws)
  - [Storage in AWS](#storage-in-aws)
    - [Overview of AWS Storage Options](#overview-of-aws-storage-options)
    - [Object vs. Block Storage](#object-vs-block-storage)
    - [Introduction to Amazon Simple Storage Service (S3)](#introduction-to-amazon-simple-storage-service-s3)
      - [Features](#features)
    - [Bucket Security with Resource Policies and ACLS](#bucket-security-with-resource-policies-and-acls)
    - [Introduction to Amazon Glacier](#introduction-to-amazon-glacier)
    - [Storage Class Management with Lifecycle Rules](#storage-class-management-with-lifecycle-rules)
    - [Instance Store Volumes](#instance-store-volumes)
    - [Introduction to Amazon Elastic Block Store (EBS)](#introduction-to-amazon-elastic-block-store-ebs)
    - [Amazon EBS Volume Types and Performance](#amazon-ebs-volume-types-and-performance)

## AWS Live Lessons

- Video course by Richard Jones
- Published 2019
- [AWS Live Lessons](https://learning.oreilly.com/videos/amazon-web-services/9780135581247/)

### Overview

### What is Cloud Computing

Characteristics of Cloud Computing

- On-Demand
- Broad Network Access
- Resource Pooling (multi-tenant environment)
- Rapid Elasticity (grow/shrink resources)
- Measure Service (pay as you go)

Service Model

- IaaS (raw computing)
- PaaS (abstracted infrastructure)
- SaaS (use IaaS & PaaS to build own software)

### Regions

Why select region

- Cost
- Serve application closest to users (reduce latency)
- Data Sovereignty (laws and compliance)

### Availability Zones

Within a region, have multiple data centers (facilities). AZ is a collection of data centers
Separated by distance for resiliency
Connected with high-speed, low latency private fiber

### Edge Locations

Provides:

- CloudFront (CDN)
- Route53
- Api Gateway

Can reside outside an AZ

### Scope of services

Where service runs

- Global: available in all regions
  - AWS IAM
  - CloudFront CDN
  - Route53
- Regional: resource lives in a region
  - DynamoDb
  - S3
  - Elastic Load Balancing (can balance across AZ in the region)
  - VPC
- AZ: Limited to an AZ (like EC2)
  - Elastic Block Store
  - EC2
  - Subnets

## Identity and Access Management

Can:

- Authentication
- authorization
- Users
- Groups
- Password policy
- Multi-factor Authorization

all applies to the

- --> AWS API AccessID + Secret Key
- Not Operating System own realm: User name + password/key pair
- Not Application own realm: user name + password

Tools using AWS API:

- CLI
- SDKs
- Management Console

Users and Groups

Can create in IAM
Option is to use federated (like Active Directory)

Users

- Created and exist within IAM Servcie
- Can give abilit to login to Management Console
- Can have long-term access keys (that can be used locally, not embedded in code)
- Can enable per-user MFA device (device, phone)

Groups

- Cannot be nested

Credential Types

| Credential                         | Usage                        | Comment                                              |
| :--------------------------------- | :--------------------------- | :--------------------------------------------------- |
| Email + Password                   | Master account (root) access | Must protect - if compromised, can do serious damage |
| Username + Password                | AWS Web Console              | IAM user                                             |
| Access Key + Secret Key            | CLI, SDK                     | Long-term credentials for local machineF             |
| Access/Secret Keys + Session Token | Role-based access            | Temporary credentials                                |

### Policies

- Determine authorization (permissions)
- Written in JSON
- Policy Types
  - Managed policies - first class entities live on their own
    - AWS Managed - created by AWS
    - Customer managed - created and managed by customer
  - Inline Policies - intrinsic to a group or user
- Create policies via
  - Generator using a console tool to write the JSON
  - Hand written policies

Example Policy

```json
{
  "Version": "2012-10-17",  //Date based version
  "Statement": [  // list of permissions
    {

      "Effect": "Allow", // can be Allow or Deny
      "Action": [
        "s3:GetObject", // name spaced to service
        "s3:PutObject"
      ],
      "Resource": [ // list of resources that the policy applies
        "arn:aws:s3:::mybucket/*" // limited to mybucket
      ],
      "Condition": { // conditions for when allowed/denied
        "IpAddress": { "aws:SourceIp": "52.98.x.x/30"} // limited to requests from this IpAddress, aws:SourceIp is a global context
      }
    }
  ]
}

{
  "Version": "2012-10-17",
  "Statement": [ 
    {

      "Effect": "Allow",
      "Action": [
        "iam:*AccessKey*", // can be wild-carded to allow all access key actions
        "iam:ChangePassword"
      ],
      "Resource": [
        "arn:aws:iam::12345678965433:user/${aws:username}" // limit to current user
      ]
    }
  ]
}
```

![Policy Evaluation](./PolicyEvaluation.png)

### Managing Access Keys

### Roles

Credential Anti-Patterns

- Embed access keys in code
- Embed in environment variables
- Share with
  - Third parties
  - Hundreds of enterprise users
  - Millions of web/app users

Credential Best Practices

- Use temporary credentials
- Delegate permissions to:
  - EC2 instance
  - AWS service
  - A user (elevate privileges)
  - Separate account
    - One you own
    - Third party

#### Roles for EC2

![Roles for EC2](./RolesForEC2.png)

#### Roles for Cross-Account Access

![Roles for Cross-Account Access](./RolesForCross-AccountAccess.png)

#### Federated Users

Existing Users

- Organizational users
  - LDAP
  - Microsoft Active Directory
- Web/mobile application users using federation:
  - Facebook
  - Google

Federation Options

- SAML 2.0
- AWS Directory Service
- Amazon Cognito (OpenID Connect)
- AWS SSO

### First Steps and Best Practices

AWS New Account First Steps

- Enable AWS CloudTrail - records all API calls
- Create an admin user in IAM (with optional condition)
- Enable MFA on root account
- Enable cost and usage report - detailed info about costs within AWS across services & regions
- Log out of root account
- Login as admin user
- Create additional users, groups, etc.

IAM Best Practices

- Master account (root) credentials
  - Email address + password
  - Protect at all costs
  - Delete any existing access/secret keys
  - Do not use for day-to-day operations
- Follow the principle of least privilege
- Rotate long-term credentials (access keys/passwords)
- Enable Multi-Factor Authentication - even enforce
  
### Networking in AWS

#### Virtual Private Cloud (VPC)

- Logically isolated network
- VPCs created per account per region
- Spans a _single_ region
- Can use all AZs within one region
- Can peer with other VPCs
- Supports numerous security mechanisms
- Internet and VPN gateways
- 5 VPCs per region - can  be increased upon request
- 200 subnets per region

Can design VPCs in many ways, e.g.

- By environment (dev, test, prod, etc)
- By application
- By department

AWS architecture begins with the network. It begins with Amazon VPC, because in order for us to really achieve security, high availability, fault tolerance, durability, for us to achieve those kinds of things, we have to design a network that allows us to make use of different Amazon features, like multiple availability zones and different security protocols or security mechanisms.

![Architecture Begins With VPC](Architecture-Begins-With-VPC.png)

#### Routing

Routing is concerned with the flow of traffic, whether or not any and all traffic is allowed to move between two different ranges of IP addresses. Routing is not at all concerned with protocols and ports. Routing is not concerned with TCP versus UVP, port 22 versus port 443. It's only concerned about whether or not any and all traffic can move. Once we have routing in place, then we can start to filter the traffic based on protocol and port and so on.

![Routing Overview](./Routing-Overview.png)

Routing configured using routing tables. First entry, the default, is typically the VPC. This allows all subnets to communicate with the other subnets on the VPC. The zero route, 0.0.0.0/0 allows traffic to flow to the internet through a gateway. This subnet would be public. Without the zero route, the subnet is private. A route to a VPN gateway allows a subnet to flow to another network.

#### Public, Private and Hybrid Subnets

If a subnet has a route to the internet, through a gateway, it is public. A hybrid subnet is an outbound only route that uses a Network Address Translation (NAT) gateway to flow traffic to the internet.

The reason we call it Network Address Translation is because it would translate the private IP of this instance into a public IP that would allow the NAT gateway to talk to the internet, and then when those responses come back from the internet, the NAT gateway then forwards those responses back to the instance. So that way the instance has the ability to reach out to the internet, get its responses, but it also remains protected from host potentially malicious activity on the internet trying to get in. This type of subnet is called hybrid.

![Public Hybrid Private](./Public-Hybrid-Private.png)

In the above diagram, 10.2.0.0/16 is a public subnet. 10.2.1.0/24 is a hybrid subnet. And 10.2.2.0/28 is private.

#### Create and Configure Internet Gateway

- In VPC Console
- Create internet gateway - will be detached
- Attach it to a VPC
- Enable routing --> Routing Tables
- Create route table
  - Add route
  - Set destination to 0.0.0.0/0
  - Target: internet gateway, the one just created
- Associate the route table with a subnet
- Edit the subnet table to be made public route table
  - Select the route table

#### Network Access Control Lists (NACLs)

Within Amazon VPC, again, one of the recommendations from AWS is to apply firewalls or security at key internal and external boundaries of our network. And so the VPC itself would be an external boundary and then subnets within the VPC would provide key internal boundaries. And then of course, the instances, or devices, would also be a boundary. In terms of a firewall for subnets, we have network access control lists.

![Firewall for subnets](.AWS/../Firewall-for-Subnets.png)

- Ingress Rules
  - All IPs are allowed to communicate with the internet through ports 80 and 443 (first and second rules)
  - All routes in the port range 1024-65535 (ephemeral port range) are allowed from the VPC (third rule)
  - All other routes from the internet are blocked (fourth rule)
- Egress Rules
  - Allow outbound traffic on ephemeral ports to all (first rule)
  - Deny outbound traffic on all other ports (second rule)

Port privileges:

- Privileged ports are ports are in the range 0-1023
- Non privileged ports (ephemeral) are in the range 1024-65535
- These ranges may be different for each OS

NACL Summary

- firewall for the subnet as a whole
- **Stateless**
- Rules applied in order
- Must specify ingress and egress
- May specify _allow_ or _deny_ rules
- Default NACL allows **all** ingress/egress

### Security Groups

Security Groups are firewalls that are applied to individual instances or other devices like load balancers.

SGs are stateful - recognize responses as responses. Don't need to open the ephemeral port range.

- Applied to "devices" - instances or load balancers
- Stateful - recognize responses
- Can specify ingress or egress, not required to specify both because they are stateful
- Defaults
  - All ingress traffic denied
  - All egress traffic allowed

A security group's destination may be another security group

### VPC Peering

- Connection between two VPCs
- Helps segregate networks
- No bottleneck or SPOF (single point of failure)
- IP ranges must not overlap when peering
- Can peer between accounts
- Always one-to-one
- No transitive peering
- No edge-to-edge routing
- $/GB bandwidth

![VPC Peering](./VPC-Peering.png)

![VPC Peering Does Not Allow Transitive](./VPC-Peering-non-transitive.png)

#### VPC Peering Scenarios

![Peering Scenarios - Centralized Resources](./Peering-Scenarios-Centralized-Resources.png)
![Peering Scenarios - Departments](./Peering-Scenarios-Departments.png)
![Peering Scenarios - Isolating Services](./Peering-Scenarios-Isolating-Services.png)
![Peering Scenarios - Third Party Access](./Peering-Scenarios-Third-Party-Access.png)

### VPN Access

![AWS Hardware VPN](./AWS-Hardware-Vpn.png)

### AWS Direct Connect

- Dedicated private fiber connection to AWS
- 1 Gbps or 10Gbps options
- Establish private connectivity to one or multiple VPCs
- Lower data transfer rate than internet traffic
- Increase bandwidth and throughput
- More consistent network performance
- Used External Border Gateway Protocol (eBPG)

Number of AWS Direct Connect providers

- Equinix
- Interxion
- Cologix
- Digital Realty
- and more...

![Direct Connect](./Direct-Connect.png)
![Direct Connect Terminations](./Direct-Connect-Terminations.png)

## Computing in AWS

### Introduction to Amazon Elastic Compute Cloud (EC2)

- Virual machines (instance)
- Your choice of Linus or Windows
- Xen or Nitro hypervisor (Nitor is newest, performance approaching bar metal)
- Bare metal is available
- Combinations of CPU, memory, disk, IO
- Launch one to thousands (according to account limits)
- Different billing models
- Hourly fee includes OS license
- Per second fee available (for some operating systems)
- AWS Marketplace offers canned solutions

### Amazon Machine Image (AMI)

- Bit-for-bit copy of root volume
- Choice of OS
  - Windows
  - Linux
  - FreeBSD
- Find AMIs:
  - AWS-provided
  - Trusted publishers
  - Community
    - Your responsibility to ensure it is secure
    - Don't know what went into them
    - OK for experiment, not production
  - AWS Marketplace
  - Private

Launching and Instance

1. Choose AMI
2. Launch instance
   1. select 'size'
   2. Install, configure, etc.
3. Create _private_ AMI

![Launching an Instance](./Launching-an-Instance.png)

### Key Pairs

Enable remote access to EC2 instances

- Public and private keys
  - Share public key with other systems
  - Enables public / private key authentication
  - Encryption
- 2048-bit SSH-2 RSA
- Create key pair
  - In AWS
  - Local and import
- AWS keeps public key
- AWS does not keep private key
- Choose key pair when launching
  - allows remote access

Linux

- Public key added to _.ssh/authorized_keys_
- Use private key for SSH login

Windows

- Administrator password encrypted
- Use private key to decrypt

### Launching EC2 Instance

Open port 22 for SSH

![Open port 22](./Open-Port-22.png)

Set pem file permission to read-only for intended user

![ssh into instance](./ssh-into-instance.png)

### Instance Metadata Service

URL for accessing instance metadata

- <http://169.254.169.254/latest/meta-data>
- Available only from an EC2 instance
- Instance can get information _about_ itself
- Can retrieve (includes but not limited to):
  - AMI ID
  - hostname
  - instance-id
  - instance-type
  - private IP address
- Often used with scripts for instance-specific configuration

![Querying Instance Metadata](./querying-instance-metadata.png)

### Bootstrapping with Userdata

- Script instance to _configure itself_
- Key to _high-availability_ & _self-healing_
- Limited to 16KB
- Executed _only at launch_ (first boot)
- Can be a list of environment variables

#### Linux

- #!/bin/bash
- #!/bin/node
- #!/bin/python
- Or other executable

#### Windows

- cmd script
- powershell script

### Single vs Multi-Tenant Instances

Can limit compute

![EC2 Host](./EC2-Host.png)

- Dedicated instance - only customer on the host
- Can have affinity to a dedicated host
- HIPAA recently allowed multi-tenancy in some cases
- Default is multi-tenance

### Amazon EC2 Billing Options

![EC2 Billing Options](./Training/EC2BillingOptions.png)

- Note the availability for On Demand and Spot billing models

Reserved Instances

- Up-front payment
  - None - least amount of discount
  - Partial
  - Full - most discount
- 1 or 3 year term
- Pay hourly fee regardless of use
- Purchasing & Launching are different
  - Pay even if not launched
  - Launched VM must match reseved
- Guaranteed availability
- Significant savings over on-demand
- Best for
  - Constant applications
  - Databases
  - Threshold - 50% time running

Spot Instances

- Fee based on supply/demand
- A way of selling unused capacity
- Fee fraction of on-demand (as high as 80%)
- Specify maximum spend
- Instance can be terminated by AWS
  - When demand is high
  - Get 2 minute warning
- Best for
  - Temporary work
  - Batch processing
  - Testing/experimentation
  - Billing/sales reports
- Apps must tolerate interruption

### Introduction to AWS Lambda

- "Server_less_" computing
  - You don't manage server
  - Lambda FaaS
- Great for
  - Scheduled tasks
  - Microservices
  - Event handlers
- There are other serverless services
- Pay for compute time per 100ms
  - Costs higher for more memory use
  - CPU allocated proportional to memory allocation
- Create functions
  - Inline editor
  - Upload zip file
    - All dependencies need to be included
- Invoke functions
  - CLI or SDK
  - Events
- AWS handles
  - Infrastructure
  - Deployment
  - Scaling
  - Defaults to 1000 concurrent executions
- Lambdas have limits (lookup what the current limits are for CPU, memory, timeouts, invocations, etc)

### Introduction to Amazon Elastic Container Service (ECS)

- Container management service
- Supports Docker containers
- Analogous to Kubernetes
- Schedule
  - long-running applications (like listening on a port)
  - batch jobs
- Integrates with
  - ELB
  - EBS
  - VPC
  - IAM

![ECS Cluster](./Training/EcsCluster.png)

- Auto Scaling Group
  - Scaled fleet of EC2 instances
  - If any fail/unhealthy, auto-scaling can replace
  - Can add/remove instances using auto-scaling
- Cluster
  - Inside of ECS
  - A logical grouping of EC2 instances (hosts)
  - Contains a cluster of services
- ECS tracks the state of containers
- Use ALB to route url to task based containers
  - Group a set of containers as a target group

![ECS Cluster 2](./Training/EcsCluster2.png)

### Introduction to Amazon ECS for Kubernetes (EKS)

- Fully-managed control plane
- AWS creates/manages master nodes
  - Including an AWS created VPC
- Customer manages worker nodes
  - Must add own VPC for worker nodes
  - Supply the EC2 instances with Kubernetes image
- Certified K8s conformant
- Compatible with applications designed for standard K8s

![EKS Architecture](./Training/EksArchitecture.png)

### Use Case: Running Container-Based Microservices

![Running Container-Based Microservices](./Training/RunningContainer-BasedMicroservices.png)

### VMware Cloud on AWS

- Integrated solution developed by AWS and VMWare
- Migrate/extend vSphere-based environments to AWS
- Sold, delivered, and supported by VMWare
- Provisioned on
  - bare-meta
  - single-tenant environment
- Continue using
  - vSphere
  - vSAN
  - NSX
  - vCenter Server

## Storage in AWS

### Overview of AWS Storage Options

Instance Store Volume:

- Built-in to EC2 instance
- High IOPs
- Considered ephemeral - data lost when machine goes away
- No snapshots - no durability
- No network

Elastic Block Store (EBS)

- Independent
- $/GB/Month in addition to EC2 costs
- Up to 16TB
- Durable - persists when machine goes away
- Snapshots
- Use with single EC2 instance

Elastic File System

- Shared file access
- Uses NGSv4
- Mount to
  - EC2
  - On-premises servers
- Automatically scales
- Redundantly store across AZs
- Use with multiple EC2 instances

Amazon Simple Storage Service (S3)

- Object storage
- All transfers via HTTP (only good for sequential access)
- Scales automatically
- Great for
  - Static assets
  - Logs
  - Data lakes

Glacier

- Archival storage
- Transition from S3
- Slow retrieval

> Glacier retrieval can take 3-5 hours

Storage Gateway

- Connects on-premises appliance with cloud-based storage
- Installed as VM on VMware ESXi or Microsoft Hyper-V
- Supports industry-standard storage protocols
- Stores all data encrypted in Amazon S3 or Amazon Glacier
- Three configurations
  - Gateway-Cached Volumes
    - Stores frequently-accessed data locally
  - Gateway-Stored Volumes
    - Stores primary data locally, point-in-time snapshots to S3
  - Gateway-Virtual Tape Library
    - Exposes iSCSI interface, each tape stored in Amazon S3 or Amazon Glacier

Storage Gateway Hardware Appliance

- Available for purchase from Amazon.com
- Software pre-loaded on Dell EMC PowerEdge
- Can greatly simplify procurement, deployment, and management

### Object vs. Block Storage

| Object Storage                               | Block Storage                                        |
| :------------------------------------------- | :--------------------------------------------------- |
| Upload entire object (e.g. entire 1 GB file) | Update volume blocks (e.g. with only 8 bits)         |
| Uses HTTP for transfer                       | Uses more efficient protocol                         |
| Cannot be mounted                            | Can be mounted                                       |
| No random IO                                 | Best for random IO                                   |
| Use when data is relatively static           | Use when data is more volatile (changes frequently) |

### Introduction to Amazon Simple Storage Service (S3)

- Object storage
- Cluster spans region
- No file system
- Flat structure inside bucket (objects in a bucket)
- Buckets are private by default
- Each bucket has its own URL, e.g. <https://s3-us-east-1.amazonaws.com/bucket-name/index1/index2/key/my-object.nnn>
  - host name is the region
  - next node is the bucket name
  - remaining nodes are index
  - last 2 nodes is the key
- No bucket size limit
- Objects up to 5TB
- Upload limit 5GB per PUT
- Multipart upload (to get up to 5TB)
  - For any object greater than 100MB, use multipart upload
- Server side encryption (SSE)
  - Advanced Encryption Standard (AES-256)
  - Key per object

#### Features

- Versioning
  - Can roll back to previous version
- Query in place with S3 Select
  - Extract ranges, subsets, of data
- Cross-Region Replication
- Event notifications
- S3 Transfer Acceleration
  - AWS has its own backbone
  - Longer distance --> greater the benefit
- Static Websites

### Bucket Security with Resource Policies and ACLS

Making a Bucket or Objects Publicly Readable

- Policy using "Principle": "*"
- Following **canned ACLs** (vs policies, prefer policies that are fine grained)
  - public-read
  - public-read-write
  - authenticated-read (applies to anyone who authenticates to AWS API - must have AWS account - but can be anyone) **avoid this**

### Introduction to Amazon Glacier

- Archival storage
  - Vaults
    - Archives
- Write archives
  - Transition from S3 - access through S3
  - Direct upload - access directly
- Download via retrieval request
  - 3-5 hour wait
  - Expedited retrieval availa (as soon as 10 minutes)
  - $/1000 pulls (pulls can get expensive if done frequently)

### Storage Class Management with Lifecycle Rules

Storage Classes

- Standard
  - Redundant copies across multiple AZs
- Standard-Infrequent Access
  -Lower cost for data accessed less frequently
- One Zone-Infrequent Access
  - Lower cost for less resiliency
- Glacier
  - Even lower cost for rarely accessed data

Lifecycle Rules

Can create rules to manage data lifecycles

![Storage Lifecycle Rules](./Training/StorageLifecycleRules.png)

### Instance Store Volumes

- Included with instance\*
- Powered by physical host - directly to physical disk
- High IOPs (100K+)
- Ephemeral\*\*
- No snapshots

\* Not every instance type includes volumes  
\*\* Ephemeral stores are deleted when instance stopped or terminated

Instance Store Volume Examples

![Instance Store Volume Examples](./Training/InstanceStoreVolumesExample.png)

### Introduction to Amazon Elastic Block Store (EBS)

- Data independent from instance
- Pay for provisioned storage (whether it is used or not!)
- Exist in a single AZ (instance and volume need to be in same AZ)
- Can be encrypted (server side - AES256)
- Connected over network\*
- Volume can attache to only one instance at a time
- Can _detach_
- Can _attach_ to different instance
- Can be used in RAID or Logical Volume Manager (LVM)
  - Already in a cluster - RAID configs can be degraded by this

\* Does NOT use NFS

### Amazon EBS Volume Types and Performance

![EBS Volume Types Performance](./Training/EbsVolumeTypesPerformance.png)
