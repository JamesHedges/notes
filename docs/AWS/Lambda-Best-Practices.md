<!-- omit in toc -->
# AWS Lambda Best Practice for C\#

- [Introduction](#introduction)
- [Prepare Development Environment](#prepare-development-environment)
  - [AWS Access and Profiles](#aws-access-and-profiles)
    - [Local Workstation Profiles](#local-workstation-profiles)
  - [.NET Core](#net-core)
  - [AWS Toolkit for Visual Studio](#aws-toolkit-for-visual-studio)
  - [AWS Toolkit for VS Code](#aws-toolkit-for-vs-code)
  - [Lambda Test Tool](#lambda-test-tool)
- [Function Handler](#function-handler)
- [Application](#application)
- [Configuration](#configuration)
  - [Required Packages](#required-packages)
  - [Constants](#constants)
  - [Environment Service](#environment-service)
  - [Configuration Service](#configuration-service)
  - [Using the IConfiguration](#using-the-iconfiguration)
- [Service Startup](#service-startup)
  - [Function Constructor](#function-constructor)
  - [Dependency Injection](#dependency-injection)
- [Logging](#logging)
- [Idempotence](#idempotence)
- [Managing Secrets](#managing-secrets)
- [Lambda with Application Load Balancer](#lambda-with-application-load-balancer)
  - [Install AWS Lambda Tools](#install-aws-lambda-tools)
  - [Configuring for Application Load Balancer](#configuring-for-application-load-balancer)
  - [Configuring Tests](#configuring-tests)
- [References](#references)

## Introduction

When we first heard of AWS Lambda or Azure Function, they were described as serverless functions. We developed a mental image, literally, of a single function. That view was a bit simplistic. AWS documentation refers to Lambdas as a serverless compute service where you only pay for the compute time used. They do have a single entry point, the FunctionHandler, making their purpose very focused. However, the more appropriate view is of a containerized service. And we just supply the code.

Lambdas are a composed of more than a single function and fall under the definition of a microservice. They must be viewed in this light and be constructed using the same principles we apply to any application or service. They should follow the software best practices engineers apply to any microservice application. These practices include SOLID and unit testing and the microservice best practices.

Lambdas support many languages, including C#. As Lambda code is executed in a Linux environment, we will are limited to .NET Core. Using .NET Core and C# opens up to use features not found in classic .NET Frameworks, like the built in dependency injection. Best practices shown here will also include techniques specific to C# and .NET Core 3.1.

## Prepare Development Environment

Having embraced .NET Core and C#, AWS had created development tools to aid Lambda developers. Including these tools in your environment will make development tasks easier. The tools include AWS Command Line Interface (CLI) for .NET Core, the AWS Visual Studio Toolkit, and the Lambda test tool. Your must have your AWS [profiles configured](#local-workstation-profiles) to use these tools.

### AWS Access and Profiles


#### Local Workstation Profiles

You will need to have profiles configured on their workstation. Numerous sources describe how to accomplish this. Here are a few:

- [Using the SDK Store (Windows only)](https://docs.aws.amazon.com/sdk-for-net/latest/developer-guide/sdk-store.html)

### .NET Core

Your developer's workstation must have .NET Core installed. By updating their Visual Studio version to the latest version, this requirement will be satisfied. AWS Lambda supports up to .NET Core 3.1. If that version is not installed, use the following link to download and install the SDK.

[.Net Core CLI Download](https://dotnet.microsoft.com/download)

### AWS Toolkit for Visual Studio

AWS provides a Visual Studio toolkit that includes templates and and the AWS Explorer. To use the explorer, you will need to have your [AWS access keys](#aws-access-and-profiles) installed.

[AWS Toolkit for Visual Studio 2017 and 2019](https://marketplace.visualstudio.com/items?itemName=AmazonWebServices.AWSToolkitforVisualStudio2017)

### AWS Toolkit for VS Code

For development with languages other that  C#, VS Code is a good option. Install the VS Code toolkit for enhanced tooling.

[AWS Toolkit for Visual Studio Code](https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/welcome.html)

### Lambda Test Tool

The _Amazon Lambda Test Tool_ is invaluable for interactively debugging your service. It can be installed using the .NET CLI _tool_ function locally using the _install_ command.

```powershell
# install
dotnet tool install -g Amazon.Lambda.TestTool-3.1

# update
dotnet tool update -g Amazon.Lambda.TestTool-3.1
```

See the [Lambda Test Tool](https://github.com/aws/aws-lambda-dotnet/tree/master/Tools/LambdaTestTool) documentation.

## Function Handler

The Lambda function handler will be the entry point for the application. However, best practice is to move all application logic out of the handler. The handler should be used for any infrastructure concerns and then to quickly delegate to a business logic component, your application. This separates the AWS infrastructure concerns from the business logic, facilitating better unit testing.

The following is an example of typical function handler that receives an request and returns a response.

```csharp
    public async Task<MyLambdaResponse> FunctionHandler(MyLambdaRequest myEvent, ILambdaContext context)
    {
        var logger = _serviceProvider.GetService<ILogger>();
        logger.Information($"Received message: {context.AwsRequestId}");

        var result = await _serviceProvider.GetService<MyLambdaApp>().Run(myEvent);

        logger.Information($"Completed processing event {context.AwsRequestId}");

        return PrepareFunctionResponse(result);
    }
```

In the above example, the handler receives a message of type MyLambdaRequest, logs that is was received, and then passes it on to the application's _Run_ function. Upon return from the application, it then logs processing completion and constructs the MyLambdaResponse return object. When constructing the response, and infrastructure concerns can be addressed while packaging the response.

The service provider is explained below in the [Dependency Injection](#dependency-injection) section.

## Application

The MyLambdaApp below is where the application business logic is implemented. The hand-off from the _Function_ is the _async Task Run()_ method. It is this structure that, along with the use of dependency injection, makes the Lambda service unit testable. Note that the constructor may be used to inject services like repositories, secrets, or even _IHttpClientFactory_.

```csharp
    public interface IMyLambdaApp
    {
        Task Run();
    }

    public class MyLambdaApp : IMyLambdaApp
    {
        private readonly ILogger _logger;
        private readonly IAppConfig _appConfig;

        public MyLambdaEventConsumerApp(ILogger logger, IAppConfig appConfig)
        {
            _logger = logger;
            _appConfig = appConfig;
        }

        public async Task Run(MyLambdaRequest myEvent)
        {
            // implement the application here
        }
    }
}
```

## Configuration

To make AWS Lambda service configuration possible, the application will require gathering information from the environment and services. The Lambda application's configuration can be composed of environment variables, application settings, logging, services, and dependency injection through an inversion of control container. The configuration is accomplished using a structure common across .NET Core applications, taking advantage of _ConfigurationBuilder_ to load configurations from environment and settings files and using the __IServicesCollection__ for dependency injection. The configuration is a pulled together in the __Startup__ class.

### Required Packages

The following NuGet packages are used to facilitate the described configuration.

```xml
    <PackageReference Include="Amazon.Lambda.Core" Version="1.1.0" />
    <PackageReference Include="Amazon.Lambda.Serialization.SystemTextJson" Version="2.0.0" />
    <PackageReference Include="Amazon.Lambda.SQSEvents" Version="1.1.0" />
    <PackageReference Include="AWS.Logger.SeriLog" Version="1.1.0" />
    <PackageReference Include="AWSSDK.Extensions.NETCore.Setup" Version="3.3.101" />
    <PackageReference Include="AWSSDK.Lambda" Version="3.5.6.7" />
    <PackageReference Include="AWSSDK.SecretsManager" Version="3.5.0.31" />
    <PackageReference Include="Microsoft.AspNet.WebApi.Client" Version="5.2.7" />
    <PackageReference Include="Microsoft.Extensions.Configuration" Version="5.0.0" />
    <PackageReference Include="Microsoft.Extensions.Configuration.Binder" Version="5.0.0" />
    <PackageReference Include="Microsoft.Extensions.Configuration.EnvironmentVariables" Version="5.0.0" />
    <PackageReference Include="Microsoft.Extensions.Configuration.FileExtensions" Version="5.0.0" />
    <PackageReference Include="Microsoft.Extensions.Configuration.Json" Version="5.0.0" />
    <PackageReference Include="Microsoft.Extensions.DependencyInjection" Version="5.0.1" />
    <PackageReference Include="Serilog" Version="2.6.0" />
    <PackageReference Include="Serilog.Formatting.Compact" Version="1.0.0" />
    <PackageReference Include="Serilog.Settings.Configuration" Version="3.1.0" />
    <PackageReference Include="Serilog.Sinks.AwsCloudWatch" Version="4.0.161" />
    <PackageReference Include="Serilog.Sinks.Console" Version="3.1.1" />
```

### Constants

A service's constant may be configured in a static class with all of the constants needed for the Lambda under development.

```csharp
namespace MyService.Configuration
{
    public static class Constants
    {
        public static class EnvironmentVariables
        {
            public const string Stage = "STAGE";
            public const string SecretsArn = "MY_SECRET_ARN";
            public const string Region = "REGION";
            public const string BusinessDomain = "BUSINESS_DOMAIN";
            public const string Team = "TEAM";
            public const string FunctionName = "FUNCTION_NAME";
        }

        public static class Environments
        {
            public const string Development = "dev";
            public const string Integration = "int";
            public const string Production = "prod";
            public const string Test = "test";
       }
    }
}
```

### Environment Service

The environment service abstracts away all of the environment variables used in the Lambda function. The environment service is used to load the correct _appsettings.{environment}.json_ file in the configuration service. The default environment will be for production.

```csharp
using System;
using static Service.Configuration.Constants;

namespace Service.Configuration
{
    public interface IEnvironmentService
    {
        string EnvironmentName { get;  set; }
    }

    namespace MyService.Configuration
    {
        public class EnvironmentService : IEnvironmentService
        {
            public EnvironmentService()
            {
                EnvironmentName = Environment.GetEnvironmentVariable(EnvironmentVariables.Stage)
                    ?? Environments.Production;
            }

            public string EnvironmentName { get; set; }
        }
    }
}
```

### Configuration Service

In .NET Core, the _ConfigurationBuilder_ is used to load settings from the appsettings.json, appsettings.{env}.json, and environment configurations. It returns the _IConfiguration_ object that can be used to query settings.

The below GetConfiguration method uses the configuration builder to load settings into the configuration object. It first sets the path where configuration files are found. The _appsettings.json_ and _appsettings.{environment}.json_ are then read. In this example, the a _appsettings.json_ file is required and a file watch is placed on it to notify when a reload is necessary. The environment variables are then loaded. Note that all environment variables will be read and made available. Finally, the configuration is built and an instance of  _IConfiguration_ returned.

```csharp
using System.IO;
using Microsoft.Extensions.Configuration;

namespace MyService.Configuration
{
    public interface IConfigurationService
    {
        IConfiguration GetConfiguration();
    }

    public class ConfigurationService : IConfigurationService
    {
        public IEnvironmentService EnvironemntService { get; }

        public ConfigurationService(IEnvironmentService environemntService)
        {
            EnvironemntService = environemntService;
        }

        public IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{EnvironemntService.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
```

### Using the IConfiguration

The _IConfiguration_ object is be used to query values form the configuration sources. An application configuration class is used to encapsulate the required configuration elements used and consolidate their queries. Below is an example.

```csharp
using System;
using MyService.Configuration;
using Microsoft.Extensions.Configuration;

namespace MyService.Models
{
    public class MyAppConfig : IMyAppConfig
    {
        private readonly IConfiguration _configuration;

        public MyAppConfig(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // Queries environment variables
        public string SecretArn => _configuration.GetValue<string>(Constants.EnvironmentVariables.SecretsArn);
        public string Region => _configuration.GetValue<string>(Constants.EnvironmentVariables.Region);

        // The following queries values from a json file having a structure lke 
        // "Command": 
        // {
        //     "Name" "sample", 
        //     "Description": "what the command does",
        //     "Timeout": 30
        // }
        //
        public string CommandName => _configuration.GetValue<string>("Command:Name");
        public int CommandTimeout => _configuration.GetValue<int>("Command:Timeout");
    }
}
```

If finer grained configurations are required, this can be accomplished by simply adding additional, more focused interfaces to the configuration. For example, an interface can be added for api client parameters, e.g.

```csharp
public interface IApiClientConfig
{
    public int Timeout { get; }
    public string Resource { get; }
    public int Retries { get; }
    public string hostName { get; }
}

public interface IAppConfig
{
    // other config needed by the app
}

public class AppConfig : IAppConfig, IApiClientConfig
{
    // implement the interfaces
}
```

## Service Startup

When the Function object is created, the service can be initialized in the constructor. Since the Function remains "warm" for some time and the initialization will not be repeated with each invocation. The amount time a function stays warm is not published by AWS, but most estimates say about 10 minutes. When the function is invoked frequently, it may stay warm up to 6 hours.

### Function Constructor

In the function constructor, the configurations are loaded, the service collection created and populated, and the IoC Container created. The IoC Container used is the .NET Core container. Alternative IoC containers may be used if more sophisticated container registrations are required. Please see the documentation for the desired IoC Container for details on how to add it to the application.

```csharp
namespace MyService
{
    public class Function
    {
        public IConfigurationService ConfigService { get; }
        private readonly ServiceProvider _serviceProvider;

        public Function()
        {
            // Loads the configurations
            var configService = new ConfigurationService(new EnvironmentService()); 
            var serviceCollection = new ServiceCollection();
            var startup = new Startup(configService.GetConfiguration());

            // Configures dependency injection
            startup.ConfigureServices(serviceCollection);

            // Creates the IoC Container
            _serviceProvider = serviceCollection.BuildServiceProvider();            

            ConfigService = _serviceProvider.GetService<IConfigurationService>();
        }
```

### Dependency Injection

The container initialization is performed using the Startup class's ConfigureServices method. It creates the services container using the given configuration in the _ConfigureServices_ method, and builds the container.

```csharp
using MyService.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MyService
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            // see below about configuring logging.
            serviceCollection.AddSingleton<ILogger>(GetLogger()); 
            serviceCollection.AddTransient<IEnvironmentService, EnvironmentService>();
            serviceCollection.AddSingleton<IConfiguration>(this.Configuration);
        }
    }
}
```

## Logging

The SeriLog package has been used for structured logging. The Log4Net package may also be used, but Serilog was preferred. The Log4Net logging tool has been surpassed in performance and flexibility by SeriLog.

SeriLog is configured using the _Serilog.json_ file. The following is an example for how to do a basic setup.

```json
{
  "Serilog": {
    "Using": [
      "AWS.Logger.Serilog"
    ],
    "MinimumLevel": "Information",
    "WriteTo": [
      {
        "Name": "AWSSerilog"
      }
    ]
  }
}
```

The logger is then configured in the Startup class. Below is a method that uses the json file to create the Serilog logger. Note that the log file name is generated based on convention where the team, stage, business domain, and function name.

```csharp
        private ILogger GetLogger()
        {
            var serilogConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("Serilog.json")
                .Build();

            var options = new CloudWatchSinkOptions
            {
                LogGroupName = GetLogGroupName,
                CreateLogGroup = true,
                MinimumLogEventLevel = Serilog.Events.LogEventLevel.Information,
                TextFormatter = new CompactJsonFormatter()
            };

            var awsOptions = Configuration.GetAWSOptions();
            var cloudWatchClient = awsOptions.CreateServiceClient<IAmazonCloudWatchLogs>();

            return new LoggerConfiguration()
                .ReadFrom.Configuration(serilogConfig)
                .Enrich.FromLogContext()
                .WriteTo.Console(new CompactJsonFormatter())
                .WriteTo.AmazonCloudWatch(options, cloudWatchClient)
                .CreateLogger();
        }

        private string GetLogGroupName()
        {
            var team = Configuration.GetValue<string>(Constants.EnvironmentVariables.Team);
            var stage = Configuration.GetValue<string>(Constants.EnvironmentVariables.Stage);
            var businessDomain = Configuration.GetValue<string>(Constants.EnvironmentVariables.BusinessDomain);
            var functionName = Configuration.GetValue<string>(Constants.EnvironmentVariables.FunctionName);
            return $"/aws/lambda/{team}-{stage}-{businessDomain}-{functionName}";
        }
```

> Note that the Lambda will executed on a Linux operating system. Files and folder in Linux are case sensitive. Make sure the file name you read a configuration from is correct, including case.

The logger configuration begins by reading the logger configuration. This is the same construct used when reading the application configuration. These settings are then applied in subsequent steps. Additional configuration settings are extracted from the application's IConfiguration object using the AWS extension method _GetAWSOptions_. For more information on how this extension works, see [Using AWSSDK.Extensions.NETCore.Setup and the IConfiguration interface](https://docs.aws.amazon.com/sdk-for-net/latest/developer-guide/net-dg-config-netcore.html).

This particular setup adds the AWS CloudWatch sink (analogous to a Log4Net adapter) that sends log messages to the named log group. The CloudWatch options are populated and passed into the logger creation. Note that if the log group does not exist, it will be created. The _CreateLogger_ returns the ILogger service to be added to the _ServiceCollection_.

## Idempotence

It is possible a service will receive a message/event more than once, especially when working with queues. Therefore, analyze the business requirements to determine if receiving a message multiple times may cause duplication issues. If the function does not naturally handle this case and idempotence is required, then it must be handled explicitly. This can be done upfront by tracking the messages received and abandoning any duplicate message immediately or by handling the duplicate later in the process. An example of handling it later in the process would be change an insert into a save, where the save inserts or updates.

## Managing Secrets

When using secrets in a Lambda, it is a good choice to create a service. As fetching secrets is a relatively expensive operation, the service should only the fetch secrets once and caches the response. The following is an example on how to create such a service. This service can them be configured into the IoC container and injected where needed.

```csharp
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using MyService.Models;
using Newtonsoft.Json;
using Serilog;

namespace MyService.Repositories
{
    public interface IAwsSecretsRepository
    {
        Task<PostgreSecret> GetPostgreSecret();
    }

    public class AwsSecretsRepository : IAwsSecretsRepository
    {
        private const string VersionStageCurrent = "AWSCURRENT";
        private readonly IAmazonSecretsManager _secretsClient;
        private readonly IAppConfig _appConfig;
        private readonly ILogger _logger;
        private PostgreSecret _cachedSecret;

        public AwsSecretsRepository(IAppConfig appConfig, ILogger logger)
        {
            _appConfig = appConfig ?? throw new ArgumentNullException(nameof(appConfig));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _secretsClient = new AmazonSecretsManagerClient(RegionEndpoint.GetBySystemName(appConfig.Region));
        }

        public async Task<PostgreSecret> GetPostgreSecret()
        {
            if (_cachedSecret == null)
            {
                var secretResponse = await GetSecretByArn(_appConfig.SecretArn).ConfigureAwait(false);
                var responseString = GetSecretString(secretResponse);

                _cachedSecret = JsonConvert.DeserializeObject<PostgreSecret>(responseString);
            }

            return _cachedSecret;
        }

        private string GetSecretString(GetSecretValueResponse response)
        {
            if (!string.IsNullOrEmpty(response.SecretString))
            {
                return response.SecretString;
            }
            else if (response.SecretBinary != null)
            {
                var reader = new StreamReader(response.SecretBinary);
                return Encoding.UTF8.GetString(Convert.FromBase64String(reader.ReadToEnd()));
            }

            return string.Empty;
        }

        private async Task<GetSecretValueResponse> GetSecretByArn(string secretArn)
        {
            GetSecretValueRequest request = new GetSecretValueRequest
            {
                SecretId = secretArn,
                VersionStage = VersionStageCurrent
            };

            try
            {
                return await _secretsClient.GetSecretValueAsync(request).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Failed retrieving secrets: {ex.Message}");
                throw;
            }

        }
    }
}
```

## Lambda with Application Load Balancer

See [Amazon.Lambda.AspNetCoreServer](https://github.com/aws/aws-lambda-dotnet/blob/master/Libraries/src/Amazon.Lambda.AspNetCoreServer/README.md)

### Install AWS Lambda Tools

If needed, load the AWS Lambda templates

```powershell
dotnet tool install -g Amazon.Lambda.Tools
```

### Configuring for Application Load Balancer

To configure this project to handle requests from an Application Load Balancer instead of API Gateway change
the base class of `LambdaEntryPoint` from `Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction` to `Amazon.Lambda.AspNetCoreServer.ApplicationLoadBalancerFunction`.

### Configuring Tests

For the application load balancer, the request object will need to be set as ApplicationLoadBalancerRequest. The request must have MultiValueHeaders set. This can be an empty `Dictionary<string, IList<string>>` or have an actual value set, like `{"Accept", new List<string> {"application/json"}}`.

This can be accomplished using the following json:

```json
{
  "requestContext": {
    "elb": {
      "targetGroupArn": "arn:aws:elasticloadbalancing:region:123456789012:targetgroup/my-target-group/6d0ecf831eec9f09"
    }
  },
  "resource": "/{proxy+}",
  "path": "/api/values",
  "httpMethod": "GET",
  "queryStringParameters": null,
  "pathParameters": {
    "proxy": "api/values"
  },
  "stageVariables": null,
  "headers": null,
  "multiValueHeaders": {
    "accept": [ "text/html,application/xhtml+xml" ],
    "accept-language": [ "en-US,en;q=0.8" ],
    "content-type": [ "text/plain" ],
    "cookie": [ "cookies" ],
    "host": [ "lambda-846800462-us-east-1.elb.amazonaws.com" ],
    "user-agent": [ "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)" ],
    "x-amzn-trace-id": [ "Root=1-5bdb40ca-556d8b0c50dc66f0511bf520" ],
    "x-forwarded-for": [ "72.21.198.66" ],
    "x-forwarded-port": [ "443" ],
    "x-forwarded-proto": [ "https" ]

  },
  "isBase64Encoded": false,
  "body": null
}
```

## References

- [Best practices for working with AWS Lambda functions](https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html)
- [Add .NET Core DI and Config Goodness to AWS Lambda Functions](https://blog.tonysneed.com/2018/12/16/add-net-core-di-and-config-goodness-to-aws-lambda-functions/)
- [Configuration in ASP.NET Core - appsettings](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1#appsettingsjson)
- [Configuration in .NET](https://docs.microsoft.com/en-us/dotnet/core/extensions/configuration)
- [Serilog vs log4net](https://blog.elmah.io/serilog-vs-log4net/)
- [Serilog and CloudWatch (with inbuilt credentials)](https://martinjt.me/2020/02/16/serilog-and-cloudwatch-with-inbuilt-credentials/)
- [AWS Extensions for .NET CLI](https://github.com/aws/aws-extensions-for-dotnet-cli)
- [.NET CLI overview](https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x)
- [Amazon.Lambda.AspNetCoreServer](https://github.com/aws/aws-lambda-dotnet/blob/master/Libraries/src/Amazon.Lambda.AspNetCoreServer/README.md)
