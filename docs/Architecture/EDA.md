# Event Driven Architecture

- Integrate several disparate systems together using message queues.
- EDA is made up of decoupled event processing components that asynchronously receive and process events.
- Event-based model reacts to a particular situation and takes action based on that event

## Characteristics

- Distributed
- Asynchronous

- Two topologies
  - Broker - used when you require a high degree of responsiveness and dynamic control over the process of an event.
    - No central mediator - messages flow across event processors in a chain-lke fashion through a lightweight message broker
    - Useful when modeling a simple event processing flow that does not need central event orchestration and coordination
    - Processors emit processing events which are handled by other processors until no process is interested
    - Key is that when a process event if published, the publisher has no concern for subsequent processes
    - Characteristics
      - Advantages
        - highly performant
        - highly responsive
        - highly scalable
        - highly decoupled
        - highly fault tolerant
      - Disadvantages
        - workflow control - no control of the overall workflow associated with initiating event
        - poor error handling - do not know when the transaction is complete
        - Recoverability - do not know if the initiating transaction fails at some point. Thus poor recoverability
        - Restart capabilities - cannot just resubmit - the state of the original request is not known
        - Data inconsistency
  - Mediator - used when you require control over the workflow of an event process
    - A mediator(s) knows the steps involved in processing the event. It generates corresponding processing events that are sent to a dedicated event channel and listens for their response.
    - You can have a chain of mediators. Each processes the event and publishes the result when complete
    - Each mediator is typically associated with a particular domain grouping of events
    - Because the mediator controls the workflow, it can maintain event state and manage error handling, recoverability, and restart capabilities
    - In mediator, a command (queue) must be processed, where event may be ignored
    - Characteristics
      - Advantages
        - Workflow control
        - Error handling
        - Recoverability
        - Restart capabilities
        - Better data consistency
      - Disadvantages
        - More coupling of event processors
        - Lower scalability (but not poor)
        - Lower performance (but not poor)
        - Lower fault tolerance
        - Modeling complex workflows
- The trade-of is between workflow control and error handling capability versus high performance and scalability
- Can use hybrid microservices/EDA

## EDA vs Microservices

### EDA

    - Course grained
    - 
