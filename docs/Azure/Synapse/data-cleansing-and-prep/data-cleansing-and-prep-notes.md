# Data cleansing and prep

[YouTube - Data profiling and cleaning in Synapse with Data Flows](https://www.youtube.com/watch?v=et1w9C8SRq4) recorded on 2021-08-05 by Mark Kromer

Azure Synapse Analytics (ASA)
Azure Data Flows (ADF)

## Slides

![Slide 01](./assets/data-cleansing-and-prep-01.png)

![Slide 02](./assets/data-cleansing-and-prep-02.png)

![Slide 03](./assets/data-cleansing-and-prep-03.png)

![Slide 04](./assets/data-cleansing-and-prep-04.png)

![Slide 05](./assets/data-cleansing-and-prep-05.png)

![Slide 06](./assets/data-cleansing-and-prep-06.png)

![Slide 07](./assets/data-cleansing-and-prep-07.png)

![Slide 08](./assets/data-cleansing-and-prep-08.png)

![Slide 09](./assets/data-cleansing-and-prep-09.png)

## History

- ASA data flows came from ADF, it's ADF embedded in ASA
    - As of the video, the Microsoft was working to achieve feature parity between them
