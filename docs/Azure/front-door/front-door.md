# Azure Front Door

## Domains

Azure Front Door allows you to route traffic to your applications using custom domains. Here’s a brief overview of how you can manage and route domains with Azure Front Door:

### Types of Domains

1. **Subdomains**: These are the most common type, like `myapp.contoso.com`.
2. **Apex Domains**: These don't contain a subdomain, such as `contoso.com`.
3. **Wildcard Domains**: These allow traffic for any subdomain, like `*.contoso.com`¹.

### Adding a Domain

To add a custom domain to your Azure Front Door profile, you need to:

1. **Configure DNS Records**:
   - **DNS TXT Record**: Required to validate ownership of your domain.
   - **DNS CNAME Record**: Controls the flow of internet traffic to Azure Front Door¹.

2. **Domain Validation**:
   - All domains must be validated to ensure proper configuration and to prevent domain spoofing¹.

## Endpoints

In Azure Front Door, an **endpoint** is a logical grouping of one or more routes associated with domain names. Each endpoint is assigned a unique domain name by Front Door, and you can also associate your own custom domains with these routes¹.

### Key Points about Azure Front Door Endpoints:

- **Domain Names**: Each endpoint gets a unique domain name, typically in the format `<endpointname>-hash.z01.azurefd.net`².
- **Custom Domains**: You can associate your own custom domains with the routes in an endpoint¹.
- **Multiple Endpoints**: A Front Door profile can contain multiple endpoints, but often a single endpoint is sufficient if all domains use similar route paths¹.
- **Configuration**: You can configure endpoints using the Azure portal, Azure CLI, or other tools².

#### References

1. [Endpoints in Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/endpoint)
2. [Add a new endpoint with Front Door manager - Azure Front Door](https://learn.microsoft.com/en-us/azure/frontdoor/how-to-configure-endpoints)
3. [azure-docs/articles/frontdoor/endpoint.md at main - GitHub](https://github.com/MicrosoftDocs/azure-docs/blob/main/articles/frontdoor/endpoint.md)

#### Multiple Endpoints

Having multiple Azure Front Door endpoints can be beneficial for several reasons:

1. **Domain Management**: If you have multiple custom domains, you might want to create separate endpoints for each domain. This allows you to manage routes and policies specific to each domain more effectively¹.

2. **Traffic Segmentation**: Different endpoints can be used to segment traffic based on various criteria, such as geographic location or service type. This helps in optimizing performance and ensuring that traffic is routed efficiently¹.

3. **Isolation and Security**: By using multiple endpoints, you can isolate different parts of your application. This can enhance security by limiting the impact of potential vulnerabilities to a specific endpoint².

4. **Scalability and Redundancy**: Multiple endpoints can provide better scalability and redundancy. If one endpoint experiences issues, traffic can be rerouted to another endpoint, ensuring high availability and reliability².

5. **Custom Routing Rules**: Different endpoints allow you to apply custom routing rules and policies tailored to specific needs. For example, you might have different caching rules or WAF (Web Application Firewall) policies for different endpoints².

#### References

1. [Endpoints in Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/endpoint)
2. [Azure Front Door - Best practices | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/best-practices)
3. [A Deep Dive Into Azure Load Balancing Options - DZone](https://dzone.com/articles/mastering-scalability-and-performance-a-deep-dive)
4. [Quick create Azure Front Door endpoints for Azure Storage accounts](https://techcommunity.microsoft.com/t5/azure-storage-blog/quick-create-azure-front-door-endpoints-for-azure-storage/ba-p/3909095)

## Route

### Route Configuration

A route in Azure Front Door defines how traffic is handled when it arrives at the edge. The route settings associate a domain with an origin group and can be configured using:
- **HTTP Protocols**: HTTP or HTTPS.
- **Domain**: For example, `www.foo.com` or `*.bar.com`.
- **Paths**: Such as `/*` or `/users/*`².

### Routing Rules

Routing rules determine how requests are matched and processed:

- **Frontend Host Matching**: Azure Front Door matches the incoming request to the most specific route based on protocol, domain, and path².

### Multi-tenant Solutions

Azure Front Door supports routing capabilities for multitenant architectures, allowing traffic distribution among origins or specific tenants based on domain names, wildcard domains, and URL paths³.

For detailed steps on configuring custom domains and routing rules, you can refer to the [Microsoft Learn documentation](https://learn.microsoft.com/en-us/azure/frontdoor/domain).

#### References

1. [Domains in Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/domain)
2. [How requests get matched to a route configuration - Azure Front Door](https://learn.microsoft.com/en-us/azure/frontdoor/front-door-route-matching)
3. [Use Azure Front Door in a multi-tenant solution](https://learn.microsoft.com/en-us/azure/architecture/guide/multitenant/service/front-door)
4. [Azure Front Door, AFD, domain fronting](https://techcommunity.microsoft.com/t5/azure-networking-blog/prohibiting-domain-fronting-with-azure-front-door-and-azure-cdn/ba-p/4006619)
5. [Endpoints in Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/endpoint)

## Origins

### Origin

In Azure Front Door, an **origin** refers to the backend application deployment from which Azure Front Door retrieves content. This can include applications hosted in Azure, on-premises datacenters, or even other cloud providers¹.

When configuring an origin, you need to specify several settings, such as:

- **Origin type**: The type of resource (e.g., Azure App Service, Cloud Service, Storage, or a custom host).
- **Host name**: The backend origin host name.
- **Priority and Weight**: These settings help manage traffic distribution and failover scenarios.
- **Private Link**: For secure connectivity to your origin¹².

#### References

1. [Origins and origin groups - Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/origin)
2. [How to configure origins - Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/how-to-configure-origin)
3. [Using Azure Front Door Standard/Premium with Cross-Origin Resource](https://learn.microsoft.com/en-us/azure/frontdoor/standard-premium/troubleshoot-cross-origin-resources)

### Origin Group

An **origin group** in Azure Front Door is a collection of origins (backends) that receive similar traffic for an application. This setup allows you to manage and distribute traffic across multiple backends efficiently. Here are some key points about origin groups:

- **Origins**: These are the endpoints where Azure Front Door retrieves content. They can be hosted in Azure, on-premises, or with another cloud provider¹.
- **Configuration**: When you add an origin to an origin group, you need to configure settings like origin type, host name, and priority¹.
- **Traffic Distribution**: You can assign weights to origins to control how traffic is distributed among them. Higher weights mean more traffic².
- **Private Link**: In the Premium tier, you can use Private Link to secure traffic to your origins¹.

#### References

1. [Origins and origin groups - Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/origin)
2. [How to configure origins - Azure Front Door | Microsoft Learn](https://learn.microsoft.com/en-us/azure/frontdoor/how-to-configure-origin)
3. [azure-docs/articles/frontdoor/how-to-configure-origin.md at main](https://github.com/MicrosoftDocs/azure-docs/blob/main/articles/frontdoor/how-to-configure-origin.md)
