# Azure functions

## Function apps

An **Azure Function App** is a serverless compute service that allows you to run event-driven code without the need to manage infrastructure. Here’s a breakdown of its key features:

1. **Event-Driven**: Azure Functions are triggered by events, such as HTTP requests, timers, or messages from other Azure services (like Azure Blob Storage or Azure Queue Storage). This makes it ideal for scenarios like processing data, integrating systems, or building APIs[^1][^2].

2. **Serverless Architecture**: You don’t have to worry about provisioning or managing servers. Azure automatically handles the scaling and infrastructure, allowing you to focus on writing your code[^3][^4].

3. **Flexible Development**: You can write functions in various programming languages, including C#, Java, JavaScript, Python, and PowerShell. This flexibility allows you to use the language you’re most comfortable with[^1][^3].

4. **Cost-Effective**: With the **Consumption Plan**, you only pay for the execution time of your functions, making it a cost-effective solution for applications with variable workloads[^3].

5. **Management and Deployment**: A Function App serves as a container for your functions, allowing you to manage and deploy them as a single unit. This simplifies the deployment process and resource management[^4].

In summary, Azure Function Apps are a powerful way to build scalable, event-driven applications without the overhead of managing servers. If you have a specific use case in mind or need help getting started, feel free to ask!

Source: Conversation with Copilot, 11/8/2024

## Create a simple http function

Create a simple HTTP POST command to echo the supplied name[^5].

!!! Note "Requires these VSCode extensions"

    - [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
    - [Azure Function](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-azurefunctions)
    - [Azurite V3](https://marketplace.visualstudio.com/items?itemName=Azurite.azurite) (local storage emulator)

1. Create a project folder and open using VSCode
2. Press ++f1++ to open the command palette and search for and run the command ==Azure Functions: Create New Project...==.
3. Choose the project directory
4. Provide the following:
      1. Language: ==Python (Programming Model V2)==
      2. Interpreter: select the preferred Python interpreter or entry a path
      3. Template: ==HTTP trigger==
      4. Name: enter the function's name
      5. Authorization level: ==ANONYMOUS==
      6. How to open: ==Open in current window==
5. The function is generated, see the ==function_app.py==
6. In the _local.settings.json_ file, set =="AzureWebJobsStorage": "UseDevelopmentStorage=true",==
7. Start the emulator:
      1. Press ++f1++ and enter ==Azurite: Start==
      2. Check the VSCode bottom bar for ==[Azurite Table Service]== to verify
8. Run the function locally
      1. Press ++f5++
      2. In the Azure activity bar, right click _Workspace → Local Project → Functions → <your function\>_ and select ==Execute Function Now...==
      3. Enter the request body =={"name": "Azure"}==
      4. See the response in the terminal window
      5. Click ++ctrl+c++ in the terminal to stop the debugger

!!! Tip "Execute the function from the command line"
    `curl -X POST http://localhost:7071/api/phapafpocfunc -H "Content-Type: application/json" -d '{"name": "Jim"}'`

## Setting environment variable

In _local.settings.json_, add the variables under "Values." [^8]

```json
{
  "IsEncrypted": false,
  "Values": {
    "AzureWebJobsStorage": "UseDevelopmentStorage=true",
    "FUNCTIONS_WORKER_RUNTIME": "python",
    "ENV1": "uno",
    "ENV2": "dos"
  }
}
```

## App service plans

Yes, a single App Service Plan can host multiple Azure Functions. This approach can be cost-effective and efficient, especially if your functions have similar scaling and resource requirements. Here are some considerations:

1. **Cost Efficiency**: Hosting multiple functions in a single App Service Plan can save costs, as you are sharing the same resources (CPU, memory, etc.) among all the functions[^6].

2. **Resource Sharing**: All functions within the same App Service Plan share the allocated resources. This means that if one function experiences high load, it could impact the performance of other functions[^6].

3. **Scaling**: In a shared App Service Plan, scaling is done at the plan level, not the function level. This means that all functions will scale together based on the overall load[^6].

4. **Management**: Managing multiple functions within a single plan can simplify deployment and monitoring, as you have a single point of management for all the functions[^7].

However, if you have functions with significantly different scaling needs or resource usage patterns, it might be better to use separate App Service Plans to ensure that one function's load doesn't affect the others[^6].

## Sample terraform script

```terraform
provider "azurerm" {
  features {}
}

# Create Resource Group
resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "centralus"
}

# Create Storage Account
resource "azurerm_storage_account" "example" {
  name                     = "examplestorageacc"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = azurerm_resource_group.example.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# Create App Service Plan
resource "azurerm_app_service_plan" "example" {
  name                = "example-appserviceplan"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "example" {
  name                       = "example-functionapp"
  location                   = azurerm_resource_group.example.location
  resource_group_name        = azurerm_resource_group.example.name
  app_service_plan_id        = azurerm_app_service_plan.example.id
  storage_account_name       = azurerm_storage_account.example.name
  storage_account_access_key = azurerm_storage_account.example.primary_access_key
  version                    = "~3"
  os_type                    = "linux"
}
```

## Footnotes

[^1]: [Azure Functions Overview | Microsoft Learn](https://learn.microsoft.com/en-us/azure/azure-functions/functions-overview)
[^2]: [Introduction To Azure Function App - C# Corner](https://www.c-sharpcorner.com/article/introduction-to-azure-function-app/)
[^3]: [Create your first function in the Azure portal | Microsoft Learn](https://learn.microsoft.com/en-us/azure/azure-functions/functions-create-function-app-portal)
[^4]: [Guidance for developing Azure Functions | Microsoft Learn](https://learn.microsoft.com/en-us/azure/azure-functions/functions-reference)
[^5]: [Quickstart: Create a function in Azure with Python using VS Code](https://learn.microsoft.com/en-us/azure/azure-functions/create-first-function-vs-code-python)
[^6]: [Multiple Azure function apps inside same app service plan](https://learn.microsoft.com/en-us/answers/questions/396440/multiple-azure-function-app-inside-same-app-servic)
[^7]: [Create multiple functions within a single function app service plan](https://learn.microsoft.com/en-us/answers/questions/2134099/create-multiple-functions-withing-a-single-functio)
[^8]: [How to set environment variables for use in an Azure function](https://gregorsuttie.com/2021/08/05/how-to-set-environment-variables-for-use-with-an-azure-function/)
