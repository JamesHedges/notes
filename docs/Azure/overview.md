# Azure overview

## Loggon with cli

Coommand Line Interface (CLI) logon[^1]
Login interactively from CLI [^2]

```bash
# Login
az login

# Set thedefault subscription
az account set -subscription "<subscription ID or name>"

```

## References

[^1]: [Sign into Azure interactively using Azure CLI](https://learn.microsoft.com/en-us/cli/azure/authenticate-azure-cli-interactively)
[^2]: [Authenticate to Azure using Azure CLI](https://learn.microsoft.com/en-us/cli/azure/authenticate-azure-cli-interactively)
