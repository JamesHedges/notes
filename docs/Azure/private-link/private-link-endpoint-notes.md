# Private link

![alt text](private-link-endpoint-concepts.png)

![alt text](private-link-workflow.png)

## References

- [What's Private Endpoint](https://learn.microsoft.com/en-us/azure/private-link/private-endpoint-overview)
- [What's Private Link](https://learn.microsoft.com/en-us/azure/private-link/private-link-service-overview)
