# Service principals

## Terms

`Application Object`

:    This is a global representation of your application in Azure AD. It defines the application's identity, permissions, and other settings. It resides in the Azure AD tenant where the application was registered1.

`Resource Group`

:    This is a logical container in Azure that holds related resources for an Azure solution. Resources like virtual machines, databases, and storage accounts are organized within resource groups

`Service Principal`

:    This is a local representation of the application within a specific tenant. It is created from the application object and is used to assign roles and permissions to the application within that tenant

`Subscription`

:    This is a logical container used to provision resources in Azure. It holds the details of all your resources like virtual machines, databases, and more.

`Tenant`

:    This is a dedicated instance of Azure AD that an organization receives when it signs up for a Microsoft cloud service. It contains users, groups, and applications.

## Object Hierarchy

![Azure Service Hierarchy](azure-service-hierarchy.drawio)

## Creating a service principal for GitHub Actions using the Azure portal

### Step 1: Create a Service Principal

1. **Sign in to the Azure portal**.
2. **Navigate to Azure Active Directory**:
   - In the left-hand menu, select the "Azure Active Directory" option.
3. **Go to App registrations**:
   - Under the "Manage section, select the "App registrations" option.
4. **Register a new application**:
   - Click on the "New registration" option.
   - Enter a name for your application (for example, "GitHub Actions SP").
   - Choose the supported account types (usually "Accounts in this organizational directory only").
   - Click hte "Register" button.

### Step 2: Assign a Role to the Service Principal

1. **Navigate to your subscription**:
   - In the left-hand menu, select the "Subscriptions" option.
   - Select the subscription you want to use.
2. **Assign a role**:
   - Click on the "Access control (IAM)" link.
   - Click on "Add" and then select "Add role assignment" link.
   - Select the role you want to assign (for example, "Contributor").
   - In the "Select" field, search for the application you registered and select it.
   - Click the "Save" button.

### Step 3: Create a Client Secret

1. **Go back to your registered application**:
   - Navigate back to "Azure Active Directory" > "App registrations" blade.
   - Select your application.
2. **Create a client secret**:
   - Under the "Manage" section, select the "Certificates & secrets" option.
   - Click on the "New client secret" link.
   - Add a description and set an expiration period.
   - Click on the "Add"button.
   - Copy the value of the client secret and save it securely (you won’t be able to see it again).

### Step 4: Add the Service Principal to GitHub Secrets

1. **Gather the necessary information**:
   - Application (client) ID: Found in the "Overview" section of your registered application.
   - Directory (tenant) ID: Also found in the "Overview" section.
   - Client secret: The value you copied in the previous step.
2. **Go to your GitHub repository** and navigate to `Settings` > `Secrets and variables` > `Actions`.
3. **Create a new repository secret**:
   - Click on `New repository secret` link.
   - Name the secret `AZURE_CREDENTIALS` option.
   - Use the following JSON format for the value:
  
     ```json
     {
       "clientId": "<APPLICATION_CLIENT_ID>",
       "clientSecret": "<CLIENT_SECRET>",
       "subscriptionId": "<SUBSCRIPTION_ID>",
       "tenantId": "<DIRECTORY_TENANT_ID>"
     }
     ```

   - Replace the placeholders with your actual values and save it.

### Step 5: Use the Service Principal in Your GitHub Actions Workflow

1. **Create or update your GitHub Actions workflow file** (for example, `.github/workflows/azure.yml`):

   ```yaml
   name: Deploy to Azure

   on: [push]

   jobs:
     build-and-deploy:
       runs-on: ubuntu-latest
       steps:
       - name: Checkout code
         uses: actions/checkout@v2

       - name: Log in to Azure
         uses: azure/login@v1
         with:
           creds: ${{ secrets.AZURE_CREDENTIALS }}

       # Add your deployment steps here
   ```

This setup will allow your GitHub Actions workflow to authenticate to Azure using the service principal.

Would you like more details on any of these steps or help with something else?

## Resources
