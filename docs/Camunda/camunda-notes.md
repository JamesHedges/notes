# Camunda Notes

## Architecture

### Embedded Camunda

![Camunda Embedded](camunda-embedded.png)

1. User starts and interacts with a workflow
2. The microproduct service serves the single-spa application
3. The application send an initiating event to the embedded Camunda product service using the API. This Spring Boot application embeds:
   - Tomcat as a web container
   - Camunda engine
   - Camunda Web applications resources.
   - An in-memory H2 database (when running) for Camunda Engine needs.
4. The Camunda engine is invoked and loads the workflow and decision tables
5. Response is sent to the client application

## Business Process Modeling and Notation (BPMN)

>Business Process Model and Notation (BPMN) is a standard for business process modeling that provides a graphical notation for specifying business processes in a Business Process Diagram (BPD), based on a flowcharting technique very similar to activity diagrams from Unified Modeling Language (UML). The objective of BPMN is to support business process management, for both technical users and business users, by providing a notation that is intuitive to business users, yet able to represent complex process semantics. The BPMN specification also provides a mapping between the graphics of the notation and the underlying constructs of execution languages, particularly Business Process Execution Language (BPEL). [BPMN](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation)

### BPMN 2.0.2

The vision of BPMN 2.0.2 is to have one single specification for a new Business Process Model and Notation that defines the notation, metamodel and interchange format but with a modified name that still preserves the "BPMN" brand. The features include:

- Formalizes the execution semantics for all BPMN elements.
- Defines an extensibility mechanism for both Process model extensions and graphical extensions.
- Refines Event composition and correlation.
- Extends the definition of human interactions.
- Defines a Choreography model.

The current version of the specification was released in January 2014.

> [!NOTE]
>BPMN does not attempt to model other business models, such as organization, strategic direction, business functions, rules/compliance aspects, etc.

### Elements

#### Flow Objects

##### Events

Denotes something has happens and denoted by a circle. Events area classified as:

- **Catching** receives an incoming message, e.g. start process
- **Throwing** send an outgoing message, e.g. process ends

The events are:

<img src='./BPMN_plain_event_types.svg' alt='Event Types' width="300">

&nbsp;

- **Start Event:** Process trigger indicated by single narrow border. It can only be _catch_. Start Events indicate the instance or initiation of a process or an Event Sub-Process and have no incoming sequence flow. A Process can have more than one Start Event, but an Event Sub-Process only have one Start Event
- **Non-interrupting Start Event:** Non-interrupting Start Events can be used to initiate an Event Sub-Process without interfering with the main process flow
- **Intermediate Event:** Represents something that happens between the start and end events and is indicated by a circle with a double border. It can be either a _catch_ or _throw_ event. Example would be to flow an event to another flow by _throwing_ a message to another pool. Intermediate Events indicate something that occurs or may occur during the course of the process, between Start and End. Intermediate Catching Events can be used to catch the event trigger and can be in the flow or attached to the boundary of an activity. Intermediate Throwing Events can be used to throw the event trigger.
- **Non-interrupting Boundary Event:** Intermediate Events indicate something that occurs or may occur during the course of the process, between Start and End. Intermediate Catching Events can be used to catch the event trigger and can be in the flow or attached to the boundary of an activity. Intermediate Throwing Events can be used to throw the event trigger.
- **End Event:** Represents the result of a process and indicated by circle with a thick border. It can only _throw_. The End Event indicates where a path in the Process will end. A Process can have more than one end. The Process ends when all active paths have ended. End Events have no outgoing sequence flows.

##### Activities

An activity describes when something is done. It is represented by a rectangle with rounded corners.

<img src='./BPMN_Activity_Types.jpg' alt='Activity Types' width="300">

###### Tasks

A task represents a single unit of work that is not or cannot be broken down to a further level of business process detail. It is referred to as an atomic activity. A task is the lowest level activity illustrated on a process diagram. A set of tasks may represent a high-level procedure.

- **User:** A User Task is a typical “workflow” task in which a human performer performs the task with the assistance of a software application and could be scheduled through a task list manager of some sort.
- **Manual:** A Manual Task is a task that is expected to be performed without the aid of any business process execution engine or application.
- **Service:** A Service Task is a task that uses some sort of service, which could be a web service or an automated application.
- **Receive:** A receive task is a simple task that is designed to wait for a message to arrive from an external participant (relative to the process)
- **Send:** A Send Task is a simple task that is designed to send a message to an external participant (relative to the process).
- **Script:** A Script Task is executed by a business process engine. The modeler or implementer defines a script in a language that the engine can interpret. When the task is ready to start, the engine will execute the script. When the script is completed, the task will also be completed.
- **Business Rule (Decision):** A Business Rule Task provides a mechanism for the process to provide input to a Business Rules Engine and to get the output of calculations that the business rules engine might provide. The input/output specification of the task will allow the process to send data to and receive data from the Business Rules Engine.

###### Sub Process

A Sub-Process is a type of activity within a process, but it also can be “opened up” to show a lower-level process. This is useful for process decomposition or general process organization.

Used to hide or reveal additional levels of business process detail. When collapsed, a sub-process is indicated by a plus sign against the bottom line of the rectangle; when expanded, the rounded rectangle expands to show all flow objects, connecting objects, and artifacts. A sub-process is referred to as a compound activity.  
Has its own self-contained start and end events; sequence flows from the parent process must not cross the boundary.  

###### Transaction

A form of sub-process in which all contained activities must be treated as a whole; i.e., they must all be completed to meet an objective, and if any one of them fails, they must all be compensated (undone). Transactions are differentiated from expanded sub-processes by being surrounded by a double border.

###### Call Activity

A Call Activity is a type of activity within a process. It provides a link to reusable activities: for example, it will call a task into the Process (see upper figure on the left) or another Process (see lower figure on the left).

A point in the process where a global process or a global Task is reused. A call activity is differentiated from other activity types by a bolded border around the activity area.  
A Call Activity is a type of activity within a process. It provides a link to reusable activities: for example, it will call a task into the Process (see upper figure on the left) or another Process (see lower figure on the left)

##### Gateways

<img src='./BPMN_gateway_types.png' alt='Gateway Types' width="300">

- **Gateway:** Gateways are used to control how process paths converge and diverge within a process.
- **Event:** The Event Gateway, when splitting, routes sequence flow to only one of the outgoing branches, based on conditions. When merging, it awaits one incoming branch to complete before continuing the flow. The Gateway can be displayed with or without the “X” marker, but the behavior is the same
- **Inclusive:**  The Inclusive Gateway, when splitting, allows one or more branches to be activated, based on conditions. All active incoming branches must complete before merging.

- **Parallel:** The Parallel Gateway, when splitting, will direct the flow down all the outgoing branches. When merging, it awaits all the in branches to complete before continuing the flow.
- **Event-based:**
  - **Catching:** The Event Gateway is always followed by catching events or receive tasks. The flow of the Process is routed to the subsequent event/task which happens first. When merging, it behaves like an Event Gateway.
  - **Start a Process:** This Gateway can be configured such that it can be used to start a Process, based on the first event that follows it (see the lower figure on the left)  
![Event-based](./event-based-gateway.png)
  - **Parallel Event-Based:** only used for starting a Process. It is configured like a regular Event Gateway, but all subsequent events must be triggered before a new process instance is created
- **Complex:** defines behavior that is not captured by other gateways. Expressions are used to determine the merging and splitting behavior.

##### Connecting Objects - Flow Description

<img src='./Different_Types_of_BPMN_connections.png' alt='Connection Types' width="300">

- **Sequence Flow:** A Sequence Flow is represented by a solid line with a solid arrowhead and is used to show the order (the sequence) in which activities will be performed in a process or choreography diagram.
- **Message Flow:** A Sequence Flow is represented by a solid line with a solid arrowhead and is used to show the order (the sequence) in which activities will be performed in a process or choreography diagram.
- **Association:** A Sequence Flow is represented by a solid line with a solid arrowhead and is used to show the order (the sequence) in which activities will be performed in a process or choreography diagram.
- **Data Association:** A Data Association is represented by a dotted line with a line arrowhead and is used to associate data (electronic or non-electronic) with flow objects. Data Associations are used to show the inputs and outputs of activities.

## Diagrams

BPMN was designed to cover many types of modeling and allow the creation of end-to-end Business Processes. There are three basic types of submodels within the BPMN modeling environment:

- Process (Orchestration)
- Choreographies
- Collaborations

### Processes (Orchestration)

#### Private Process

Processes internal to a specific organization. These processes have been generally called workflow or BMP processes.

If a swim lanes-like notation is used (e.g., a Collaboration, see below) then a private Business Process will be contained within a single Pool. The Process flow is therefore contained within the Pool and cannot cross the boundaries of the Pool. The flow of Messages can cross the Pool boundary to show the interactions that exist between separate private or public Business Processes

##### Non-executable (internal) Business Process

A non-executable Process is a private Process that has been modeled for documenting Process behavior at a modeler-defined level of detail. Thus, information needed for execution, such as formal condition expressions are typically not included in a non-executable Process.

##### Executable (internal) Business Process

An executable Process is a Process that has been modeled for being executed according to the defined BPMN execution semantics. Of course, during the development cycle of the Process, there will be stages in which the Process does not have enough detail to be “executable.”

#### Public Process

A public Process represents the interactions to and from another Process or Participant. Only those Activities and Events that are used to communicate to the other Participants are included in the public Process. These Activities and Events can be considered the “touch-points” between the participants. All other “internal” Activities of the private Business Process are not shown in the public Process. Thus, the public Process shows to the outside world the Message Flows and the order of those Message Flows that is needed to interact with that Process. Public Processes can be modeled separately or within a Collaboration to show the directional flow of Messages.

![Public Process](./public-process.png)

### Collaborations

A Collaboration depicts the interactions between two or more business entities. A Collaboration usually contains two or more Pools, representing the Participants in the Collaboration. Or a Pool MAY be empty, a “black box.”

![Collaborations](./collaboration-process.png)

#### Swim Lanes

- **Pool:** within pools (same participant), across pools (different participants). If a swim lanes-like notation is used (e.g., a Collaboration, see below) then a private Business Process will be contained within a single Pool. The Process flow is therefore contained within the Pool and cannot cross the boundaries of the Pool. The flow of Messages can cross the Pool boundary to show the interactions that exist between separate private or public Business Processes.

- Lane

- **Dark Pool:** a Pool MAY be empty, a “black box.”

### Choreography

A self-contained Choreography (no Pools or Orchestration) is a definition of the expected behavior, basically, a procedural contract between interacting Participants. Although a normal Process exists within a Pool, a Choreography exists between Pools (or Participants).

The Choreography looks similar to a private Business Process because it consists of a network of Activities, Events, and Gateways (see Figure 4). However, a Choreography is different in that the Activities are interactions that represent a set (one or more) of Message exchanges, which involves two or more Participants. In addition, unlike a normal Process, no central controller, responsible entity, or observer of the Process exists

![Choreography](./choreography.png)

### Conversations

The Conversation diagram is a particular usage and an informal description of a Collaboration diagram. However, the Pools of a Conversation diagram usually do not contain a Process, and a Choreography is usually not placed between the Pools of a Conversation diagram. An individual Conversation (within the diagram) is the logical relation of Message exchanges. The logical relation, in practice, often concerns a business object(s) of interest, for example, “Order,” “Shipment and Delivery,” or “Invoice.Thus, the Conversation diagram is a high-level modeling diagram that depicts a set of related Conversations that reflect a distinct business scenario.

A Conversation diagram, as shown in below, shows Conversations (as hexagons) between Participants (Pools). This provides a “bird’s eye” perspective of the different Conversations that relate to the domain

![Conversation](./conversation.png)_**Figure x** Conversation_

#### Conversation

A Conversation defines a set of logically related Message Flows. When marked with a (+) symbol it indicates a Sub-Conversation, a compound conversation element.

![Conversation](./conversation-icon.png)

#### Call Conversation

A Call Conversation is a wrapper for a globally defined, re-usable Conversation or Collaboration. A call to a Collaboration is marked with a (+) symbol.

![Call Conversation](./call-conversation.png)

#### Conversation Link

Connects Conversations and Participants.

![Conversation Link](./conversation-link.png)

##### Artifacts

- Data object
- Group
- Annotation

## Decision Modeling and Notation

>A standard approach for describing and modeling repeatable decisions within organizations to ensure that decision models are interchangeable across organizations. [DMN](https://en.wikipedia.org/wiki/Decision_Model_and_Notation)

### Terminology - Process vs. Decisions

- DMN is a modeling language for decisions
- BPMN is a language for processes
- CMMN defines a common metamodel and notation for modeling and graphically expressing a Case, as well as an interchange format for exchanging Case models among different tools.

## Decision Modeling and Notation (DMN)

## Case Management Modeling and Notation (CMMN)

Case Management Modeling and Notation (CMMN)

## References

- [OMG White Papers](https://www.omg.org/news/whitepapers/index.htm)
- [The Complete Business Process Handbook, Volume 1](https://www.omg.org/news/whitepapers/Business_Process_Model_and_Notation.pdf)
- [An Introduction to Decision Modeling with DMN](https://www.omg.org/news/whitepapers/An_Introduction_to_Decision_Modeling_with_DMN.pdf)
- [DMN Spec](https://www.omg.org/spec/DMN)
- [CMMN Spec](https://www.omg.org/spec/CMMN)
- [BPMN Wikipedia](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation)
- [DMN Wikipedia](https://en.wikipedia.org/wiki/Decision_Model_and_Notation)
- [Camunda Modeler Reference](https://camunda.com/bpmn/reference/)
