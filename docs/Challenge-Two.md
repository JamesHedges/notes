
<!-- omit in toc -->  
# Python Challenge Two - Moving Beyond Basics

  - [More Data  Types](#more-data-types)
    - [Declaring Explicitly](#declaring-explicitly)
    - [Collections](#collections)

## More Data Types
### Declaring Explicitly
* Everything is an object
* Any type can be declared explicityly, without an assignment
  * int: `myInt = int()`
  * float: `myFloat = float()`
  * string: `myString = str()`

### Collections
#### List
* Represent a list of objects
* Create with [] or list()
  * `numberList = [1, 2, 3]`
  * `emptyList = list()`
* Lists are mutable - can insert and remove items

Example:

```python
    def listTest():
        myList = ["one", "two", "three" ]
        print(myList, "is of type", type(myList))
        printList(myList)
        myList.append("four")
        printList(myList)
        myList.remove("two")
        myList.append("Oh My!")
        printList(myList)

      def printList(theList):
         print("\nPrinting a", type(theList).__name__)
         for thing in theList:
             print("\t"+thing)
```

> See the documentation for many more operations on lists: https://docs.python.org/3/library/stdtypes.html#list

#### Tuple
* Sequence of items
* Is immutable - can NOT add or remove items
* Create using ()
 
Example:

```python
    def tupleTest():
        myTuple = ("one", 2, 3.14)
        print(myTuple)
        print(myTuple[1])
```

* Can also create from a list
```python
    myList = ['one', 'two', 'three']
    myTuple = tuple(myList)
```
##### Why use tuples?
* Use less space
* Cannot accidentally alter them
* Can use as dictionary keys
* Can pack function args / returns

#### Range

* Immutable sequence of numbers
* Create with 
  * First n numbers: `myRange = range(10)` -->  returns [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  * Numbers start to stop: `myRange = range(2,5)` --> returns [2, 3, 4, 5]
  * Numbers start to stop by an increment: `myRange = range(1, 9, 2)` --> returns [1, 3, 5, 7, 9]

 #### Set

* Unordered set of unique objects
* Can perform set operations like intersection, union, and difference
* Create with {} or set()
  * `mySet = {"one", "two", "three"}`
  * `mySet = set()`
* Can add and remove members

Example:

```python
    def setTest():
            mySet = { "one", "two", "three" }
            print(mySet, "is of type", type(mySet))
            printList(mySet)
            mySet.add("four")
            printList(mySet)
            mySet.remove("two")
            mySet.add("Oh My!")
            printList(mySet)
            secondSet = { "uno", "dos", "tres" }
            printList(secondSet)

            # union
            unionSet = mySet.union(secondSet)
            printList(unionSet)

            mySet.add("two")
            secondSet.add("cinco")

            # intersction
            printList(unionSet.intersection(mySet))
            printList(unionSet.intersection(secondSet))
```

#### Many more operations
##### Frozen Set
  * Same as set except it is immutable
  * See: https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset
##### Dictionary
  * Maps objects to a key
  * The key must be unique
  * Declare two ways:
    * With constructor: `myDict = dict(key1 = value1, key2 = value2, ... keyn = valuen)`
    * With initializer: `myDict = { key1: value1, key2: value2, ... keyn: valuen }`
  * Can get a value by index: `myDict["one"]`
    * Note the key "one" must exist in the dicrionary or the get will generate an exception. You can test with `myDict.contains(key)`
  * Can set a value by index: `myDict["one"] = "uno"`

Example: 
   
```python
    def dictionaryTest():
        myDict = {"one": "uno", "two": "dos", "three": "tres"} 
        printDictionary(myDict) 

        myDict["four"] = "quatro"
        printDictionary(myDict)

        myDict.pop("three")
        printDictionary(myDict)

        myDict["two"] = "2"
        printDictionary(myDict)
```

> See all dictionary operations: https://docs.python.org/3/library/stdtypes.html#mapping-types-dict

### Other Built In Types
#### Date Time
  * Must import datetime module
  * Date - Naive (cannot locate) date
  * Time - Idealized (can locate (tz)) time
  * DateTime - Idealized (can locate) date and time
  * Time Delta
  
```python
    import datetime as dt

    def TryDateTime():
        print("\n*** Date Time Examples")
        myTime = dt.datetime(2019, 7, 1)
        print(f"Now: {dt.datetime.now()}")
        print(f"myTime: {myTime}")
```

#### Array - Efficient arrays of basic values (no objects)
  * Must import array module
    * Declare with a type
      * Signed int 'i'
      * Unsigned int 'I'
      * Float 'f'
      * Signed char 'b'
      * Unsigned char 'B'
      * Unicode char 'u'
  * Create explicitly: `myIntArray = array.array('i')`

Example:

```python
    from array import array

    def TryArray():
        print("\n*** Array Examples")
        
        myArray = array('I', [1,2,3])
        print(f"myArra[2] = {myArray[2]}")
        ShowArray(myArray)
        myArray.append(5)
        myArray.insert(3, 4)
        ShowArray(myArray)
```

#### Enum
  * Must import enum module
  * A set of symbolic names
  * Example:
        
```python
    from enum import Enum

    class Color(Enum):
        Red = 1,
        Green = 2,
        Blue = 3

    def TryEnum():
        x = Color.Red
        print (f"Color.Red: {x.name}")

        if (x is Color.Red):
            print(f"x has color red: {repr(x)}")
```

### Iterating
* Can iterate over a collection that implements __itr__
* Use for each loop `for x in myCollection:`

Example:

```python
    from array import array

    def ShowArray(showMe):
        print("[ ", end= '')
        for element in showMe:
            print(f"{element} ", end='')
        print("]")
```

### Truth Value Testing
#### Comparison Operators
  * Equal: ==
  * Not Equal: !=
  * Less Than: <
  * Less Than or Equal: <=
  * Greater Than: >
  * Greater Than or Equal: >=
  * Is Object Identity: is
  * Is Not Object Identity: is not
 
#### Boolean operators
* And - x and y
* Or - x or y
* Not - not x

### Flow Control Statements
#### If Statement
* Conditional execution when true
* Syntax: if \<condition\>:
* May optionally have an elif: / else:

Example:
```python
    x = 2
    if x == 2:
        x += 1
    elif x == 3:
        x -= 1
    else:
        x = 5

    print(x)
```
#### For Statement
* See iterating above

#### While Statement
* Used to repeat execution while an expression is true
* Syntax
    ```python
    while expression:
        # do something
    
    ```
* Can _break_ out of the loop
* Can _continue_ to next iteration

Example:
```python
    notFound = True
    while notFound:
            if TimeIsUp():
                continue          # do the next iteration
            if GettingTired()
            break              # done with the loop
            if LookForSomething():
                notFound = False  # when something is found, end the loop
```

#### Try Except Statement  
* Used to handle an error (exception)
* Executes statements. If an exception is thrown, the error can be caught.
* An optional block can be added to run at the end to do any necessary cleanup
* Syntax:

 ```python
    try:
        # try some statement
    except Exception as ex:
        # handle the exception ex
    finally:
        # optionally do some cleanup

```

Example:

```python
    myInts = [1, 2, 3, 4]
    try:
        myInts[4] = 3
    except IndexError as ex:
        print (f"Error: {ex}")
```

### More Functions

#### Parameters
* Parameters are inputs to the function
* Can have zero or more inputs
* Prameters can have default values - from left to right

Example:

```python
    def MyFuncWithParams(name, isUpper = False):
        if isUpper:
            test = name.lower()
        else:
            test = name.upper()
        if test == 'SHOWIT'
            print (test)

    MyFunc("TEST NAME", isUpper=True)
    MyFunc("test name")
```

#### Return Values
* Function may return a value using a return statement

Example:
```python
    def MyFuncWithParamsAndReturn(name, isUpper = False):
        if isUpper:
            test = name
        else:
            test = name.upper()
        if test == 'SHOWIT'
            print (test)
        
        return "DONE"

    MyFunc("TEST NAME", isUpper=True)
    MyFunc("test name")
```

> See more on functions: https://www.w3schools.com/python/python_functions.asp

## For You To Do
### Lists - From the Python command line
* Create an empty list of strings and fill it with 10 strings
* Create a list of integers using the initialization syntax []
* Create a list using a range of even integers from 50 to 100
### Tuples - From the Python command line
* Create a tuple with a string, an integer, and a float
### Sets - From the Python command line
* Create a set of numbers from 1 to 10
* Create a set of odd numbers from 5 to 16
* Get and show the union of the sets
* Get and show the intersection of the sets
* Get and show the numbers in set 2 not in set 1
* Create a frozen set of fruit names
* Create a set of your favorite fruit
* Are all of you favorites in the frozen set of fruit names?
### Dictionary - From the Python command line
* Create a dictionary to translate english number one to ten to another language
* Lookup and display the numbers 3, 6, 8 
### DateTime - From the Python command line
* Show today's date and time
* Show your birth date
* Show what time you get to work
### if, for, while
### Try/Except
### Functions with parameters and return values
* Create a function that will print a list with each element on a separate line
* Create a function that will divide two numbers
  * Accepts two numbers as parameter, the numerator and denominator
  * Returns the result
  * Watch out for divide by zero. Should catch the error and display a message
* Create a function to return a message based on the value of an code
* Create a function that will print every third element of a list
* Create a function that will compute age in years base on a given date
