# Python Challenge Zero - Setting Up Your Environment

Set-up Python Environment *** REQUIRES ADMIN PRIVILEGES
## Install Python
  * Download version 3.7.3 from [Python Download](https://www.python.org/downloads/) - save to file, do not run it
  * Go to the download folder and run the Python install as administrator
  * Select option add Python to the path
## Install VS Code -- optional, but you will need a development environment
  * Download VS Code from [Visual Studio Download](https://code.visualstudio.com/Download)
    - select the version that matches your computer
    - Save to a file
  * Go to the download folder  and run the VS Code install as an administrator
  * Once installed and running, go to the Extensions (bottom icon on the left) and search for "python"
  * Select the Microsoft Python extention and click the install button
## Verify your Python environment is working
  * In VS Code, a terminal window should be open in the bottom pane.
    - If not, click on the Terminal menu and select the New Terminal option
  * In the terminal window at the prompt, type: python
  * You should have a >>> prompt, enter: print("Hello Python")
  * Press enter and you should see "Hello Python"
  * At the >>> prompt, enter: import this
  * You should some text printed that starts with "Simple is better than complex."
  * If you see the expected text displayed, congratulations! Your Python environment is now working
  * If you don't see the expected text, contact your team's mentor for assistance

## Install PIP
The `pip (Python Package Index)` package is standard for Python installs > 3.4. If you need to install it, e.g. you are in a virtual environment, do the following:

### [Best way to Install PIP for Python in Mac, Windows, and Linux](https://techworm.net/programming/install-pip-python-mac-windows-linux/)
- Download the installer script [get-pip.py](https://bootstrap.pypa.io/get-pip.py)
- If you’re on Python 3.2, you’ll need this version of [get-pip.py](https://bootstrap.pypa.io/3.2/get-pip.py)
- Open the Command Prompt and navigate to the get-pip.py file
- Run the following command: `python get-pip.py` and you are done

## Install virtualenv
Allows projects to have their own downloads that are not in the global space. This prevents projects with different requirements to use the package versions they need and not conflict with other projects. See 
- [The Hitchhiker's Guid to Packaging](https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/pip.html)
- [A non-magical introduction to Pip and Virtualenv for Python beginners](https://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/). You can find the docs [here](https://virtualenv.pypa.io/en/latest/). Install using pip:

```python
    pip install virtualenv
```

