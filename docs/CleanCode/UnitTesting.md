<!-- omit in toc -->
# Unit Testing

- [Pillars of Unit Testing](#pillars-of-unit-testing)
- [FIST](#fist)
- [Techniques](#techniques)

## Pillars of Unit Testing

## FIST

- **F**ast: Unit tests should be fast because we want to execute them frequently.
- **I**ndependent: Test conditions shouldn’t rely on any previous tests.
- **R**epeatable: We should be able to execute them in any environment.
- **S**elf-Validating: No need for further validations to ensure whether the test passed or not.
- **T**imely: Ideally tests should be written just before writing code.

## Techniques
