<!-- omit in toc -->
# Cloud Native Notes

- [Cloud Native Patterns](#cloud-native-patterns)
  - [Chapter 1](#chapter-1)
    - [Failure](#failure)
    - [Application Requirements](#application-requirements)
    - [Introducing Cloud-Native software](#introducing-cloud-native-software)
    - [Cloud vs Cloud-Native](#cloud-vs-cloud-native)
  - [Chapter 2. Running cloud-native applications in production](#chapter-2-running-cloud-native-applications-in-production)
- [Cloud Native Transformation](#cloud-native-transformation)
- [Accelerate](#accelerate)
- [The Twelve Factors](#the-twelve-factors)
- [Spotify Model](#spotify-model)
- [Fallacies fo Distributed Computing](#fallacies-fo-distributed-computing)
- [My Thoughts](#my-thoughts)
- [How to (not) succeed](#how-to-not-succeed)
- [An insider's guide to cloud computing](#an-insiders-guide-to-cloud-computing)
  - [How "real" is the value of cloud computing](#how-real-is-the-value-of-cloud-computing)

## Cloud Native Patterns

### Chapter 1

#### Failure

- Cloud-native software is designed to anticipate failure and remain stable even when the infrastructure it’s running on is        experiencing outages or is otherwise changing.
- treat change or failure as the rule
- we had this in previous times, but the difference is in how it is viewed and should be built in up front
- Failure is the rule, not the exception.
- new patterns and practices

#### Application Requirements

Modern expectation for digital experiences have changed approach to application requirements

- Zero downtime
  - can lead to significant loss of revenue
  - World is the audience, no deployment windows
  - loss of user satisfaction
- Shortened Feedback Cycles
  - Competition
  - excites customers
  - if feature is not successful, can abort it quicker - reduces risk
  - Old monolith days were annual/semi-annual deployments
    - more risky deployments
- Mobile and multidevice support
  - for mobile, must at least support iOS & Android
  - desktop (web support)
  - additionally - game devices, media devices (Apple TV, Firestick)
  - adapt to expanding and contracting demands
- Connected devices - also known as the Internet of Things
  - internet not just for humans
  - Internect connected devices change the nature of software
    - massive flow of data from billions of devices
    - capturing data requires significantly different computing substrate
    - highly distributed
  - Data-driven
    - due to volumes and distributed nature, large centralized shared Dbs are unusable
    - shared Dbs require coordination
    - impediment to shortened release cycles
    - switch to small localized data stores
    - need agility all through the data tier
    - applications must increasingly use data to provide greater value
      - no place for the painstakingly designed algorithms carefully tuned for usage scenarios
      - must adjust on the fly

#### Introducing Cloud-Native software

- Software
  - that is always up must be resilient to infrastructure failures and changes
  - must be able to adapt
  - limit its blast radius
  - drives to a modular design
  - include redundancy throughout design
- Release frequently
  - monolith too complex, many interdependent pieces, complex
  - loosely coupled, independently deployable and releasable components (services)
- Scales dynamically
  - Users not limited to accessing digital solutions from desktop
  - demand access from mobile devices 24/7
  - nonhuman entities (sensors, device controllers) always connected
  - data volumes can fluctuate widely

- Architectural implications
  - software constructed of independent components, redundantly deployed, implies distribution
    - cannot be deployed adjacent to each other - across wide swath
  - Adaptable by definition
    - able to adjust to new conditions
      - infrastructure changes
      - software changes
      - fluctuating request volumes

>Cloud-native software is highly distributed, must operate in a constantly changing environment, and is itself constantly changing.

- A mental model for cloud-native software
  - cloud-native app
    - code is the business logic
    - single app is rarely a solution
    - an app is at one end of an arrow - must maintain certain behaviors to make it participate in that relationship
    - constructed in manner that allows for cloud-nave operational practices
  - cloud-native data
    - where state lives
    - cloud-native software is broken into mans smaller apps, the data store is similarly decomposed and distributed
  - cloud-native interactions
    - how composition of app and data entities interact determines the functioning and qualities of a solution
    - extreme distribution and constant change characterize systems
    - some entirely new interaction patterns have evolved

- Implications for apps
  - everything is a service
  - capacity is scaled in/out, not up/down.
    - Multiple instances also offers levels of resilience
  - keeping state out of the apps allow for apps to be created and connected back to any stateful services (data store) it depends on
  - configuration must be managed differently due to apps transient nature
  - changes how app lifecycle is managed. Must examine how to start, configure, re-configure, shut down apps
- Implications for data
  - Apps are stateless, it is held in the data
  - Large monolithic data stores approach does not work in the context of distributed apps
    - they rob from agility and robustness
    - need to create a distributed data fabric
  - Fabric is made up of fit-for-purpose databases
    - caching is a key pattern and technology in cloud-native software
  - When entities exit in multiple databases, need to address how to keep them in sync
  - Treating state as an outcome of a series of events forms the core of the distributed data fabric

- Implications for interactions
  - Accessing an app with multiple instances requires routing
  - In a highly distributed, constantly changing environment, automatic retries are an essential pattern. They must be properly governed (circuit breakers)
  - A single request is served through a multitude of related services. Application metrics and logging will need to be specialized for this new setting
  - Protocols for the independent pieces must be suitable for the cloud-native context.

#### Cloud vs Cloud-Native

- Cloud is about _where_ we're computing. Cloud-native is about _how_.
- What is not cloud-native
  - Not all software should be cloud-native
    - If the software is not distributed and is rarely changing, is not cloud-native
    - If the software requires immediate consistency, it will not be cloud-native
    - Legacy software that has no value in rewrite should
  - You can mix cloud and cloud-native. Cloud software may have cloud-native components interacting with it

### Chapter 2. Running cloud-native applications in production



## Cloud Native Transformation

## Accelerate

## The Twelve Factors

## Spotify Model

## Fallacies fo Distributed Computing

## My Thoughts

- Approach with intention
- not just tech, a practice, attitude --> culture
- Robust vs efficient
- use Interstate highway project as an example. Wander country roads, narrow, dangerous. New highways required driver to adapt to new environment.

## How to (not) succeed

[Are we worse at cloud computing than 10 years ago?](https://www.infoworld.com/article/3614055/are-we-worse-at-cloud-computing-than-10-years-ago.html)

Let’s explore the key factors that contribute to this dismal success rate:

**Inadequate planning.** A lack of preparation leads to compatibility issues, unexpected costs, and technical roadblocks that could have been anticipated with proper assessment. Cloud projects have many dependencies. You need to pick a database before picking a development platform, and you have to determine performance requirements before moving to containers and microservices. I’m seeing more projects stall or fail due to the lack of simple planning.

**The increasing complexity of IT systems.** Organizations struggle with intricate IT architectures and interdependencies. I’ve covered the complexity issue to death because complexity is becoming the “silent killer” of cloud development and deployment. It can be managed, but it requires adequate planning (see the previous point).

**The talent gap.** The shortage of technical cloud expertise makes it increasingly difficult for organizations to execute and maintain cloud initiatives. This has become a critical bottleneck in cloud project success.

**Uncontrolled cloud costs.** Many organizations are seeing unexpected increases in post-migration operational expenses. The lack of adequate cost controls and automated mitigations leads to budget overruns and project failures. Projects coming in on budget are rare. Also, as I covered in my latest book, enterprises spend about 2.5 times the amount they budgeted to operate their cloud-based systems. Although finops can address some of these issues, strategic cost-planning problems are not being effectively managed.

**Compliance and security challenges.** Compared to earlier cloud adoption phases, today’s projects face a significant increase in security-related complications and compliance issues.

## An insider's guide to cloud computing

### How "real" is the value of cloud computing

The value proposition isn't

- CapEx → OpEx
- Cost reduction

The real value resides in

- Agility - the business's ability to change around business requirements
- Speed - business's ability to move fast in a single business (speed-to-market)
- Innovation - ability to use the latest technologies to discover and unlock unexpected value
