<!-- omit in toc -->
# Docker Cheat Sheet

## Commands

### Version

`docker --version`

### Managing Images

| Command                          | Option        | Description                                  | Note                                             |
| :------------------------------- | :------------ | :------------------------------------------- | :----------------------------------------------- |
| `pull <image_name>`              |               | Downloads an image from the Docker registry  |                                                  |
| `images`                         |               | List the images in the local repository      |                                                  |
| `run <image_name>`               |               |                                              |                                                  |
| `run -it <image_name> <command>` | -it           | Runs the image in interactive mode           | e.g. use command _bin/sh_ to open a Linux shell  |
| `ps`                             |               | List the running containers and their status |                                                  |
|                                  | -a            | Adds containers that have are stopped        |                                                  |
| `rmi <image_id>`                 |               | Remove an image from local repository        |                                                  |
| `pull`                           |               |                                              |                                                  |
| `rm`                             |               | Remove a stopped container                   |                                                  |
|                                  | -f or --force | Remove a running container                   |                                                  |
| `inspect`                        |               | Inspect the contents of a container          | `docker inspect <name or ID>`                    |
|                                  | -f            | Filters the response                         | e.g. inspect -f "{{json .State}}" \<name or ID\> |
| `exec`                           |               | Execute a command in a container             |                                                  |
|                                  | -i            | run command interactively                    |                                                  |
|                                  | -t            | provide a terminal emulator                  | e.g. docker exec -i -t \<name \| id\> /bin/sh    |
| `attach`                         |               | Attach container to  terminal's standard io  |                                                  |
| `logs`                           |               | Gets the active log for the container        |                                                  |
|                                  | -t or --tail  | Gets the last n entries from the log         | e.g. docker logs -t 5 \<name \| id\>             |
|                                  | --follow      | Continues to show new log entries            |                                                  |

## Images

Docker images are found in the  [Docker registry](https://hub.docker.com)

## Run Container with Privileged Access

By default, the user is not privileged and cannot run some administrative commands. To elevate the user, run the container with the privileged switch  
`docker container run --privileged [my_container]`

## Docker File

A **Dockerfile** is a plain text file containing the instructions for building a docker image. Each instruction will create a layer in the container.

### Instructions

| Instruction | Description                                                                                                | Example                                                     | Additional Info                                                  |
| :---------- | :--------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------- | :--------------------------------------------------------------- |
| FROM        | Base image                                                                                                 | `FROM mcr.microsoft.com/dotnet/framework/sdk`               | Use `scratch` image for empty base                               |
| LABEL       | Add extra information about container (not functional)                                                     | `LABEL description="About this container"`                  | [Label Schema](http://label-schema.org/rc1)                      |
| RUN         | Runs scripts, commands, and other tasks                                                                    | `RUN apk add --update nginx`                                |                                                                  |
| COPY        | Copies files from host to the image                                                                        | `COPY . /source`                                            |                                                                  |
| ADD         | Automatically uploads, uncompresses, and add the resulting folders and files to the requested path         | `ADD html.tar.gz /usr/share/nginx`                          |                                                                  |
| EXPOSE      | Lets Docker know that when the image is executed, the port and protocol defined will be exposed at runtime | `EXPOSE 80/tcp`                                             |                                                                  |
| ENTRYPOINT  | Command to run if container is executable                                                                  | `ENTRYPOINT ["nginx"]`                                      |                                                                  |
| CMD         | Command executed on the entrypoint. Used with entrypoint                                                   | `CMD ["-g", "daemon off;"]`                                 | Combines with entry point, executes `$ nginx -g daemon off;`     |
| USER        | Specify the user name to be used when a command is run                                                     | `USER containeradministrator`                               | Two built-in users, `containeruser` and `containeradministrator` |
| WORKDIR     | Sets the working directory                                                                                 | `WORKDIR /source`                                           |                                                                  |
| ONBUILD     | Stash a set of commands to be used when the image is used as a base image                                  | `ONBUILD RUN apk update&& apd upgrade && rm -rf /var/cache` |                                                                  |
| ENV         | Sets environment variables within the image when built and executed. Can be overridden when image launched | `ENV`                                                       |                                                                  |

### Building Docker Images

Build docker image using the **Build** command. Basic use is `docker image build -t <image name> .`. It is typically run in the same folder with the `dockerfile` file. Some common switches are

| Parameter    | Example                          | User                      | Note                                                 |
| :----------- | :------------------------------- | :------------------------ | :--------------------------------------------------- |
| -t \| --tag  | `--tag local:dockerfile-example` | Name of the image         | local is the default, used to specify the repository |
| -f \| --file | `--file /path/to/dockerfile`     | Name of the file to build | default is `dockerfile` in current folder            |

#### Docker Ignore

The `.dockerignore` file is used to exclude those file or folders we don't want included in the docker build.

### Other Image Commands

| Command | Description                             | Example                                         | Note                      |
| :------ | :-------------------------------------- | :---------------------------------------------- | :------------------------ |
| ls      | Lists images in the repository          | `docker image ls                                | Use -a switch to list all |
| inspect | Display information about the container | `docker container inspect docker-example-image` |                           |

## Docker Containers

| Command | Description               | Example                                                              | Note |
| :------ | :------------------------ | :------------------------------------------------------------------- | :--- |
| run     | Launch a container        | `docker container run -d --name docker-example docker-example-image` |      |
| ls      | List conatiners           | `docker container ls --all`                                          |      |
| stop    | Stops a running container | `docker container stop docker-example`                               |      |
| rm      | Removes a container       | `docker container rm docker-example version`                         |      |

## Builds

### Clean Build

When an image is built, cached layers are used unless invalidated by Dockerfile changes. To force a "clean" build, do the following sequence:

```powershell
docker system prune
docker image build --no-cache -t my-image .
```

- Reference: [Docker cleanup, build and force to rebuild images, containers, volumes and networks](https://kenanbek.medium.com/docker-cleanup-build-and-force-to-rebuild-images-containers-volumes-and-networks-dc70fd4ccec0)

### Update Base Container

Docker builds will first look for base containers in the local image registry. If found, it will take that image without looking to any remote registry for a newer version. To get the latest image version of a base container from a remote registry, use the **--pull** option.

```powershell
docker image build --pull -t my-image .
```

### Install MSI File

Installing Windows applications in a container will ofter require using a _*.msi_ file. These cannot be run directly and require the use of **Start-Process -PassThru | Wait-Process**

```powershell
Start-Process 'c:/installer/my-app.msi' '/qn /log ./my-app-install.log' -PassThru | Wait-Process
```

### Mulit-Stage Build Dockerfile

Create a container that builds an app and then removes those layers used for app generation.

```dockerfile
FROM mcr.microsoft.com/dotnet/framework/sdk AS builder

WORKDIR /source

COPY . .

WORKDIR /source
RUN msbuild -t:Build -p:Configuration=Release

FROM scratch

COPY --from=builder /source/src/DnfDocker.Console/bin/release

CMD ./DnfDocker.Console.exe
```

### Run Containerized App

```powershell
# build the image
docker image build -t my-app-image .

# run the containerized app
docker container run --name my-app my-app-image

# run indefinitetly
docker run ubuntu tail -f /dev/null
docker run ubuntu while true: do sleep 1; done
docker run ubuntu sleep infinity
```

See [Running Docker Containers Indefinitely](https://www.baeldung.com/ops/running-docker-containers-indefinitely)

#### Show Container's IP Address

`docker container inspect --format '{{.NetworkSettings.Networks.nat.IPAddress }}' 9c506e4f963f`

## Configure Windows Server 2019

[Prep Windows for containers](https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment?tabs=Windows-Server)

```powershell
# As Admin
Install-Module -Name DockerMsftProvider -Repository PSGallery -Force
Install-Package -Name docker -ProviderName DockerMsftProvider
Restart-Computer -Force
```

## Extract File

[Extract file from docker image](https://itectec.com/unixlinux/extract-file-from-docker-image/)

You can extract files from an image with the following commands:

```powershell
$container_id = $(docker create $image)  # returns container ID
docker cp $container_id`:$source_path $destination_path
docker rm $container_id
```

According to the docker create documentation, this doesn't run the container:

The docker create command creates a writeable container layer over the specified image and prepares it for running the specified command. The container ID is then printed to STDOUT. This is similar to docker run -d except the container is never started. You can then use the docker start <container_id> command to start the container at any point.

## Windows Server Docker in Docker

From [Learn Docker Fundamentals - Chapter 8, Section Running Docker in Docker](https://learning.oreilly.com/library/view/learn-docker/9781838827472/378f28d3-5506-4c98-99f3-fdd5face84e2.xhtml)

A special notice to all those of you who want to try Windows containers. On Docker for Windows, you can create a similar environment by bind-mounting Docker's named pipe instead of a socket. A named pipe on Windows is roughly the same as a socket on a Unix-based system. Assuming you're using a PowerShell Terminal, the command to bind-mount a named pipe when running a Windows container hosting Jenkins looks like this:

```powershell
PS> docker container run `
--name jenkins `
-p 8080:8080 `
-v \\.\pipe\docker_engine:\\.\pipe\docker_engine `
friism/jenkins
```

>Note the special syntax, \\.\pipe\docker_engine, to access Docker's named pipe.

## Format Docker Output

From [Learn Docker Fundamentals - Chapter 8, Formatting the Output of Common Docker Commands](https://learning.oreilly.com/library/view/learn-docker/9781838827472/94ecc2ea-f58e-4c58-b5d7-97154ee6198b.xhtml)

```powershell
$ docker container ps -a \
--format "table {{.Names}}\t{{.Image}}\t{{.Status}}"
```

> The format string is case sensitive

## Bind Volume for Windows Container

```powershell
docker run --rm -it -v c:\source\folder:c:\container\folder
```
