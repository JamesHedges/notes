<!-- omit in toc -->
# Docker for the absolute Beginner - Hands-On

[O'Reilly Video](https://learning.oreilly.com/videos/docker-for-the/9781788991315)

- docker is lxc type container

## Basic Docker Commands

- docker run \<image\> | runs the container then stops
- docker ps | lists running containers
- docker ps -a | lists all installed containers
- docker stop \<name or ID\> | stops a running container
- docker rm \<name or ID\>  | removes a container form disk
- docker images | see a list of images
- docker rmi | removes an image, all dependent container must be stopped and removed first
- docker pull | downloads an image to local disk
- docker exec \<container name\> \<command\> | executes the command
- docker run -d \<image\> | run in background
- docker pull \<image\> | downloads an image into local repository

## Docker File

### Steps to Create Containerized Application

Create a containerized app example. Here are the steps for a simple Python Flask app:

1. OS - Ubuntu
2. Update ap repo
3. Install dependencies using apt
4. Install Python dependencies using pip
5. Copy source code to /opt folder
6. Run te web server using "flask" command

### Example docker file

```docker
FROM Ubuntu  # base OS

RUN apt-get update           # Install all dependencies
RUN apt-get install python python-pip

RUN pip install flask
RUN pip install flask-mysql

COPY . /op/source-code        # copy source code

ENTRYPOINT FLASK_APP=/opt/source-code/app.py flask run     # command to run
```

Build container with command: `docker build Dockerfile -t <app_tag_name>`

Place in local repository: `docker push <tag>`

Run docker history command to see the image's build layers

## Docker Compose

Create the compose file **docker-compose.yml**

```yml
services:
    web:
        image: "myWebApp"
        ports:
            - "80:5000"

    database:
        image: "mysql"
        volumes:
            - /opt/data:/var/lib/mysql
```

Build with the command: `docker-compose up`
Stop the container: `docker-compose stop`
Remove the container: `docker-compose down`

## Docker Networking

![DockerDefaultNetworks](./DockerDefaultNetworks.png)

To see all networks: `docker network ls`
