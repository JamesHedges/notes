# Alpine image notes

## Shell

Alpine uses the `ash` shell:

```sh
docker run --rm -it alpine /bin/ash
```

## References

[^1]: [Alpine Linux Cheat Sheet](https://github.com/masoudei/alpine-linux-cheat-sheet/blob/main/README.md)