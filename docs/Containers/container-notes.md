# Containers

## Container registries

- [CNCF - Deploy a registry server](https://distribution.github.io/distribution/about/deploying/)
- [CNCF - Test an insecure registry](https://distribution.github.io/distribution/about/insecure/)
- [Docker Hub - Registry](https://hub.docker.com/_/registry)
- [How to set up a local image repository with Podman](https://www.techrepublic.com/article/how-to-set-up-a-local-image-repository-with-podman/)
- [Podman v2 config for insecure registries](https://williamlieurance.com/insecure-podman-registry/)
