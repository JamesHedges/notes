# Domain Driven Design: Tackling Complexity in the Heart of Software

Three basic uses determine the choice of model

1. The model nad the heart of the design shape each other
2. The model is the backbone of a language used by all team members.
3. The model is distilled knowledge.

## Chapter 1: Crunching Knowledge

Knowledge crunching is an exploration, and you can't know where you will end up.

### Ingredients of Effective Modeling

1. Binding the model and the implementation
2. Cultivating a language based on the model
3. Developing a knowledge-rich model
4. Distilling the model
5. Brainstorming and experimenting

### Continuous Learning

Continuously grow your domain and technical knowledge base. This will cultivate a shared language and create a feedback loop that will improve the model.

## Knowledge-Rich Design

Domains are often more complex than they originally appear. What is intuitive by a domain expert is not easily captured. Through knowledge crunching in close collaboration with software experts is when the rules are clarified, fleshed out, reconciled, or placed out of scope.

### Deep Models

Useful model seldom lie on the surface. As domain understanding deepens, we will discard the superficial elements and shift perspectives. Subtle abstractions emerge that will pierce the heart of the matter.

## Chapter 2: Communication and the Use of Language

To create a supple, knowledge-rich design calls for a versatile, shared team language.

Ubiquitous Language:

- Includes the names of classes and prominent operations
- Will force the model's weaknesses into the open
- In the scope of the model, use the model-based language. A concern is raised when the language cannot be used to express model concepts.
- A change to the ubiquitous language is a change to the model.
- Is the primary carrier of the design aspects that don't appear in code.
- Becomes more fluent when used between experts and developers.
- Language is based on a model, use the ubiquitous language in requirements.
- Conversations among developers, discussions among domain experts, and expressions in the code are all base on the the same language - derived from a shared domain model.
- Documents should use the ubiquitous language.
- If terms in documents are not in conversations and code, the documents are not fulfilling their purpose. They are too big, too complicated, or not compelling.
- The ubiquitous language naturally changes, documents must reflect this.
- Allows documents to be more concise
- Directly connected to the model driven design

Problems arise when the language used by the domain experts and technical teams is fractured. Use the language of the domain, embed it in the code. Note that domain experts may have differences between speaking and writing. A single language needs to emerge. One that blunts translation and has no dialect.

Commit to using the shared language in all communications. In speech, documents, diagrams, and code. Resolve confusion over terms. Refactor code to conform with any ubiquitous language changes. Domain experts should object or to terms or structures that are awkward or inadequate; developers should watch for ambiguity or inconsistency.

Describe scenarios out loud and find easier ways to express concepts. Then take those new ideas back to the the documents, diagrams, and code.

## Chapter 3: Binding Model and Implementation

### Model Driven Design

If the design, or any part of it, do not map back the the model. Then the model is of little value and the correctness of the software is suspect. Model-driven design discards the dichotomy of analysis model and design to search out a single model that serves both purposes. The problem space should have a one-to-one mapping to the solution space.

Draw from the model the terminology used in design. The code then becomes an expression of the model. Changes to the model must ripple through the rest of the project's activities.

### Modeling Paradigms and Tool Support

To make the MODEL-DRIVEN DESIGN, there needs to be a tool, paradigm, the facilitates the one-to-one mapping from mode to design. It is essential to work within a paradigm supported with software tools that can capture the concepts in the model.

- Object oriented is powerful because it is based on a modeling paradigm. It provides implementations of the model construct using classes with behavior and messaging. It dominates the majority of ambitious projects.
- Prolog's paradigm is logic, and the model is a set of logical rules and facts they operate on.
- Has limited applicability using languages such as C, because there is no modeling paradigm. It is procedural.

### Why Models Matter to Users

MODEL-DRIVEN DESIGN calls for working with one one model, within a context. If the business model analysis is separate from the design analysis, the design may not reflect the concerns of the user. Revealing the model gives the user more access to the potential of the software and yields consistent, predictable behavior.

### Hands-On Modelers

- Software development is all design
  - team members have specialized roles
  - over-separation of responsibility for analysis, modeling, design, and programming interferes with MODEL-DRIVEN DESIGN
- When separated, a model's intent may be lost in the handoff.
  - The overall effect of a model can ber very sensitive to details
  - Those details don't always come across in a UML diagram or general discussion
  - Models don't receive feedback from the technologies in a timely manner

- If the people who don't feel responsible for the model, or don't understand how to make the model work for an application, then the model has nothing to do with the software.
- If developers don't realize that changing code changes the model, the their refactoring will weaken the model rather than strengthen it.
- When a modeler is separated from the implementation process, they never acquire, or quickly lose, a feel for the constraints fo implementation
- Leading to:
  - The basic constraint of MODEL-DRIVEN DESIGN - that the model supports an effective implementation and abstracts key domain knowledge - is half gone, and the resulting models will be impractical.
  - The knowledge ans skills of experienced designers won't be transferred to other developers if the division of labor prevents the kind of collaboration that conveys the subtleties of coding a MODEL-DRIVEN DESIGN

Therefore:

- Any technical person contributing to the model must spend some time touching the code, whatever primary role they paly on the project.
- Anyone responsible for changing code must learn to express a model through the code. Every developer must be involved in some level of discussion about the model and have contact with domain experts.
- Those who contribute in different ways must consciously engage those who touch the code in a dynamic exchange of model ideas through teh ubiquitous language

## Chapter 4: Isolating the Domain

- Keep design aligned with the model
  - Reinforces the other's effectivenss
- To make process resilient, developers how the fundamentals of MODEL-DRIVEN-DESIGN work
- Use pattern language to bring order to design and make it easier to share
- Isolate the domain
- Develop its ubiquitous language
- Define model elements based on their distinctions
- Follow proven patterns

![Language of MODEL-DRIVEN-DESIGN](Navigation%20map%20of%20MODEL-DRIVEN-DESIGN%20language.png)

### Layered Architecture

- The domain usually constitutes only a small portion of the entire software system, although its importance is disproportionate to its size.
- Look at elements of the model and see them as a system
- Decouple the domain objects from other functions of the system to avoid confusing the domain concepts with other concepts related only to software technology
- Use a layered architecture

![Layered Architecture](LayeredArchitecture.png)

- Isolate the domain-related implementation from other concerns. When:
  - Embedding business logic in the UI and application layers is the easiest in the short run.
  - Allowing business logic to seep into persistence concerns
- Then:
  - However, making changes to business functions that have become dispersed are difficult to manage
  - Testing is awkward
  - The system becomes impossible to understand
- Partition a complex domain into layers
  - Keep each layer as cohesive as possible
  - Concentrate business domain code into a single layer
  - All responsibilities for persistence should be in its own layer
  - Presentation details will be designated to its own layer
- Relating layers
  - Layers are meant to be loosely coupled
  - Have dependencies in only one direction
  - Infrastructure layer must not have any knowledge of the domain layer
  - Application and domain layers call services provided by the infrastructure layer and remain loosely coupled
- Architectural frameworks
  - When infrastructure is provided via services called through interfaces, it is intuitive how layering works to keep the keep the layer loosely coupled
  - Some frameworks require layers to be implemented in very particular ways and can get in the way by
    - Making too many assumptions
    - Constraining domain design choices
    - Make implementation heavyweight
  - Focus on goal of solving a domain problem
  - Choose frameworks that server you well.
  - Not required to use all of the framework's features, judiciously apply the most valuable framework features
  - Keep business objects readable and expressive
  - Elaborate frameworks can also become a straightjacket for developers
- Simple applications can get away with embedding business domain logic on other components
  - These are unsophisticated
  - If become more complex, will need to be refactored
- Other Kinds of Isolation
  - Other domain components
  - Other development teams using different models
  - Includes 3rd party components
  - See BOUNDED CONTEXT and ANTICORRUPTION LAYER concepts presented later on patterns to deal with these cases.

## Chapter 5: A Model Expressed in Software

Connect the model and implementation at a detailed level. This chapter focuses on detail level components.

### Associations

> For every traversable association in the model, there is a mechanism in the software with the same properties.

- The design has to specify a particular traversal mechanism whose behavior is consistent with the association in the model.
- Associations may be:
  - Expressed in many different ways
  - There are lots of many-to-many associations, a great number are naturally bidirectional
  - But these general associations often complicate an implementation
- Three was of making associations more tractable
  - Imposing a traversal direction. Constraining traversal direction of a many-to-many reduces its implementation to one-to-many, which is much easier design.
  - Adding a qualifier, effectively reducing multiplicity. A deeper understanding leads to "qualified" relationships.
  - Eliminating nonessential associations. When the model is biased toward traversal direction, constrain the model to that direction. Ultimately remove it when not essential to the job at hand.
- It is important to constrain relationships
  - Bidirectional associations means objects can only be understood together
  - When application requirements do not call for traversal in both directions, implementing a single direction traversal reduces object interdependence and simplifies the model

### Entities (a.k.a. Reference Objects)

- Many objects are not fundamentally defined by their attributes
- They represent a thread of continuity and identity through time and often across distinct representations.
- Objects may need to be matched, even when their attributes differ.
- Some object must be distinguished from other object even through they might have the same attributes.
- Mistaken identity can lead to data corruption
- An object defined primarily by its identity is called an ENTITY
- An ENTITY is anything that has continuity through a lifecycle and distinctions independent of attributes that are important to the application's user.
- ENTITY objects have an identifier,  that is guaranteed unique, assigned at creation and exists throughout the object's lifecycle
- Equality is based on the identifier
- Not all objects are entities, see VALUE OBJECTS

#### Modeling Entities

- Basic responsibility of ENTITIES is to establish continuity so that behavior can be clear and predictable
- Do this best when kept spare
  - Don't focus on attributes and behavior
  - Strip it down to the most intrinsic characteristics
  - Add only behavior that is essential to the concept and attributes that are required by that behavior
  - Look to remove behavior and attributes into other objects associated with the core ENTITY
    - Some will be other ENTITIES
    - Some will be VALUE OBJECTS
- Entities tend to fulfill their responsibilities by coordinating the operations of objects they own

##### Designing the Identity Operation

- Each ENTITY must have an operational way of establishing identity
- Identifying attribute must be guaranteed unique within the system
  - However that system is bounded
  - Even if distributed
  - Even when archived
- Definition of identity emerges from the model and demands understanding of the domain
  - Sometimes certain attributes, or combinations of attributes, can be guaranteed to be unique within the system
  - When there is no true unique key made up of attributes, it is common to attach each instance with a symbol that is unique
  - Once generated an attribute of an ENTITY, it is immutable
- It is not required that the unique identity of an object is revealed outside the system

### VALUE OBJECTS

Many objects have no conceptual identity. These objects describe some characteristic of a thing.

- It is natural to consider assigning an identity to all domain objects
- Some objects describe things and have no identity
- Some frameworks assign a unique ID to every object
- But the system then needs to track these objects
  - Can lead to poor performance
  - Requires effort to analyze and design a fool proof identity algorithm
  - Muddles the model forcing all object to be entities

- An object that represents a descriptive aspect of the domain with no conceptual identity is called a VALUE OBJECT
  - VALUE OBJECTS may reference entities
  - You only care about the attributes
  - Is immutable
  - Don't give it any identity. This reduces design complexity
  - Comparisons made based on its attributes

#### Designing VALUE OBJECTS

- Defined by attributes
  - Equality based on attributes. When all attributes are equal, the VALUE OBJECT is equal
- Is immutable
  - Can be shared or copied
  - Not referenced
  - Avoids unnecessary constraints in a model
- There is no change, only replacement

#### Designing Associations the Involve VALUE OBJECTS

- Bidirectional associations between VALUE OBJECTS make no sense
- If a bidirectional association is required, then rethink whether the object is an ENTITY or VALUE OBJECT

### Services

In some cases, the clearest and most pragmatic design includes operations that do not conceptually belong to any object. Rather than force the issue, we can follow the natural contours of the problem space an include SERVICES explicitly in the model.

- Some concepts from the domain aren't natural to model as objects
- Forcing required domain functionality to be an ENTITY or VALUE OBJECT either distorts the definition of a model-based object or adds meaningless artificial objects.
- A SERVICE is an operation that stands alone in the model
  - Does not have state
  - Named based on an activity, a verb instead of a noun
  - Its name comes from the UBIQUITOUS LANGUAGE
  - Its parameters and results should be domain objects
- A good SERVICE has three characteristics:
  - The operation relates to a domain concept that is not a natural part of an ENTITY or VALUE OBJECT
  - The interface is defined in terms of other elements fo the domain model
  - The operation is stateless
- When a significant process or transformation in the domain is not a natural responsibility of an ENTITY or VALUE OBJECT
  - Add an operation to the model as a standalone interface declared as a SERVICE
  - Define the interface in terms of the UBIQUITOUS LANGUAGE
  - Make the service stateless

#### Services and the Isolated Domain Layer

Three types of services:

- Application
  - Coordinates between domains
- Domain
  - Coordinates between ENTITIES and VALUE OBJECTS
  - Encapsulates business rules
  - Fits in the domain's UBIQUITOUS LANGUAGE
- Infrastructure
  - Has no business logic
  - Purely technical
  - Application and domain SERVICES collaborate with them

##### Granularity

- Services can be used to control the granularity in domain layer interfaces by decoupling clients from ENTITIES and VALUE OBJECTS
- Medium-grained SERVICES easier to reuse in large systems
- Fine-grained SERVICES can
  - Lead to inefficient messaging
  - Contribute to knowledge leaks from domain into the application layers
- Favor interface simplicity over client control and versatility (medium-grained)

##### Access to Services

- Means of SERVICE access is less important than design
- Coding conventions can make it clear that these objects are just delivery mechanisms for SERVICE interfaces
- Only use elaborate architecture when there is a real need to distribute the system
- Otherwise, relay on the framework's capabilities

### Modules (a.k.a. Packages)

- MODULES
  - Are a technical consideration
  - Reduce cognitive overload
  - Treat them as a full-fledged part of the model
  - Should demonstrate low coupling and high cohesion
  - Not just technical consideration, but conceptual
    - Limit how many things a person can think about at once (low coupling)
    - Incoherent fragments of ideas are hard to understand (high cohesion)

Therefore:

- Choose MODULES that tell the story of the system and contain a cohesive set of concepts
- Should yield low couplings between modules. If it does not, return to the model and search for overlooked concepts
- Seek low coupling in terms of concepts that can be reasoned independently
- Refine the model until it partitions according to high-level domain concepts
- MODULES names should become part of the UBIQUITOUS LANGUAGE and reflect insight
- Prefer conceptual clarity over technical measures

#### Agile Modules

- MODULES need to coevolve with the rest of the model
- Refactor along with the model and code
- MODULES tend to be difficult to refactor
  - They are based on early model assumption and mistakes
  - Leads to high coupling
- Refactoring early will
  - Maintaining consistency with the model and will result in less couplings and higher cohesion
  - Create MODULES that are easier to reason about

##### The Pitfalls of Infrastructure-Driven Packaging

A useful framework standard is the enforcement of a LAYERED ARCHITECTURE that places infrastructure and user interface into separate groups of packages, leaving the domain layer physically separated into another set of packages. However, this tiered architecture can fragment the implementation of model objects.

Makes viewing the various objects and mentally fitting them back together as a single conceptual ENTTITY too much effort. We lose the connection between the model and design. With little mental space left to think about modules, developers will deliver an anemic domain model.

Such divisions make it easier to understand the functioning of each tier and make it easier to switch out layers. But at what cost? Better to trade off these benefits for a more cohesive domain layer.

One other motivation for tiers and packaging schemes around them is to distribute layers across different servers. This usually does not happen and that flexibility is never needed.

Elaborate technically driven packaging schemes impose two costs:

- The framework's partitioning conventions pull apart the elements implementing conceptual (domain) objects and the code no longer reveals the model.
- There is only so much partitioning a mind can stitch back together and developers lose their ability to break the model into meaningful pieces.

__Unless there is a real intention to distribute code on different servers, keep all the code that implements a single conceptual object in the same MODULE, if not the same object.__

__Use packaging to separate the domain layer from other code. Otherwise, leave as much freedom as possible to the domain developers to package the domain objects in way that support the model and design choices.__

One exception. Generated code should be packaged separately.

Each concept from the domain model should be reflected in an element of implementation. Resist the temptation to add anything to the domain objects that does not closely relate to the concepts they represent.

There are also other model paradigms supported by tools, such as rules engines. Projects have to make pragmatic trade-offs between them. These other tools and techniques are means to the end of a MODEL-DRIVEN DESIGN, not alternatives to it.

### Modeling Paradigms

The dominant paradigm is object-oriented design.

#### Why the Object Paradigm Predominates

- Object modeling stikes a nice balance of simplicity and sophistication.
- Fundamentals of object-oriented design come naturally to most people
  - If nontechnical team members can't grasp at least the rudiments of the paradigm, they will not understand the model. The UBIQUITOUS LANGUAGE will be lost
  - non-technologists can follow a diagram of an object model
- It has been proven rich enough to capture important domain knowledge
- Has advantages of wide adoption.
  - Maturity of the developer community and the design culture makes it easier to find people with expertise
- Most new technologies provide means to integrate with object-oriented platforms.
  - Makes integration easier
  - Allows for the mixing of other modeling paradigms
- Object models address a large number of practical software problems, but there are domains that are not natural to model as discrete packets of encapsulated behavior

#### Non-Objects in an Object World

- Model paradigms have been conceived to address ways people like to think about domains.
  - Models are shaped by the paradigm
  - Resulting model conforms to the paradigm
  - Can be effectively implemented using the tools of that modeling style.
- There are bound to be parts of a domain that would be easier to express in another paradigm
  - If just a few, developers can manage a few awkward objects in an otherwise consistent model
- When major parts of the domain appear to belong to different paradigms
  - It is appealing to model each part in a paradigm that fits
  - When the interdependence is small, a subsystem in the other paradigm can be encapsulated.
  - Other times, the different aspects are more intertwined, such as when the interaction of the objects depends on some mathematical relationships
- Mixing paradigms allows developers to model particular concepts in the style that fits best
- Making a coherent model that spans paradigms is hard
  - When developers can't clearly see a coherent model embodied in the software, MODEL-DRIVEN DESIGN can go out the window

#### Sticking with MODEL-DRIVEN DESIGN When Mixing Paradigms

- When mixing paradigms, one common outcome is an application fractured in two
- Important to continue to think in terms of models
- Each paradigm should allow for the expressive implementation of a the model
- The most effective tool for holding the parts together is a robust UBIQUITOUS LANGUAGE that underlies the whole heterogeneous model.
- Although a MODEL-DRIVEN DESIGN does not have to be object oriented, it does depend on having an expressive implementation of the model constructs
- Four rules of thumb for mixing non-object elements into a predominantly objet-oriented system:
  - Don't fight the implementation paradigm
  - Lean on the ubiquitous language
  - Don't get hung up on UML
  - Be skeptical
- Before mixing paradigms, exhaust all option of the dominant paradigm

## Chapter 6: The Life Cycle of a Domain Object

## Chapter 7: Using the Language: An Extended Example

## Chapter 8: Breakthrough

## Chapter 9: Making Implicit Concepts Explicit

## Chapter 10: Supple Design

## Chapter 11: Applying Analysis Patterns

## Chapter 12 Relating Design Patters to the Model

## Chapter 13: Refactoring Toward Deeper Insight

## Chapter 14: Maintaining Model Integrity

## Chapter 15: Distillation

## Chapter 16: Large-Scale Structure

## Chapter 17: Bringing the Strategy Together
