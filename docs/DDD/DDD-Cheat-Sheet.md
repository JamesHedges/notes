# Domain Driven Design

## What is DDD

A technique for analyzing and defining the problem space that focuses on business model. How this is achieved

>From **Modern Software Engineering:**
>
>_Allowing our thinking, and our designs, to be guided by the problem domain helps us to identify paths that are more likely to be profitable in the long run._
>
>**_Domain-Driven Design_**
>
>_Domain-driven design is an approach to design where we aim to capture the core behaviors of our code in essence as simulations of the problem domain. The design of our system aims to accurately model the problem._
>
>_This approach includes a number of important, valuable ideas._
>
>_It allows us to reduce the chance of misunderstanding. We aim to create a “ubiquitous language” to express ideas in the problem domain. This is an agreed, accurate way of describing ideas in the problem domain, using words consistently, and with agreed meanings. We then apply this language to the way that we talk about the design of our systems too._
>
>_So if I am talking about my software and I say that this “Limit-order matched,” then that makes sense in terms of the code, where the concepts of “limit orders” and “matching” are clearly represented, and named LimitOrder and Match. These are precisely the same words that we use when describing the scenario in business terms with nontechnical people._
>
>_This ubiquitous language is effectively developed and refined through capturing requirements and the kind of high-level test cases that can act as “executable specifications for the behavior of the system” that can drive the development process._
>
>_DDD also introduced the concept of the “bounded context.” This is a part of a system that shares common concepts. For example, an order-management system probably has a different concept of “order” from a billing system, so these are two, distinct bounded contexts._
>
>_This is an extremely useful concept for helping to identify sensible modules or subsystems when designing our systems. The big advantage of using bounded contexts in this way is that they are naturally more loosely coupled in the real problem domain, so they are likely to guide us to create more loosely coupled systems._
>
>_We can use ideas like ubiquitous language and bounded context to guide the design of our systems. If we follow their lead, we tend to build better systems, and they help us to more clearly see the core, essential complexity of our system and differentiate that from the accidental complexity that often, otherwise, can obscure what our code is really attempting to do._
>
>_If we design our system so that it is a simulation of the problem domain, as far as we understand it, then an idea that is viewed as a small change from the perspective of the problem domain will also be a small step in the code. This is a nice property to have._
>
>_Domain-driven design is a powerful tool in creating better designs and provides a suite of organizing principles that can help guide our design efforts and encourages us to improve the modularity, cohesion, and separation of concerns in our code. At the same time, it leads us toward a coarse-grained organization of our code that is naturally more loosely coupled._

- ubiquitous language: the language of the system is taken from the language of the domain experts and is encapsulated in a context
- conversations with domain experts
- defining contexts

### From Building Microservices

- Just Enough Domain-Driven Design
  - As I introduced in Chapter 1, the primary mechanism we use for finding microservice boundaries is around the domain itself, making use of domain-driven design (DDD) to help create a model of our domain. Let’s now extend our understanding of how DDD works in the context of microservices.
- The desire to have our programs better represent the real world in which they will operate is not new. Object-oriented programming languages like Simula were developed to allow us to model real domains. But it takes more than program language capabilities for this idea to really take shape.
- Eric Evans’s Domain-Driven Design presented a series of important ideas that helped us better represent the problem domain in our programs. A full exploration of these ideas is outside the scope of this book, but there are some core concepts of DDD that are worth highlighting, including:
- Ubiquitous language
  - Defining and adopting a common language to be used in code and in describing the domain, to aid communication.
- Aggregate
  - A collection of objects that are managed as a single entity, typically referring to real-world concepts.
- Bounded context
  - An explicit boundary within a business domain that provides functionality to the wider system but that also hides complexity.

## Bounded Context

Bounded context is a course-grained area of responsibility at the top level of organization.

## DDD Concepts

### Entity

- Has an identity
- Unit of behavior

### Value Object

- Describe something
- Identity is defined by its properties
- Immutable

### Aggregates

- Defines a consistency boundary
- Cluster of entities and value objects
- One entity is the root
- Referenced only through the root entity
- Has invariants that enforce consistency

From DDD book chapter 6:

>Cluster the ENTITIES and VALUE OBJECTS into AGGREGATES and define boundaries around each. Choose one ENTITY to be the root of each AGGREGATE, and control all access to the objects inside the boundary through the root. Allow external objects to hold references to the root only. Transient references to internal members can be passed out for use within a single operation only. Because the root controls access, it cannot be blindsided by changes to the internals. This arrangement makes it practical to enforce all invariants for objects in the AGGREGATE and for the AGGREGATE as a whole in any state change.

#### Root Entity

- Consumers refer to directly

### Invariant

- Rule that must be maintained whenever data changes
- Involves relationships between members of an aggregate
- All invariants must be satisfied before a change is committed

### Services

#### Domain Service

- Operation or process (business activity) that does not have an identity or lifecycle in the domain
- stateless
- highly cohesive
- Often provide a single public method

Example domain service:

```csharp
public class PolicyRenewalProcesor 
{ 
    private readonly IAuditNotifier _notifier; 
    public PolicyRenewalProcessor(IAuditNotifier notifier) 
    { 
        _notifier = notifier; 
    } 
    public void Renew(Policy policy) 
    { 
        policy.Renew(); _notifier.ScheduleAuditFor(policy); 
    } 
}
```

#### Application Service

- integrate multiple models
- Can contain infrastructure dependencies
- Broad and shallow

Example application service

```csharp
public IPolicyService 
{ 
    void Renew(PolicyRenewalDTO renewal); 
    void Terminate(PolicyTerminationDTO termination); 
    void Write(QuoteDTO quote); 
} 

public PolicyService : Service 
{ 
    private readonly ILogger _logger; 

    public PolicyService(ILogger logger, IPolicyRepository policies) 
    { 
        _logger = logger; 
        _policies = policies; 
    } 
    
    public void Renew(PolicyRenewalDTO renewal) 
    { 
        var policy = _policies.Find(renewal.PolicyID); policy.Renew(); 
        var logMessage = string.Format( "Policy {0} was successfully renewed by {1}.", Policy.Number, renewal.RequestedBy); 
        _logger.Log(logMessage); 
    } 
}
```

### Repository

- Save and dispense aggregate
- May serve as an anti-corruption layer
