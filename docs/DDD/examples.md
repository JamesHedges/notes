
```csharp
public class PolicyRenewalProcesor 
{ 
    private readonly IAuditNotifier _notifier; 
    public PolicyRenewalProcessor(IAuditNotifier notifier) 
    { 
        _notifier = notifier; 
    } 
    public void Renew(Policy policy) 
    { 
        policy.Renew(); _notifier.ScheduleAuditFor(policy); 
    } 
}
```
