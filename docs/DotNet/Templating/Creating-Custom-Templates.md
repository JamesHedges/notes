# Creating Custom dotnet Templates

## Folder Structure

```text
|── dotnet-templates                                             <-- all templates group
    |
    |── templates-project                                        <-- Git project (working folder)
        |── TemplatePack.csproj                                  <-- package all the templates
        |── .gitignore
        |── .gitlab-ci.yml                                       <-- pipeline for packaging templates
        |
        |── templates
            |
            |── lambda-project-template
            |   |
            |   |── .template.config                             <--- required
            |   |   |
            |   |   |── application.ico
            |   |   |── ide.host.json
            |   |   └── template.json                            <--- Template rule definitions
            |   |
            |   |── src                                          <--- Folder for each project to template
            |   |   └── mytemplate.func_name
            |   |   ├── aws-lambda-tools-defaults.json
            |   |   ├── Function.cs
            |   |   ├── Readme.md
            |   |   └── mytemplate.func_name.csproj
            |   |
            |   |── test                                         <--- Folder for test projects (mirrors src folder)
            |   |   |── mytemplate.func_name.Tests
            |   |   ├── FunctionTest.cs
            |   |   └── mytemplate.func_name.Tests.csproj
            |   |
            |   |── tf                                           <--- Terraform files
            |   |   ├── constants.tf
            |   |   ├── data.tf
            |   |   ├── dev
            │   |   |   └── us-east-1
            │   |   |       ├── backend.tf
            │   |   |       ├── main.tf
            │   |   |       ├── provider.tf
            │   |   |       └── variables.tf
            |   |   ├── lambda.tf
            |   |   ├── tagging.tf
            |   |   └── variables.tf
            |   |
            |   |__ .gitignore
            |   |
            |   |__ .gitlab-ci.yml
            |   |
            |   |__ Dockerfile
            |   |
            |   |__ ReadMe.md
            |── lambda-item-template
                |
                |── .template.config                             <--- required
                    |
                    |── application.ico
                    |── ide.host.json
                    └── template.json                            <--- Template rule definitions
             
```

## Template File

### Example

```json
{
    "$schema": "http://json.schemastore.org/template",
    "author": "jhedges",
    "classifications": ["cloud", "service", "AWS", "lambda"],
    "name": "application.lambda.01",
    "shortName": "mylambda",
    "defaultName": "my-lambda",
    "preferNameDirectory": true,
    "identity": "application.lambda.CSharp",
    "tags": {
      "language": "C#",
      "type": "project"
    },
    "sourceName": "application.lambda",
    "symbols": {
        "func_name": {
            "description": "",
            "type": "parameter",
            "replaces": "mytemplate.func_name"
        },
        "account_name": {
            "description": "",
            "type": "parameter",
            "defaultValue": "clinicalnetworks",
            "replaces": "mytemplate.account_name"
        },
        "team_name": {
            "description": "",
            "type": "parameter",
            "defaultValue": "application",
            "replaces": "mytemplate.team_name"
        },
        "business_domain": {
            "description": "",
            "type": "parameter",
            "replaces": "mytemplate.business_domain"
        },
        "namespace": {
            "description": "",
            "type": "parameter",
            "replaces": "mytemplate.domain_namespace"
        },
        "app_id": {
            "description": "",
            "type": "generated",
            "generator": "constant",
            "parameters": {
                "value": "1234"
            },
            "replaces": "mytemplate.app_id"
        },
        "cost_center": {
            "description": "",
            "type": "generated",
            "generator": "constant",
            "parameters": {
                "value": "999999"
            },
            "replaces": "mytemplate.cost_center"
        },
        "module_domain": {
            "description": "",
            "type": "generated",
            "generator": "casing",
            "parameters": {
                "source": "business_domain",
                "toLower": true
            },
            "replaces": "mytemplate.module_domain"
        },
        "module_name": { 

            "description": "",
            "type": "generated",
            "generator": "casing",
            "parameters": {
                "source": "func_name",
                "toLower": true
            },
            "replaces": "mytemplate.module_name"
        },
        "team_stage_domain": {
            "description": "",
            "type": "generated",
            "generator": "join",
            "replaces": "mytemplate.team_stage_domain",
            "parameters": {
                "symbols": [
                    {
                        "type": "ref",
                        "value": "team_name"
                    },
                    {
                        "type": "const",
                        "value": "dev"
                    },
                    {
                        "type": "ref",
                        "value": "module_domain"
                    }
                ],
                "separator": "-"
            }
        },
        "terraform-state-locking-table": {
            "description": "",
            "type": "generated",
            "generator": "join",
            "replaces": "mytemplate.terraform_state_locking_table",
            "parameters": {
                "symbols": [
                    {
                        "type": "ref",
                        "value": "team_name"
                    },
                    {
                        "type": "const",
                        "value": "state"
                    },
                    {
                        "type": "const",
                        "value": "locking"
                    },
                    {
                        "type": "const",
                        "value": "table"
                    }
                ],
                "separator": "-"
            }
        },
        "terraform-state-bucket": {
            "description": "",
            "type": "generated",
            "generator": "join",
            "replaces": "mytemplate.terraform_state_bucket",
            "parameters": {
                "symbols": [
                    {
                        "type": "ref",
                        "value": "team_name"
                    },
                    {
                        "type": "const",
                        "value": "test"
                    },
                    {
                        "type": "const",
                        "value": "dev"
                    },
                    {
                        "type": "ref",
                        "value": "account_name"
                    },
                    {
                        "type": "const",
                        "value": "terraform"
                    },
                    {
                        "type": "const",
                        "value": "state"
                    }
                ],
                "separator": "-"
            }
        },
        "terraform-bucket-key": {
            "description": "",
            "type": "generated",
            "generator": "join",
            "replaces": "mytemplate.terraform_bucket_key",
            "parameters": {

                "symbols": [
                    {
                        "type": "ref",
                        "value": "team_name"
                    },
                    {
                        "type": "ref",
                        "value": "module_domain"
                    },
                    {
                        "type": "ref",
                        "value": "module_name"
                    },
                    {
                        "type": "const",
                        "value": "dev"
                    },
                    {
                        "type": "const",
                        "value": "us-east-1"
                    },
                    {
                        "type": "const",
                        "value": "terraform.tfstate"
                    }
                ],
                "separator": "/"
            }
        },
        "application.lambda": {
            "type": "bind",
            "binding": "name",
            "replaces": "application.lambda"
        }
    }
}
```

## Packaging

- [Tutorial: Create a template package](https://docs.microsoft.com/en-us/dotnet/core/tutorials/cli-templates-create-template-package)

### Create .csproj

In the template working folder

## dotnet Commands

```powershell
# Install from template dir
dotnet new --install ./

# Uninstall from template dir
dotnet new --uninstall ./

# List installed templates
dotnet new -l
```

## References

- [dotnet Templating](https://github.com/dotnet/templating/wiki)
- [How to create your own templates for dotnet new](https://devblogs.microsoft.com/dotnet/how-to-create-your-own-templates-for-dotnet-new/)
- [Custom templates for dotnet new](https://docs.microsoft.com/en-us/dotnet/core/tools/custom-templates)
