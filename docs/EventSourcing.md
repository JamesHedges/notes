# Event Sourcing

## Events vs Commands

- Commands
  - Request asked to happen
  - Can be rejected
- Events
  - Something that has happened
  - Cannot be rejected

For conceptual clarity, they are represented separately. They are both messages, but modeled explicitly to enforce the concepts. This also ensures loose coupling.
