# Functional Programming Cheat Sheet

## Pure Function

Pure functions are:

- Idempotent
- Stateless
- Side-affect free
