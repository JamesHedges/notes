<!-- omit in toc -->
# Git Cheat Sheet

<!-- omit in toc -->
## Index

## Syntax Quick Reference

| Action   | Command          | Description                     | Reference             |
| :------- | :--------------- | :------------------------------ | :-------------------- |
| Stashing | `git stash push` | stash changed, tracked files    | [Stashing](#stashing) |
|          | `git stash pop`  | reapply stash from top of stack |                       |

## Stashing

Stash will save change to a stack and can be later applied.

| Command                               | Description                                     |
| :------------------------------------ | :---------------------------------------------- |
| ~~`git stash save`~~                  | DEPRECATED use Push                             |
| `git stash push`                      | push to a stash (untracked file not stashed)    |
| `git stash push -k`                   | push to a stash and keep the index              |
| `git stash push -u`                   | push to a stash and include untraced files      |
| `git stash push -m "add description"` | push to a stash with description                |
| `git stash list`                      | List the stashes                                |
| `git stash pop`                       | reapply and remove the latest stashed changes   |
| `git stash apply 1`                   | applies stash @1                                |
| `git stash branch _new_feature_`      | Applies stash into the new branch _new_feature_ |
| `git stash drop 1`                    | Removes stash @1                                |

## Branching

| Command                              | Description                                  |
| :----------------------------------- | :------------------------------------------- |
| `git branch`                         | list all branches                            |
| `git branch --list`                  | list all branches                            |
| `git brach --no-merge`               | list branches not merged to master           |
| `git branch new_feature`             | creates new branch named _new_feature_       |
| `git checkout <new_feature>`         | switches to branch _new_feature_             |
| `git checkout -b <new_feature>`      | create and switched to _new_feature_ branch  |
| `git reset --hard <commit_id>`       | move branch to a different parent            |
| `git branch -D <branch_name>`        | Delete branch                                |
| `git branch -u <remote_branch>`      | Start tracking the remote_branch             |
| `git branch --unset <remote_branch>` | Stop tracking the remote_branch              |
| `git branch --show-current`          | Show the current branch                      |
| `git pull origin other-branch`       | merge from remote branch when no association |

### Rename

```git
git checkout <branch_name>
git branch -m <new_branch_name>
```

## Log

| Command                   | Description                    |
| :------------------------ | :----------------------------- |
| `git log <remote/branch>` | Show log for the remote branch |
| ``                        |                                |

### Pretty

```git
git log --graph --pretty=oneline --abbrev-commit
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```

### Log Paging

- `j` line down
- `k` line up
- `space` page down
- `b` page up

## Remote

| Command                                   | Description         |
| :---------------------------------------- | :------------------ |
| `git remote -v`                           | Show remotes        |
| `git remote`                              | List remotes        |
| `git remote show`                         | List remotes        |
| `git remote show <remote_name>`           | Show remote details |
| `git remote add <remote_name> <url_path>` | Create a new remote |
| ``                                        |                     |

## Techniques

### List Remote Branches

`git ls-remote --heads origin`

### Associate Local Branch with a Remote Repository

This is how you can associate an existing local repository with a remote repository

1. Make sure your local repository has a clean status
1. Create the remote repository and get the SSH or Https clone string
1. On the local repository, execute: `git remote add origin <SSH/HTTPS string>`
1. Set the default association with master (or whichever branch desired): `git branch --set-upstream-to origin/master`

Example

```powershell
git remote add origin git@gitlab.net:commonwell/devops/new-repo.git
git push -u origin master
```

### Pull Remote Branch to Local

To get a remote branch into a local branch with tracking,

```powershell
# Check remote exists
git branch -r
# Check local branch is bound
git remote -v

# Get the remote branch
# Quickest technique
git checkout --track origin/remoteBranch
# To name local branch
git checkout --track -b localBranch origin/remoteBranch

# Long Handed Technique
git fetch origin remoteBranchName
git checkout -b localBranch origin/remoteBranchName
git pull origin remoteBranchName
```

#### With One Remote

Jakub's answer actually improves on this. With Git versions ≥ 1.6.6, with only one remote, you can do:

```powershell
git fetch
git checkout test
````

As user masukomi points out in a comment, git checkout test will NOT work in modern git if you have multiple remotes. In this case use

```powershell
git checkout -b test <name of remote>/test
or the shorthand

git checkout -t <name of remote>/test
```

#### With >1 Remotes

Before you can start working locally on a remote branch, you need to fetch it as called out in answers below.

To fetch a branch, you simply need to:

```powershell
git fetch origin
```
This will fetch all of the remote branches for you. You can see the branches available for checkout with:

```powershell
git branch -v -a
```

With the remote branches in hand, you now need to check out the branch you are interested in, giving you a local working copy:

```powershell
git checkout -b test origin/test
```

#### Checkout Remote References

- [Git pull remote branch to local](https://developpaper.com/git-pull-remote-branch-to-local/)
- [Git fetch remote branch](https://stackoverflow.com/questions/9537392/git-fetch-remote-branch)
- [How do I check out a remote Git branch?](https://stackoverflow.com/questions/1783405/how-do-i-check-out-a-remote-git-branch)

### Pull Branch - Overwrite Local

Force git pull from [How to Use Git Force Pull Properly](https://itsyndicate.org/blog/how-to-use-git-force-pull-properly/)

```powershell
git fetch origin <branch>
git reset --hard origin/<branch>
```

### Associate Local Repository with a Second Remote

Use this to associate a local repository with the it was forked from.

1. In the local repository, checkout the top branch: `git checkout develop`
1. Add the upstream remote: `git remote add upstream <SSH/HTTPS string>`

### Undo a Commit

[How to undo a Git commit that was not pushed](https://bytefreaks.net/programming-2/how-to-undo-a-git-commit-that-was-not-pushed#:~:text=%20To%20undo%20a%20Git%20commit%20that%20was,the%20commit%20and%20lose%20all%20changes%20More%20)

>These undo commands are for local repository.

#### Undo Commit and Keep All Files Staged

```powershell
# 1. Undo and Keep All Files Staged
git reset --soft HEAD~

# 2. Undo Commit and Unstage All Files
git reset HEAD~
#or
git reset --mixed HEAD~

# 3. Undo the commit and Completely Remove All Changes
git reset --hard HEAD~
```

#### Undo Commit and Unstage Files

```powershell
git reset Head!
   # OR
git reset --mixed Head~```

#### Undo the Commit and Completely Remove All Changes

```powershell
git reset --hard HEAD~
```

### Pushing to Remote

#### Force Push to Remote

> **Use with Extreme Caution**  
> ALL subsequent commits after the one being forced will be destroyed!

##### Reasons to Force Push

- Local version is better than the remote version
- Remote version went wrong and needs repair
- Version have diverged and merging is undesirable

##### Executing Forced Push

Execute a push with the force option to override all commits since the last local one. Once this is done, other collaborators will need to clean out their commits by doing a reset.

```powershell
git push -f
# or
git push --force

# Collaborators will need to reset

git reset --hard origin/master
```

#### Push Size Limit

Large upload size when pushing a commit may result in the following errors:

```text
$ push -u origin main
Enumerating objects: 142, done.
Counting objects: 100% (142/142), done.
Delta compression using up to 12 threads
Compressing objects: 100% (137/137), done.
error: RPC failed; HTTP 400 curl 22 The requested URL returned error: 400
send-pack: unexpected disconnect while reading sideband packet
Writing objects: 100% (142/142), 55.52 MiB | 33.06 MiB/s, done.
Total 142 (delta 11), reused 0 (delta 0), pack-reused 0
fatal: the remote end hung up unexpectedly
```

Execute the following command to make the buffer size larger:

`git config http.postBuffer 524288000`

### Identify Merged Branches

We may need to identify what branches have been merged into another. This is useful for:

- Knowing what features have been incorporated
- Cleanup after merging many features

The branch command:

- Use current branch by default
- Documentation say "Branches whose tips are reachable form the specified commit (HEAD if not specified)"
- Branch tip is in the history of the specified commit
- Can specify other branch names or commits

```powershell
# Show branches merged into current branch
git branch --merged

# Show branches not merged into current branch
git branch --no-merged

# show remote branches merged with current branch
git branch -r --merged

# show commits since
git branch --merged HEAD
git branch --merged <branch name>
git branch --merged origin/<branch name>
git branch --merged <commit ID>
```

### Deleting Local and Remote Branches

#### Deleting Local Branch

- Cannot delete the current branch
- Branch must be merged into current branch
- Use -D to force delete

```powershell
# Must be on different branch
git checkout <parent branch>
# check to see if delete branch has been merged
git branch --merged
# Branch to delete must appear in results
# If in merged results
git branch -d <delete branch name>
# Else to force the delete without a merger
git branch -D <delete branch name>
```

#### Deleting Remote Branch

> Colon syntax say to push 'nothing' to the remote branch

```powershell
# Delete remote branch
git push origin <local>:<remote
# e.g. (local is the default)
git push origin :delete_branch

# newer version, git v1.7.0+
git push --delete origin delete_branch
# since v2.8.0+
git push -d origin delete_branch
```

### Pruning Branches

- Delete all stale remote-tracking branches
- Remote-tracking branches, not remote branches
- Stale branch: a remote-tracking Branch that no longer tracks anything because the actual branch in the remote repository has been deleted.

#### Remote Branches

- Branch on the remote repository (bugfix)
- Local snapshot of the remote branch (origin/bugfix)
- Local branch, tracking the remote branch (bugfix). This branch sits between the remote and local branches

#### Pruning Stale Branches

- Deleting a remote prunes the remote-tracking branch automatically
- Necessary when collaborators delete branches
- Fetch does not automatically prune - tracking branch remains
- Can alway prune before fetch by setting config:  
`git config --global fetch.prune true`

```powershell
# Delete stale remote-tracking branches
git remote prune origin
# to see what the command will do
git remote prune origin --dry-run
# to fetch and prune in one step
git fetch --prune
git fetch -p
```

> **git prune**
>
> - Prune all unreachable objects
> - Do not need to use!
> - Is part of garbage collection `git gc`

### Tags

- Tags allow marking points in history as important
- A named reference to a commit
- Most often used to mark releases (v1.0, v1.1, v2.0, etc.)
- Can mark key feature or changes
- Can mark points for team discussion
- Two type of tags

```powershell
# Lightweight (name and commit #):  
git tag <name> <commit number
# Annotated (includes a message):  
git tag -a <annotation> -m <name> <commit number>
```

#### List Tags

```powershell
# List Tags
git tag
git tag -l
git tag --list
# see annotations
git tag -l -n
git tag -ln
# filter name with wildcard (*)
git tag -l "v2*"
```

#### Using Tags

```powershell
# Work with tags (like SHAs)
git show v1.1
git diff v1.0..v1.1
```

#### Deleting Tags

```powershell
# Delete a tag
git tag --delete <name>
git tag -d <name>
```

#### Remote Tags

- Like branches, tags are local unless shared to a remote
- git push does not transfer tags
- Tags must be explicitly transferred
- git fetch automatically retrieves shared tags

```powershell
# Push a single tag to a remote server - and only the tag
git push origin v1.1
# PUsh all tags to a remote server
git push origin --tags
# Fetch commits and tags
git fetch
# Fetch only tags (with necessary commits)
git fetch --tags

# Delete remote tags like remote branches
git push origin :v1.1
git push --delete origin v1.1
git push -d origin v1.1
```

#### Checking Out Tags

- Tags are not branches
- Tags can be checked out, just like any commit
- Checkout into a new branch: `git checkout -b new_branch v1.1`
- Checkout tag: `git checkout v1.1`
  - Checking out a commit puts the local repository in a "detached HEAD state
  - LIke being on an unnamed branch
  - New commits will not belong to any branch
  - Detached commits wi be garbage collected (~2 weeks)
  - Can reattach by
  
```powershell
        # Tag the commit (HEAD detached)
        git tag temp

        # Create a branch (HEAD detached)
        git branch temp_branch

        # Create a branch and reattach HEAD
        git checkout -b temp_branch
```

### Interactive Mode

- Stage changes interactively
- Allows staging portions of changed files
- Helps to make smaller, focused commits
- Feature of many Git GUI tools
- Enter interactive mode:

```powershell
    git add --interactive
    # or
    git add -i
```

#### Patch Mode

- Allows staging portions of a changed file
- "Hunk": an area where two files differ
- Hunks can be staged, skipped, or split into smaller hunks

![Patch](./Patch.png)

#### Split Hunks

- Hunks can contain multiple changes
- Tell Git to try to split a hunk further
- Requires one or more unchanged lines between changes

![Patch Split 1](./PatchSplit1.png)
![Patch Split 2](./PatchSplit2.png)

#### Edit Hunk

- Can edit a hunk manually
- Most useful when a hunk cannot be split automatically
- Diff-style line prefixes: +, -, #, space

#### Commands with Patch Option

Patch can also be used with these commands:

```powershell
git add --patch
git add -p

git stash -p
git reset -p
git checkout -p
git commit -p
```

### Cherry Picking

#### Commits

- Apply the changes from one or more existing commits
- Each existing commit is recorded as a new commit on the current branch
- Conceptually similar to copy-paste
- New commits have different SHAs
- Cannot cherry pick a merge commit
- Use --edit / -e to edit the commit message
- Can result in conflicts which must be resolved

```powershell
# Pick a specific commit
git cherry-pick <commit SHA>
# Pick a range of commits
git cherry-pick <start commit SHA>..<end commit SHA>
```

##### Resolve Conflicts

### Patches

#### Create Patch

- Share changes via files
- Useful when changes are not ready for a public branch
- Useful when collaborators do not share a remote
- Discussion, review, approval process

```powershell
git diff <from-commit> <to-commit> > output.diff
```

#### Apply Patch

- Apply changes in a diff patch file to the working directory
- Makes changes, but not commits
- No commit history transferred
- Working directory must be in a state to accept the changes (no conflicts)

```powershell
git apply output.diff
```

#### Create Formatted Patches

- Export each commit in a Unit mailbox format
- Useful for email distribution of changes
- Includes commit messages
- One commit per file

```powershell
# Export all commits in the range
git format-patch <from SHA>..<to SHA>

# Export all commits on current branch
@ which are not in master branch
git format-patch master

# Export a single commit
git format-patch -1 <commit SHA>

# Put patch files into a directory
git format-patch master -o feature

# Output patches as a single file
git format-patch <from SHA>..<to SHA> --stdout > feature.patch
```

#### Apply Formatted Patch

- Extract author, commit message, and changes from a mailbox message and apply them to th4e current branch
- Similar to cherry-picking: same changes, different SHAs
- Commit history is transferred
- Applied in sequential order

```powershell
# apply single patch
git am feaure/0001-some-name.patch

# Apply all patches in a directory
git am feature/*.patch
```

### Rebasing

#### Rebase Commits

- Take commits from a branch and replay them at the end of another branch
- Useful to integrates recent commit without merging
- Maintains a cleaner, more linear project history
- Ensures topic branch commits apply cleanly

```powershell
# Rebase current branch to tip of master
git rebase master

# Rebase new_feature to tip of master
git rebase master new_feature

# Useful for visualizing branches
git log --graph --all --decorate --oneline

# Return commit where topic branch diverges
git merge-base master new_feature
```

#### Merge vs. Rebase

- Two way to incorporate changes from one branch into another branch
- Similar ends, but the means are different

##### Merge

- Adds a merge commit
- Nondestructive
- Complete record of what happened and when
- Easy to undo
- Log can become cluttered, non-linear

##### Rebase

- No additional merge commit
- Destructive: SHA changes, commits are rewritten
- Tricky to undo
- Logs are cleaner, more linear
- **Golden Rule of Rebasing: Thou shalt not rebase a public branch**
  - Rebase abandons existing, shared commits and create new, similar commits instead
  - Collaborators would see the project history vanish
  - Getting all collaborators back in sync can be a nightmare

##### How to Choose

- **Merge** to allow commits to stand out or to be clearly grouped
- **Merge** to bring large topic branches back into master
- **Rebase** to add minor commits in master to a topic branch
- **Rebase** to move commits from one branch to another
- **Merge** anytime the topic branch is already public and being used by others (The Golden Rule of Rebasing)

### Handle Rebase Conflicts

- Rebasing creates new commits on existing code
- May conflict with existing code
- Git pauses rebase before each conflicting commit
- Similar to resolving merge conflicts
- When conflicts occur in a rebase, the rebase will pause. Either:
  - Fix the conflicts and issue `git rebase --continue`.
  - Skip the commit and accept the topic branch's commit with `git rebase --skip`
  - Stop the rebase with `git rebase --abort`

### Rebase Onto Other Branches

- use the onto option: `git rebase --onto [newbase] [upstream] [branch]`
  - take the commits from [branch]
  - up to where they diverged from [upstream]
  - and replay onto [newbase]
- Effectively moves the [branch] branch to the tip of the [newbase] branch

### Undo a Rebase

- Can undo simple rebases
- Rebase is destructive
- SHAs, commit messages, change sets, and more
- Undoing complex rebases may lose data
- Can use the git temporary variable ORIG_HEAD to reset
  - Only works if ORIG_HEAD has not changed again
  - `git reset --hard ORG_HEAD`
- If the ORIG_HEAD has changed, can move back (using terms from above rebase)
  - `git rebase --onto [origbase SHA] [newbase] [branch]`
  - [origbase SHA] is the SHA where the original commit diverged from

### Interactive Rebasing

Allows user to give input on the rebase tasks

- Chance to modify commits as they are being replayed
- Opens the git-rebase-todo file for editing
- Can reorder or skip commits
- Can edit commit contents

#### Interactive Rebase Choices

- pick commit to use
- drop commit to skip
- reword commit message
- edit contents of commit
- squash (more later)
- fixup (more later)
- exec runs a command from the shell
- To enter interactive rebase, add _-i_ option to rebase command: `git rebase -i master new_feature`

#### Interactive Rebase Examples

```powershell
# Rebase last 3 commits onto the same branch
# but with the opportunity to modify them
# Allows editing commits, but is destructive! Useful for cleaning up local commits before pushing to remote. DO NOT use on public shared repository where other collaborators are working. Also used in squashing too (next)
git rebase -i HEAD~3
```

### Rebasing Stacked Branches

See [Working with stacked branches in Git is easier with --update-refs](https://andrewlock.net/working-with-stacked-branches-in-git-is-easier-with-update-refs/)

#### What are Stacked Branches

Stacked branches are created by a series of branches that are based on each other. That is branch 2 is based on branch 1, branch 3 is based on branch 2, etc. You will end up with a log that looks like this:

### Squash Commits

- Fold two or more commits into one
- squash: combine change sets, concatenate messages
  - must have a pick above it int todo list
- fixup: combine change sets, discard this message
- Uses first author in the commit series
- Use `git rebase -i HEAD~3`

#### Squash local working directory before committing without rebase

```powershell
git reset --soft HEAD~<_# commits_>
git commit -m <_your new commit message_>
```

#### To squash a remote branch

Taken from [Stackoverflow](https://stackoverflow.com/questions/5667884/how-to-squash-commits-in-git-after-they-have-been-pushed)

```powershell
git rebase -i origin/<_branch name_>~<_# commits_> <_branch name_>
git push origin +<_branch name_>
```

> The first command will put you in the editor, pick the first entry and squash the remaining. See [squashing](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History#_squashing)

#### Pull Rebase

- Fetch from remote, then rebase instead of merging
- Keeps history cleaner by reducing merge commits
- Only use on local commits not shared to a remote

```powershell
git pull --rebase
git pull -r

# preserve local merge commits and not flatten them
git pull --rebase=preserve

# interactive mode
git pull --rebase=interactive
```

### Rewrite Commit Message

To rewrite the commit message, use `get -amend`

### WorkTree

You can create multiple working trees with different branches checked out. This allows work on two different branches without having to switch or create duplicate files.

```powershell

# Create new work tree - note that other-branch must exist
git worktree add ../NewTree other-branch

# Create new work tree based on remote branch
git worktree add ../NewTree origin/master -b other-branch

# Remove working tree - add -force if there are uncommitted changes
git worktree remove ../NewTree
```

If you intend to have long running branches, may want to consider local file structure that has empty base folder. E.g.

```text
src
 │
 └─ MyApp
     │
     └─ _              # Main working folder (named '_' to keep at top)
     |   |
     |   └─ .git
     |
     └─ other-branch   # Other Working Folder
```

See [Working on two git branches at once with git worktree](https://andrewlock.net/working-on-two-git-branches-at-once-with-git-worktree/)

#### How to checkout a file from a previous commit

If you want to restore a file from a previous commit, you can use the git checkout command.

##### Step-by-step guide

1. **Identify the commit SHA:** First, find the commit SHA (a unique identifying code for each commit) of the version of the file you want to restore. You can find this by using:

Terminal

```bash
git log -- path/to/file
```

This command will show a list of commits that affected the specified file. Note the SHA of the commit from which you want to restore the file. This will show you the commit history of the specified file.

2. **Checkout the file:** Once you have the commit SHA, use the following command to checkout the file:

Terminal

```bash
git checkout <commit-sha> -- path/to/file
```

Replace \<commit-sha> with the actual SHA of the commit and path/to/file with the actual path to the file.

This command will restore the file to its state at the specified commit without changing the current branch or affecting other files.

From [Checking out files from previous Git commits](https://graphite.dev/guides/checkout-file-from-prev-commit)

## Track Down Problems

### Log Options

- Log is the primary interface to Git
- Log has many options
- Sorting filtering, output formatting

```powershell
#To see options:
gtit help log

# List commits as patches (diffs)
git log -p
git log -patch

# List edits to lines 100-150 in filename.txt
git log -L 100,150:filename.txt

```

### Repository Age

Get first and last commits.

```powershell
git log --pretty="format: %cd %h %s" --date=short --reverse | Select-Object -First 1 -Last 1
```

### Who Committed

Who mad how many commits?

```powershell
git shortlog --after='2013-12-31' --before='2019-07-04' -sn --all

<#
Results
183  Steve Roth
 94  Andrew Williams
 52  Keith J Moseley
 17  AndrewWilliams
 13  Michael Noble
 10  SteveRoth
  7  Tim Song
  4  timsong
  3  Keith Moseley
  2  Christopher P. Daigle
  2  Harish Venugopal
  2  Keith M
  2  NAMCK\eaf5a7b
  2  unknown
  1  Zhang
  #>
```

### Blame

- Browse annotated file
- Determine who changed which lines in a file and why
- Useful for probing the history behind a file's contents
- Useful for identifying which commit introduced a bug

```powershell
# Annotate file with commit details
git blame filename.txt

# Ignoore whitespace changes
git blame -w filename.txt

# Annotate lines 100-150
git blame -L 100,150 filename.txt

# Annotate lines with incrementer
git blame -L 100,+50 filename.txt

# Annotate file at revision d9dba0
git blame d9dba0 filename.txt
git blame d9dba0 -- filename.txt

# Add a global alias for "praise"
git config --global alias.praise blame

# Similar to blame, different output format (don't alias this, actual command)
git annotate filename.txt
```

### Bisect

- Find the commit that introduced a bug or regression
- Mak last good revision and first bad revision
- Resets code to mid-point
- Mak as good or bad revision
- Repeat

```powershell
# Start the bisect
git bisect start
# Set the first know bad commit/branch/tag
git bisect bad <treeish>
# Set the last known good commit/branch/tag
git bisect good <treeish>
# Resets the working directory to HEAD
git bisect reset
```

### Remove Untracked Items from Repository

#### Remove File

Set a file to not be tracked

```git rm --cached [file names]```

### Remove Folder

To remove a folder from tracking (removes the folder .vs and all of its contents)

```powershell
git rm -rf --cached .vs/\*
git add .
git commit -m "removing a directory"
git push
```

## Submodules

## Remove Submodule

```powershell
git submodule deinit <asubmodule>    
git rm <asubmodule>
```

- [What is the current way to remove a git submodule?](https://stackoverflow.com/questions/29850029/what-is-the-current-way-to-remove-a-git-submodule)

## User Profile

Create a user profile

## SSH Keys

Both GitHub and GitLab can use SSH keys to authenticate users. The key needs to be set-up on the client and registered with the Git host.

You can Create the key using PowerShell following these instructions [Generate SSH Keys](./Powershell/PowerShell-Cheat-Sheet#ssh-keys)

### Register SSH Key with GitLab

1. Switch to the .ssh directory: `cd ~/.ssh`
1. Copy the public key - NOT the private key! `cat id_rsa.pub | clip`
1. Register with GitLab
    1. Navigate to your personal GitLab settings
    1. Select **SSH Key** from menu on right
    1. Paste your key into the **Key** text area
    1. The **Title** should be populated with the email entered when creating the key. If not, enter it in the **Title** textbox
    1. Click the **Add key** button
1. Verify key by executing `ssh -T git@gitlab.com` in your console and following the prompts

    > If prompted with  
    > _The authenticity of host 'gitlab.com (35.231.145.151)' can't be established.
    > ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
    > Are you sure you want to continue connecting (yes/no)?_  
    > Then enter `yes` to associate the key

1. If the keys are rejected **Corrupted MAC on input** error, then  
Execute: `ssh -m hmac-sha2-256 git@gitlab.com`

For more details, see [GitLab and SSH keys](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)

## Less

- Program git uses to show files / content
- [Less Help](https://ss64.com/bash/less.html)

## Install Using Chocolatey

Use the command ```choco install -y git```

## Configure VS Code

### Git Editor

Add VS Code as the git editor and then verify. The verification will open VS Code with the git global configurations similar to what is shown below in the [.gitconfig settings section](#gitconfig-settings).
```powershell
git config --global core.editor "code --wait"

# Verify
git config --global -e
```

#### .gitconfig Settings

Settings for the .gitconfig file found in C:/Users/\<username\>

```config
# This is Git's per-user configuration file.
[user]
# Please adapt and uncomment the following lines:
  name = John Doe
  email = John.Doe@outlook.com
[core]
  editor = code --wait
[diff]
  tool = default-difftool
[difftool "default-difftool"]
  cmd = code --wait --diff $LOCAL $REMOTE
[difftool]
  prompt = true
[difftool "default-difftool"]
  cmd = code --wait --diff $LOCAL $REMOTE
[merge]
  tool = vscode
[mergetool]
  prompt = true
[mergetool "vscode"]
  cmd = code --wait $MERGED
```

See [VS Code as Git Editor](https://code.visualstudio.com/docs/sourcecontrol/overview#_vs-code-as-git-editor)

### VS Code Extension

Install the VS Code extension GitLens
