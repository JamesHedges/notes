# GitHub Copilot

## VSCode configuration

### Extensions

- [GitHub Copilot](https://marketplace.visualstudio.com/items?itemName=GitHub.copilot)
- [GitHub Copilot Chat](https://marketplace.visualstudio.com/items?itemName=GitHub.copilot-chat)

## Observations

- Works well when
    - Doing small things
    - Helping with syntax
    - Scaffolding
- Not so good at
    - Generating anything larger than a function
    - Implementing 3rd party API / Libraries
    - Complex constructs

## Workspaces

The `@workspace` command in GitHub Copilot is a powerful feature that allows you to ask questions about your entire codebase within Visual Studio Code. Here are some key uses:

1. **Finding Existing Code**: You can locate specific code snippets or configurations. For example, you might ask `@workspace where is the database connection string configured?` to find where and how the database connection is set up[^1].

2. **Planning Code Edits**: It helps in making plans for complex code changes. For instance, `@workspace add date validation to #selection` will guide you on how to apply existing date validation to the selected code[^1].

3. **Explaining Code Concepts**: You can get explanations of higher-level concepts in your codebase. For example, `@workspace how is authentication implemented?` will provide an overview of the authentication flow and reference relevant code[^1].

4. **Contextual Understanding**: `@workspace` uses the context of your entire project, including all files in the workspace, directory structure, and symbols, to provide accurate and relevant answers[^1][^2].

This feature makes GitHub Copilot a more effective assistant by leveraging the full context of your project to provide detailed and accurate responses.

Is there a specific task you're trying to accomplish with `@workspace`?

Setting up a workspace in GitHub Copilot is straightforward. Here are the steps to get you started:

1. **Prerequisites**:
   - A GitHub account.
   - A GitHub Copilot subscription.
   - A compatible IDE, such as Visual Studio Code[[^3].

2. **Getting Started**:
   - **Option 1**: Open an issue in a GitHub repository and click the “Open in Workspace” button. This will start a new Copilot Workspace session, pre-seeded with the issue as the task, allowing you to iterate on the spec, plan, and implementation[[^3].
   - **Option 2**: Visit the Copilot Workspace dashboard and start a new session by clicking the “New Session” button. You can search for a repository and define an ad-hoc task for it[^3].

3. **Using the Workspace**:
   - **Iterate on a Pull Request**: Click the “Open in Workspace” button, define the change you’d like to make, and implement them. Changes suggested by GitHub Copilot will be reflected in the workspace[^3].
   - **Codespaces Integration**: Open a workspace session in a Codespace by clicking the “Open in Codespace” button. Your workspace edits will sync with the Codespace, allowing for larger edits and debugging[^3].

4. **Additional Features**:
   - **File Explorer**: Use the file explorer within the browser to navigate your project.
   - **Integrated Terminal**: Compile code, manage packages, and customize your environment directly from the terminal[^3].

These steps should help you set up and start using a workspace in GitHub Copilot effectively. If you have any specific questions or run into issues, feel free to ask!

## References

[^1]: [Workspace Context](https://code.visualstudio.com/docs/copilot/workspace-context)
[^2]: [GitHub Copilot's Workspace Deep Dive](https://learn.microsoft.com/en-us/shows/visual-studio-code/github-copilots-workspace-deep-dive)
[^3]: [Unlock the Power of GitHub Copilot Workspaces: A Beginner’s Guide](https://techcommunity.microsoft.com/blog/educatordeveloperblog/unlock-the-power-of-github-copilot-workspaces-a-beginner%E2%80%99s-guide/4210179)
