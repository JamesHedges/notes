<!-- omit in toc -->
# HttpClient Cheat Sheet

- [HttpClient in .Net Framework](#httpclient-in-net-framework)
  - [Using HttpClient](#using-httpclient)
  - [HttpClient Class Structure](#httpclient-class-structure)
      - [HttpRequestMessage](#httprequestmessage)
      - [Proxying Handlers](#proxying-handlers)

## HttpClient in .Net Framework

### Using HttpClient

- Do not dispose an instance.
  - Intended to live for as long as an application needs to make HTTP requests
  - It is thread safe
- Cannot change these properties once first request has been made
  - public Uri BaseAddress
  - public TimeSpan Timeout
  - public long MaxResponseContentBufferSize
- Do NOT use _.Result_ on async calls
- Completed requests do not throw exceptions
  - _IsSuccessStatusCode_ set to true if response is 2xx
  - Not all status codes warrant an exception and may be passed on to the application
  - _EnsureSuccessStatusCode_ method can be used to trigger an exception
- A number of _HttpClient_ methods have a completion parameter
  - If set: The async task will complete as soon as the response headers have been received
  - If not set: Response body will be completely read into a buffer first
  - Useful for:
    - Media type of the response may not be understood by the client and can avoid downloading the content. Especially on a metered network.
    - May wish to do parallel processing on response header while downloading the content
- Cancelling an async request will cause an exception. Be prepared to handle it.

### HttpClient Class Structure

- _HttpMessageHandler_ is the building block of request and response pipelines
- Defers the work to a aggregated objet derived from _HttpMessageHandler_
  - A constructor is available for the HTTP message handler `new HttpClient((HttpMessageHandler) new HttpClientHandler());`
  - _HttpClientHandler_ is the lowest common denominator. Features not available:
    - client caching
    - pipelining
    - client certificates
  - Use _WebRequestHandler_ to use those features
    - Deployed separately in _System.Net.Http.WebRequest_
    - It is derived from _HttpClientHandler_
  - _HttpMessageHandler_ implements _IDisposable_
    - This is why _HttpClient_ has _IDisposable_
    - Closes the underlying socket
  - Each new instance creates a new socket connections
    - Very expensive compared to just making a request
  - If an _HttpMesageHandler_ is created outside of _HttpClient_
    - Disposing the HttpClient will close the socket, rendering it useless
    - You can reuse the _HttpMessageHandler_
    - In _HttpClient_ constructor, set _disposeHandler_ to **false**:
    - Allows the _HttpMessageHandler_ to be reused across multiple _HttpClient_ instances

```c#
var handler = HttpHandlerFactory.CreateExpensiveHandler();
var httpClient = new HttpClient(handler, disposeHandler: false);
```

#### Helper Methods

- GetStringAsync
- GetStreamAsync
- GetByteArrayAsync

#### Content

- _HttpContent_ is an abstract base class
  - Implemented by
    - fill in here
  - Provide access to the HTTP headers specifically related to the HTTP payload
  - Can access the content using the _ReadAs_ method

#### SendAsync

- Helpers are just wrappers for _SendAsync_ method

```c#
public Task<HttpResponseMessage> SendAsync(
        HttpRequestMessage request,
        HttpCompletionOption completionOption,
        CancellationToken cancellationToken)
```

##### HttpRequestMessage

- Can only be used for one request, not reused.
- The _HttpContent_ object wraps a forward-only stream
  - Not possible to resend the content without reinitializing the stream
  - Work around using a link class as a request factory:

```c#
var httpClient = new HttpClient();
httpClient.BaseAddress = new Uri("http://www.ietf.org/rfc/");

var request = new HttpRequestMessage() {
        RequestUri = new Uri("rfc2616.txt"),
        Method = HttpMethod.Get
}
var response = await httpClient.SendAsync(request,
        HttpCompletionOption.ResponseContentRead, new CancellationToken());
```

#### Client Message Handlers

- Message handlers are one of the key architectural components in both _HttpClient_ and _Web API_
- Every request and response is passed through a chain of classes that derive from _HttpMessageHandler_
- _HttpClient_ uses _HttpClientHandler_ by default
- Can extend this behavior by inserting additional _HttpMessageHandler_ instances at the beginning of the chain

```c#
var customHandler = new MyCustomHandler()
        { InnerHandler = new HttpClientHandler()};

  var client = new HttpClient(customHandler);

  client.GetAsync("http://example.org",content);
```

![HTTP Message Handlers](./HttpClient-HttpMessageHandlers.png)

- Note, the base _HttpMessageHandler_ does not have a built-in chaining capability.
- _DelegatingHandler_ is a derived class that provides the _InnerHandler_ property to support chaining

```c#
public class HttpMethodOverrideHandler: DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request,
                System.Threading.CancellationToken cancellationToken)
        {
            if (request.Method == HttpMethod.Put)
            {
                request.Method = HttpMethod.Post;
                request.Headers.Add("X-HTTP-Method-Override", "PUT");
            }
            if (request.Method == HttpMethod.Delete)
            {
                request.Method = HttpMethod.Post;
                request.Headers.Add("X-HTTP-Method-Override", "DELETE");
            }
            return base.SendAsync(request, cancellationToken);
        }
    }
```

- _MessageProcessingHandler_ makes this easier
  - Derived from _DelegatingHandler_
  - Should not do any long-running work that rquires async operations
  - Be aware they will often execute on a different thread than the thread that issued the request
    - Deadlock can occur when the handler attempts to switch back onto the requesting thread
    - If the original request is blocking, a deadlock will occur.
    - A good reason to avoid using _.Result_ to simulate synchronous requests.

```c#
public class HttpMethodOverrideMessageProcessor : MessageProcessingHandler {

        protected override HttpRequestMessage ProcessRequest(
                HttpRequestMessage request,
                CancellationToken cancellationToken) {

            if (request.Method == HttpMethod.Put)
            {
                request.Method = HttpMethod.Post;
                request.Headers.Add("X-HTTP-Method-Override", "PUT");
            }
            if (request.Method == HttpMethod.Delete)
            {
                request.Method = HttpMethod.Post;
                request.Headers.Add("X-HTTP-Method-Override", "DELETE");
            }
            return request;
        }

        protected override HttpResponseMessage ProcessResponse(
                HttpResponseMessage response,
                CancellationToken cancellationToken) {
            return response;
        }
    }
 ```

##### Proxying Handlers

- Can be used as a proxy for manipulating outgoing requests

```c#
public class RunscopeMessageHandler : DelegatingHandler
    {
        private readonly string _bucketKey;

        public RunscopeMessageHandler(string bucketKey,
                HttpMessageHandler innerHandler)
        {
            _bucketKey = bucketKey;
            InnerHandler = innerHandler;
        }

        protected override Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request,
                CancellationToken cancellationToken)
        {
            var requestUri = request.RequestUri;
            var port = requestUri.Port;

            request.RequestUri = ProxifyUri(requestUri, _bucketKey);
            if ((requestUri.Scheme == "http" && port != 80 )
                    ||  requestUri.Scheme == "https" && port != 443)
            {
                request.Headers.TryAddWithoutValidation(
                    "Runscope-Request-Port", port.ToString());
            }
            return base.SendAsync(request, cancellationToken);
        }

                private Uri ProxifyUri(Uri requestUri,
                                string bucketKey,
                                string gatewayHost = "runscope.net")
        {
         ...
        }
   }
```

##### Fake Response Handlers

- Can be used for testing client code

```c#
public class FakeResponseHandler : DelegatingHandler
    {
        private readonly Dictionary<Uri, HttpResponseMessage> _FakeResponses
            = new Dictionary<Uri, HttpResponseMessage>();

        public void AddFakeResponse(Uri uri, HttpResponseMessage responseMessage)
        {
                _FakeResponses.Add(uri,responseMessage);
        }

        protected async override Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request,
                CancellationToken cancellationToken)
        {
            if (_FakeResponses.ContainsKey(request.RequestUri))
            {
                return _FakeResponses[request.RequestUri];
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound)
                    { RequestMessage = request};
            }

        }
    }

// Used to replace HttpClientHandler

[Fact]
public async Task CallFakeRequest()
{
    var fakeResponseHandler = new FakeResponseHandler();
    fakeResponseHandler.AddFakeResponse(
        new Uri("http://example.org/test"),
        new HttpResponseMessage(HttpStatusCode.OK));

    var httpClient = new HttpClient(fakeResponseHandler);

    var response1 = await httpClient.GetAsync("http://example.org/notthere");
    var response2 = await httpClient.GetAsync("http://example.org/test");

    Assert.Equal(response1.StatusCode,HttpStatusCode.NotFound);
    Assert.Equal(response2.StatusCode, HttpStatusCode.OK);
}
```

- In order to test client-side services, _HttpClient_ needs to be injected
- Another example of why it is better to share an _HttpClient_ instance
- The _FakeResponseHandler_ needs to be pre-populated with the response

```c#
[Fact]
public async Task ServiceUnderTest()
{
    var fakeResponseHandler = new FakeResponseHandler();
    fakeResponseHandler.AddFakeResponse(
            new Uri("http://example.org/test"),
            new HttpResponseMessage(HttpStatusCode.OK)
                    {Content = new StringContent("99")});

    var httpClient = new HttpClient(fakeResponseHandler);

    var service = new ServiceUnderTest(httpClient);
    var value = await service.GetTestValue();

    Assert.Equal(value, 99);
}
```

##### Creating Reusable Response Handlers

- Message handlers simplify the process of decoupling the response handling form the context of the request
- Example of a pluggable response handler
  - Delegating handler will dispatch response to a particular _ResonseAction_ class if _ShouldResponse_ return **true**
  - Allows arbitrary number of response actions to be defined and plugged in

```c#
public abstract class ResponseAction
    {
        abstract public bool ShouldRespond(
                ClientState state,
                HttpResponseMessage response);

        abstract public HttpResponseMessage HandleResponse(
                    ClientState state,
                    HttpResponseMessage response);
    }

public class ResponseHandler : DelegatingHandler
    {
         private static readonly List<ResponseAction> _responseActions
                = new List<ResponseAction>();


        public void AddResponseAction(ResponseAction action)
        {
            _responseActions.Add(action);
        }

        protected override Task<HttpResponseMessage> SendAsync(
                HttpRequestMessage request,
                CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken)
                .ContinueWith<HttpResponseMessage>(t =>
                     ApplyResponseHandler(t.Result));
        }

        private HttpResponseMessage ApplyResponseHandler(
                    HttpResponseMessage response)
        {

            foreach (var responseAction in _responseActions)
            {
                if (responseAction.ShouldRespond(response))
                {
                    var response = responseAction.HandleResponse(response);
                    if (response == null) break;
                }
            }
            return response;
        }

// Using response handlers
var responseHandler = new ResponseHandler()
        {InnerHandler = new HttpClientHandler()};

responseHandler.AddAction(new NotFoundHandler());
responseHandler.AddAction(new BadRequestHandler());
responseHandler.AddAction(new ServiceUnavailableRetryHandler());
responseHandler.AddAction(new ContactRenderingHandler());

var httpClient = new HttpClient(responseHandler);

httpClient.GetAsync("http://example.org/contacts");
```
