# When in Doubt

When in doubt about what you are endeavoring, consider if you need a course correction.

## Laws of Holes

### First Law

When you find yourself in a hole, stop digging

### Second Law

When you stop digging, you are still in a hole.

### Third Law

A hole not filled in will cause more issues in the future.

### Forth Law

Don't jump into another person's hole

### Fifth Law

Pug logic above ego

## Escalation of Commitment

a human behavior pattern in which an individual or group facing increasingly negative outcomes from a decision, action, or investment nevertheless continue the behavior instead of altering course. The actor maintains behaviors that are irrational, but align with previous decisions and actions.

AKA: Sunk-Cost Fallacy

## Gresham's Law

In economics, Gresham's law is a monetary principle stating that "bad money drives out good".

## References

- [5 Laws of Holes: Meaning?](https://techwithtech.com/laws-of-holes/)
- [Law of holes](https://en.wikipedia.org/wiki/Law_of_holes)
- [Escalation of commitment](https://en.wikipedia.org/wiki/Escalation_of_commitment)
- [Gresham's law](https://en.wikipedia.org/wiki/Gresham%27s_law)
