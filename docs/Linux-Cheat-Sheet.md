<!-- omit in toc -->
# Linus Cheat Sheet

## Commands

### List

| ls option                  | Description                                                                                                                                 |
| :------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------ |
| ls -a                      | The  (ls -a) command will enlist the whole list of the current directory including the hidden files (start with '.').                       |
| ls -l                      | It will show the list in a long list format.                                                                                                |
| ls -lh                     | This command will show you the file sizes in human readable format. The (ls -lh)command will give you the data in terms of Mb, Gb, Tb, etc. |
| ls -lhS                    | If you want to display your files in descending order (highest at the top) according to their size, then you can use (ls -lhS) command.     |
| ls -l - -block-size=[SIZE] | It is used to display the files in a specific size format. Here, in [SIZE] you can assign size according to your requirement.               |
| ls -d */                   | It is used to display only subdirectories.                                                                                                  |
| ls -g or ls -lG            | With this you can exclude column of group information and owner.                                                                            |
| ls -n                      | It is used to print group ID and owner ID instead of their names.                                                                           |
| ls --color=[VALUE]         | This command is used to print list as colored or discolored.                                                                                |
| ls -li                     | This command prints the index number if file is in the first column.                                                                        |
| ls -p                      | It is used to identify the directory easily by marking the directories with a slash (/) line sign.                                          |
| ls -r                      | It is used to print the list in reverse order.                                                                                              |
| ls -R                      | It will display the content of the sub-directories also.                                                                                    |
| ls -lX                     | It will group the files with same extensions together in the list.                                                                          |
| ls -lt                     | It will sort the list by displaying recently modified filed at top.                                                                         |
| ls ~                       | It gives the contents of home directory.                                                                                                    |
| ls ../                     | It give the contents of parent directory.                                                                                                   |
| ls --version               | It checks the version of ls command.                                                                                                        |

### Change Permission

CHMOD

## SSH Key

1. Create the ssh key pair using ssh-keygen command.
1. Copy and install the public ssh key using ssh-copy-id command on a Linux or Unix server.
1. Add yourself to sudo or wheel group admin account.
1. Disable the password login for root account.
1. Test your password less ssh keys login using ssh user@server-name command.

[How to Setup SSH Keys on GitHub](https://devconnected.com/how-to-setup-ssh-keys-on-github/)

## Archiving

Reference: [How to create and extract zip, tar, tar.gz and tar.bz2 files in Linux](https://www.simplehelp.net/2008/12/15/how-to-create-and-extract-zip-tar-targz-and-tarbz2-files-in-linux/)

### TAR

Tar is a very commonly used archiving format on Linux systems. The advantage with tar is that it consumes very little time and CPU to compress files, but the compression isn’t very much either.

- Archive: `tar -cvf <archive_file>.tar <directory_to_compress>`
- Extract: `tar -xvf <archive_file>.tar -C <target_directory>`

### TAR.GZ

Gives very good compression while not utilizing too much of the CPU while it is compressing the data.

- Archive: `tar -zcvf <archive_file>.tar <directory_to_compress>`
- Extract: `tar -zxvf <archive_file>.tar -C <target_directory>`

## Symbolic Links

```bash
# Create a symbolic link, fails if already exists
ln -s /path/to/file /path/to/symlink

# Create or update symbolic link
ln -sf /path/to/file /path/to/symlink
```

## Set PATH

```bash
echo $PATH
export PATH=$PATH:/add/your/path
```