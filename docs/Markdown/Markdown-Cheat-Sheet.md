<!-- omit in toc -->
# Markdown Cheat Sheet

## Hyperlinks and Anchors

### Hyperlinks

Hyperlinks may be added using the `[your text here](https://some-site.com/link)`. If an https string is entered, Markdown will treat it as a hyperlink too. To prevent the text from being a hyperlink, add `<nolink>` to the URL as follows:

`https<nolink>://code.visualstudio.com`

### Anchors

#### Define

Set an anchor: `<a name="anchor_name"></a>`

#### Reference

In same document: `[anchor name](#anchor_name)`

In different document: `[anchor name](./other-file-name#anchor_name`

## Formatting

### New Line

To split two lines in a paragraph, add 2 or more spaces and then a new line (enter key). Like this:
>Line one has two spaces at the end  
>Line two then shows on a separate line

### Indenting a line

Markdown renderers ignore leading whitespace. To indicate a line should be indented, use ASCII spacing codes

- `&nbsp;` to fill with a single leading blank characters
- `&ensp;` fo fill with 2 leading blank characters
- `&emsp;` fo fill with 4 leading blank characters

!!! Example "Using Leading Whitespace"
    `&nbsp`Sindgle leading space  
    Renders as:  
    &nbsp;Single leading space

    `&ensp`Two leading spaces  
    Renders as:  
    &ensp;Two leading spaces

    `&emsp`Four leading spaces  
    Renders as:  
    &emsp;Four leading spaces


### CSS Styling

#### Create CSS sheet

- Create the CSS file
  - For workspace, add it to the _.vscode_ folder.
  - For use in all workspaces, add it to the _~/.vscode_ folder.
- In workspace, add setting `"markdown.syle"=["./.vscode/markdown.css"]` in _settings.json_.
- For all workspaces, add setting `"markdown.style"=["file///users/<username>/.vscode/markdown.css]` to the ~/.vscode_ folder.

```css
/* Add this CSS to the markdown.css file */
/* Hides text in a span */
.hidden {
  background: #fff;
  color: #fff;
  transition: background 0.5s ease;
}

.hidden:hover {
  background: none;
}
```

#### Use Style

In the document text where you want to apply the CSS, wrap in a span.

```markdown
# some section

Test with <span class="hidden">hidden text </span>you can't see
```

## Collapsible Panel

Use html _detail_ and _summary_ tags to create a collapsible panel

```markdown
<details>
<summary>My Hidden Stuff</Summary>

Add hidden things here

### Hidden Section

- one
  - a
  - b
- two
  - c
  - d

</details>
```

<details>
<summary>My Hidden Stuff</Summary>
Add hidden things here

### Hidden Section

- one
  - a
  - b
- two
  - c
  - d
&nbsp;

</details>

&nbsp;

>Note:
>
>Not all markdown will work inside the details. Move the markdown to left and try again.
>
> If using MkDocs with Material, use `???[+,-]` to create collapsible panels


## Escaping special characters in markdown

[markdownBy tomo on March 15, 2016 - 12:44pm](http://tech.saigonist.com/b/code/escaping-special-characters-markdown.html)

These characters need to be escaped in Markdown in order to appear as literal characters instead of performing some markdown functions:

```\ ` * _ { } [ ] ( ) # + - . !```

#### Special escaping for hash (#)

You can usually escape a `#` with the `\` character like others in the above list, but if your hash appears on the same line as an h1/h2/h3 which themselves start with `#`, `##`, or `###` then you need extra escaping for the inline `#`. In this case, add another # at the end of the line to trick the markdown compiler into closing matching hashes. This behavior may vary between markdown implementations.

#### Special escaping for backtick (`)

The simple way to escape a backtick is with the `\` character before it. If you just write a \` by itself, then any text between it and the next matching \` MAY be treated as quoted code and the actual backticks won't be seen (sometimes it still works).

You can escape a backtick by putting it in a code block, any block of text which is preprended by 4 spaces on each line.

Multiple consecutive ` will print out, e.g. `` or ```.

You can also use HTML entities to print any special characters and in this case it would be `&#96;`

#### Handling underscores

In Markdown, a word or string which begins and ends with the underscrore () _will appear \_emphasized_. But in other cases, using an underscore should be fine:

Examples:

- _test
- test_
- te_st
- `_test

### Display Alternate ASCII Characters

>See list of alternative ASCII codes [ALT Codes](https://www.lookuptables.com/text/alt-codes)

1. Num lock is on
2. Press _Alt_ + alt code on numeric pad

Example: _Alt_ + 2 + 7 results in a left arrow (←)

#### Emojis and Alternate ASCII Characters on MacOS

Use the `fn-e` or `cntrl-command-space_bar` to invoke the emojis dialog.

### Nested Lists

#### Ordered List in Numbered List

1. One
2. Two

    - bullet 2.1 --> **leads with 4 spaces!**
    - bullet 2.2
    - bullet 2.3

3. Three
4. Four

#### Indented Sub List

- One
- Two
  - 2.1 --> **Leads with 2 spaces**
  - 2.2
    - 2.2.1 --> **Leads with 4 spaces**
    - 2.2.2
- Three

## Markdown Lint Rules

### Set Rule in Code

Add a line for the rule

```powershell
<!-- rule-parameter MDxxxx -->
```

### Configuration File

Add the file _.markdownlint.json_ to the root directory of the project. Rules settings are set as follows.

```json
{
  "default": true,
  "MD003": { "style": "atx_closed" },
  "MD007": { "indent": 4 },
  "no-hard-tabs": false,
  "whitespace": false
}
```

To see all the rules:

- [Markdown Lint Rules](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md)
- [David Anson Markdown Lint Rules](https://github.com/DavidAnson/markdownlint/blob/main/doc/Rules.md)
- [Markdown Rules/Aliases](https://github.com/DavidAnson/markdownlint#rules--aliases)

## Mermaid

```html
<div class="mermaid">
    sequenceDiagram
        A->> B: StepOne
        B->> C: StepTwo
        Note right of C: Test note
        C->> B: Back to Two
        B->> A: Back to One
        A ->> C: Skip B
</div>
```

```mermaid
sequenceDiagram
    A->> B: StepOne
    B->> C: StepTwo
    Note right of C: Test note
    C->> B: Back to Two
    B->> A: Back to One
    A ->> C: Skip B
```

### Mermaid References

- [Mermaid Layouts](https://handbook.gitlab.com/handbook/tools-and-tips/mermaid)
- [Mermaid](https://mermaid.js.org/intro/)
- [Mermaid Diagram Syntax](https://mermaid.js.org/syntax/flowchart.html)

## Paste An Image

- Capture image to the clipboard
- In VS Code Markdown document:
    - Set insertion point in a Markdown document
    - Click ctrl-alt-v (windows) or  cmd-opt-v (mac)
- The image file will be added in the same folder as the document
- The document will be displayed in the document: `![](image.png)`
- Also works for drag & drop of image file
- See [Copy External Media Files Release Notes](https://code.visualstudio.com/updates/v1_79#_copy-external-media-files-into-workspace-on-drop-or-paste-for-markdown) for more details

## Embed pdf

```markdown title="Embed PDF in Markdown document"
<object data="../19062_Manual_20230331.pdf" type="application/pdf" width="1200px" height="3000px">
    <embed src="../19062_Manual_20230331.pdf" type="application/pdf" />
</object>
```
