# Markdown overview

## Markdown Documentation

- [GitHub - Basic writing and formatting syntax](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
- [Markdown Guide - Basic Syntax](https://www.markdownguide.org/basic-syntax/)
- [Getting started with writing and formatting on GitHub](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github)
- [GitHub Flavored Markdown Specification](https://github.github.com/gfm/)
- [Markdown Originators Syntax Help](https://daringfireball.net/projects/markdown/syntax)
- [Markdown Alerts](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#alerts)

## VSCode Extensions

- [Markdown All in One by Yu Zhang](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
- [Markdownlint by David Anson](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- [LTex - Language Tool by Julian Valentin](https://marketplace.visualstudio.com/items?itemName=valentjn.vscode-ltex) (grammar / spell checker)
- [LaTex Workshop by James Yu](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) (works with LTex)
- [Draw.io by Henning Dieterichs](https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio)

### Additional Extensions

You may find these extension helpful too.

- [Markdown Preview Github Styling](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-preview-github-styles)
- [Markdown Preview Mermaid Support](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid)
- [Mermaid Markdown Syntax Highlighting](https://marketplace.visualstudio.com/items?itemName=bpruitt-goddard.mermaid-markdown-syntax-highlighting)
- [Miranum: Camunda Modeler](https://marketplace.visualstudio.com/items?itemName=miragon-gmbh.vs-code-bpmn-modeler)

### MacOS Keyboard Twister

[Keyboard Shortcuts for MacOS (pdf)](./assets/keyboard-shortcuts-macos.pdf)

|Keys|Command|
|:--- | :---|
|fn-e| Insert emoticon and symbols|

## Markdown VSCode Rules

The [Markdown linting rules](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md) can often be a bit annoying. You can adjust the rules by adding an _.markdownlint.json_ file to the repository's root folder. A default one will be provided with rules for:

- [Repeating headings](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md013---line-length)
- [Multiple Header with the Same Content](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md024---multiple-headers-with-the-same-content)
- [Inline HTML](https://github.com/markdownlint/markdownlint/blob/main/docs/RULES.md#md033---inline-html)

## GitHub alerts

!!! Tip "If using MkDocs..."

    When creating callouts for MkDocs, use the [Admonitions](#admonitions) tool.

The GitHub, and other Markdown renderers, will create "alerts" from Markdown notes (lines preceded by `>`) that used the `> [!<alert name>]` syntax as follows:

```markdown
> [!NOTE]
>My note text
```

The alert will look like this when rendered on GitHub:

![GitHub Alert](./assets/github-alert.png)

### HTML

Markdown renderers will typically render HTML inside a document. However, that's not the case in MkDocs' Markdown renderer. Only Markdown syntax will be honored. For example:

```markdown
Using this:

<img alt='My Image Text' src='./my-image.png' >

The HTML will not render and you must use:

![My Image Text](./my-image.png)
```

## Embed pdf file

```markdown title="Embed PDF in Markdown document"
<object data="../19062_Manual_20230331.pdf" type="application/pdf" width="1200px" height="3000px">
    <embed src="../19062_Manual_20230331.pdf" type="application/pdf" />
</object>
```
