# Microservice Cheat Sheet

## Independently Deployable

What does it mean?

- Can be tested without other microservices
- Does not share data store with other services
- fast feedback!!!
- well defined protocols that are stable
- independent teams, minimal interactions between them
- our system is scalable
