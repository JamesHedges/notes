# Micro Frontends

Micro frontend (MFE)

## Guidance and Boundaries

### Note to Chris Daigle

Skimming through a couple of books on micro frontends looking for guidance on where to draw boundaries, how to isolate them, and how they are engaged. First, this:
 
Warning Share generic infrastructure components only. <mark>Avoid sharing business logic this way, because it creates coupling and undermines team autonomy.</mark>
And this:

 ![Team Structure](image.png)

- Micro frontends are based on subdomains. A solution constitutes a domain, its components (micro frontends) are subdomains and contain business logic as defined by that solution. Cross-cutting concerns are fine when they don't infringe on business logic. If we have multiple products sharing business components, then do we have multiple products? Without differentiation, we don't.
- If you want a true plug 'n play environment, it will require significant investment in defining the pluggable interfaces and establishing means for components to interact (maybe the don't, that is easier). Single Spa actually attempts to do this, but I am skeptical that engineers are too concerned with playing by the rules. And who is policing that?
- Micro frontends should be API driven. I like the idea that each MFE has its own backend-for-frontend
- MFEs should be independently deployable
- MFEs require quality and up to date documentation  

## From Bing CoPilot

Certainly! **Micro frontends**—a fascinating architectural approach—have been gaining traction lately. Let's dive into this topic.

Imagine you're building a sprawling digital city, and each building represents a different part of your web application. Now, instead of constructing one massive skyscraper, you decide to create smaller, modular buildings that can stand independently but still work together harmoniously. That's the essence of micro frontends!

Here's the lowdown:

1. **What Are Micro Frontends?**
   - **Autonomous Units**: Micro frontends extend the principles of microservices to the frontend world. They break down a web application's frontend into smaller, more digestible units.
   - **Independently Deployable**: Each micro frontend can be developed, tested, and deployed separately. This independence allows teams to work on specific features without stepping on each other's toes.
   - **Shared Workflow Requirements**: Micro frontends shine when you have multiple applications with shared functionality. Think of common features like user authentication, payment processing, or search functionality. Instead of duplicating code across applications, micro frontends allow you to share these features seamlessly.

2. **How Do They Work?**
   - **Decomposition**: You take your monolithic frontend and break it down into smaller pieces. Each piece becomes a micro frontend.
   - **Intercommunication**: These micro frontends need to talk to each other. They can do so through shared APIs, global state management, or event-based systems. Imagine them exchanging friendly waves across the digital cityscape.
   - **Autonomy with Cooperation**: While each micro frontend operates independently, they collaborate to create a cohesive user experience. It's like having individual food trucks at a food festival—they serve different dishes, but together they make the event delightful.

3. **Module Federation and Shared Dependencies**:
   - **Webpack's Magic**: Enter **Module Federation**, powered by Webpack. It's like the teleportation system for micro frontends. It allows different modules from various micro frontends to be dynamically loaded and shared across your application.
   - **No Duplication**: With Module Federation, you avoid duplicating code. For instance, if you have a payment processing module, it can be shared seamlessly across different parts of your digital city without unnecessary replication¹².

Remember, micro frontends aren't a silver bullet—they come with their own challenges. Coordinating shared dependencies, ensuring consistent styling, and managing cross-cutting concerns can be tricky. But hey, every city has its quirks, right?

So, are you ready to build your own digital metropolis with micro frontends? 🏙️ If you have any more questions or need further guidance, feel free to ask! 😊

Source: Conversation with Copilot, 8/30/2024
(1) Micro Frontends: What and When to Use Them? | StarTechUP. https://www.startechup.com/blog/micro-frontends/.
(2) A Guide to Micro Frontend Architecture with Module Federation. https://blog.miraclesoft.com/a-guide-to-micro-frontend-architecture-with-module-federation/.
(3) What Is Micro Frontend Architecture? When & Why to Use It? - VisionX. https://visionx.io/blog/micro-frontend-architecture/.
(4) Micro Frontends - Martin Fowler. https://martinfowler.com/articles/micro-frontends.html.

## References

- [What are Micro Frontends?](https://micro-frontends.org/)
- [Micro Frontends in Action by Michael Geers](https://learning.oreilly.com/library/view/micro-frontends-in/9781617296871/)