# MkDocs

For more detail, see [MkDocs Overview](mkdocs-overview.md)

## Admonitions

- Admonitions are a small, marked area of text denoting information relevant to the topic
- Typical admonitions

!!! Info
    additional information of interest but not essential

    MkDocs Example:

    ```markdown
    !!! Info

        content here
    ```

!!! Note
    additional information that's useful to some people in situations

    MkDocs Example:
    
    ```markdown
    !!! Note

        content here
    ```

!!! Tip
    helpful information useful to most, but not essential

    MkDocs Example:
    
    ```markdown
    !!! Tip

        content here
    ```

!!! Caution "Caution or warning"

    somewhat important for most readers

    MkDocs Example:
    
    ```markdown
    !!! Caution "Caution or warning"

        content here
    ```

!!! Danger "Error or danger"

    information that's essential for readers to notice and read

    MkDocs Example:
    
    ```markdown
    !!! Danger "Error or danger"

        content here
    ```
