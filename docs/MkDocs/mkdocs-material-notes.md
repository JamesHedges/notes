# Material for MkDocs

## Material theme

[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/) is a powerful documentation framework on top of MkDocs, a static site generator for project documentation. Industry leaders, as well as many successful Open Source projects, rely on Material for MkDocs to create professional and beautiful documentation – no frontend experience required. Choose a mature and actively maintained solution and start writing in minutes.

Why?

- It's just Markdown
- Made to for users to adopt your own branding
- Fast and lightweight
- Works on all devices
- 100% Open Source

If you're familiar with Python, you can install Material for MkDocs with pip, the Python package manager. If not, they recommend using docker.

## Features

### Admonitions

Admonitions are a small, marked area of text denoting information relevant to the topic. Be careful not to overuse them or risk making your content difficult to consume.

Typical admonitions:

- note
- abstract
- info
- tip
- success
- question
- warning
- failure
- danger
- bug
- example
- quote

#### Admonition examples

!!! Info

    Additional information of interest but not essential

    MkDocs Example:

    ```markdown
    !!! Info

        content here
    ```

!!! Note

    Additional information that's useful to some people in situations

    MkDocs Example:
    
    ```markdown
    !!! Note

        content here
    ```

!!! Tip

    Helpful information useful to most, but not essential

    MkDocs Example:
    
    ```markdown
    !!! Tip

        content here
    ```

!!! Caution "Caution or warning"

    Somewhat important for most readers

    MkDocs Example:
    
    ```markdown
    !!! Caution "Caution or warning"

        content here
    ```

!!! Danger "Error or danger"

    Information that's essential for readers to notice and read

    MkDocs Example:
    
    ```markdown
    !!! Danger "Error or danger"

        content here
    ```

See [Material for MkDocs: Admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)

### Footnotes

- In _mkdocs.yml_, add
    - features:
        - content.footnote.tooltips
    - markdown_extensions:
        - footnotes

```yaml
site_name: my Developer Docs
theme:
  name: material
  features:
    - content.footnote.tooltips
  markdown_extensions:
    - footnotes
nav:
  - Home: 
    - 'index.md'
```

- In your MarkDown document
    - Add footnote reference using `[^#]`
    - At bottom of page, add footnote  
`[^#]: My footnote`

```markdown
# My Doc

In this important text, I need to cite me sources[^1]. 

If you have been diligent, there may be more than one footnote[^2].

## Footnotes

[^1]: Important source number one
[^2]: Important source number two
```

See [Material for MkDocs: Footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/)

