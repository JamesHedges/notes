# MkDocs overview

## Overview

Developers need a forum to curate and share project, team, and technology documentation. They should be able to produce this using familiar editing tools that requires little overhead. It must be simple to use, delivered in a way that's well organized, searchable, and maintains accessibility.

It's common for developers to produce documentation using the Markdown syntax. This format provides a rich set of formatting capabilities while remaining keyboarding friendly. Driving its adoption is the popularity of GitHub and its ability to display Markdown documents natively.

While the displaying formatted Markdown documents in git-based repositories is a great advancement, it leaves room for improvement. In those environments, the tendency is to place documents across several folders. It's possible the environment structure lacks a cohesive organization. Used within the git repository's context, the reader must navigate and read documents while framed in the same display as the code and its supporting tools.

Enter MkDocs.

MkDocs is a fast and simple static site generator that's geared towards building project documentation. Documentarians write source document files in Markdown and organize them using a single YAML configuration file. When documents are ready to share with others, use the build tool to generate a complete static documentation site that's ready for a host environment.

Take this a step further and integrate the MkDocs tool into Continuous Integration (CI) pipelines. After generating, CI tool can push the HTML, CSS, and asset files to their own branch, popular cloud storage targets, or any other location reachable by the pipeline. For developers, this means the documentation generation process follows a typical coding development pattern. Code, commit, push, approve, and merge. Automation takes over from there.

## Getting started

How simple is it to integrate MkDocs into your project? Follow these simple steps for a basic setup:

1. Add the configuration file [_mkdocs.yml_](#configuring-mkdocs)
2. Insert the _docs_ folder for your documents
3. Create the _docs/index.md_ file. This is the site's home page
4. Add a CI pipeline to build your site

It will look like this.

```text
my-project
  │
  ├⎯ docs
  │     │
  │     └⎯ index.md
  │
  └⎯ mkdocs.yml
```

!!! Info "_docs_ folder"

    The _docs_ folder is the default. Using the _docs\_dir_ setting in the configuration file allows for setting base documents' folder to another location.

### Configuring MkDocs

MkDocs has a rich set of configuration and navigation options found in the site's _mkdocs.yml_ file. It is required to place the _mkdocs.yml_ file in the project's root folder, adjacent to the _docs\_dir_ folder.

There are two sections in the _mkdocs.yml_, MkDocs settings and navigation. Setting are found at the YAML root and navigations under the _nav element. The only required setting is the `site\_name` field. A basic configuration is as follows:

```yaml
site_name: my Developer Docs
nav:
  - Home: 
    - 'index.md'
```

A documentarian will want to expand on the configuration options and take advantage of MkDocs' feature and ecosystem of plugins.

In the `nav` section, configure your virtual organization. Add sections and subsections to define document categories. The labels used in the `mkdocs.yml` will be those displayed in the site's navigation tree.

## Extending settings

### Icons

Add Images and icons to enhance and brand your MkDocs site.

#### Add images

1. Create a folder for your images in the `docs_dir`
2. Place image used for the theme  
    - Image for the site's badge displayed on the header
    - Icon used for site

```text
my-project
  │
  ├⎯ docs
  │     │
  │     └⎯ index.md
  │     │
  │     └⎯ img
  │         │
  │         └⎯ my-logo.jpeg
  │         │
  │         └⎯ favicon.ico
  │
  └⎯ mkdocs.yml
```

!!! Note "Log file type"

    The logo file type can be any image type. It does not need to be a .jpeg file

#### Configure images

In the _mkdocs.yml_ file under the _theme_ section, add the following:

- `logo`: image for the site's badge displayed on the header
- `favicon`: icon used for site

```yaml
site_name: my Developer Docs
theme:
  name: material
  logo: img/my-logo.jpeg
  favicon: img/favicon.ico
```

## Adding plugins

MkDocs and the Material theme make extensive use of plugins as means to add functionality. They strive to be modular, interoperable, and performant. Enabling them requires:

- Installing a Python package in the environment. See [Local Development Setup](#local-development-setup) for more details.
- Updating the MkDocs configuration by:
    - Adding the `plugins` setting
    - Adding any necessary `features` settings
    - Adding or updating any needed `markdown_extension` settings

Refer to the plugins' documentation for install details.

### Plugins used

- `mkdocs-mermaid2-plugin`: Render [Mermaid](https://mermaid.js.org/intro/) graphs in Markdown
- `mkdocs-material`: [Material theme](https://squidfunk.github.io/mkdocs-material/) for MkDocs including [MkDocs Built-in plugins](https://squidfunk.github.io/mkdocs-material/plugins/)
- `mkdocs-material-extensions`: Adds features like admonitions and footnotes. See [Material for MkDocs Reference](https://squidfunk.github.io/mkdocs-material/reference/) to explore all options.
- `mkdocs-open-in-new-tab`: Open links in a new tab
- `mkdocs-table-reader-plugin`: Utility to read files and render tables
- `pymdown-extensions`: A collection of extensions for [Python Markdown](https://facelessuser.github.io/pymdown-extensions/)
- `Pygments`: Generic syntax highlighter
- `openpyxl`: a Python library to read/write Excel 2010 xlsx/xlsm/xltx/xltm files. See [openpyxl documentation](https://openpyxl.readthedocs.io/en/stable/index.html) for details

### Example mkdocs.yml

!!! Example "Example mkdocs.yml file with plugins configured"

    ```yaml
    --8<-- "docs/MkDocs/assets/sample-mkdocs.yml"
    ```

## Featured plugins

### Search plugin

Enabling search is simple and accomplished by adding the `search` to the MkDocs `plubins` configurations.

Plugin: `mkdocs-material`

```yaml title="Add Search"
site_name: my Developer Docs
plugins:
  - search
```

### Drawio plugin

[PyPi Drawio plugin](https://pypi.org/project/mkdocs-drawio-url/)

```dockerfile title="Add to Dockerfile"
RUN pip install mkdocs-drawio-url
```

```yaml title="Add drawio plugin"
site_name: my Developer Docs
plugins:
  - search
  - drawio-url
```

```markdown title="Add diagram to document"
![](../assets/sample-diagram.drawio)
```

![](./assets/sample-diagram.drawio)

### Table readers plugin

- pip install mkdocs-table-reader-plugin
- _mkdocd.yml_
    - plugins:
        - table-reader

```yaml
site_name: my Developer Docs
plugins:
  - search
  - table-reader
```

!!! Note "Compatible file types"

    .csv, .fwf, .json, .xls, .xlsx, .yaml, .feather

See [mkdocs-table-reader-plugin](https://timvink.github.io/mkdocs-table-reader-plugin/#documentation-and-how-to-guides)

#### Reading csv

```json title="sample-table.csv"
--8<-- "docs/MkDocs/assets/sample-table.csv"
```

Use the `read_csv` command to load a _CSV_ file into a table using the command `read_csv(<path-to-file>)`.

**Example:** \{\{ read_csv('./assets/sample-table.csv') \}\}

{{ read_csv('./assets/sample-table.csv') }}

#### Reading json

```json title="sample-table.json"
--8<-- "docs/MkDocs/assets/sample-table.json"
```

Use the `read_json` command to load a _JSON_ file into a table using the command `read_json(<path-to-file>)`.

**Example:** \{\{ read_json('./assets/sample-table.json') \}\}

{{ read_json('./assets/sample-table.json') }}

#### Reading excel

Use the `read_excel` command to load an Excel workbook's sheet into a table using the command `read_excel(<path-to-file>)`.

The command has additional parameters. Most relevant are

- `engine` parameter used to indicate the Excel reader engine. Use `engine='openpyxl'`
- `sheet_name` parameter indicates the sheet's ordinal position or name. The default value is `0`. Use `sheet_name='Sheet1'`

**Example:** \{\{ read_excel('./assets/sample-table.xlsx', engine='openpyxl', sheet_name='Sheet1') \}\}

{{ read_excel('./assets/sample-table.xlsx', engine='openpyxl', sheet_name='Sheet1') }}

See [pandas.read_excel](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html) to explore more options when reading Excel files.

### Snippets plugin and code blocks

The Snippets plugin and syntax highlighting enable rich code blocks in documentations.

- requires `Pygments` package to be installed
- add markdown_extension:
    - pymdownx.highlight
    - pymdownx.superfences
    - pymdownx.inlinehilite
    - pymdownx.snippets

```yaml
site_name: my Developer Docs
markdown_extensions:
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:mermaid2.fence_mermaid_custom
  - pymdownx.inlinehilite
  - pymdownx.snippets
```

See [Material code blocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks) for more details

#### Embedding external files

```markdown
# ```json title="my json file"
# --8<-- "docs/.../my-json-file.json"
# ```
```

## Themes

MkDocs is a site generator for documentation with two out-of-the-box themes available:

- [`mkdocs`](https://www.mkdocs.org/user-guide/choosing-your-theme/#mkdocs): The default theme, customizable with various options
- [`readthedocs`](https://www.mkdocs.org/user-guide/choosing-your-theme/#readthedocs): A clone of the MkDocs theme, designed for compatibility with Read the Docs

It's possible to create custom themes using CSS and Bootstrap along with plugins. However, with a wide array of time tested themes, this may not be the first choice. Here are some other popular themes to consider:

- Material for MkDocs: A modern, responsive theme with a clean design and extensive features.
- Cinder: A clean and responsive theme focused on simplicity.
- Dracula: A dark theme that’s visually appealing and easy on the eyes.
- Terminal for MkDocs: A monospace theme that mimics a terminal interface.
- Bootstrap: A theme based on the popular Bootstrap framework.
- Windmill: Focuses on navigation and usability.
- GitBook: Mimics the GitBook style for a familiar look.

The Material theme is a popular choice and has grown to become its own framework. This discussion will focus on this theme. See [MkDocs Mateial Overview](mkdocs-material-notes.md) for more details.

<!-- ## Material theme

[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/) is a powerful documentation framework on top of MkDocs, a static site generator for project documentation. Industry leaders, as well as many successful Open Source projects, rely on Material for MkDocs to create professional and beautiful documentation – no frontend experience required. Choose a mature and actively maintained solution and start writing in minutes.

Why?

- It's just Markdown
- Made to easily adopt your own branding
- Fast and lightweight
- Works on all devices
- 100% Open Source

If you're familiar with Python, you can install Material for MkDocs with pip, the Python package manager. If not, they recommend using docker.

### Admonitions

Admonitions are a small, marked area of text denoting information relevant to the topic. Be careful not to overuse them or risk making your content difficult to consume.

Typical admonitions:

- note
- abstract
- info
- tip
- success
- question
- warning
- failure
- danger
- bug
- example
- quote

#### Admonition examples

!!! Info

    Additional information of interest but not essential

    MkDocs Example:

    ```markdown
    !!! Info

        content here
    ```

!!! Note

    Additional information that's useful to some people in situations

    MkDocs Example:
    
    ```markdown
    !!! Note

        content here
    ```

!!! Tip

    Helpful information useful to most, but not essential

    MkDocs Example:
    
    ```markdown
    !!! Tip

        content here
    ```

!!! Caution "Caution or warning"

    Somewhat important for most readers

    MkDocs Example:
    
    ```markdown
    !!! Caution "Caution or warning"

        content here
    ```

!!! Danger "Error or danger"

    Information that's essential for readers to notice and read

    MkDocs Example:
    
    ```markdown
    !!! Danger "Error or danger"

        content here
    ```

See [Material for MkDocs: Admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)

### Footnotes

- In _mkdocs.yml_, add
    - features:
        - content.footnote.tooltips
    - markdown_extensions:
        - footnotes

```yaml
site_name: my Developer Docs
theme:
  name: material
  features:
    - content.footnote.tooltips
  markdown_extensions:
    - footnotes
nav:
  - Home: 
    - 'index.md'
```

- In your MarkDown document
    - Add footnote reference using `[^#]`
    - At bottom of page, add footnote  
`[^#]: My footnote`

```markdown
# My Doc

In this important text, I need to cite me sources[^1]. 

If you have been diligent, there may be more than one footnote[^2].

## Footnotes

[^1]: Important source number one
[^2]: Important source number two
```

See [Material for MkDocs: Footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/)
 -->
## Integrations

### GitHub action

In your GitHub repository, add an Action to build the site. A basic Action that deploys the site for use with [GitHub Pages](./github-pages.md) will look like the following:

```yaml
name: build
on:
  push:
    branches:
      - main
jobs:
  deploy:
    runs-on: uhg-runner
    steps:
      - uses: actions/checkout@v4
      - uses: actions/setup-python@v5
        with:
          python-version: 3.x
      - run: pip install mkdocs
      - run: pip install mkdocs-mermaid2-plugin
      - run: pip install mkdocs-material
      - run: mkdocs gh-deploy --force --clean --verbose
```

## Local development setup

While it's possible to check documents locally for spelling, grammar, and basic Markdown rendering issues using developer tools, reviewing how documents appear in MkDocs requires extra work. Serving the pages from the developer's machine accomplishes this goal. A couple of approaches are available to the document authors, using a container image to run MkDocs or building an environment that executes MkDocs.

Maintaining a local version requires developers to install and manage MkDocs and the plugins used to render documents. Keeping up with versions and changes can be demanding. Simplify the process by delegating the rendering and serving MkDocs to a container. In this case, the image specification is trivial and easy to use.

### Containerized virtual environment

The only requirement is the author have an environment for building and running containers. A container manager like Docker or Podman with the ability to execute `docker-compose` is sufficient. Given that most developer/authors will already need a containerization tool for their daily work, no extra effort is necessary.

The commands used in the next few sections use the `Docker` containerization tool. If not using Docker, then substitute the environment's containerization manager's command.

#### MkDocs image

A `Dockerfile` with a few lines to specifying an image for the MkDocs environment is the first step. The requirements are:

- A minimal Linux base image with Python included
- Installing MkDocs and desired plugins
- Starting the server.

For a MkDocs instance using the Material Design themes and Mermaid diagramming, an image specification will look like the following:

```docker
FROM docker.repo1.uhc.com/python:3.12.3-alpine3.20

RUN pip install mkdocs
RUN pip install mkdocs-mermaid2-plugin
RUN pip install mkdocs-material
RUN pip install mkdocs-material-extensions
RUN pip install mkdocs-open-in-new-tab
RUN pip install mkdocs-table-reader-plugin
RUN pip install pymdown-extensions
RUN pip install Pygments

WORKDIR /mnt/research-docs

CMD ["mkdocs", "serve"]
```

The GitHub action CI pipeline must use the same packages as the development image. A developer / documentarian making changes to the installed packages must update them in both places. A package update will require an image rebuild.

#### Add a volume (manual)

The `MkDocs serve` utility serves the HTTP pages while watching the documents folder and rebuilding the pages upon any file change. This means the container image must be aware of the source files on the authors machine. This feat is accomplished by mounting the project's parent folder to a volume and sharing it with the running container.

!!! Note "Versus docker-compose"

    This section documents the "manual" way to create volumes. Use this technique when docker-compose is not available. Docker-compose will be discussed in the next section.

Create a Docker volume using the `docker volume create` command

```bash
docker volume create --name my-docs-volume --opt type=none --opt device=<full path to your project folder> --opt o=bind
```

Once created, reuse a volume by mounting it when running a container.

```bash
# If you have not built the image, do it first
docker build -t my-docs .
docker run -p 8888:8000 -v my-docs-volume:/mnt/my-docs my-docs
```

It's possible to consolidate both the image and volume specifications in a single file.

#### Pulling it all together

While running the mount and run commands manually works, orchestrate MkDocs using docker compose a better option. The following YAML file specifies that docker-compose runs the `localhost/my-docs` image, mapping port `8000` to the local machine's port `8888`, while mounting the my-docs volume.

The specified `docs_volume` uses the same parameters as in the docker commands in the previous section. One minor difference is the `device` parameter may use a relative path. Here is the simple `compose.yaml` file to run the MkDocs development environment:

```yaml
services:
  my-docs:
    image: localhost/my-docs
    build: .
    ports:
      - "8888:8000"
    volumes:
      - my-docs-volume:/mnt/my-docs
volumes:
  my-docs-volume:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: .
```

Run the image with `up` command while in the directory containing the compose file. See the [docker-compose up](https://docs.docker.com/reference/cli/docker/compose/up/) reference for more options.

&ensp;`docker-compose up -d`

To review the documentation site, use the localhost with the specified port in a browser. For the example compose file, use the address `127.0.0.1:8888`.

##### Building the image

When a `docker-compose`'s image doesn't exist, the `up` command will build it. However, a re-build isn't automatic when the `dockerfile` changes. Use the `--build` option to force the image build.

The `-d`, or `--detach`, option controls running the image's container in the background. When troubleshooting, remove the `-d` option to view the output.

```text
LAMU0MQ6L946DWD:my-documentation jhedges3$ docker-compose up
[+] Running 1/0
 ✔ Container my-documentation-my-docs-1  Created                                                                                                                     0.0s
Attaching to my-docs-1
my-docs-1  | WARNING -  Config value 'dev_addr': The use of the IP address '0.0.0.0' suggests a production environment or the use of a proxy to connect to the MkDocs server. However, the MkDocs' server is intended for local development purposes only. Please use a third party production-ready server instead.
my-docs-1  | INFO    -  Building documentation...
my-docs-1  | INFO    -  MERMAID2  - Initialization arguments: {}
my-docs-1  | INFO    -  MERMAID2  - Using javascript library (10.4.0):
my-docs-1  |    https://unpkg.com/mermaid@10.4.0/dist/mermaid.esm.min.mjs
my-docs-1  | INFO    -  Cleaning site directory
my-docs-1  | INFO    -  The following pages exist in the docs directory, but are not included in the "nav" configuration:
my-docs-1  |   - engineering/adr/adr-template.md
my-docs-1  |   - engineering/adr/bpmn-dmn-configurations.md
my-docs-1  |   - engineering/adr/custom-configurations.md
my-docs-1  |   - engineering/adr/data-migrations.md
my-docs-1  | INFO    -  Documentation built in 1.02 seconds
my-docs-1  | INFO    -  [21:01:27] Watching paths for changes: 'docs', 'mkdocs.yml'
my-docs-1  | INFO    -  [21:01:27] Serving on http://0.0.0.0:8000/
my-docs-1  | INFO    -  [21:01:29] Browser connected: http://127.0.0.1:8888/documentation-help/mkdocs/
  Gracefully stopping... (press Ctrl+C again to force)
[+] Stopping 1/1
 ✔ Container my-documentation-my-docs-1  Stopped                                                                                                                    10.3s
canceled
LAMU0MQ6L946DWD:my-documentation jhedges3$
```

Use the `control-c` keyboard command to exit the interactive mode.

!!! Info
    The `mkdocs.yml` file will need to have the `dev_addr` field set to `0.0.0.0:8000. The URL to view the pages is <http://127.0.0.1:8888>.

## Markdown gotchas

### GitHub alerts

!!! Danger "Don't use GitHub Alerts"

    When creating callouts for MkDocs, use the [Admonitions](#admonitions) tool. They create a more professional looking presentation.

The GitHub rendering engine creates "alerts" from Markdown notes (lines preceded by `>`) that used the `> [!<alert name>]` syntax as follows:

```markdown
> [!NOTE]
>My note text
```

The alert will look like this when rendered on GitHub:

![GitHub Alert](./assets/github-alert.png)

In MkDocs, it will render as:

```text
    [!NOTE] My Note Text
```

### Hyperlinks

In regular Markdown renders hyperlinks. However, the MkDocs renderer doesn't generate the link. Use the Markdown syntax for hyperlinks to get the same result:

`[https://code.visualstudio.com](https://code.visualstudio.com)`

### Indented lists

Markdown renders indented ordered and unordered lists with a minimum of 2 spaces or more, as long as they're consistent. When rendered by MkDocs, nested lists **must** use four spaces or a tab.

## References

- [docker-compose up](https://docs.docker.com/reference/cli/docker/compose/up/)
- [GitHub Pages Use](./github-pages.md)
- [Material code blocks](https://squidfunk.github.io/mkdocs-material/reference/code-blocks)
- [Material for MkDocs: Admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)
- [Material for MkDocs: Footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/)
- [Material for MkDocs Reference](https://squidfunk.github.io/mkdocs-material/reference/)
- [Material theme](https://squidfunk.github.io/mkdocs-material/)
- [Mermaid](https://mermaid.js.org/intro/)
- [MkDocs Built-in plugins](https://squidfunk.github.io/mkdocs-material/plugins/)
- [MkDocs ReadTheDocs](https://mkdocs-origin.readthedocs.io/en/latest/)
- [mkdocs-table-reader-plugin](https://timvink.github.io/mkdocs-table-reader-plugin/#documentation-and-how-to-guides)
- [pandas.read_excel](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html)
- [Python Markdown](https://facelessuser.github.io/pymdown-extensions/)
