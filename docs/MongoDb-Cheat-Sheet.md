<!-- omit in toc -->
# MongoDB Cheat Sheet

- [Db Info Commands](#db-info-commands)
  - [Start a mongo shell to remote Db](#start-a-mongo-shell-to-remote-db)
  - [Commands](#commands)
- [Users](#users)
  - [Create User](#create-user)
- [Backup / Restore](#backup--restore)
  - [Backup](#backup)
  - [Restore with mongorestore](#restore-with-mongorestore)
- [Find](#find)
  - [Querying References](#querying-references)

## Db Info Commands

### Start a mongo shell to remote Db

`mongo --host Test01 --port <port#> --username adminuser --password <pwd> --authenticationDatabase admin`

### Commands

- use [db name]
- db
- show users
- show collections -->

## Users

### Create User

```js
use reporting
db.createUser(
   {
     user: "reportUser256",
     pwd: passwordPrompt(),   // Or  "<cleartext password>"
     roles: [ { role: "readWrite", db: "reporting" } ],
     mechanisms: [ "SCRAM-SHA-256" ]
   }
)
```

## Backup / Restore

### Backup

Backup Mongo database using `mongodump`

```js
mongodump --host Test01 --port <port#> --username adminuser --password <pwd> --db CommonWell_Switching --out dump_switch
mongodump --host Test01 --port <port#> --username adminuser --password <pwd> --authenticationDatabase admin --db CommonWell_Scheduling --out dump
```

> Authenticating to admin database to get permissions on all databases in the Mongo instance

### Restore with mongorestore

```js
mongorestore --nsInclude CommonWell_Reporting.* dump2/
```

## Find

| Filter       | Note                 | Example                                                                                                                    | Reference                                                                                       |
| :----------- | :------------------- | :------------------------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------- |
| Date         | Use ISODate function | `.find("some_field": {$gt: ISODate(YYYY-MM-DDTHH:MM:SS.mmmZ")})`                                                           |                                                                                                 |
| Starts With  | Use regex            | `find({"some_field.v": {$regex:/^abc/}})`                                                                                  | [regex](https://docs.mongodb.com/manual/reference/operator/query/regex/index.html)              |
| And          | $and                 | `.find({$and: [{"some_field": "some_value"}, {"date_field": {$gt: {"date_field": ISODate(YYYY-MM-DDTHH:MM:SS.mmmZ")}}}]})` | [And Operator](https://docs.mongodb.com/manual/reference/operator/query/and/index.html)         |
| Or|$or|`.find({$or: [{field1: "some_value"}, {field2: {$gt: "other_value" }}]})`|[Or Operator](https://docs.mongodb.com/manual/reference/operator/query/or/index.html)         |
| Greater Than | $gt                  | `.find({$gt:{"some_date_field": ISODate(YYYY-MM-DDTHH:MM:SS.mmmZ")}})`                                                     | [Greater Than Operator](https://docs.mongodb.com/manual/reference/operator/query/gt/index.html) |
| Exists       | $exists              | `.find({ "Measurement.Errors": {$exists: true}})`                                                                        |                                                                                                 |

### Querying References

- [Find](https://docs.mongodb.com/manual/reference/method/db.collection.find/index.html)
- [Comparison](https://docs.mongodb.com/manual/reference/operator/query-comparison/)
- [Logical](https://docs.mongodb.com/manual/reference/operator/query-logical/)
