<!-- omit in toc -->
# Open SSL Cheat Sheet

- [To Do](#to-do)
- [PowerShell](#powershell)
- [Convert PFX to PEM](#convert-pfx-to-pem)
- [References](#references)

## To Do

| Task           | Command                                                                  | Notes             |
| :------------- | :----------------------------------------------------------------------- | :---------------- |
| Get Client PEM | openssl pkcs12 -in client_ssl.pfx -nokeys -out client.pem -clcerts       | requires password |
| Get CA PEM     | openssl pkcs12 -in client_ssl.pfx -nokeys -out client_chain.pem -cacerts | requires password |
| GET PK PEM     | openssl pkcs12 -in client_ssl.pfx -nocerts -out client.key               | requires password |
| Get Server Key | openssl rsa -in client.key -out client_server.key                        |                   |

## PowerShell

Install OpenSSL Module: `Install-Module -Name OpenSSL`

## Convert PFX to PEM

```powershell
# export client cert
openssl pk12cs -in <filename.pfx> -clcerts -out <filename.pem>
# export the certificate chain
openssl pk12cs -in <filename.pfx> -cacerts -out <filename_chain.pem>
# get the private key
openssl pk12cs -in <filename.pfx> -nocerts -out <filename.key>
# convert the PK to a server PK
openssl rsa -in filename.key -out <filename_server.key>
```

## References

- [How to Convert Windows PFX Certificate Files Into PEM Format for Use in Apache/NGINX on Linux](https://www.bluelabellabs.com/blog/how-to-export-certificates-from-windows-for-use-in-apache-nginx-on-linux/)
- [OpenSSL Docs](https://www.openssl.org/docs/man3.0/man1/)
- [How to create a .pem file for SSL Certificate Installations](https://support.microfocus.com/kb/doc.php?id=7013103#:~:text=How%20to%20create%20a%20PEM%20file%20with%20the,path%20to%20the%20directory%20containing%20the%20certificate%20files.)
- [How To Set up OpenSSL on Windows 10 (PowerShell)](https://adamtheautomator.com/openssl-windows-10/#:~:text=Open%20up%20PowerShell%20and%20run%20the%20below%20command.,you%20should%20see%20something%20that%20looks%20like%20below.)
- [PowerShell Gallery - OpenSSL](https://www.powershellgallery.com/packages/OpenSSL/1.0.0)
