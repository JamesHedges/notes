<#

 .Synopsis
   Prints hello message

 .Description
   Test command that prints Hello PowerShell World

 .Example
   Start-HelloWorld

#>
function Start-HelloWorld {
    Write-Host "Hello PowerShell World"
}

<#

 .Synopsis
   Prints Bye Message

 .Description
   Test command that prints Bye-Bye PowerShell World message

 .Parameter Name
   Name to say bye to

 .Example
   Stop-HelloWorld -Name Jim

 .Example
   Stop-HelloWorld Jim

#>
function Stop-HelloWorld {
    param (
        [Parameter(Mandatory)]
        $Name
    )
    Write-Host "Bye-Bye $Name"
    Write-Host "Love,"
    Write-Host "The PowerShell World"
}
