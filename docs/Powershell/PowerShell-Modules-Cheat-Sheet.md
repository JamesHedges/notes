# PowerShell Modules Cheat Sheet

## Module Commands

| Action          | Module        | Reference                                                                                                                                       |
| :-------------- | :------------ | :---------------------------------------------------------------------------------------------------------------------------------------------- |
| Register Repo   | PowerShellGet | [Register-PSRepository](https://docs.microsoft.com/en-us/powershell/module/powershellget/register-psrepository?view=powershell-5.1)             |
| Set Repo        | PowerShellGet | [Set-PSRepository](https://docs.microsoft.com/en-us/powershell/module/powershellget/set-psrepository?view=powershell-5.1)                       |
| Get Repo        | PowerShellGet | [Get-PSRepository](https://docs.microsoft.com/en-us/powershell/module/powershellget/get-psrepository?view=powershell-5.1)                       |
| Publish         | PowerShellGet | [Publish-Module](https://docs.microsoft.com/en-us/powershell/module/powershellget/publish-module?view=powershell-5.1)                           |
| Find            | PowerShellGet | [Find-Module](https://docs.microsoft.com/en-us/powershell/module/powershellget/find-module?view=powershell-5.1)                                 |
| Install         | PowerShellGet | [Install-Module](https://docs.microsoft.com/en-us/powershell/module/powershellget/install-module?view=powershell-5.1)                           |
| Import          | Core          | [Import-Module](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/import-module?view=powershell-5.1)                 |
| Get             | Core          | [Get-Module](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/get-module?view=powershell-5.1)                       |
| Remove          | Core          | [Remove-Module](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/remove-module?view=powershell-5.1)                 |
| Export Member   | Core          | [Export-ModuleMember](https://docs.microsoft.com/en-us/powershell/module/Microsoft.PowerShell.Core/Export-ModuleMember?view=powershell-5.1)     |
| New Manifest    | Core          | [New-ModuleManifest](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/new-modulemanifest?view=powershell-5.1)       |
| Test Manifest   | Core          | [Test-ModuleManifest](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/Test-modulemanifest?view=powershell-5.1)     |
| Update Manifest | Core          | [Update-ModuleManifest](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/Update-modulemanifest?view=powershell-5.1) |
| Get Commands    | Core          | [Get-Command](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/get-command?view=powershell-5.1)                     |

## Creating Modules

Creating and publishing PowerShell basic modules is relatively straight forward. The basic steps are:

1. Create a PowerShell module file (.psm1) with the module code. There can be more than one .psm1 file, but one need to be the root file.
2. Create a PoserShell manifest file (.psd1) that describes the module
3. Test the module
4. Upload to a module repository

### Project Structure

It is recommended the GitLab project have a structure where the root folder contains the project level files like _.gitlab-ci.yml_ and _.gitignore_. A modules'subfolder will hold the module's _.psd1_ file and _.psm1_ files.

```text
MyPowerShellProject
  |
  └── MyPowerShellModule
       |
       ├── MyPowerShell.psd1
       |
       ├── MyPowerShell.psm1
       |
       ├── Public
       |    |
       |    ├── PublicFunctionOne.psm1
       │    └── ...
       |
       ├── Private
       |    |
       |    ├── PrivateFunctionOne.psm1
       │    └── ...
       |
       └── Tests/
            ├── _InitializeTests.ps1
            ├── Get-PrivateFunction.Tests.ps1
            ├── Get-PublicFunction.Tests.ps1
            ├── Get-ModuleName.Tests.ps1
            └── ...
```

For more details on how to structure a PowerShell module, see [Structure of a PowerShell Module](https://tomasdeceuninck.github.io/2018/04/17/PowerShellModuleStructure.html)

### Module File (psm1)

Add a module file, a script file having the a _.psm1_ extension. The file will contain variables and functions. To limit access to them, use the `Export-ModuleMember` command, exposing only those variables and functions to made public.

#### Example Module File

```powershell
# BuildTools.psm1
# Routines for build .NET Framework applications in a container.

$ErrorActionPreference = "Stop"

$public = @( Get-ChildItem -Path $PSScriptRoot/Public/*.ps1 -ErrorAction SilentlyContinue)
$private = @(Get-ChildItem -Path $PSScriptRoot/Private/*.ps1 -ErrorAction SilentlyContinue)

Foreach($import in @($public + $private)) {
    try {
        . $import.fullname
    }
    Catch {
        Write-Error -Message "Failed to import function $($import.fullname) = $_"
    }
}

## Script Variables
$script:DefaultOptions = @('-IncludeReferencedProjects','-NonInteractive','-Verbosity', 'detailed')
$script:RunnerPath = 'C:/Program Files (x86)/TypeMock/bin/tmockrunner.exe'

```

- [How to Write a PowerShell Script Module](https://docs.microsoft.com/en-us/powershell/scripting/developer/module/how-to-write-a-powershell-script-module?view=powershell-5.1)

#### Root Module File

##### Naming Commands

PowerShell by convention uses a `verb-noun`, or `action-resource`, command naming structure. It can be picky about the verbs used and will warn if the chosen verb is not found on the approved list. Use the `Get-Verb` command to see a list of approved verbs.

#### Adding Help

Add a help comment block ( `<# #>` ) function having the following possible entries:

| Elemennt                   | Description                                                                                                                                                                                                        |
| :------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **.SYNOPSIS**              | A brief description of the function or script. This keyword can be used only once in each topic.                                                                                                                   |
| **.DESCRIPTION**           | A detailed description of the function or script. This keyword can be used only once in each topic.                                                                                                                |
| **.PARAMETER**             | The description of a parameter. Add a .PARAMETER keyword for each parameter in the function or script syntax.                                                                                                      |
| **.EXAMPLE**               | A sample command that uses the function or script, optionally followed by sample output and a description. Repeat this keyword for each example.                                                                   |
| **.INPUTS**                | The .NET types of objects that can be piped to the function or script. You can also include a description of the input objects.                                                                                    |
| **.OUTPUTS**               | The .NET type of the objects that the cmdlet returns. You can also include a description of the returned objects.                                                                                                  |
| **.NOTES**                 | Additional information about the function or script.                                                                                                                                                               |
| **.LINK**                  | The name of a related topic. The value appears on the line below the ".LINK" keyword and must be preceded by a comment symbol # or included in the comment block. Repeat the .LINK keyword for each related topic. |
| **.COMPONENT**             | The name of the technology or feature that the function or script uses, or to which it is related. The Component parameter of Get-Help uses this value to filter the search results returned by Get-Help.          |
| **.ROLE**                  | The name of the user role for the help topic. The Role parameter of Get-Help uses this value to filter the search results returned by Get-Help.                                                                    |
| **.FUNCTIONALITY**         | The keywords that describe the intended use of the function. The Functionality parameter of Get-Help uses this value to filter the search results returned by Get-Help.                                            |
| **.FORWARDHELPTARGETNAME** | Redirects to the help topic for the specified command. You can redirect users to any help topic, including help topics for a function, script, cmdlet, or provider.                                                |
| **.REMOTEHELPRUNSPACE**    | Specifies a session that contains the help topic. Enter a variable that contains a PSSession object. This keyword is used by the Export-PSSession cmdlet to find the help topics for the exported commands.        |
| **.EXTERNALHELP**          | Specifies an XML-based help file for the script or function.                                                                                                                                                       |

- [Comment Based Help](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comment_based_help?view=powershell-5.1)

#### Sample Module Function

```powershell
<#

 .Synopsis
   Prints Bye Message

 .Description
   Test command that prints Bye-Bye PowerShell World message

 .Parameter Name
   Name to say bye to

 .Example
   Stop-HelloWorld -Name Jim

 .Example
   Stop-HelloWorld Jim

#>
function Show-Name
{
    param (
        $Name
    )
    
    # Add function code here
    Write-Console "Show Name: $Name"
}

Export-ModuleMember -Function Show-Name
```

- [Hey, Scripting Guy! How Do I Add Help Information for Windows PowerShell Parameters?](https://devblogs.microsoft.com/scripting/hey-scripting-guy-how-do-i-add-help-information-for-windows-powershell-parameters/)

## Module Manifest

Module manifests, _.psd1_ files, contain information about the module. It is recommended to us a module manifest whenever possible. The module manifest will also be required to publish the module to an online gallery. Use the `New-ModuleManifest` command to generate the _.psd1_ file. Validate manifest files using the `Test-ModuleManifest` command.

See [How to write a PowerShell module manifest](https://docs.microsoft.com/en-us/powershell/scripting/developer/module/how-to-write-a-powershell-module-manifest?view=powershell-5.1)

### Example Manifest File (psd1)

``` powershell
Import-Module PowerShellGet

New-ModuleManifest -Path Example.psd1
Test-ModuleManifest
Import-Module -Name ./Example.psd1
```

```powershell
#
# Module manifest for module 'Example'
#
# Generated by: jhedges
#
# Generated on: 12/1/2021
#

@{

Script module or binary module file associated with this manifest.
RootModule = 'Example.ps1'

ModuleVersion = '0.0.1'

GUID = '60311487-1aa6-4631-a99a-4e27c97f8396'

Author = 'My Co.'

CompanyName = 'My Co.'

Copyright = '(c) 2021 My Co. All rights reserved.'

Description of the functionality provided by this module
Description = 'Example PowerShell Module'

FunctionsToExport = @('MyFunc')

# Cmdlets to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no cmdlets to export.
CmdletsToExport = @()

# Variables to export from this module
VariablesToExport = '*'

# Aliases to export from this module, for best performance, do not use wildcards and do not delete the entry, use an empty array if there are no aliases to export.
AliasesToExport = @()

# Private data to pass to the module specified in RootModule/ModuleToProcess. This may also contain a PSData hashtable with additional module metadata used by PowerShell.
PrivateData = @{

    PSData = @{

        # Tags applied to this module. These help with module discovery in online galleries.
        # Tags = @()

        # A URL to the license for this module.
        # LicenseUri = ''

        # A URL to the main website for this project.
        # ProjectUri = ''

        # A URL to an icon representing this module.
        # IconUri = ''

        # Prerelease string of this module
        Prerelease = 'beta1235'

        # ReleaseNotes of this module
        # ReleaseNotes = ''

    } # End of PSData hashtable

} # End of PrivateData hashtable

# HelpInfo URI of this module
# HelpInfoURI = ''

# Default prefix for commands exported from this module. Override the default prefix using Import-Module -Prefix.
# DefaultCommandPrefix = ''

}
```

## Module Repositories

### Setting Up The Repository

```powershell
Import-Module PowerShellGet

# Register the repository for the user, not registered system wide
Register-PSRepository -Name $repoName -SourceLocation $sourceUrl -PublishLocation $publishUrl -Installation-Policy Trusted

# If already registered repository, set it
Set-PSRepository -Name $repoName -SourceLocation $sourceUrl -PublishLocation $publishUrl -InstallationPolicy Trusted

# To see the registered repository
Get-PSRepository -Name $repoName -ErrorAction SilentlyContinue
```

## Publish Module to a Gallery

Publishing a module to a gallery requires metadata for the module name, version, description, and author. This is taken from the module's manifest file.

### Example With Artifactory

```powershell
param (
    [Parameter(Mandatory)] $version
)

Import-Module PowerShellGet

$cwRepoName = "my-ps-repository"

Write-Host "Check if Artifactory registered..."
$repo = Get-PSRepository -Name $cwRepoName -ErrorAction SilentlyContinue
if (-not($repo)) {
    Write-Host "Register Artifactory"
    Register-PSRepository -Name $cwRepoName `
        -SourceLocation "https://artifactory.xxx.net/artifactory/api/nuget/PSModules" `
        -PublishLocation "https://artifactory.xxx.net/artifactory/api/nuget/PSModules" `
        -InstallationPolicy Trusted
}

Get-PSRepository

Write-Host "Update manifest..."
$params = @{
    Path = "./MyModule/MyModule.psd1"
    ModuleVersion = $version
    Prerelease = "-beta1235"
}
Update-ModuleManifest @params

Write-Host "Publish module..."
Publish-Module -Path "./MyModule" `
    -Repository $cwRepoName `
    -NugetApiKey "user-name:#####password/api key################" `
    -Force

Write-Host "Module Published"
```

## Module Exploration

## Get-Module

## Get Module Functions

```powershell
Get-Module -Module <module-name>
```

## Function Parameters

### Cmdlet Binding

### Parameter

#### Default Value

#### Pipeline Input

```powershell
Function MyFunc {
  [CmdletBinding()]
  param (
    [Paramater(ValueFromPipelineByPropertyName)]
    $ValueFromPipeline
  )
  Write-Host "From the pipeline: $ValueFromPipeline"
}
```
