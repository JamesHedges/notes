
<!-- omit in toc -->
# PowerShell Cheat Sheet

## PowerShell Version

```powershell
$PSversionTable

#Use the automatic $PSVersionTable variable, and check the PSVersion property, for example:
$PSVersionTable.PSVersion
```

-[PowerTip: Check Version of PowerShell](https://devblogs.microsoft.com/scripting/powertip-check-version-of-powershell/)

## Execution Policy

An execution policy is part of the PowerShell security strategy. Execution policies determine whether you can load configuration files, such as your PowerShell profile, or run scripts. And, whether scripts must be digitally signed before they are run.

The Set-ExecutionPolicy cmdlet's default scope is LocalMachine, which affects everyone who uses the computer. To change the execution policy for LocalMachine, start PowerShell with Run as Administrator.

To display the execution policies for each scope in the order of precedence, use Get-ExecutionPolicy -List. To see the effective execution policy for your PowerShell session use Get-ExecutionPolicy with no parameters.

```powershell
# View execution policy
Get-ExecutionPolicy -List

# Set execution policy for local computer
Set-ExecutionPolicy -ExecutionPolicy RemoteSinged -Scope LocalMachine
```

See the documentation for more options: [Set-ExecutionPolicy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-7)

## Show Things

### List All Environment Variables

`gci env:* | Sort-Object Name`

### Split Path

`$env:Path -split ';'`

## Linux Equivelants

### Touch

`echo $null > target_file`

### Commands You Should Know

[20 Windows PowerShell Commands You Must Know](https://www.hongkiat.com/blog/windows-powershell-commands/)

## Aliases

| Alias | Commandlete   |
| :---- | :------------ |
| gci   | Get-ChildItem |

## Profile

### Six Profiles

| Description                          | Path                                                                       |
| :----------------------------------- | :------------------------------------------------------------------------- |
| Current User, Current Host – console | $Home\[My ]Documents\WindowsPowerShell\Profile.ps1                         |
| Current User, All Hosts              | $Home\[My ]Documents\Profile.ps1                                           |
| All Users, Current Host – console    | $PsHome\Microsoft.PowerShell_profile.ps1                                   |
| All Users, All Hosts                 | $PsHome\Profile.ps1                                                        |
| Current user, Current Host – ISE     | $Home\[My ]Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1 |
| All users, Current Host – ISE        | $PsHome\Microsoft.PowerShellISE_profile.ps1                                |

from [Understanding the Six PowerShell Profiles](https://devblogs.microsoft.com/scripting/understanding-the-six-powershell-profiles/)

To see what profile was applied, run `$profile`

## Utility Scripts

### Add Path Variable

```powershell
function Add-PathVariable
{
    param (
        [Parameter(Mandatory=$true)]
        [string] $addPath
    )

    if (Test-Path $addPath)
    {
        $regexAddPath = [regex]::Escape($addPath)
        $arrPath = $env:Path -split ';' | Where-Object { $_ -notmatch "^$regexAddPath\\?"}
        $env:Path = ($arrPath + $addPath) -join ';'
    }
    else {
        Throw "'$addPath' is not a valid path."
    }
}
Set-Alias Add-PathVariable AddPathVariable
```

### Remove Path Variable

```powershell
function Remove-PathVariable
{
    param (
        [Parameter(Mandatory=$true)]
        [string] $removePath
    )

    if (Test-Path $removePath)
    {
        $regexRemovePath = [regex]::Escape($removePath)
        $arrPath = $env:Path -split ';' | Where-Object { $_ -notmatch "^$regexRemovePath\\?"}
        $env:Path = ($arrPath) -join ';'
    }
    else {
        Throw "'$removePath' is not a valid path."
    }
}
Set-Alias Remove-PathVariable RemovePathVariable
```

### Set Colors

```powershell
$DefaultForeground = (Get-Host).UI.RawUI.ForegroundColor
$DefaultBackground = (Get-Host).UI.RawUI.BackgroundColor
function SetColors
{
    Param
    (
        [string]$Foreground = "",
        [string]$Background = ""
    )

    $ValidColors = "black","blue","cyan","darkblue" ,"darkcyan","darkgray",
        "darkgreen","darkmagenta","darkred","darkyellow","gray","green",
        "magenta","red","white","yellow";

    $Foreground = $Foreground.ToLower()
    $Background = $Background.ToLower()

    if ( $Foreground -eq "" )
    {
        $Foreground = $DefaultForeground
    }
    if ( $Background -eq "" )
    {
        $Background = $DefaultBackground
    }

    if ( $ValidColors -contains $Foreground -and
         $ValidColors -contains $Background )
    {
        $a = (Get-Host).UI.RawUI
        $a.ForegroundColor = $Foreground
        $a.BackgroundColor = $Background
    }
    else
    {
        write-host "Foreground/Background Colors must be one of the following:"
        $ValidColors
    }
}
set-alias set-colors SetColors
```

from [StackOverflow](https://stackoverflow.com/questions/18685772/how-to-set-powershell-background-color-programmatically-to-rgb-value
)

## Posh Git

See [posh-git](https://github.com/dahlbyk/posh-git) site for more details

### Set-up

- Ensure you have the ability to execute scripts:  
`Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Confirm`
- Must have Git installed and available on the PATH. Verify by executing  
`git --version`

### Install posh-git

Execute the following as an administrator:

- Install: `PowerShellGet\Install-Module posh-git -Scope CurrentUser -AllowPrerelease -Force`
- Update: `PowerShellGet\Update-Module posh-git`

Alternatively, use Chocolatey: `choco install poshgit`

### Install oh-my-posh

See [Oh My Posh](https://ohmyposh.dev/docs/pwsh)

```powershell
# for current user
Install-Module oh-my-posh -Scope CurrentUser

# for all users - must run as administrator
Install-Module oh-my-posh -Scope AllUsers
```

#### Nerd Fonts

Oh My Posh is designed to use [Nerd Fonts](https://www.nerdfonts.com/).

- Download the **Meslo Nerd Font** from [Nerd Font Downloads](https://www.nerdfonts.com/font-downloads)
- Go to Windows Settings --> Personalization --> Fonts
- Drag downloaded file to the drop box

![Install Windows Font](InstallWindowsFont.png)

### Create Profile

You can add posh-git to your profile for all users and hosts: `Add-PoshGitToProfile -AllUsers -AllHosts`

Alternative, edit Your profile directly.

- Locate your profile by executing `$profile` at command prompt
- Add this line to your profile

```powershell
if ($host.Name -eq 'ConsoleHost')
{
    Import-Module PSReadLine
    Import-Module oh-my-posh
    Set-PoshPrompt -Theme pure
    Import-Module 'C:\tools\poshgit\dahlbyk-posh-git-a4faccd\src\posh-git.psd1'
}
```

>You can substitute the theme of your choice. To view available themes, run PowerShell command `Get-PoshThemes`

## SSH Keys

At some point, we may need SSH keys in order to authenticate with a remote application. This can be done using PowerShell. These instructions are for setting up keys for the local machine only. See [Managing OpenSSH Keys](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement) for more details

### Install and Start Services

1. Open a PowerShell console having administrator level access
1. Install OpenSSH PowerShell module Execute  
`Install-Module -Force OpenSSHUtils -Scope AllUsers`
1. Start the service  
`Start-Service ssh-agent`
1. Start the sshd service  
`Start-Service sshd`

### Generate Keys

1. Pen a PowerShell console having administrator level access
1. Change to the .ssh folder in the home directory. Create it if it does not exist:  
`cd ~\.ssh\'
1. Generate the key. Note that key types can be [rsa | dsa | ecdsa | ed25519]. Follow the prompts:  
`ssh-keygen -t rsa -b 4096 -C "<comment like user's email>"`
    1. Enter file in which to save the key (C:/Users/[username]/.ssh/id_rsa): `Enter accepts default or enter file path`
    2. Enter passphrase (empty for not passphrase): `use passphrase to best security`

You will see something like this:

```cmd
Your public key has been saved in C:\Users\username/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:0UVeyk1xim7TDJoMOowt+ftiJK6ETmH/Us9V79noIOY username@my-domain.com
The key's randomart image is:
+---[RSA 4096]----+
|           .o +..|
|         . + * o |
|        o . * o  |
|     = . + = +   |
| o  + = S = = o  |
|..o .+.. . . o   |
|...o.o+ .o .. +  |
|o. .o o+o . .+ . |
| ....o.o.E  ..   |
+----[SHA256]-----+
```

## Base64 Encode

[Encoding a Text using Base64](https://techexpert.tips/powershell/powershell-base64-encoding/)

```powershell
# Encode
$MYTEXT = 'This is my secret text'
$ENCODED = [Convert]::ToBase64String([Text.Encoding]::Unicode.GetBytes($MYTEXT))
Write-Output $ENCODED

# Decode
$MYTEXT = 'VABoAGkAcwAgAGkAcwAgAG0AeQAgAHMAZQBjAHIAZQB0ACAAdABlAHgAdAA='
$DECODED = [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String($MYTEXT))
Write-Output $DECODED
```

## Invoke Web Request

[Invoke-WebRequest](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-webrequest?view=powershell-7.1)

```powershell
# Making the API Key Header
$apiKey = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
$headers = @{'X-JFrog-Art-Api' = $apiKey} 
# build the request url
$request = "https://artifactory.somewhere/artifactory/api/search/artifact?name=auto*&repos=3rdparty-nuget"
$response = Invoke-WebRequest -Uri $request -Headers $headers -Method Get
# Look at the result, found in the response's content
$([String]::new($response.Content))
```

### Download a File

- [Windows: PowerShell – Download File From URL](https://www.shellhacks.com/windows-powershell-download-file-from-url/)
- [Use Invoke-WebRequest with a username and password for basic authentication on the GitHub API](https://www.howtobuildsoftware.com/index.php/how-do/brhb/powershell-github-api-http-basic-authentication-use-invoke-webrequest-with-a-username-and-password-for-basic-authentication-on-the-github-api)
- [HTTP requests with PowerShell’s Invoke-WebRequest – by Example](https://davidhamann.de/2019/04/12/powershell-invoke-webrequest-by-example/)

### Find OS Version and Service Pack number from CMD

```powershell
systeminfo | findstr /B /C:"OS Name" /C:"OS Version"
```

### Compress / Expand Archive

Archive files, like zip files, can be compressed and expanded using PowerShell

- [Compress-Archive](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.archive/compress-archive?view=powershell-5.1)
- [Expand-Archive](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.archive/expand-archive?view=powershell-5.1)

```powershell

$compress = @{
  Path = "C:/source/archive-me"
  DestinationPath = "C:/source/archive-me.zip"
  CompressionLevel = fastest
}
Compress-Archive @compress

Expand-Archive -Path "C:/source/archive-me.zip" -DestinationPath "C:/target-path"
```

### Split Output Stream

The [Tee-Object](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/tee-object?view=powershell-7.1) can be used to split a commands output to a variable or file and then pass it on. Used this in GitLab pipeline script when invoking an external command.

```powershell
$myApp = "C:\Program Files (x86)\NuGet\NuGet.exe"
$myApp -someParam | Tee-Object -Variable target | Out-Null
Write-Host $target
```

### Configure Windows Features

Windows features can be configured using powershell. This example enables .NET Framework, including WCF.

```powershell
# List Features
Get-WindowsFeature

# Name of features to install
$DotNETFeatures = "NET-Framework-45-Core","NET-Framework-45-ASPNET","NET-WCF-HTTP-Activation45","WAS-Process-Model","WAS-Config-APIs"
 
# Install the features
Install-WindowsFeature -Name $DotNETFeatures
```

## Install Chocolatey

Use PowerShell to install Chocolatey with the following command [PowerShell script to install Chocolatey and a list of packages](https://stackoverflow.com/questions/48144104/powershell-script-to-install-chocolatey-and-a-list-of-packages) and here [Install with PowerShell.exe](https://stackoverflow.com/questions/48144104/powershell-script-to-install-chocolatey-and-a-list-of-packages)

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

## Regular Expressions

- [Powershell regular expressions](https://www.powershelladmin.com/wiki/Powershell_regular_expressions#The_-match_Operator_on_Collections.2FArrays)
- [About Regular Expressions](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_regular_expressions?view=powershell-7.1#:~:text=A%20regular%20expression%20is%20a%20pattern%20used%20to,syntax%20and%20usage%20at%20the%20links%20below.%20Select-String)

```powershell
$GetThis = 'SecretCode'
'foo bar baz SecretCode 42 boo bam' -match "$GetThis ([0-9]+)"
```

## Directories and Files

### Path Exists

- [How to use PowerShell to Check if a File Exists](read://https_adamtheautomator.com/?url=https%3A%2F%2Fadamtheautomator.com%2Fpowershell-check-if-file-exists%2F)

```powershell

# Test if file exists
Test-Path -Path c:/some/path/to/file.nnn -PathType Leaf

# Use in if when exists
if (Test-Path -Path c:/some/path/to/file.nnn -PathType Leaf){
  # work with existing file
}

# Use in if when exists
if (-not(Test-Path -Path c:/some/path/to/file.nnn -PathType Leaf)){
  # you need to create the file
}
```

### Create Directory

[New-Item](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/new-item?view=powershell-5.1)

```powershell
New-Item -Path . -Name new-dir-name -ItemType "directory"
```

## Using Powershell Objects

[Back to Basics: Understanding PowerShell Objects](https://adamtheautomator.com/powershell-objects/)

### Object Schema

Get the object schema using Get-Member

```powershell
Get-Service | Get-Member -MemberType Property
```

Output:

```text
   TypeName: System.ServiceProcess.ServiceController

Name                MemberType Definition
----                ---------- ----------
CanPauseAndContinue Property   bool CanPauseAndContinue {get;}
CanShutdown         Property   bool CanShutdown {get;}
CanStop             Property   bool CanStop {get;}
Container           Property   System.ComponentModel.IContainer Container {get;}
DependentServices   Property   System.ServiceProcess.ServiceController[] DependentServices {get;}
DisplayName         Property   string DisplayName {get;set;}
MachineName         Property   string MachineName {get;set;}
ServiceHandle       Property   System.Runtime.InteropServices.SafeHandle ServiceHandle {get;}
ServiceName         Property   string ServiceName {get;set;}
ServicesDependedOn  Property   System.ServiceProcess.ServiceController[] ServicesDependedOn {get;}
ServiceType         Property   System.ServiceProcess.ServiceType ServiceType {get;}
Site                Property   System.ComponentModel.ISite Site {get;set;}
StartType           Property   System.ServiceProcess.ServiceStartMode StartType {get;}
Status              Property   System.ServiceProcess.ServiceControllerStatus Status {get;}
```

### Select Members

```powershell
Get-Service -ServiceName 'Docker' | Select-Object -Property Status, StartType
```

Output:

```text
 Status StartType
 ------ ---------
Running Automatic
```

### Sort Objects

```powershell
Get-Service | Select-Object -Property 'Status', 'Name' | Sort-Object -Property 'Status', 'Name'
```

### Filter Objects

```powershell
Get-Service | Where-Object -FilterScript { $_.Status -eq 'Running' }
```

### Counting and Measuring Objects

```powershell
Get-Service * | Select-Object -Property 'Status','DisplayName' |
  Where-Object {$_.Status -eq 'Running' -and $_.DisplayName -like "Windows*" |
    Sort-Object -Property 'DisplayName' -Descending | Measure-Object
```

### For Each with Objects

```powershell
Get-ChildItem ./packages/*.symbols.nupkg -File | Foreach-Object { 
    Nuget add $_ -source ./PDFeed 
}
```

### Custom Objects

#### Create from Hashtables

```powershell
$CarHashtable = @{
  Brand = 'Ford'
  Style = "Truck'
  Model = 'F-150
}

$CarObject = [PSCustomObject]$CarHashtable
# or
$CarObject = New-Item -TypeName PsObject -Properties $CarHashtable
```

#### Add / Remove Properties

```powershell
$CarObject | Add-Member -MemberType NoteProperty -Name 'Year' -Value '2021'
$CarObject.psObject.properties.remove('NoteProperty')
```

### Create HashMap from JSON

```powershell
$jsonMap = {"dev":{"Arn":"dev-arn","Key":"dev-key"},"test":{"Arn":"test-arn","Key":"test-key"},"cert":{"Arn":"cert-arn","Key":"cert-key"},"prod":{"Arn":"prod-arn","Key":"prod-key"}}
$jsonObj = ConvertFrom -Json $jsonMap
$hashMap = @{}
foreach ($property in $jsonObj.PSObject.Properties) {
  $hashMap[$property.Name] = $property.Value
}

$hashMap

# Displays:
# Name                           Value
# ----                           -----
# prod                           @{Arn=prod-arn; Key=prod-key}
# cert                           @{Arn=cert-arn; Key=cert-key}
# dev                            @{Arn=dev-arn; Key=dev-key}
# test                           @{Arn=test-arn; Key=test-key}

```

See [Convert JSON to a PowerShell hash table](https://4sysops.com/archives/convert-json-to-a-powershell-hash-table/)

## Building Modules

See the [PowerShell Modules Cheat Sheet](./PowerShell-Modules-Cheat-Sheet) for details.

## AWS

- [Installing the AWS Tools for PowerShell on Windows](https://docs.aws.amazon.com/powershell/latest/userguide/pstools-getting-set-up-windows.html)
- [AWS Powershell Cmdlets Documentation](https://docs.aws.amazon.com/powershell/latest/reference/Index.html)
- [How to uninstall AWSPowerShell module from Powershell?](https://stackoverflow.com/questions/68519677/how-to-uninstall-awspowershell-module-from-powershell)
- [Deploying Docker containers on ECS](https://docs.docker.com/cloud/ecs-integration)
- [Deploy applications on Amazon ECS using Docker Compose](https://aws.amazon.com/blogs/containers/deploy-applications-on-amazon-ecs-using-docker-compose)

## Filtered Search with PowerShell

```powershell
$search = @(
  @{
     key = 'Name'
     values = 'commonwell/dev'
   }
)

Get-SECSecretList -Filter $search  | Select-Object -Property Name, ARN
```

## Certificates

- [Managing Windows PFX certificates through PowerShell](https://dev.to/iamthecarisma/managing-windows-pfx-certificates-through-powershell-3pj)

## Get Disk Space

```powershell
Get-Volume -DriveLetter C
```

## Event Logs

To view logs, use the **Get-EventLog** command:

```powershell
Get-EventLog -LogName Application | Where-Object {$_.Source -eq "AWSOTelCollector"}
```

- See: [Get-EventLog](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-eventlog?view=powershell-5.1) for detailed description.

## Scheduled Tasks

Create scheduled tasks to perform system activity. For example, to clean-up docker images, run a script that performs `docker image prune`.

First, create a scheduled task.

```powershell
## Create an action that runs the PowerShell script
$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument 'c:\src\Powershell\ScheduleTask\MyScheduledTask.ps1'
## For unattended tasks, use the 'NT Authority/System' user
$principal = New-ScheduledTaskPrincipal -UserId "System" -RunLevel Highest
## Set when the task will run
$trigger = New-ScheduledTaskTrigger -Daily -At 10:12
## Create default settings (the compatibility is optional, Win8 will target anything Win8 and later)
$settings = New-ScheduledTaskSettingsSet -Compatibility Win8
## Create the task (it will not be registered)
$task = New-ScheduledTask -Action $action -Principal $principal -Trigger $trigger -Settings $settings
## Register the task. Use the task path to group related tasks
Register-ScheduledTask -TaskPath '\cleaner\' -TaskName 'task-test-1' -InputObject $task
```

See [ScheduledTasks](https://learn.microsoft.com/en-us/powershell/module/scheduledtasks/?view=windowsserver2019-ps)
