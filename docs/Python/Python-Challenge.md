# Python Challenge

Denver Python Challenge

## Challenge Zero - [Setting Up Your Environment](./Challenge-Zero)

## Challenge One - [Some Basics](./Challenge-One)
### Code Structure
### Variable and Types
### Collections
 
## Challenge Two - [Moving Beyond Basics](./Challenge-Two)
### More Data Types
### Iterating
### Truth Value Testing
### Flow Control Statements
### More Functions
### Classes


## Challenge Three - [Gotta Have Class](./Challenge-Three)
