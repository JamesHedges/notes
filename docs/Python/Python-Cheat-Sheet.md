<!-- omit in toc -->
# Python Cheat Sheet

## Documentation

- [Python 3.9.7 documentation](https://docs.python.org/3/reference/index.html)
- [The Python Language Reference](https://docs.python.org/3/reference/index.html)
- [The Python Standard Library](https://docs.python.org/3/howto/)
- [Python HowTos](https://docs.python.org/3/howto/)
- [Python Module Index](https://docs.python.org/3/py-modindex.html)
- [General Index](https://docs.python.org/3/genindex.html)

## Installing Python

Python and pip will need to be installed. Python will be used for some of the scripting needs and pip is the installer for Python libraries.

Download and install from [Python Download Page](https://www.python.org/downloads/windows/) or by using Chocolatey:

- [Chocolatey Python](https://community.chocolatey.org/packages/python/)
 [PYTHON 3.X 3.10.0-A5](https://chocolatey.org/packages/python3)

```powershell
# install Python when not already installed, see the installer link above 
# on how to install to a specific folder
choco install python3 --pre
```

### PIP Install

- [A non-magical introduction to Pip and Virtualenv for Python beginners](https://www.dabapps.com/blog/introduction-to-pip-and-virtualenv-python/)

Install the Python package manager

```powershell
python -m pip install --upgrade pip
```

### Poetry

- [Poetry Docs](https://python-poetry.org/docs/)
- [Poetry Installation](https://python-poetry.org/docs/#installation)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Basics

### Dunder

Dunder is short for _Double Underscore_. Any name surrounded with double underscores are [system defined names](https://docs.python.org/3/reference/datamodel.html#basic-customization)

### Main Scope

When a Python file is executed, the module's name scope, `__name__`, is set to _'\_\_main\_\_'_. This indicates it is in the _main_ scope and can be used to conditionally execute the entry function. When a file is imported, the module's `__name__` is set to the name of the file. The practice for the _main_ file is to add a check for the module name and conditionally execute the entry function.

```python
import sys

def Main() -> int:
    print ("This is the main function, start here")
    return 0

if __name__ == "__main__":
    # only execute this if file was executed
    sys.exit(Main())
```

Note that `sys.exit()` is being used to return the success/fail code of the main module, where success is **0**. For failure codes, `sys.exit()` will raise an exception.

### Exception Handling

```python
def myFunc():
    try:
        doSomething()
    except Exception as ex:
        print(f'The greedy exception handler says says: {ex}')
    else:
        trySomethingElse()
    finally:
        doSomeCleanup()

    return 'That was fun'
```

### Ternary Operator

The python ternary operator has this form: `[on_true] if [expression] else [on_false]`

```python
testVar = True

var testResult = "Is True" if testVar else "Is False"

print (f'testResult is {testResult}')
# prints True
```

### Getting Help

Help with built in functions: `help("builtins")`

## PyInstaller

An application can be compiled into an executable using [PyInstaller](https://pyinstaller.readthedocs.io/en/stable/). Use `pip install pyinstaller` to install this tool. There other tools that for this task

[PyInstall Home Page](https://www.pyinstaller.org/)

When a build complains about a missing module, include it as a hidden import in the installer command: `pyinstaller -D --clean --hidden-import gitlab.v4.objects -n TagRelease main.py`

## Virtual Environment

- [Virtual Environments](https://docs.python.org/3/using/windows.html#virtual-environments)

If packages are loaded at a global level, applications expecting different versions of the package will may no longer work as expected. It is best then to isolate an application's environment.  The virtual environment will isolate installed packages for that application and the version of Python used for the application.

### Install

```python
pip install virualenv
```

### Create Environment

1. Create project folder
2. Switch to project folder
3. Execute `virtualenv env`
4. Add the _env_ folder to .gitignore file

### Activate Environment

```powershell
env/Scripts/activate
```

## GitLab Module

- [Python Gitlab Docs](https://python-gitlab.readthedocs.io/)

Basic use:

```python
import gitlab.v4

gl = gitlab.Gitlab('path/to/repo/folder', private_token='your GitLab access token')
project = gl.projects.get('project ID')
```

## Git Module

- [GitPython Docs](https://gitpython.readthedocs.io/)
- [Git Use Examples](https://github.com/gitpython-developers/GitPython/blob/main/test/test_docs.py)
- [Getting Started with GitPython](https://azzamsa.com/n/gitpython-intro/)
- [Display help message with python argparse when script is called without any arguments](https://stackoverflow.com/questions/4042452/display-help-message-with-python-argparse-when-script-is-called-without-any-argu)

Basic use:

```python
import git
from pathlib import Path

gitFolder = Path('path/to/repo/folder')
repo = git.Repo(gitFolder)

print (f"Untracked file: {repo.untracked_files}")

# If repo does not have the functionality needed, git binary can be executed using the git command
gitCmd = repo.git
print(gitCmd.status())
print(gitCmd.branch())
print (repo.remote().url)
```

### JSON Module

- [Single versus double quotes in json loads in Python](https://newbedev.com/single-versus-double-quotes-in-json-loads-in-python)

## AWS Module

- [Boto3 documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)

## Handling Arguments

- [argparse module](https://docs.python.org/3/library/argparse.html?highlight=argparse)
- [Argparse Tutorial](https://docs.python.org/3/howto/argparse.html?highlight=argparse)

### Example Argument Parser Config

```python
import argparse
import sys
import os

COMMAND_SHOWACCESSKEYS = "ShowAccessKeys"
COMMAND_SHOWACCESSKEYLASTUSED = "ShowAccessKeyLastUsed"
COMMAND_SETACCESSKEYSTATUS = "SetAccessKeyStatus"

def parseCmdLine(commandFuncs) -> argparse.Namespace:
    commandParser = argparse.ArgumentParser()
    if len(sys.argv) == 1:
        commandParser.print_help(sys.stderr)
        sys.exit(1)
    commandParser = BuildNestedArgParse(commandParser, commandFuncs)

    return commandParser.parse_args(sys.argv[1:])

def BuildNestedArgParse(parser, commandFuncs) -> argparse.ArgumentParser:
    subparsers = parser.add_subparsers(dest="command", help="Commands")

    # Main ShowAccessKeys --profile cw-dev --user-nameCommonWell-EDAPublisher
    if (COMMAND_SHOWACCESSKEYS in commandFuncs):
        showKeysParser = subparsers.add_parser(COMMAND_SHOWACCESSKEYS, help="Show a user's access keys")
        showKeysParser.add_argument("-p", "--profile", action="store", dest="profile", required=False, default='', help="AWS profile name. Default is taken from environment variable AWS_PROFILE.")
        showKeysParser.add_argument("-u", "--user-name", action="store", dest="userName", required=True, help="Name of user")
        showKeysParser.set_defaults(func=commandFuncs[COMMAND_SHOWACCESSKEYS])

    # Main ShowAccessKeyLastUsed --profile 'cw-dev' --user-name 'CommonWell-EDAPublisher' --keyId 'AKIAXUKFVSWGZBO3HYNV'
    if (COMMAND_SHOWACCESSKEYLASTUSED in commandFuncs):
        showKeysParser = subparsers.add_parser(COMMAND_SHOWACCESSKEYLASTUSED, help="Show when a key was last used")
        showKeysParser.add_argument("-p", "--profile", action="store", dest="profile", required=False, default='', help="AWS profile name. Default is taken from environment variable AWS_PROFILE.")
        showKeysParser.add_argument("-u", "--user-name", action="store", dest="userName", required=True, help="Name of user")
        showKeysParser.add_argument("-s", "--secret_id", action="store", dest="secretId", required=True, help="Secret's ID (ARN)")
        showKeysParser.set_defaults(func=commandFuncs[COMMAND_SHOWACCESSKEYLASTUSED])

    if (COMMAND_SETACCESSKEYSTATUS in commandFuncs):
        statusKeysParser = subparsers.add_parser(COMMAND_SETACCESSKEYSTATUS, help="Set a user's access key status")
        statusKeysParser.add_argument("-p", "--profile", action="store", dest="profile", required=False, default='', help="AWS profile name. Default is taken from environment variable AWS_PROFILE.")
        statusKeysParser.add_argument("-u", "--user-name", action="store", dest="userName", required=True, help="Name of user")
        statusKeysParser.add_argument("-k", "--access-key-id", action="store", dest="accessKeyId", required=True, help="Name of access key to update")
        statusGrp = statusKeysParser.add_mutually_exclusive_group(required=True)
        statusGrp.add_argument("-a", "--active", action="store_true", dest="accessKeyStatus", help="Set access key status to active")
        statusGrp.add_argument("-i", "--inactive", action="store_false", dest="accessKeyStatus", help="Set access key status to inactive")

        statusKeysParser.set_defaults(func=commandFuncs[COMMAND_SETACCESSKEYSTATUS])

        return parser
```

### Example Using the Parser

```python
import sys
import ArgumentsProcessor as argsProc

def ShowUserAccessKeysHandler(command):
    pass

def ShowAccessKeyLastUsedHandler(command):
    pass
    
def SetAccessKeyStatusHandler(command):
    pass

def Main():
    
    try:
        commandFuncs = { 
            argsProc.COMMAND_SHOWACCESSKEYS: ShowUserAccessKeysHandler, 
            argsProc.COMMAND_SHOWACCESSKEYLASTUSED: ShowAccessKeyLastUsedHandler, 
            argsProc.COMMAND_SETACCESSKEYSTATUS: SetAccessKeyStatusHandler, 
        }
        command = argsProc.parseCmdLine(commandFuncs)
        command.func(command)
    except Exception as ex:
        print (f'Command {command.command} encountered an error: {ex}')
        return -1

if (__name__ == "__main__"):
    sys.exit(Main())
```

## Object Oriented

### Classes

See [Class Definitions](https://docs.python.org/3.9/reference/compound_stmts.html#class-definitions) reference for more details.

- Declaration: `class class_name(<base_class_name>):`
- Constructor: `__init__(self, ...):`
- Getter: Decorate with `@property`
- Setter: Decorate with `@property.setter`
- Private Variable: name starts with double underscore, e.g. `__my_private_var`
- Public Method: first param is _self_. e.g. `def public_method_name(self, ...):`
- Private Method: name starts with single underscore and first param is _self_. e.g. `def _private_method_name(self, ...):`
- ToString Method: `def __str__(self):`
- Representation - printable representation of method: `def __repr__(self):`
- Length: `def __len__(self):`
- [Alternative constructor](https://docs.python.org/3/library/functions.html#classmethod): `@classmethod`
- [Static method](https://docs.python.org/3/library/functions.html#staticmethod): `@staticamethod`

```python
from decimal import Decimal


class Account():
    def __init__(self, name, balance):
        if balance < Decimal('0.00'):
            raise ValueError('Initial balance must be greater than zero.')
        self.__name = name
        self.__balance = balance
    
    @classmethod
    def jim_constructor(cls, balance):
        jim = cls('Jim', balance)
        return jim

    def __repr__(self):
        return f'Account("{self.name}")'

    def __str__(self):
        return self.name

    def deposit(self, amount):
        if amount < Decimal('0.00'):
            raise ValueError('Deposit amount must be greater than zero')

        self.__balance += amount

    @property
    def balance(self):
        return self.__balance

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        self.__name = new_name
    
    @staticmethod
    def calc_monthly_payment(amount, rate, months):
        payment = 1 #use amount, rate, months to calc payment
        return payment
```

### Testing Classes

When using PyTest, test classes may be used to organize them. It features the ability to set-up ant teardown classes and methods

#### Test Class

A test class is indicated by prepending the class name with 'Test". In addition, the test class must **not** include an `__init__()` function.

```python
import pytest

class TestClass:
    def test_given_when_then():
        ''' test x given y when z then result '''
        assert true
```

#### Test Class Set-Up / Teardown

Test class [set-up and teardown](https://docs.pytest.org/en/7.1.x/how-to/xunit_setup.html#class-level-setup-teardown) is accomplished by adding the class methods `setup_class` and `teardown_class` methods. Each class includes the `cls` (class name) param and included the `@classmethod` decorator.

```python
import pytest

class TestClass:
    ''' Test Class'''
    
    @classmethod
    def setup_class(cls):
        ''' Add TestClass set-up here'''
        print('\nTestClass set-up')

    @classmethod
    def teardown_class(cls):
        ''' Add TestClass teardown here '''
        print('\nTestClass teardown')
```

#### Test Class Method Set-Up / Teardown

Test class method [set-up and teardown](https://docs.pytest.org/en/7.1.x/how-to/xunit_setup.html#method-and-function-level-setup-teardown) is accomplished by adding the class methods `setup_method` and `teardown_method` methods. Each class includes the `method` (method reference) param.

```python
import pytest

class TestClass:
    ''' Test Class'''
    
    def setup_method(method):
        ''' Add Test method set-up here'''
        print('\nTest method set-up')

    def teardown_method(method):
        ''' Add Test method teardown here '''
        print('\nTest method teardown')
```

#### Test Class Tests

Each test is designated by prepending the method name with `test`. Assertions

```python
import pytest

class TestClass:
    def test_given_when_then():
        ''' test x given y when z then result '''
        assert true
```

##### Assertions

Use the standard Python assert in tests. The syntax is `assert <expression>`.

[How to write and report assertions in tests](https://docs.pytest.org/en/7.1.x/how-to/assert.html)
[Python assert statement](https://docs.python.org/3/reference/simple_stmts.html#the-assert-statement)

##### Assertions for Exceptions

When testing for exceptions, use the `with pytest.raises(<exception_name>)`

```python
    with pytest.raises(Exception):
        f(x)
```

#### Example Test Class

Below is an example test class demonstrating the basic concepts described above.

```python
""" My Module """


from decimal import Decimal
import pytest

from oo_python.account import Account


@pytest.fixture()
def fixture_test():
    print("Set-up the tests")


class TestAccount:
    """ Test the Account class"""

    # def __init__(self):
    #     self._msg = "In init"
    #     print(self._msg)

    @classmethod
    def setup_class(cls):
        '''
        this is a class setup
        '''
        print(f'\nsetup_class: {cls}')

    @classmethod
    def teardown_class(cls):
        '''
        this is a class teardown
        '''
        print(f'\nteardown_class: {cls}')

    def setup_method(self, method):
        '''
        this is a method setup
        '''
        print(f'\nsetup_method: {method}')

    def teardown_method(self, method):
        '''
        this is a method teardown
        '''
        print(f'\nteardown_method: {method}')

    def test_balance_greater_than_zero(self):
        """An account must have a balance greater than zero"""
        print("\nTesting balance greater than zero")
        test_balance = Decimal('500.00')
        test_name = "Test Account"
        acct = Account(test_name, test_balance)

        assert acct.balance == test_balance

    def test_balance_greater_zero_raises_exception(self):
        """When an account is set with a balance of zero or less, an exception is raised"""
        print("\nTesting balance zero or less raises exception")
        test_balance = Decimal('-0.01')
        test_name = "Test Account"

        with pytest.raises(ValueError):
            _ = Account(test_name, test_balance)
```

#### Running PyTest

Before running the tests, PyTest will need to be installed. Used the command:

```python
pip install -u pytest
```

Run the tests from the command line. For details on the commands and options, set [Command-line Flags](https://docs.pytest.org/en/7.1.x/reference/reference.html#command-line-flags) in the PyTest reference.

```python
pytest -A #Run with all output
pytest tests -A #Run all tests found in the ./Tests folder with all output

# When using poetry
poetry run pytest -A
poetry run pytest testes -A

```

## Converting Objects

### Class to Dictionary

- [Python object and dictionary conversion](https://kiennt.com/blog/2012/06/14/python-object-and-dictionary-convertion.html)

```python
class Struct(object):
    def __init__(self, adict):
        self.__dict__.update(adict)
        # for k, v in adict.items():
        #     if isinstance(v, dict):
        #         self.__dict__[k] = Struct(v)
    
    def properties(self):
        return  [method_name for method_name in dir(self) if not(callable(getattr(self, method_name)))]

    def methods(self):
        return [method_name for method_name in dir(self) if callable(getattr(self, method_name))]

def get_object(adict):
    return Struct(adict)
```

## Iterating

- [How to Iterate Through a Dictionary in Python](https://realpython.com/iterate-through-dictionary-python/)

## Packaging

[The Hitchhiker's Guid to Packaging](https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/quickstart.html)

## VS Code

### Installs

- Install AWS CLI
- Install AWS SAM CLI
- Install VS Code Extensions:
  - [AWS Toolkit](https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/setup-toolkit.html)
  - Black Formatter (Microsoft)
  - Flake8 (Microsoft)
  - ISort (Microsoft)
  - Docker (Microsoft)
  - Pylance (Microsoft)
  - Python (Microsoft)
- Poetry

### Use Git Bash Terminal

Use the Git Bash terminal to run make file. To start a bash shell:

1. Start a new terminal if one is not running
2. On the right, next to the '+', click the dropdown
3. Select Git Bash shell
4. If having issues with AWS authenticating, add environment variable ```export AWS_PROFILE='ecdr-dev'```

>You must have Git Bash installed for this to work. Installed with the Git for Windows application.

## AWS Credentials

VS Code / AWS Extension / SAM CLI can be finicky about credential. Make sure there are no repeated sections and the config and credentials files a synched.

[Unable to Parse Config File Error in AWS CLI [Solved]](https://bobbyhadz.com/blog/aws-cli-unable-parse-config-file-credentials)

## AWS Development

### Commands

Use ctrl-shift-p to get the commands drop-list.

See the SAM debug configuration: **Edit SAM Debug Configuration**

## Configuring and using Poetry

### Set Python Version

Set path to python (if not using default): ```poetry env use /full/path/to/python```  
e.g. ```poetry env use 'C:\Program Files\Python38\python.exe'```

In pyproject.toml

```yaml
[tool.poetry.dependencies]
python = "~3.8"
...

[tool.mypy]
python_version - "3.8"
....
```

### Lambda Powertools

[AWS Lambd Powertools for Python](https://awslabs.github.io/aws-lambda-powertools-python/)

## Artifactory

## Failing Installs

If you get the following error, then you likely have an issue with authenticating Artifactory requests.

```text
  • Installing cdr-utils (0.1.413)

  UnicodeEncodeError

  'latin-1' codec can't encode characters in position 0-7: ordinal not in range(256)

  at ~\AppData\Roaming\pypoetry\venv\lib\site-packages\requests\auth.py:60 in _basic_auth_str
       56│     if isinstance(username, str):
       57│         username = username.encode("latin1")
       58│
       59│     if isinstance(password, str):
    →  60│         password = password.encode("latin1")
       61│
       62│     authstr = "Basic " + to_native_string(
       63│         b64encode(b":".join((username, password))).strip()
       64│     )
```

 You will need to:

 1. Get our Artifactory user name
 2. Get your Artifactory token
 3. Get the name of the Artifactory tool source - found in the yproject.toml under ```[[tool.poetry.source]]```
 4. Run ```poetry config http-basic.ch <user-name> <token>```

## Make Files

### Install Gnu Make

Use the Gnu Make utility for make files. Install using Chocolatey: `choco install -U make`

