# Python Set-Up

## VS Code

### Installs

- Install AWS CLI
- Install AWS SAM CLI
- Install VS Code Extensions:
  - [AWS Toolkit](https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/setup-toolkit.html)
  - Black Formatter (Microsoft)
  - Flake8 (Microsoft)
  - ISort (Microsoft)
  - Docker (Microsoft)
  - Pylance (Microsoft)
  - Python (Microsoft)
- Poetry

### Use Git Bash Terminal

Use the Git Bash terminal to run make file. To start a bash shell:

1. Start a new terminal if one is not running
2. On the right, next to the '+', click the dropdown
3. Select Git Bash shell
4. If having issues with AWS authenticating, add environment variable ```export AWS_PROFILE='ecdr-dev'```

>You must have Git Bash installed for this to work. Installed with the Git for Windows application.

## AWS Credentials

VS Code / AWS Extension / SAM CLI can be finicky about credential. Make sure there are no repeated sections and the config and credentials files a synched.

[Unable to Parse Config File Error in AWS CLI [Solved]](https://bobbyhadz.com/blog/aws-cli-unable-parse-config-file-credentials)

## AWS Development

### Commands

Use ctrl-shift-p to get the commands drop-list.

See the SAM debug configuration: **Edit SAM Debug Configuration**

## Poetry

### Set Python Version

Set path to python (if not using default): ```poetry env use /full/path/to/python```  
e.g. ```poetry env use 'C:\Program Files\Python38\python.exe'```

In pyproject.toml

```yaml
[tool.poetry.dependencies]
python = "~3.8"
...

[tool.mypy]
python_version - "3.8"
....
```

## Artifactory

## Failing Installs

If you get the following error, then you likely have an issue with authenticating Artifactory requests.

```text
  • Installing cdr-utils (0.1.413)

  UnicodeEncodeError

  'latin-1' codec can't encode characters in position 0-7: ordinal not in range(256)

  at ~\AppData\Roaming\pypoetry\venv\lib\site-packages\requests\auth.py:60 in _basic_auth_str
       56│     if isinstance(username, str):
       57│         username = username.encode("latin1")
       58│
       59│     if isinstance(password, str):
    →  60│         password = password.encode("latin1")
       61│
       62│     authstr = "Basic " + to_native_string(
       63│         b64encode(b":".join((username, password))).strip()
       64│     )
```

 You will need to:

 1. Get our Artifactory user name
 2. Get your Artifactory token
 3. Get the name of the Artifactory tool source - found in the yproject.toml under ```[[tool.poetry.source]]```
 4. Run ```poetry config http-basic.ch <user-name> <token>```

## Using dev containers

Using Visual Studio Code (VS Code) with remote containers for Python development is a powerful way to create a consistent and isolated development environment. Here’s a quick guide to get you started:

### Setting up

1. **Install Docker**: Ensure Docker is installed on your machine. You can use Docker Desktop for Windows/macOS or Docker CE/EE for Linux¹.
2. **Install VS Code**: Download and install Visual Studio Code.
3. **Install the Dev Containers Extension**: In VS Code, go to the Extensions view (`Ctrl+Shift+X`), search for "Dev Containers," and install it¹.

### Creating a dev container

1. **Open Your Project**: Open your existing Python project in VS Code.
2. **Add Dev Container Configuration**: Press `Ctrl+Shift+P` to open the Command Palette, then type `Remote-Containers: Add Development Container Configuration Files`. Select a Python environment (e.g., Python 3)².
3. **Customize Your Container**: This will create a `.devcontainer` folder with a `devcontainer.json` file. You can customize this file to include specific tools and dependencies your project needs¹.

### Working in the container

1. **Open Folder in Container**: Use the Command Palette (`Ctrl+Shift+P`) and select `Remote-Containers: Open Folder in Container`. Choose your project folder².
2. **Develop as Usual**: Once the container is built and running, you can develop your Python code as if you were working locally. You’ll have access to all VS Code features like IntelliSense, debugging, and extensions³.

### Benefits

- **Consistency**: Ensures all team members have the same development environment.
- **Isolation**: Keeps your local machine clean and avoids dependency conflicts.
- **Portability**: Easily share your development setup with others.

¹ [Developing inside a Container using Visual Studio Code Remote Development](https://code.visualstudio.com/docs/devcontainers/containers)  
² [VSCode Remote Containers - Qiita](https://qiita.com/I_s/items/6faa45327e89ed1d6531)  
³ [Remote Python Development in Visual Studio Code](https://devblogs.microsoft.com/python/remote-python-development-in-visual-studio-code/)

Source: Conversation with Copilot, 10/22/2024

1. [Developing inside a Container using Visual Studio Code Remote Development](https://code.visualstudio.com/docs/devcontainers/containers)
2. [VSCode Remote Containers - Qiita](https://qiita.com/I_s/items/6faa45327e89ed1d6531)
3. [Remote Python Development in Visual Studio Code - Python](https://devblogs.microsoft.com/python/remote-python-development-in-visual-studio-code/)
