# Python Object-Oriented Programming

Fourth Edition

by Steven F. Lott & Dusty Phillips
Published by Packt Publishing

Build robust and maintainable object-oriented Python applications and libraries

## Object-Oriented Design

## Objects in Python

## When Objects are Alike

## Expecting the Unexpected

## When to Use Object-Oriented Programming

