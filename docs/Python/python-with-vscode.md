# Python with VS Code

## Dependency Management

Reference: [Use Poetry for Python dependency management with Visual Studio Code]((https://crapts.org/2020/05/11/use-poetry-for-python-dependency-management-with-visual-studio-code/))

You will need to have first configured the Poetry virtual environment

1. Set Python Virtual Environment Path
   1. Go to settings (**ctrl+,**)
   2. Search `python:venv`
   3. Set Python: Venv Path to `~\AppData\Local\pypoetry\Cache\virtualenvs` (verify your exact path)
2. Use **ctrl+shift+P** and search for `python:select interpreter`
   1. Select your projects virtual environment

### Development Dependencies

Poetry has the ability to configure the local (dev) environment dependencies. This is helpful when installing development tools. Use the command `poetry add --group dev <tools>` to install to the dev environment.

```bash
# Install mypy into the virtual environment
poetry add --group dev mypy
```

## Debugging

With the dependencies set, configure you launch settings. When using a module, set the _launch.json_ config as follows. This assumes you have a module named jh_test_lambda with the file _\_\_main\_\_.py_ that will run the module.

To add a launch file

- Use _ctl+shift+p_ to activate menu
- Enter _debug: add configuration_ and select then entry
- To debug the project, select _Module_ to invoke Python with the **-m** parameter
- Enter the module name. This will be the folder containing the application's source code
- The launch.json file will be added to the _.vscode_ folder

The generated file will look similar to the below snippet. Add the _env_ section for environment variables. Add the **AWS_PROFILE** to define the AWS profile to use when debugging and any additional variables the application may expect.

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Module",
            "type": "python",
            "request": "launch",
            "module": "my_service_name",
            "console": "integratedTerminal",
            "justMyCode": true
            "env": {
                "AWS_PROFILE": "test-dev",
                "POWERTOOLS_SERVICE_NAME": "MyServiceName"
            }
        }
    ]
}
```

#### Poetry Environment Settings

Add the file _./.vscode/settings.json_ having the following content:

```json
{
    "python.poetryPath": "~/AppData/Local/pypoetry/Cache/virtualenvs/jh-test-lambda-7F6_vWjQ-py3.9",
    "python.defaultInterpreterPath": "~/AppData/Local/pypoetry/Cache/virtualenvs/jh-test-lambda-7F6_vWjQ-py3.9/Scripts/python.exe"
}
```

Or, complete the following steps:

1. Set Python Virtual Environment Path
   1. Go to settings (**ctrl+,**)
   2. Search `python:venv`
   3. Set Python: Venv Path to `~/AppData/Local/pypoetry/Cache/virtualenvs/` (verify your exact path)
2. Use **ctrl+shift+P** and search for `python:select interpreter`
   1. Select your projects virtual environment (e.g. `jh-test-lambda-7F6_vWjQ-py3.9`)
