# Serveless

## Functions as a Service (Faas)

### Lambda Pinball

Serverless Paradox: Abstracting away the runtime is supposed to relieve you from infrastructural concerns and let you focus on the business logic, but instead the pinball machine of lambdas, buckets and queues yields an anemic domain.

![Lambda Pinball](LambdaPinball.png)
