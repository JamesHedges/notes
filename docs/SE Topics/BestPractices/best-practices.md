# Software Engineering Best Practices

## Coupling

!!! Quote
    _"As you navigate the landscape of software architecture, remember that coupling is a multifaceted issue. It’s not enough to focus on just one aspect; you must consider how changes in one area can ripple through the entire system. By taking a holistic view of coupling, you can make informed decisions that lead to more resilient and maintainable systems."_

    [You're Not as Loosely Coupled as You Think](https://codeopinion.com/youre-not-as-loosely-coupled-as-you-think/)

### Forms of coupling

- Temporal
- Schema
- Database
- Location
- Technology

## Excessive configuration

code becomes:

- Overly complex and challenging to understand
- Creates complex interactions
- Non-deterministic code
- Stateful code
- Difficult to set up
- Challenging tests because they're:
    - Fragile due to reliance on configs - configs increases test cases
    - Reduced clarity in tests
    - Encumbered with mocking and stubbing

When configuration becomes a DSL, you have gone too far

### Using sidecars to handle configurations

Yes, sidecar containers in Kubernetes can effectively handle configurations! Here’s how they can be utilized for this purpose:

1. **Separation of Concerns**: Sidecars allow you to separate configuration management from your main application logic. This means you can update configuration settings without modifying the application itself, making deployments smoother and less error-prone[1](https://www.groundcover.com/blog/kubernetes-sidecar).

2. **Dynamic Configuration**: A sidecar can host configuration data that the main application reads at startup. This setup enables you to change configurations dynamically, which is particularly useful in microservices architectures where services may need to adapt to different environments or conditions[2](https://spacelift.io/blog/kubernetes-sidecar-container).

3. **Configuration Management Tools**: Sidecars can run configuration management tools or agents that fetch and apply configurations from external sources (like a configuration server or a secrets management system). This allows for centralized management of configurations across multiple instances of your application[3](https://kubernetes.io/docs/tutorials/configuration/pod-sidecar-containers/).

4. **Monitoring and Logging**: Sidecars can also handle logging and monitoring of configuration changes, providing insights into how configurations affect application behavior over time[2](https://spacelift.io/blog/kubernetes-sidecar-container).

Using sidecars for configuration management can enhance the flexibility and maintainability of your applications in Kubernetes. Do you have a specific use case in mind where you’re considering using sidecars for configuration?

## The art of destroying software

[The Art of Destroying Software](https://www.youtube.com/watch?v=1FPsJ-if2RU) by Greg Young

### Transcript

okay guys let's get started what I'm going to be talking about today
0:08
is a bit different than most of the talks at conferences how many of you have been to a talk about writing code
0:15
how many been to talk about refactoring code ours we're going to learn most people don't refactor they refactor
0:23
I want to talk to you guys about deleting code how many have been to a talk before about deleting code
0:30
oh actually apparently there is one I've never seen one before how many have put through a pull request
0:37
that removed more code than it put in and didn't you feel good
0:43
I spent my entire morning today actually building up an entire slide deck
0:49
one of the beautiful things about deleting code is it allows you to change your mind and I actually changed my mind
0:55
today about using my entire slide deck I literally built 40 slides and decide I'm not going to use them
1:01
so you're only going to get this one and this one
1:06
does anybody know where this comes from
1:12
and I really don't believe many of you are doing octal patches these days
1:17
this is actually from a waterfall paper and the waterfall paper is one of the greatest ironies in our industry
1:25
how many have heard of waterfall before how many have actually read the paper
1:30
one hand two hands oh sorry didn't see you cassia
1:38
the waterfall paper is one of the biggest ironies in our industry so if you go and read the waterfall
1:43
paper and I really recommend people to do it it basically describes what you know as agile
1:49
throughout the paper the very first page describes what you currently call waterfall
1:56
and this is what happens when you turn from the first page to the second what it basically States is that in his
2:02
heart he truly believes that waterfall is the optimal way of building software
2:07
the only problem is it doesn't actually work a paper that's like this
2:14
and it really gets into what we're going to be talking about today heard of the big ball of mud
2:22
yeah it's a nasty thing you don't want it how many have actually read the big ball of mud paper
2:28
oh again we have like three or four what's interesting is a big ball of mud paper basically argues a big ball of mud
2:34
is inevitable in fact it's optimal you will be stuck with a big ball of mud
2:39
due to the economics of software if you are not ending up with a big ball of mud perhaps you work in the Craftsman
2:45
industry um you're basically gold plating outhouses
2:51
the big ball of mud what it basically starts talking about is you need to go through and you need
2:56
to start making small Pockets inside of your ball of mud you cannot deal with one giant ball of mud you have to use
3:02
many many little balls of mud and this leads you towards the concept
3:07
of writing code for the purpose of deleting it
3:13
the idea is I can walk into any one of these areas of code and I can burn it to the ground when I don't like it anymore
3:19
how many have had to put a feature into software before where your current model didn't quite work well with it
3:29
however that current model is Tangled throughout everything and you end up spending two weeks for something that
3:35
would have been really easy if you could have changed the model but you can't
3:40
because you have no idea what this may affect what if you were to optimize from the
3:46
very beginning to be able to delete code
3:52
this is not a New Concept I really learned this lesson when I got
3:57
into erling when you build a system in erling you conceive the system as a series of very
4:03
small programs and you'll find if you start working in erlang you almost never
4:10
go back and change code if I have a feature that I need to put into one of these small programs I
4:16
normally rewrite the program this is a wonderful thing to have
4:22
how many of you are afraid to delete code in your system
4:28
maybe you have some unit tests around it but unit tests they only show that the tests pass they don't show a
4:34
lack of bugs how many have had a system that was out there that you actually had people
4:40
relying on your bugs Microsoft is wonderful for this
4:48
they literally have bugs that are currently backwards compatible from Windows 3.
4:53
but they can't get rid of the bug because people depend on it what's interesting for me is when we
5:01
start getting into erlang code it's completely different than the code you guys are used to working with
5:08
and erlang is becoming the hot new thing and no I don't think people will actually be coding in erlang what people are doing is they're taking
5:14
the lessons of erlang and they're applying it in other places how many have heard of the newfangled
5:20
thing called microservices so there's another word for them they're
5:25
also called objects and if you're doing proper object oriented programming when I say proper I
5:32
don't mean C plus plus I'm looking at going all the way back to small talk if you look at how people actually coded
5:38
in small talk they were basically doing microservices when we start talking about
5:44
microservices we are envisioning our system as a series of very small programs
5:51
if I have a proper microservice I've got no problem going in and deleting it
5:56
it's very rare that you refactor how many of you refactor today
6:04
how many use refactoring tools so what's interesting for me is that
6:09
most people don't actually understand what refactoring is when I refactor something the very
6:15
definition of a refactor is I either change my test or my code and I
6:21
only change one out of the two how many of you have refactored where you're changing tests in code at the
6:28
same time okay that's called a refactor you are screwing up
6:34
so the whole benefit of refactoring in tdd comes from the idea that one side
6:39
stays stable and the other side pivots yeah that's a really different view of
6:45
tdd isn't it if I let's say refactor my test I move
6:50
something into the setup my code stays stable so if the test was green before and it's
6:57
green after I have a set of measurements that I did before and after I made my changes
7:02
that's good if I refactor my code my tests stay
7:08
stable my test ran before they run after it's a measurement I'm predicting what
7:15
will happen most people aren't doing this though and the refactoring tools they even push you
7:22
to not do this most refactoring tools try to get you to change your code in your tests at the
7:27
same time I think I'm going to call this Greg's law I can actually look at your software
7:33
and I can know what tools you used because your tools actually affect your code
7:40
how many of you have worked in a system where F5 step debugging was the only way of fixing problems
7:47
it was probably in vb.net if you go back and watch when that
7:53
software was being written guess how the original developers did it and now there is no other way of dealing
7:58
with the software because that's the tool they were using and it comes all the way out to the end software
8:06
this is common people that are using big Ides
8:11
I'm looking at all of you I know of exactly three people in the room right now that are using Vim
8:19
if you use a big IDE that affects the code that you're building all of this stuff affects the code that
8:25
you're building if I were to go for instance and use a big IDE and I'm sure most of you have
8:32
used an IV at some point have you ever tried using that code outside the ID
8:38
and it doesn't work very well it's the same type of thing with the F5
8:44
debugging let's come back to erlang again so
8:50
when people are using a tool such as erlang they no longer consider their problem to be one big program
8:58
they look at it as being many little programs how many of you have used Linux before
9:06
do you really think that we could have a 12 to 18 month project to rewrite LS
9:13
these are not new ideas the Unix way of doing things is also the microservices
9:18
way which is also the erlang way how many would be afraid to delete all
9:25
the code for grep and rewrite it from scratch okay to be fair grep is a lot of options
9:31
it's kind of lost its way
9:37
these are small programs and we composed them when we start wanting to talk about
9:42
building software and optimizing for its delete ability what we focus on is making small
9:49
programs you would be amazed at how
9:58
liberalizing it is to know at any point in time that you can walk into a piece of code and you can delete it
10:05
rewrite it from scratch and it's a one or two day problem it's not a 12-month
10:11
rewrite the ability to rewrite code is extraordinarily valuable
10:17
because what's going to happen is overtimes you're going to get features being brought in and sometimes a new
10:23
feature doesn't fit well with what you were doing before I actually just ran into this
10:29
um probably about two three weeks ago working on event store I've been working for like the last two
10:35
or three months and I feel really bad for James Nugent right now because it's a 10 000 line pull request that he has
10:41
to review he'll enjoy that I'm sure but what I've been adding is competing consumers
10:47
um so basically how rapidmq works and I wanted to add in some new features to competing consumers and I found it
10:53
did not work well in my model like it just didn't fit
10:59
now you can imagine I delivered the first version to our customers probably about two
11:05
weeks before and I decided I was going to rewrite the entire back end that sounds risky doesn't it
11:13
over the course of two days I rewrote the entire back into the system and you know what
11:19
when I went to go put my features in it worked perfectly and it was beautiful
11:25
my guess is it would have taken me two weeks to go back through and to get the code that was there
11:31
having the new features inside of it this is common if you don't have the right model and you try to add
11:37
functionality you run into issues you can oftentimes end up spending more
11:43
effort trying to get your bad model to have the new features than just write a new model from scratch
11:52
so the big question is how do we optimize for deletability and the way we optimize for deletability
11:58
is we start moving away from monoliths we try to optimize for decoupling
12:06
how many of you run coupling analysis on your software there's a lot of great tools for doing
12:12
the sonar and depend and they give you a lot of valuable information
12:18
for me the right size that I want and I know you guys all know microservices how
12:23
many have heard of definition of microservices by the way my big question is can you separate
12:30
object microservice and actor I've yet to get a real answer to that question
12:36
but how do we find the Goldilocks zone when we start talking about a microservice
12:43
you don't want them too big you don't want them too small I've heard rules of thumb you should never have more than 200 lines of code in a microservice
12:50
this reminds me in my first job we actually had a rule that you could not have more than 24 lines in a function do
12:57
you know why we were actually coding on VT terminals so you could not have a function that
13:04
was bigger than the screen that's an absolutely insane rule because
13:10
I can give you really quickly a great system that will break it what if I were doing an options pricing
13:16
inside of my microservices I've got one method called price option except I want to price it on a video
13:23
card in Cuda so I'm God I don't know 30 000 lines of
13:28
code behind the one method that actually runs in the video card and does the options pricing for you is that no longer a microservice
13:39
finding the Goldilocks zone is really the hard part when we start talking about this and my rule of thumb is you
13:44
should not end up with one of these microservices or we can call them services or we can call them actors or we could just go back and call them
13:50
objects you should not end up with one of these that's more than a week's worth of work
13:56
to rewrite what that's saying is that my personal
14:01
risk at any point in time that I want to do a full rewrite of this thing is one week
14:07
if later I have a better understanding of my problem it's a one-week rewrite
14:14
how many of you have worked with a bad model
14:20
how many have gone and then said it's a three to six month project to fix it
14:25
how do you explain that to the business so I'm I need six months worth of work
14:30
that you're going to see no outside benefit from we will not add any new features we will
14:36
not do anything but we will lower our technical debt because business people certainly
14:41
understand technical debt they understand this it's a really clear concept for them
14:50
I can't explain that so my goal is I want to optimize for my deletability I
14:55
want to optimize to rewrite things without a risk greater than one week
15:01
and keep in mind this is a rule of thumb this is not a hard concrete rule that you should go off and say but Greg said
15:08
we should never have functions more than 24 lines there are times where you'll end up with
15:15
a single service that does actually have more than one week's of work behind it but in general you should not find
15:20
yourself doing this if you find that you've got more than one week's worth of work this should be
15:25
a flag to you this is a high risk area of code
15:32
this ability to burn what's there to the ground and start from scratch
15:37
is massively valuable when I start finding that my models are
15:45
wrong I delete the code it is not a huge risk from my
15:51
perspective it also allows me to do a lot of other interesting things
15:56
how many of you have heard of technical debt before technical debt is bad right
16:02
I've always loved people that said technical debts bad how many of them mortgage
16:09
do you think there might be a reason why they called it technical debt is debt bad
16:15
debt allowed you to buy your house even though you didn't actually have all the cash and you're going to pay it back over time
16:22
otherwise you'd still be living in an apartment debt is not inherently a bad idea
16:29
of course every once in a while you might find the 22 year old that makes a hundred thousand SEC per year and drives
16:36
a Ferrari yeah that's probably not good debt
16:41
the same is true with technical debt so if I go through I can get something for the debt
16:48
that I'm taking out how many of you have done a crap job on something in order to put it to
16:53
production fast happens right I need this feature tomorrow why because
17:00
if I don't have this feature tomorrow we're going to lose money to a competitor
17:06
and it happens I may put technical debt into the system
17:12
by doing a completely crap job on it but I get time to Market as a return as
17:17
an example do I care about technical debt as much
17:23
if I can delete the code
17:29
if I've made things to the point where we have microservices and I really hate that term you guys already know
17:35
everything about microservices if you if you want to learn more about how to get them right go read some Alan K and Carl
17:40
Hewitt small microservice let's say that it's
17:46
at maximum one week's worth of work how worried about technical debt are you really going to be at this point
17:53
if technical debt starts accumulating delete it
18:00
I cannot stress enough how important it is to optimize your code for deleting
18:08
when people talk with me they always talk about their 18-month project that they want to get in on no
18:14
try a one week project and if you can't rewrite this thing in a
18:19
week you've failed so my next question would be how can you make it so you can rewrite the thing in
18:25
a week this ability to delete code
18:31
it removes your fear how many of you have spent three months
18:37
trying to optimize or trying to estimate an 18-month project just to figure out whether it's 18
18:42
months or 24. now I want just keep your hands up if
18:48
you've ever done that how many of you actually were correct in your estimate
18:55
within even 50 percent
19:00
it's waste any time that we're doing something that's a 24 month project an 18-month
19:06
project a six-month project it's waste
19:12
try working on one week projects try working in code that you've optimized you can delete any of the code at any
19:18
point in time and it's a one week project and you'd be amazed at how good we are
19:24
at estimating a one-week project compared to how bad we are at an 18-month project
19:32
optimizing your code for deletability will also improve your velocity
19:40
when you get into a piece of code a one-week rewrite is a piece of code that's basically manageable in your head
19:49
how many have come into a new system before someone else's code
19:54
or even worse it was a Junior's code the junior being you a week ago
20:02
and your first five days you spend reading and trying to understand what's connected with what and how
20:11
some projects it might be a month I've worked on projects that it was a year how do you understand a large
20:17
connected code base if I had something that could take you
20:23
one week to rewrite from scratch how long would it take you to understand that program
20:29
at most one week by definition if you can rewrite it in a week you can understand it in a week
20:37
what we're trying to do is we're trying to get things to the point that they are small manageable and understandable
20:44
the moment that you start optimizing for the deleting of your code this is where you end up
20:50
and we're not talking about microservices or SOA or by the way does anyone here know what SOA means in Dutch
20:58
I used to have a slide that I would put up it was so a test.nl it's a website I'll give you a hint they don't test
21:04
your services uh so I mean sexually transmitted disease
21:09
to be fair once the middleware vendors got a hold of so it's basically become one
21:18
about with soah it's the same the same same thing with objects it's the same thing with actors okay actors
21:24
had a concurrency model but what we're really coming back to is the 1970s
21:30
write small manageable programs that coordinate to get a job done as opposed
21:36
to writing one big lump of crap this is what people meant inside of the
21:42
big ball of mud paper try to get small projects inside of a
21:48
big project try to keep things that are manageable
21:53
again my rule of thumb is about one week if you go into an area of code and you
21:59
say this is going to take me six months to rewrite what are the component pieces that would
22:04
take you one week to rewrite each and I'm not saying that everyone should go delete all their code all at once
22:13
when we start optimizing this way we end up in a very different place and it's a much nicer place
22:20
by the way how many of you have really heard of people optimizing it's an architectural quality for deletability
22:29
it's an interesting perspective on the problem
22:34
almost never will you be going through and doing refactoring
22:39
refactoring is also known as delete all the code and start from scratch
22:44
when we talk about things like technical debt don't worry too much about it because you can delete the code and you
22:50
can rewrite it in a week when do you know the most about your
22:56
project is it at the beginning when you're doing your planning and trying to figure out
23:01
how to do things or is it at the end after you've done everything
23:08
at any point in time I'm willing to delete my code and rewrite it from scratch
23:14
this is the Unix philosophy this is the erlang philosophy this is microservices this is SOA this is actors
23:26
the problem is most people are picking up these ideas and they're never understanding the fact that the goal is to delete your code
23:33
they pick up microservices or SOA is a concept and you'll walk in and you'll find that
23:39
behind a single service they've got 17 000 lines of code how many of you can rewrite seventeen
23:45
thousand lines of code in a week okay to be fair if it's Java code we can
23:51
probably rewrite it in a thousand lines of closure
23:57
I cannot understand seventeen thousand lines of code in a week and it's important to remember because
24:03
people will be reading your code believe it or not some poor sucker is
24:09
going to have to go through your code in the future even if that poor sucker is yourself
24:17
optimize to keep things small and manageable
24:22
in going through and doing this this will also help us in a lot of other ways
24:29
don't focus on things and Technologies don't focus on things like microservices this idea is not new
24:36
and if you understood the Unix philosophy 30 years ago you understand microservices today
24:45
try to find yourself in the Goldilocks zone and focus on your rewrite ability
24:52
the ability to just burn the entire section of the ground and start from scratch
24:58
I cannot even explain to you without you guys having actually worked with it before how liberalizing this is
25:06
your fear goes away can you imagine working as a developer and not being afraid
25:15
not having all these tools to try to tell you what is hooked to what and if I rename this database column what we'll
25:20
break in it it's a completely different experience
25:26
to be in these kinds of systems where I can bring in a junior or a new developer and they can be productive their first
25:33
day without having gone through and watched 19 hours of videos explaining how our
25:39
system actually works but again there's absolutely nothing new
25:45
here it's just a different perspective on the same ideas
25:50
go through and focus on keeping everything as small independent programs
25:58
and I mentioned before that tools will actually focus on your code you depending on the
26:03
tool that you use your output will change go learn erling
26:10
I can't stress it enough learn erling if you really want to learn object orientation go learn erling I know it
26:15
sounds weird because it's not an object-oriented language but you will understand much better how
26:22
objects work when you start looking at them from the perspective of being processes in early
26:28
at the same time it will actually improve your object orientation
26:34
and when we talk about these things nothing here is new by the way how many of you have heard of
26:40
an aggregate before from domain driven design does that sound familiar
26:47
to let's say a small process where there's one process per document
26:53
in your system you have consistency inside of it you have no consistency outside of it in
26:59
order to talk to it you send it messages have any of you ever read Alan Kaye
27:07
how many of you work in an object-oriented language now and you've never read Alan K
27:13
ay he had a lot of interesting things to say when he defined the word object orientation
27:18
in fact if you go all the way back to his definition of object orientation an object is a little computer that you
27:25
send messages to to tell it to do stuff the beauty of that is it's a recursive model so if if an object is a little
27:32
computer I send messages and tell it to do stuff what what's a big computer well it's a bigger computer I send
27:39
messages to tell it to do stuff and it routes them to little computers inside the big computer
27:44
nothing that we're talking about here is new whether we talk about objects or actors
27:50
or services or microservices or components it's all the same idea we're trying to
27:56
make little programs inside of big programs
28:02
there's a lot of good academic research you can actually go back and read to be fair Alan Kaye actually said the
28:09
biggest mistake he made with object orientation was naming it object orientation
28:15
he should have called it message orientation because people think about it now as the
28:20
objects not about the messages that go between them when you take this fundamental view this
28:26
is the exact same thing we talk about inside of erlang correct except we can replace the word object
28:31
with process and okay erlang has a concurrency model because it's actor based
28:37
single thread inside of each one of them but what we're getting back to is the
28:42
same idea that each object or each process in erlang is a little process
28:47
and it's a process a program that I send messages to and I tell it to do stuff
28:53
if you want to hit the Goldilocks zone of these programs they should be roughly one week to
28:59
rewrite and the way we conceptualize our system
29:04
is a slew of little tiny programs how many have written a shell script in
29:09
Linux before isn't it beautiful compared to Windows okay powershell's coming along
29:19
we compose small programs in order to get Behavior out of them
29:25
and our focus should be keeping the program small enough that we're willing to delete them at any point in time
29:33
and I've mentioned it before but I cannot stress enough how liberalizing it
29:39
actually is to delete code you are taking the handcuffs off of
29:45
yourself you're allowing yourself to do things you wouldn't be able to do otherwise
29:52
and you can do this in object-oriented code you can do this in any type of code that you want okay functional is a
29:57
little bit different but at the end of the day what we're working with is small programs
30:05
and the trick the one big secret from a Michigan mom that 80 of software
30:10
developers don't know it's the secret to Great Consulting is to never build big programs
30:20
you can literally make a career as a consultant telling people nothing but that
30:28
there's a lot of Consultants that actually do that understand nothing but that simple idea
30:39
and you can find it historically it's it's happened over and over and over and over again why because people don't
30:46
understand it the difference between great code and
30:51
sucky code is the size of the programs nothing more
30:58
when I have a hundred thousand lines of code I am Shackled to it
31:04
I will never understand a hundred thousand lines of code at the same time it's impossible you cannot keep that
31:10
much in your head there will always be subtle details that are happening and the 3000 unit tests around it really
31:17
won't help you that much because you won't be able to keep in mind all of the things that the code actually does
31:26
I need to get things to be smaller there are some other benefits about having very small programs
31:32
how many of you have deployed things before
31:39
the Big Bang release is always scary isn't it if you release a hundred thousand lines of code all at once are
31:46
you sure that's going to work in production foreign
31:53
so I refused to release more than a few thousand lines of code at a time now I I
31:58
will never do it what scares the crap out of me about releasing this size of a program
32:05
is not that I'm going to go release it and I'm going to run into problems and you know it fails because what I'm gonna
32:11
do is I'm Gonna Roll it back at that point that's no worries how many have released something and it
32:17
worked great when you released it it's and it died four days later
32:23
how do you roll back four days you guys all right SQL migration scripts
32:29
how many of you write a SQL migration script from your new data back to the old schema
32:35
so the running joke and I've done this with a number of teams is at this point you either get to wear the cowboy hat or
32:40
the fireman hat oh come on you've all worked on production issues before and isn't it
32:46
wonderful your your knee-deep in a production issue and someone comes in they want to talk with you about the
32:51
Christmas party the idea is if you're wearing the cowboy hat or the fireman hat someone will walk
32:57
over to you and just go oh okay I know what you're doing and walk away
33:02
um I actually recommend it for teams if I'm releasing 3 000 lines of code to
33:09
production can I be reasonably certain that that 3000 lines of code is going to work
33:16
and this will come back to Paul's talk that he was just having if I'm only releasing 2 000 lines of
33:22
code at a time why don't I do that 50 times a day It's relatively low risk
33:29
and if I conceptualize my system as being a series of little programs could I run two programs side by side
33:39
could I have two little computers that I send messages to and tell them to do stuff and I put half the load on this
33:44
one and half oh wait this is called a blue green deploy isn't it
33:50
we can take all the things we've actually learned on these big systems and apply it at very very small scales
33:56
I may want to take 10 of my load and put it on the new program that I put out
34:04
this is normally how you actually change these kinds of programs what you do is you go in you delete all of the code
34:10
inside of it you write it from scratch and you push it to production sitting next to the old program
34:16
once you're comfortable that your changes have not really broken anything and maybe you have some tests around it
34:21
as well then you delete the old
34:27
software today there's literally nothing new in anything that we're doing
34:35
understanding these very basic ideas can turn you into a high paid consultant
34:41
no literally what most Consultants do is they just explain this one idea over and over and over again
34:46
because teens don't get it teams inherently want to build programs
34:51
that are too big and too complex as I've always liked to joke developers have this wonderful habit of solving
34:58
problems that nobody has I could do it in a simple way but if I did the simple way it's not up to my
35:04
level of intellect so I need to make it more complicated to make it a problem worthy of me
35:11
don't fall down these traps always focus on dealing with your code
35:17
that you will never have a project that's longer than one week
35:23
you will be able to fail your projects or succeed your projects within one week every time
35:31
you are optimizing for your ability to rewrite your system as opposed to planning for change that will happen in
35:38
your system how many of you can predict what use
35:43
cases your system will have to do a year from now
35:49
okay how about next week next week I'm normally pretty good with
35:54
I'm not a hundred percent um I've actually challenged a lot of teams to do this every time that you add
36:00
an abstraction I want you to create an options pricing model on that abstraction and I want you to forecast
36:06
the probability that someone actually needs the abstraction in the future
36:12
and I want you to measure yourself and then I want to compare you against a
36:18
dartboard in other words a null hypothesis my guess is you will be slightly better
36:25
Maybe but you will not be much better than dartboard or we could get the German octopus
36:34
don't try to plan for future changes focus on the ability to completely
36:39
rewrite everything from scratch when that change actually occurs
36:45
and I understand that you'll be writing a lot of really simple stuff that's not a bad thing
36:52
don't forecast out what changes may happen and try to take your model today to accept those changes in the future
37:01
focus on making little tiny programs and lots of them that are very easily
37:07
rewritable make the decision at the last
37:12
responsible moment as they say in agile parlance
37:18
but again these are not new ideas you can find this going all the way back
37:24
to the 70s and apparently developers are just too dumb to actually realize this thing
37:29
happening over and over and over again and we have different ways of explaining the same thing
37:35
but almost nobody gets it how many of you have a program that would take you more than one month to
37:41
rewrite today almost every system I go into has this
37:50
even if you go into good code bases they're they're usually too large
37:57
again the identifying trait of good code is small isolated programs that can be
38:04
deleted on the Fly almost every single aspect of your
38:10
system will improve if you think about it in this way
38:15
now with that I will invite Martin up who's going to do the closing
38:22
but does anyone have any questions just before we bring him up I don't want to hold you guys back from beer I know
38:27
about getting between a Swede and their beer
38:46
so at any given point in time I should only have a small amount of State for any given program
38:52
and yes you you will need to have migrations but your migrations will be much much smaller also on a per program basis I can start
39:00
looking at varying methods of storage um one that I like to talk with people a lot is event sourcing because if I'm
39:07
actually event sourcing and I say my state is a first level derivative off my log then I have a lot less migration
39:13
problems when I'm dealing with it there's no way that you will be able to
39:18
store State let's say in a read model that you're coming off of we can just say it's even a file
39:23
um I I can't just say I'm going to change my file without writing some form of migration for it
39:28
but ideally I'm going to keep things small and isolated so I'm not saying I migrate my database
39:35
this one thing might be talking to one table if that makes sense
39:49
conceptually yes but they may come back to the same physical storage
39:54
I may have 13 little programs that are all talking back to the same SQL database because I really don't want to
40:00
have to manage 13 SQL databases in production but conceptually they're 13 different stores
40:06
and they can vary independently of each other if you go down that road be very very
40:12
careful about integrating the processes through the data ideally each process should should have
40:18
its own data and if I want access to the data I talk to the process I don't go directly to the data
40:23
there are times where you're going to have to actually integrate through the data for performance reasons but be very
40:30
wary about doing that because you're building in a fairly nasty coupling between things how many of you have integrated through a database before
40:38
and people will tell you that it's awful and you should never ever do it but you know what sometimes it actually works
40:43
really well and it's a form of integration understand that it's got its own trade-offs associated with it you're you're putting in Fairly nasty coupling
40:51
um has anyone ever worked in a company that had a rule that you were not allowed to rename a database column because no one had any idea what it
40:58
would affect but it is one form of doing the
41:03
integration ideally you will do the integration through messaging but there are times where you actually need to do it through the data itself for
41:10
performance reasons
41:15
uh schema list can help anything that's weak schema is a lot easier to version than things that are strong schemed
41:21
um on other that I really like to use is what's known as a hybrid schema
41:27
so let's imagine that I were to use an xsd that defined three or four things that were must understand
41:33
but everything else that was there was dynamic um that's a very common way of dealing with things
41:45
yes uh protobus uh absolutely it's something that can be shared um normally I look at things at protobufts and I
41:51
would consider that to be a library and yes libraries get used from many places
41:56
um many programs linked to g-lib C if we
42:01
talk about it from a Unix perspective um and going back to your question with weak schema another way of doing a
42:06
hybrid schema would be for instance if I were to use protobufs and I were to say required on three things and optional
42:12
for everything else and that will again help you a lot in terms of your versioning of data over time
42:18
any other questions okay well then I will give it to Martin
42:25
who will close up the conference and thank you guys for having me out

### Main points

- what goes around, comes around - our modern concepts are not new, likely invented in the 60's and 70's
- keep it as small as possible - this does not put a limit on size
    - these are generalizations, but exceptions should be rare
    - if you cannot rewrite it one week, it is too big
    - even juniors can understand
        - No one can understand 17,000 lines of code in a short period of time
- if you are afraid to delete it, something is wrong
- concept: many small objects communicating through messages
- small isolates technical debt
- excessive abstractions: what is the probability that someone actually needs the abstraction in the future
