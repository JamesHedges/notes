# CAP Theorem and Eventual Consistency

BASE transactions (basic availability, soft state, and eventual consistency)

ACID transactions (atomicity, consistency, isolation, and durability)

[Building Microservices](https://learning.oreilly.com/library/view/building-microservices-2nd/9781492034018/ch12.html#idm45699526112512)
