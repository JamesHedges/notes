# Cloud Native Best Practices

## Tips from Oli Bage

[LSEG Cloud Lessons Learned](https://www.infoq.com/presentations/lseg-cloud-lessons/)

### Organizational

- Pace of change: Cloud technology moving faster than traditional on-premise. Need to:
  - Technology - flexible architecture
    - offers modular swap-out over the medium term
    - incurs additional costs
    - is more difficult to optimize performance
    - can move to better, more efficient services faster as they become available
  - People - developer agility
    - need a skill set change to be successful in cloud
- Patterns are important
  - Central control for sharing the patterns
    - Right tools for the problem
    - Best practices for each cloud service / scenario
  - Allows teams to fail faster
  - Spreads the learning

### Cloud Economics

- Cloud Migration
  - Fast lift vs re-architecture
    - Fast lift does not deliver cloud native characteristics
    - Re-architecture takes advantage of cloud native capabilities
    - Conflicting outcomes for end users, technology group, and cloud service providers
  - Use data-driven architecture (for making decisions?)
    - Link migration to:
      - Product road maps
      - Infrastructure costs
      - Obsolescence data
    - Identifies opportunities to get out of on-premise infrastructure and where savings may occur
- Technical Debt
  - Risk building a lot of capability on the cloud, but not replacing your on-premise capabilities
  - Dual (on-premise & cloud) run costs
  - On-premise integration costs
  - Done = switch off the old
    - Implies getting existing functionality to the cloud
- Provision for performance
  - Performance-testing is expensive
  - Scale back when confident

### Technical Design

- Landing Zones
  - Too broad, too narrow...
    - Too few account/subscriptions
    - Too many account/subscriptions
  - Ecosystem of dependencies
    - Finding the right balance
    - Encapsulate change cycle for a set of applications to control dependencies
    - Do an impact map of ecosystem and use to decide landing zone granularity
- IP Ranges
  - Containers are IP hungry
    - don't need internally addressable IP addresses with ones that are unique on the entire network
  - Use secondary CIDR ranges
    - Unique withing a VPC (AWS) / Virtual Network (Azure)
- Observability
  - Real-time feedback
  - Central Logging
  - Make observability part of the architecture
- FinTech Focus
  - DR & Resilience
    - Multi-region vs Multi-AZ
    - Consider the cross system dependencies
    - Place the highest tier (afferent dependencies) in multi-region as well as multi-availability zones
  - Real-Time Data
    - Real-time optimized market data
      - Real-time data in the cloud is very expensive
      - Predictable, ultra-low latency not available, maybe in the future
    - Low latency - gaps
      - Colocate in specific physical locations
        - Sometimes down to the rack location
      - Need support for multicast for real-time data feeds
  - Need ultra-high levels of redundancy
    - Power
    - Dual comms
    - Dual cooling
    - Predictable distances between services

### Cloud Data Management Capabilities

- CDMC is an organization setting standards data management
- Run by Enterprise Data Management (EDM) Council
- See the spec [CDMC+](https://edmcouncil.org/cdmc)

## Million Dollar Lines of Code

- [Video](https://www.infoq.com/presentations/cloud-cost-optimizationq/)
- [Article](https://www.infoq.com/news/2023/10/engineering-optimize-cost/)

- In cloud engineering, every decision is a buying decision
- Small things in lines of code can have big costs.
- Costs can explode at scale
- We can no longer build like "the old way". Think on-premise versus in the cloud

### Lessons Learned

- Storage is cheap
- Calling APIs costs money  
It may be pennies each call but how many times are you going to do that?
- We have infinite scale, we don't have an infinite wallet
- CDN's are great at eating traffic... and eating your money
- Think about how cloud resources are used
  - Avoid premature optimization (it is evil and may cost you more)
- As engineers, think about (iteratively):
  - Can it run
  - Will it even work
  - Can I achieve solving this problem
  - How will the solution work at scale
- Remember you work on a team

### Cloud Efficiency Rate (CER)

(Revenue - Cloud Costs) / Revenue

- **Research and Development:** A negative CER is acceptable
- **Version 1/MVP:** Break even or low CER (0% to 25%)]
- **Product Market Fit (PMF):** Acceptable margins are conceivable (25% to 50%)
- **Steady State:** Healthy margins = Healthy Business (CER is 80%)

- This should become a non-functional requirement