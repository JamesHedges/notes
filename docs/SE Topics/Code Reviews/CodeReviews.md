<!-- omit in toc -->
# Code Reviews

- [General Info](#general-info)
  - [Audience](#audience)
  - [What Type of Writing](#what-type-of-writing)
- [Reference Ideas](#reference-ideas)
  - [Summary of the tutorial with cross-references](#summary-of-the-tutorial-with-cross-references)
  - [Summary of preparing for a code review](#summary-of-preparing-for-a-code-review)
  - [Review must understand the coding standards](#review-must-understand-the-coding-standards)
- [Process Ideas](#process-ideas)
  - [Pre-Review Check](#pre-review-check)
  - [Review the Commit in Context](#review-the-commit-in-context)
  - [What to look for in code](#what-to-look-for-in-code)
  - [Reviewing a code change with too may updates is challenging](#reviewing-a-code-change-with-too-may-updates-is-challenging)
  - [What to look for in unit tests](#what-to-look-for-in-unit-tests)
    - [Number of unit tests](#number-of-unit-tests)
    - [Quality of unit tests](#quality-of-unit-tests)
  - [What to look for in IaC](#what-to-look-for-in-iac)
  - [Documentation needs to be Included in review](#documentation-needs-to-be-included-in-review)
  - [Responding to MR Request](#responding-to-mr-request)
- [Tutorial Ideas](#tutorial-ideas)
  - [Preparing for a code review](#preparing-for-a-code-review)
  - [The documentation needs to be complete](#the-documentation-needs-to-be-complete)
  - [Code review commit needs to be small](#code-review-commit-needs-to-be-small)
  - [What non-functional requirements should be examined](#what-non-functional-requirements-should-be-examined)
  - [Necessary unit tests](#necessary-unit-tests)
  - [Do the unit tests have excessive mocking](#do-the-unit-tests-have-excessive-mocking)
  - [Does the code follow SOLID](#does-the-code-follow-solid)
  - [Does the code follow clean coding standards](#does-the-code-follow-clean-coding-standards)
  - [Have the static code scans been completed](#have-the-static-code-scans-been-completed)
  - [Static code analysis metrics](#static-code-analysis-metrics)
  - [Does the code follow the domain's ubiquitous language](#does-the-code-follow-the-domains-ubiquitous-language)
  - [Does the IaC apply tags to all resources](#does-the-iac-apply-tags-to-all-resources)
  - [Is the CI/CD pipeline created and appropriate](#is-the-cicd-pipeline-created-and-appropriate)
  - [Is a Docker file required?](#is-a-docker-file-required)
  - [Does the Docker file follow best practices](#does-the-docker-file-follow-best-practices)
  - [Are naming best practice used](#are-naming-best-practice-used)
  - [Are feature flags used for new code](#are-feature-flags-used-for-new-code)
  - [Unit tests test for both positive and negative cases](#unit-tests-test-for-both-positive-and-negative-cases)
  - [Does IaC use standard naming convention (team/env/domain/...)](#does-iac-use-standard-naming-convention-teamenvdomain)
  - [Online reviews](#online-reviews)
  - [In person reviews](#in-person-reviews)
  - [Does the code fit the architecture](#does-the-code-fit-the-architecture)
  - [Review the code in context and not just the diffs](#review-the-code-in-context-and-not-just-the-diffs)
  - [Best way to give feedback](#best-way-to-give-feedback)
  - [All agreed upon fixes must be completed before approval](#all-agreed-upon-fixes-must-be-completed-before-approval)
  - [Any change not completed prior to approvaL will have tech debt ticket written against it](#any-change-not-completed-prior-to-approval-will-have-tech-debt-ticket-written-against-it)
  - [Does the code follow OOP / functional best practices](#does-the-code-follow-oop--functional-best-practices)
  - [Are all inputs validated](#are-all-inputs-validated)

## General Info

### Audience

- Software Engineers
- Architects
- Engineering Managers

### What Type of Writing

- Reference: Overview section for code review standards
- Tutorial: Guidelines for when evaluating code

## Reference Ideas

### Summary of the tutorial with cross-references

### Summary of preparing for a code review

### Review must understand the coding standards

## Process Ideas

### Pre-Review Check

Before beginning a code review, the reviewer must understand the purpose and scope of the commit under review.

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - Where can I find details about the commit?
  - What details should they expect?
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - It is frustrating to review a commit when you are not informed about the scope of the changes.
- **You:** (Character/Ethics)
  - If you are not knowledgeable about what a commit is for, the code review will lack the rigor and quality expected

### Review the Commit in Context

When only looking at commit diffs, it may not be possible to understand the context of a commit.

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - What needs to be done to understand the commit's code context?
  - Where should the committed code be examined?
  - When should I run unit tests or execute code?
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Reviews done without understanding the context a suspect
- **You:** (Character/Ethics)
  - A reviewer should not be content to only review a commit's diffs, unless it is very small and they are familiar with the context
  - Complex commits will require more thoughtful reviews and warrant a deeper examination of the commit's context

### What to look for in code

Reviewers need to have a guideline on what to look for in order to do consistent quality code reviews.

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - What are the coding guidelines?
  - Where would a reviewer find them?
  - How strict must they be adhered?
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Trying to review code without guideline leads to inconsistent results
  - Sometimes code gets through with a hand wave and results in technical debt being accumulated and entropy to the code base
- **You:** (Character/Ethics)
  - Code need to be clean and readable?
  - Is the code easy to reason, even if reviewer is not a domain expert?

### Reviewing a code change with too may updates is challenging

When faced with a large code review, reviewers are stressed to complete it in a timely manner.

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - What is too large?
  - How do reviewers handle large commits?
  - When should reviewer request in person reviews or walkthroughs?
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Large commits are a significant interruption to a developers daily schedule
  - Frustrating to not be able to get a quality review done in a timely manner when it is too large
- **You:** (Character/Ethics)
  - Developers need to do quality code reviews in order to preserve the integrity of the code base
  - Expecting others to stop what they are working on in order to perform a large code review is inconsiderate

### What to look for in unit tests

#### Number of unit tests

Reviewers should know how to handle unit tests when their types and number vary depending on the feature's requirements?

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - How many unit tests should a reviewer find?
  - What is expected for the number of 'negative' unit test cases?
  - How am I supposed to know when there are enough unit tests? Too many (low quality, redundant)?
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Too few tests will leave the code vulnerable to bugs, especially when changes are made at a later time
- **You:** (Character/Ethics)
  - Getting the right number of tests is critical for creating a maintainable code base that is easy to reason

#### Quality of unit tests

Reviewers need to evaluate a unit test suite.

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - How does a developer recognize quality unit tests
  - Are the unit tests 'quality' unit tests?
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Poor unit tests can be difficult to review
  - It is frustrating to review code with poorly written unit tests, especially those with on over reliance on mocks.
- **You:** (Character/Ethics)
  - Well written tests will help future developer to understand the code

>Don't turn this into a unit testing standard. That should be a separate stand-alone document

### What to look for in IaC

Infrastructure code has different concerns than the imperative code and has different concerns.

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - What are the concerns to be aware of
  - When should it be used, not be used
  - Where can reviewers find the standards
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Reviewers not familiar with the idiosyncrasies of the IaC language will struggle with reviews
- **You:** (Character/Ethics)
  - Need to be vigilant about how it is used, infrastructure only. Especially with data store content.

### Documentation needs to be Included in review

### Responding to MR Request

Developers must respond appropriately to an MR request

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
  - What type of response is appropriate
  - When should it be completed by
  - Who should conduct a review
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
  - Developers can evoke emotional responses with hyper-critical responses
  - Requests should be handled in an appropriate amount of time
- **You:** (Character/Ethics)
  - Keep critiques fair
  - When you have an opinion, you can make a recommendation which is not a demand
  - Base critiques on best practices for issues beyond potential bugs or not completely implementing a feature

## Tutorial Ideas

The bulk of this will comprise the coding standard --> should that be a second document?

### Preparing for a code review

### The documentation needs to be complete

### Code review commit needs to be small

- Head
  - What is small?

### What non-functional requirements should be examined

### Necessary unit tests

### Do the unit tests have excessive mocking

### Does the code follow SOLID

### Does the code follow clean coding standards

### Have the static code scans been completed

### Static code analysis metrics

### Does the code follow the domain's ubiquitous language

### Does the IaC apply tags to all resources

### Is the CI/CD pipeline created and appropriate

### Is a Docker file required?

### Does the Docker file follow best practices

    Staged build
    Non-root user

### Are naming best practice used

### Are feature flags used for new code

### Unit tests test for both positive and negative cases

### Does IaC use standard naming convention (team/env/domain/...)

### Online reviews

### In person reviews

### Does the code fit the architecture

### Review the code in context and not just the diffs

### Best way to give feedback

### All agreed upon fixes must be completed before approval

### Any change not completed prior to approvaL will have tech debt ticket written against it

### Does the code follow OOP / functional best practices

### Are all inputs validated
