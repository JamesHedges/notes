<!-- omit in toc -->
# Coding Standards

## Naming Things

From _Clean Code_, chapter 10, section **Class Should Be Small!!!**

_"The name of a class should describe what responsibilities it fulfills. In fact, naming is probably the first way of helping determine class size. If we cannot derive a concise name for a class, then it’s likely too large. The more ambiguous the class name, the more likely it has too many responsibilities. For example, **class names including weasel words like Processor or Manager or Super often hint at unfortunate aggregation of responsibilities**."_