<!-- omit in toc -->
# AppConfig Analysis

<!--
-->
- [Criteria Checklist](#criteria-checklist)
- [IAM Permissions](#iam-permissions)
- [Deployment Strategy](#deployment-strategy)
- [Structure](#structure)
  - [Application](#application)
    - [Application Environment](#application-environment)
    - [Application Profile](#application-profile)
      - [Free Form Profile](#free-form-profile)
        - [JSON Schema Validation](#json-schema-validation)
        - [Lambda Validation](#lambda-validation)
      - [Feature Flag Profile](#feature-flag-profile)
      - [Configuration Versions](#configuration-versions)
- [Deployments](#deployments)
  - [Rollbacks](#rollbacks)
- [Service Cost \& Quotas](#service-cost--quotas)
  - [Cost](#cost)
  - [Quotas](#quotas)
- [AppConfig Clients](#appconfig-clients)
  - [Lambda](#lambda)
  - [ECS](#ecs)

## Criteria Checklist

| Criteria               | Implemented | Comment                                                                |
| :--------------------- | :---------: | :--------------------------------------------------------------------- |
| Multi-Language Support |      X      |                                                                        |
| Usage Patterns         |             |                                                                        |
| - Polling              |      X      |                                                                        |
| - Streaming            |             | Has the ability to publish deployment events                           |
| Versioning             |      X      |                                                                        |
| Formatting             |      X      | Feature flag specific formatting                                       |
| Attribute Flags        |      X      |                                                                        |
| Access Security        |      X      | IAM Policies                                                           |
| Monitoring             |             |                                                                        |
| - Dashboard            |      X      |                                                                        |
| - Deployment Failure   |      X      |                                                                        |
| Experimentation        |             | All experimentation features implemented using the Evidently extension |
| - Targeting            |      X      |                                                                        |
| - Tracking             |             |                                                                        |
| - Statistical Analysis |             |                                                                        |
| Scaling                |      X      |                                                                        |
| Storage                |      x      | AWS Hosted config, S3, or Systems Manager Params                       |
| Dynamism               |             |                                                                        |
| Longevity              |             |                                                                        |
| Integration            |             |                                                                        |
| - Lambda               |      X      | Lambda Layer                                                           |
| - ECS                  |      X      | Sidecar, Linux ONLY                                                    |

## IAM Permissions

[Setting up AWS AppConfig](https://docs.aws.amazon.com/appconfig/latest/userguide/setting-up-appconfig.html)

## Deployment Strategy

[Creating a deployment strategy](https://docs.aws.amazon.com/appconfig/latest/userguide/appconfig-creating-deployment-strategy.html)

A deployment strategy describes how to deploy a configuration. They are defined by their deployment type, step percentage, deployment time, and bake time.

| Setting         | |Description |
| :-------------- | :---|:---------- |
| Deployment type ||Deployment type defines how the configuration deploys or rolls out. AWS AppConfig supports Linear and Exponential deployment types.|
||Linear: |For this type, AWS AppConfig processes the deployment by increments of the growth factor evenly distributed over the deployment. For example, a linear deployment that uses a step percentage of 20 initially makes the configuration available to 20 percent of the targets. After 1/5th of the deployment time has passed, the system updates the percentage to 40 percent. This continues until 100% of the targets are set to receive the deployed configuration.|
||Exponential: |For this type, AWS AppConfig processes the deployment exponentially using the following formula: G*(2^N). In this formula, G is the step percentage specified by the user and N is the number of steps until the configuration is deployed to all targets. For example, if you specify a growth factor of 2, then the system rolls out the configuration as follows:
|||2*(2^0)|
|||2*(2^1)|
|||2*(2^2)|
|||Expressed numerically, the deployment rolls out as follows: 2% of the targets, 4% of the targets, 8% of the targets, and continues until the configuration has been deployed to all targets.|
|Step percentage (growth factor)||This setting specifies the percentage of callers to target during each step of the deployment.|
||| **Note** In the SDK and the AWS AppConfig API Reference, step percentage is called growth factor.|
|Deployment time||This setting specifies an amount of time during which AWS AppConfig deploys to hosts. This is not a timeout value. It is a window of time during which the deployment is processed in intervals.|
|Bake time||This setting specifies the amount of time AWS AppConfig monitors for Amazon CloudWatch alarms after the configuration has been deployed to 100% of its targets, before considering the deployment to be complete. If an alarm is triggered during this time, AWS AppConfig rolls back the deployment. You must configure permissions for AWS AppConfig to roll back based on CloudWatch alarms. For more information, see (Optional) Configure permissions for rollback based on CloudWatch alarms.|

There are three AWS defined strategies.

|Stragegy|Type|Step|Deploy|Bake|
|:---|:---|:---|:---|:---|
|AppConfig.AllAtOnce: Quick|Linear|100%|0|10m|
|AppConfig.Linear50PercentEvery30Seconds: Testing/Demonstration|Linear|50%|1m|1m|
|AppConfig.Canary10Percent20Minutes: AWS recommended|exponential|10%|20m|10m|

Up to 20 custom strategies can be created. Strategies are not part of any AppConfig application, profile, configuration version, or environment and may be used for any deployment in the account.

## Structure

Configurations have a hierarchical structure where an application has environments and profiles. Each profile contains the configuration versions to be deployed to environments.

### Application

An applications is the top level configuration container. It will have a relationship with some unit of executable code. An application container must have at least one environment and at least one profile.

#### Application Environment

[Creating an environment](https://docs.aws.amazon.com/appconfig/latest/userguide/appconfig-creating-environment.html)

Environments are logical constructs for targeting deployments. Before an application's configuration may be deployed, it must have at least one environment to target.

An environment may also have a monitor that will watches a CloudWatch alarm and rolls back a deployment when it goes into alarm state during a deployment (including its bake time).

![Application Environments](ApplicationEnvironments%20(Small).png)  
_An application's environments_

Environments are per account and they are not connected to the physical environments of dev, test, integration (cert), or production.

#### Application Profile

Applications profiles contain configuration definitions, which includes the configuration versions and validators.There are two types of configurations, free form and feature flag.

![Application Profiles](ApplicationProfiles%20(Small).png)

##### Free Form Profile

Free form configurations do not have any implied schema constraints. Configuration content may be presented as JSON, YAML, or plain text files.

Configuration source options are:

- Hosted Configuration
- S3 Object (use when have large configuration files) - requires an access role that can be assumed
- Systems Manager Document - requires an access role that can be assumed
- Systems Manager Parameter - requires an access role that can be assumed
- AWS CodePipeline

###### JSON Schema Validation

If a profile has a JSON schema validation, it is applied prior to deployment. If the validation fails, the deployment is not initiated.

[Getting Started Step-By-Step](http://json-schema.org/learn/getting-started-step-by-step)
[About validators](https://docs.aws.amazon.com/appconfig/latest/userguide/appconfig-creating-configuration-and-profile.html#appconfig-creating-configuration-and-profile-validators)
[Example from Terraform](https://github.com/terraform-aws-modules/terraform-aws-appconfig/blob/v1.1.2/examples/_configs/config_validator.json)

```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "foo": {
      "type": "boolean"
    },
    "bar": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        },
        "notbefore": {
          "type": "string"
        }
      },
      "required" : [ "enabled" ]
    }
  },
  "required": [
    "foo"
  ]
}
```

Based on the above schema, the following is a valid configuration.

![Profile](Profile%20(Small).png)  
_Configuration Version_

###### Lambda Validation

[Example Python Lambda Validator](https://github.com/terraform-aws-modules/terraform-aws-appconfig/blob/v1.1.2/examples/_configs/validate.py)

When a more extensive validation is required beyond schema, validation may be applied using a Lambda function. The set-up requires a Lambda functions ARN and version number. The validator Lambda function will receive an event schema, that includes the current configuration as a base-64 encoded string, to evaluate. An exception is raised when the function's validation fails.

##### Feature Flag Profile

The feature flag type profile requires a configuration to use a specific JSON schema. There is no option for a YAML or text file configuration. A configuration's only source option is a HostedConfiguration, it cannot be sourced from S3, System Manager documents or parameters, or a code pipeline. The feature flag profile does not have a custom validation option, but all configurations are validated against an feature flag type schema.

##### Configuration Versions

Flag values are set in a configuration version which must follow a specific schema. This schema first defines the flags and their attributes and then provide the values. All flags will have an enabled option.

> When a client queries a feature flag configuration, a flag's  attributes are only returned when the enabled value is set to true.

- _flags_ element to define feature flags and their attributes
  - each _flags_ element starts a new feature flag
    - must set a name property for each feature flag
    - the _attributes_ element to define a feature flags attribute elements
      - element naming each attribute
        - constraints for the attribute
          - _type_ property of the attribute. May be string, number, string[], number[], or boolean.
          - _required_ flag
          - _minimum_ value for number types
          - _maximum_ value for number types
          - _regular\_expression_ for string types
          - _enum_ for string types
- _values_ element to set the configuration's state and attributes
  - _enabled_ field to indicate the flag is active. This is an implied field for all feature flags
    - elements setting the feature flag's attribute values in the form for _"element": "value"_
- _version_ element that must be set with value _"1"_ but has no effect

The following example, the _flags_ section defines a configuration having three flags, _foo, bar_, and _fun_. The _foo_ flag does not have any attributes. The _bar_ and _fun_ flags has two attributes _someAttribute_ and _otherAttribute_. The _fun_ flag also has two attributes, _smile_ and _smirk_. The _values_ section then assigns the enabled and attribute values.

```json
{
    "flags": {
        "foo": {
            "name": "foo"
        },
        "bar": {
            "name": "bar",
            "attributes": {
                "someAttribute": {
                    "constraints": {
                        "type": "string"
                    }
                },
                "otherAttribute": {
                    "constraints": {
                        "type": "number"
                    }
                }
            }
        },
        "fun": {
            "name": "fun",
            "attributes": {
                "smile": {
                    "constraints": {
                        "type": "boolean"
                    }
                },
                "smirk": {
                    "constraints": {
                        "type": "boolean"
                    }
                }
            }
        }
    },
    "values": {
        "foo": {
            "enabled": "true"
        },
        "bar": {
            "enabled": "true",
            "someAttribute": "Hello CommonWell Devs",
            "otherAttribute": 1
        },
        "fun": {
            "enabled": "true",
            "smile": true,
            "smirk": false
        }
    },
    "version": "1"
}
```

These values may also be set in the console.

![Feature Flag Details](FeatureFlagDetails%20(Small).png)  
_Feature Flag Details_

## Deployments

Before a configuration profile version is available to clients, it must be deployed to an environment.

![Application Configuration Deployments](ConfiguratinoDeployments%20(Small).png)  
_Deployments to an environment_

When a configuration profile version is deployed, it is done using either an AWS or custom deployment strategy. An event topic may be monitored

![Deployment Details](DeploymentDetails%20(Small).png)  
_Deployment Details_

### Rollbacks

If any monitor defined for the environment goes into an alarm state, the deployment will rolled back. Additionally, a deployment may be manually rolled back from the AppConfig console any time during the deployment, which includes the bake time. This is found on the deployment details screen.

![Deployment Rollback](DeploymentRollback%20(Small).png)  
_Deployment Rollback Option_

## Service Cost & Quotas

### Cost

AppConfig is available at no extra charge. Only pay for the resources used.

### Quotas

[AWS AppConfig endpoints and quotas](https://docs.aws.amazon.com/general/latest/gr/appconfig.html)]

| Name                                                                 | Default                                | Adjustable | Description                                                                                                                                                                                                                                                                                                                                        |
| :------------------------------------------------------------------- | :------------------------------------- | :--------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Configuration size limit in AWS AppConfig hosted configuration store | Each supported Region: 1,024 Kilobytes | Yes        | AWS AppConfig hosted configurations have a limit for each version of configuration data. Hosted configurations do not have additional costs to use in AWS AppConfig.                                                                                                                                                                                |
| Deployment size limit                                                | Each supported Region: 1,024 Kilobytes | Yes        | AWS AppConfig limits configuration data stored in Amazon S3 to 1 MB. Please see S3s pricing information to understand costs of storage in S3.                                                                                                                                                                                                      |
| Maximum number of applications                                       | Each supported Region: 100             | Yes        | An application in AWS AppConfig is a logical unit of code (a namespace) that provides capabilities for your customers. An application can be a microservice that runs on EC2 instances, a mobile application installed by your users, a serverless application using Amazon API Gateway and AWS Lambda, or any system you run on behalf of others. |
| Maximum number of configuration profiles per application             | Each supported Region: 100             | Yes        | Configuration profiles contain metadata about a particular set of configuration data used by your application.                                                                                                                                                                                                                                     |
| Maximum number of deployment strategies                              | Each supported Region: 20              | Yes        | A deployment strategy defines how configuration deploys, or rolls out, across the collection of instances within a specific application environment.                                                                                                                                                                                               |
| Maximum number of environments per application                       | Each supported Region: 20              | Yes        | An environment corresponds to a grouping of instances associated with an application. Examples of environments include stages such as beta and prod, or application sub-components such as web, mobile, and service.                                                                                                                                |

## AppConfig Clients

### Lambda

Lambda functions can query its feature flag configuration using the AppConfig API directly or by using one of the available Lambda Layer extensions. If using the API, the Lambda function must implement its own strategy for getting and caching the feature flag configuration. Lambda layers can manage the API calls, AppConfig sessions, and the caching of configurations.

### ECS

A sidecar image exists, but it is only for Linux hosts. Containers running on Windows hosts will need to manage AppConfig from and API level or create their own sidecar.
