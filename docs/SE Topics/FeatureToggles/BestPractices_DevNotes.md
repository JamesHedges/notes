<!-- markdownlint-disable MD024 -->

# Best Practices Dev Notes

## Identify Type and Audience

- Type: Reference
- Audience: Collector and CommonWell SEs
  - value
    - Find info quick
    - Concise

## Ideas

### Sidebar for dynamic configuration definition

**Introduction** Section

### Idea: history of FF

- **Head**
  - where did the practices come from
  - when did they evolve
- **Heart**
  - surprise that FFs are not new
- **You**

#### Idea: Problem Statement

To understand where the FF best practices have evolved from, you first need to understand FFs long history.

#### Arc

- **Observe:** If you understand where FFs came from, you better understand and accept the best practices
  - Grew out of conditional compiler statements and static code conditionals to the ability to dynamically change FF value
  - Continuous delivery and independent services drove wider adoption of FFs, making them essential
- **Orient:** It is beneficial to use FFs
  - The history of FFs demonstrates their maturity
- **Decide:** Need to understand the history of FFs
  - Understanding where FFs came from and their long history will give greater appreciation for the maturity of practices
- **Act:** Outline how FFs evolved, including the influence of CI/CD
  - Conditional compiler flags and static code
  - Need to externalize FF configuration
  - Influence of continuous deployment
  - Services architectures have made them essential
  - Complex paths through services not always predictable
  - Enable / disable services based on performance needs

___

### Idea: getting it right

- **Head**
  - why should they follow the practices
- **Heart**
- **You**
  - follow good SE principles

#### Problem Statement

Poorly implemented FFs will make managing the technical debt they create even more difficult.

#### Arc

- **Observe:** Poorly implemented FFs are difficult to manage and may lead to failures
  - Not following FF best practices
  - Not adhering to fundamental software engineering practices
- **Orient:** Knowing best practices can help minimize the technical debt
  - Introducing FFs can adversely affect code quality
- **Decide:** Describe how poorly implemented flags are difficult to manage
  - We need to pay attention to how FFs influence code quality
- **Act:** Show how FFs increase technical debt and can cause failures
  - FFs create conditional paths through code
  - FFs involve introducing orthogonal logic
  - Their presence and temporary nature mean they are technical debt
  - Careless implementations can leave can accelerate code entropy

___

### Idea: learn from others

- **Head**
  - not repeating mistakes of others
- **Heart**
  - joy of not having to suffer discovering what to do / not do
- **You**

#### Problem Statement

The techniques and technologies around FFs take time and consideration to establish.
Following best practices will allow teams to learn from those who have already implemented FFs

#### Arc

- **Observe:** Prior developers have already experienced issues when implement FFs
  - Due to the long history, a sufficient number of well validated best practices have emerged and we should benefit from those who have come before us and follow them
  - A long list of best practices has emerged
- **Orient:** We should learn from their experiences
  - Reading the long list of best practices and their details can take a lot of time.
- **Decide:** Let's look what they have learned
  - TLDR look at the FF best practices
- **Act:** Show a list of FF best practices with short description
  - the practices are
    - ... list them w/ brief description

___
___

**Practices** Section

### Idea: deployment vs. release

- **Head**
  - what is a deployment, release
  - why are they separate
  - how to deploy, release
  - can do fast deployments
  - releases follow own cadence
  - can roll back release
- **Heart**
  - happy not to have pending release blocking deployment
- **You**
  - can roll back release

#### Problem Statement

Modern development practices have altered the way we manage software deployments and releases and how they are treated.

#### Arc

- **Observe:** In a continuous deployment scenario, feature code must be deployed as soon as it is completed. Other factors will be taken on when to releasing a feature.
  - Development will be ready to deploy before the business is ready to release
  - The need to rollback feature releases requires re-deploying prior versions
- **Orient:** Feature release may not coincide with deployment
  - The development deployment and business' release cycle need to be independent of each other.
- **Decide:** Dissociate feature deployment with feature release
  - Adopt the practice of separating deployment and release.
  - FFs enable the ability to independently deploy and release features.
- **Act:** Use FF system to release independently from deployment
  - Continuous deployment and incremental development techniques favors frequent deployments.
  - The business' release cycle is not likely to by in sync with development's cycle.
  - Trunk based development without feature flags will can bring development to a halt
  - A feature that is outside of specification in an environment, especially production, will need to rolled back immediately.

___

### Idea: never reuse or share ff

- **Head**
  - how is this a problem
  - what happens with shared flags
  - why not share
  - what problems will sharing cause
  - not in shared libraries
  - dry applies within a module
- **Heart**
  - mistakes can be devastating / expensive
  - frustrating it makes more work - not DRY
- **You**
  - confident when a flag is set, its scope will be limited
  - code quality, pride in craft
  - no unintended side-affects

#### Problem Statement

Reusing or sharing FFs makes their purpose ambiguous, making them more difficult to manage and exposes the system to the risk of catastrophic failures.

#### Arc

- **Observe:** When a feature flag is reused, its purpose become ambiguous.
  - Share or reused flags may not conform to a flag's stated purpose
  - When the flag's purpose is ambiguous, it can cause confusion with the development, operations, and business teams.
- **Orient:** Thus more difficult to maintain and exposed to failures
  - Understanding a feature flag that is shared or reused can be difficult
- **Decide:** Don't share or reuse FFs
  - It is best not to reuse or share FFs
- **Act:** New feature must have its own FF
  - When a FF is shared or reused its meaning becomes ambiguous
  - Conflict with how a flag is implemented versus its stated purpose means adverse effects can occur when making changes to a shared or reused FF's configuration.
  - Such poorly defined FFs are difficult to manage maintenance and removal

___

### Idea: remove unused flags

- expiration
- **Head**
  - they become technical debt
  - how to make code more maintainable, extensible
- **Heart**
  - happy that future you / peer will not have to navigate FF intrusions
- **You**
  - pride of quality code

#### Problem Statement

When FFs exist in code beyond their useful lifetime, code will become difficult to read and manage. Their presence also increases risks for unintentional flag state changes causing system failures.

#### Arc

- **Observe:** Expired / unused flags often remain in code
  - An unused FF can inadvertently be set to a value not consistent with expectations
  - Presence of flags makes development more difficult
- **Orient:** This reduces its maintainability and make it difficult
  - Unused FFs pose a risk to the system and future development efforts
- **Decide:** Remove FFs when no longer in use
  - When a FF is no longer in use, it needs to removed in a timely manner
- **Act:** Removing FFs from code
  - The presence of FFs represents tech debt and removing them reduces it.
  - Code complexity may be reduced when FFs removed
  - Presence of FFs are a risk vector
  - Cleaner code will facilitate faster maintenance and development of new features

___

### Idea: ownership

- **Head**
  - who created FF, needs to be consulted about it
- **Heart**
  - not having to call around to get info on FF
- **You**

#### Problem Statement

FFs not having a responsible party are at risk of being lost and present in code beyond their lifetime. Not having an owner makes it difficult to determine who is responsible for the FF when issues arise.

#### Arc

- **Observe:** Determining who is responsible for a FF can be difficult
  - When a FF needs attention, a party responsible for it needs to be done in a timely manner
  - FFs may lack any attribute or metadata indicating its responsible party
- **Orient:** If flags indicated the responsible team, finding the owner would be easy
  - Determine responsibility for a FF can be laborious
- **Decide:** Add an owner to the FF
  - Each flag should have an attribute that indicates its owner
- **Act:** How to add an owner
  - When a FF requires development attention, having an indicated owner allows the responsible party easier to locate.
  - Owner can be an individual or team

___

### Idea: limit the number of flags

- **Head**
  - what issues might arise
  - how many is too many
- **Heart**
  - pain having to manage, especially since manual
- **You**

#### Problem Statement

A code base with a large number of FFs will become difficult to reason.

#### Arc

- **Observe:** A module can have a high number of FFs and get difficult to manage
  - An very active code base may accumulate FFs
  - Lack of FF removal plan leads to more FFs in a code base
- **Orient:**  How do you manage too many FFs
  - The presence of a number of FFs makes a code base less maintainable and extensible.
- **Decide:** Place a limit on the number of FFs in a code base
  - Set a limit for a the number of FFs allowed in a code base.
- **Act:** How to limit the number of FFs (example: use FF validation to limit # of FFs in config)
  - A code base having many FFs will need to have a number of them removed in order to manage its maintainability and extensibility.
  - Runs the risk of having FFs that interact.
  - The size of the code base will influence the number of FFs that can be tolerated at any point in time.

___

### Idea: plan to retire flags

- **Head**
  - how this makes thing smoother
  - who should create the plan
  - when to create the plan
- **Heart**
  - more overhead things to do, remember to do
  - can this be automated?
- **You**
  - maintain code integrity

#### Problem Statement

Not having a plan to remove expired FFs puts their timely removal at risk.

#### Arc

- **Observe:** Need to remove old FFs
  - FFs get left in code and accumulate
  - No one monitors when flags need to be removed
- **Orient:** Make removal compulsory
  - It is easy to overlook FF removal
- **Decide:** Plan to remove FFs
  - Schedule FF removal when adding a new one.
- **Act:** How to plan removal
  - Systematically removal will aid keeping FFs clean
  - Add a removal ticket when creating a FF
  - Avoid's overlooking the need to remove a flag
  - Make scheduling flag removal compulsory

___

### Idea: naming conventions (policy?)

- **Head**
  - why do I need a convention
  - what can happen if I don't follow it
  - how can a convention help
- **Heart**
  - frustrating to have a standard imposed
- **You**

#### Problem Statement

When FFs are named, they need to convey their purpose and support uniqueness.

#### Arc

- **Observe:** Flags without meaningful names can be confusing
  - Feature flag names can poorly reflect their purpose.
  - Need a way to make FF names unique
- **Orient:** Name FFs to indicate their purpose
  - FFs named in consistently makes more difficult to reason a flag's purpose
- **Decide:** Create a naming convention
  - A naming convention will keep FF names consistent
- **Act:** Common naming convention
  - Guarantee uniqueness so names are not shared
  - Convention gives engineers a way to make name consistent that all will understand
  - Not following a naming pattern can lead to some confusion on what feature a flag is associated with.
  - Use the convention _tickt#\_domain\_purpose\_lifetime_

___
___

**Deploying FF** Section

### Idea: flag configuration ownership

- **Head**
  - why owned by the application
  - where is it stored
- **Heart**
  - relieved not to have to chase down who is responsible
- **You**

#### Problem Statement

An application's FF configurations will be specific and kept close to the code they support.

#### Arc

- **Observe:** FF configurations can be located anywhere
  - Most FF systems don't have built-in metadata
  - Flag configuration files are spread across source control with the applications
- **Orient:** FF configuration located with the code base it serves is easier to locate and manage
  - It can be difficult to determine who is responsible for a FF
- **Decide:** Place FF configuration with the code base
  - Add FF metadata identifying the flag owner
- **Act:** How to use SCM and manage FF configuration
  - FF platform should allow flag attributes
  - Use the attributes to indicate owner, a person or group
  - Should have the means to query to flag state store

___

### REMOVE Idea: separate of applications config

- **Head**
  - why is there a different cadence
- **Heart**
  - now i have to maintain another deployment
- **You**
  - and yet another thing to keep track of

#### Problem Statement

Application configuration has a purpose and cadence not consistent with FF configuration.

#### Arc

- **Observe:** An FF configuration may be share a file with application configuration. Must deploy app to get FF change.
  - A simple FF change needs an entire application deployment.
  - FF changes may occur at any time.
- **Orient:** Combined FF and application configurations must be deployed together
  - Deploying configurations can be confusing when linked to application deployment
- **Decide:** Let's separate the FF and application configuration
  - Devise a system where FFs are independent of application configuration
- **Act:** How to use SCM to separate configurations and deployments
  - FF configurations have a different cadence than application configurations
  - What are dynamic configurations
  - Avoid having to deploy entire application to only make a flag change effective
  - Maintain a FF version history, who and when.
  - Strategy for separating them

___

### REMOVE Idea: deploy ff versions independently of application

- **Head**
  - like above, why is there a different cadence
  - what is dynamic config
- **Heart**
- **You**

#### Problem Statement

An application's FF state may be changed independently from the application code.

#### Arc

- **Observe:** Combined FF and application configuration get deployed together
- **Orient:** FF change requires application deployment
- **Decide:** Separate application and FF configurations and deploy independently
- **Act:** How to separate FF and application configs

___

### Idea: configuration state store

- **Head**
  - what is it
  - why do I need it
- **Heart**
- **You**
  - state store is separate from config store, can roll back

#### Problem Statement

Applications need to access their FF states from a single source.

#### Arc

- **Observe:** If a FF setting is problematic, need to roll back to previous configuration version
  - FF configurations are store in versions
  - There would be many places to query for each FF config and would need to understand its versioning
- **Orient:** If there is no versioning, it is not possible to roll back
  - Need a way to get the current FF config w/o needing to know the version
- **Decide:** When designing your own FF system or using a third party platform, the FF state store must be separate from the config store
  - We need to have a FF current state store for all dynamic configs
- **Act:** How to separate state store and deploy configurations to it.
  - Place FF state in a centralized store
  - One place to query for current state
  - FF deployments push a config change to state store
  - Rollbacks accomplished by deploying old version to state store

___

### Idea: validate configuration changes

- **Head**
  - how would I do this
  - why would I need to
  - when does this happen
- **Heart**
  - **You** know you should
- **You**
  - safety-net for typos

#### Problem Statement

A FF configuration deployed with errors can result in anywhere from function errors to system failures.

#### Arc

- **Observe:** If a deployed FF configuration has errors, it can cause functional and system errors
  - What happens when a FF configuration is deployed with errors?
  - Does anyone verify deployments?
- **Orient:** When we deploy an FF config, it needs to be validated
  - It is possible to deploy a configuration that has invalid values.
- **Decide:** Use tools to validate configuration
  - All configurations should be validated during deployment
- **Act:** How and when to validate FF configuration
  - When should the config be validated
  - Are there standard mechanisms or tools for configuration validation
  - What happens if validation fails

___
___

**Monitor flags** section

### Idea: finding zombie flags

- **Head**
  - how does this happen
  - when do I look for them
- **Heart**
  - Zombies!
- **You**
  - keep it clean

#### Problem Statement

Identifying abandoned flags can be difficult without the correct context.

#### Arc

- **Observe:** Expired flags in a FF configuration can go undetected
  - FFs may be set to the released state but never removed
  - How do we know when a flag is expired
- **Orient:** Undetected expired flags need to be detected and reported
  - Expired flags can be left in the store and code
- **Decide:** We should monitor for zombie flag
  - We need a way to identify expired FFs
- **Act:** Details on why to monitor for zombie flags
  - Add FF attributes for flag creation and expiration
  - Ability to search for expired flags
  - Can we be alerted when a flag is expired?

___

### Idea: failure to access flag state

- **Head**
  - why would that happen
  - what can I do
- **Heart**
- **You**
  - have a fallback, ability to re-sync

#### Problem Statement

When a client application fails to access a FF configuration an application will fail unless a default behavior is defined.

#### Arc

- **Observe:** When application attempt to communicate with a FF state store, it sometimes fails
  - FF state store requires a network call
  - What do we do when we cannot access FF store
- **Orient:** Tracking FF state store access failures allows systems to detect alert operations
  - FF state store will not always be accessible
- **Decide:** It is best to report FF state store access
  - We need to handle cases where the FF state store access fails
- **Act:** Details about why you should reports failures to access FF state store
  - Have a default value when FF state store access fails
  - Use retries and circuit breakers
  - Cashing mechanism to retain last know FF state
  - Report failed access and keep a metric

___
___

**FF Client implementation** Section

### Idea: decouple decision point from decision logic

- push to the edge
  - **Head**
    - where is the edge
    - how does this help
    - what are toggle point, toggle router, and toggle context
  - **Heart**
    - no fear, you can do this
  - **You**
    - good software engineering

#### Problem Statement

Flags implemented deeper in modules are difficult to maintain and make software more difficult to reason.

#### Arc

- **Observe:** When FF logic is mixed with business logic, code become less cohesive and difficult to test.
  - FFs are implemented placing FF logic in the flow of business logic
  - Code becomes difficult to reason, not maintainable or extensible
- **Orient:** Use SE practices to factor business logic from flag logic and make testable
  - SEs often will implement FFs without considering how it affects the overall code structure
- **Decide:** Abstract the FF use, separating the toggle decision from the path selection
  - Implement FFs in code using good SE practices
- **Act:** Describe abstracting FF decision and alternative paths
  - separate toggle router from toggle point
  - abstract toggle router
  - abstract toggle point from business logic
  - how is this more testable

___

### Idea: avoid multiple layers of flags

- **Head**
  - what are multiple layers
  - how can I avoid it, solve the problem
- **Heart**
- **You**

#### Problem Statement

FFs implemented various abstraction levels become difficult to reason and maintain. They often require extra coordination across services or domains.

#### Arc

- **Observe:** A set of related flags may be placed at different layers of a module or system
  - Feature releases may require coordination across services or code modules
  - Implementations in this case can be messy and complicated
- **Orient:** When you have multiple layers of FFs, especially in different code bases, they require higher levels of coordination
  - Related
- **Decide:** Avoid Multiple Layers of FFs
  - Don't place a set of FFs at various levels
- **Act:** Describe the issues that may be caused by layers of FFs
  - Move implementation higher in the code / service hierarchy
  - Use abstractions
  - Use factories
  - Use release coordination strategy

___

### Idea: nested flags

- **Head**
  - how does this happen
  - why is it discouraged
- **Heart**
  - makes me shiver
- **You**
  - avoid unnecessary complexity

#### Problem Statement

When flags are nested, possible alternative paths will grow exponentially making code difficult to reason and maintain.

#### Arc

- **Observe:** If a FF is nested inside another FF, the create complex paths and are difficult to test and maintain
  - Contained FF now has implied context (container FF)
  - Creates greater number of paths that increase exponentially
  - Very difficult to test the paths
  - Maintenance complexity increased by the presence of nested FFs
- **Orient:** Nested FFs can become problematic to test and maintain
  - FFs are being implemented inside existing FFs
- **Decide:** Avoid placing FFs inside of other FFs
  - Identify when there are nested flags and manage them with the cleanest possible solution
- **Act:** Describe issues caused by nested flags
  - Remove older, expired flags before implementing new
  - Possibly defer new feature until prior FF removed
  - Can flags be abstracted to higher level

### DEMOTE Idea: first impl attempt

> This would be tutorial type, this doc is reference. Put in appendix

- **Head**
  - why isn't this good enough
  - how difficult is this to maintain
- **Heart**
  - can do this quickly
- **You**

#### Problem Statement

A simple FF implementation inserts code into the business logic, hindering a function's cohesiveness.

#### Arc

- **Observe:** FF implementations are done quickly
  - SEs do not spend any time on FF implementations
  - No effort is spent on design and SE practices
- **Orient:** FFs are often implemented with minimal effort
  - FFs can be simply implemented
- **Decide:** Just get it done
  - Make minimal effort to implement a FF
- **Act:** Implement an if statement that gets the FF value and switches paths
  - Insert if statement to quickly implement a FF

___

### DEMOTE Idea: moving decision to another object

> This would be tutorial type, this doc is reference. Put in appendix

- **Head**
  - why is this better
- **Heart**
- **You**

#### Problem Statement

Moving the toggle decision to another object separates the concerns, but the business logic will have too much knowledge about the flags.

#### Arc

- **Observe:** Simple implementation has all decision and path logic nested inside the business logic
  - Must know how and get the configuration
  - FF logic nested inside business logic
- **Orient:**
  - Separate the FF decision from the use of it
- **Decide:**
  - Move FF decision to it's own object
- **Act:** Extract the decision into its own object
  - Add an object that gets the configuration and returns the FF toggle decision

___

### DEMOTE Idea: Inversion of control

> This would be tutorial type, this doc is reference. Put in appendix

- **Head**
  - where do I put this
  - when should it be taken this far
  - can this be injected with IoC container
- **Heart**
  - this complexity scares me
- **You**
  - gaining flexibility

#### Problem Statement

Injecting the toggle router into the business logic with the feature relieves the business object from having to know too much about the toggle router. FF conditionals and alternative paths remain embedded.

#### Arc

- **Observe:** Business logic still has to know about configuration
  - Switched code needs to know how to create FF decision object
  - Businees code still knows about and loads FF configuration
- **Orient:**
  - Business code is still intimate about FF implementation
- **Decide:** Isolate all FF knowledge and decision in toggle router
  - Inject self contained toggle router into business code
- **Act:** Implement revised toggle router
  - Update router to get configurations
  - Inject the toggle router into the business code
  - Use the toggle router without knowing how to create it or obtain FF config

___

### DEMOTE Avoiding conditionals

> This would be tutorial type, this doc is reference. Put in appendix.

- **Head**
  - why go to these length
  - how does this scale
  - how is it more maintainable
- **Heart**
  - no fear
- **You**
  - now my code reads seamlessly

#### Problem Statement

When conditionals are avoided, the business logic can remain isolated and cohesive.

#### Arc

- **Observe:** Business code still knows about FF paths
  - The business logic still has multiple paths embedded in it
- **Orient:** Business logic is interrupted with FF logic for the paths
  - Remove all FF logic from implementation
- **Decide:** Business log is free of any FF conditional statements
  - Move the FF path decision out of business code
- **Act:** Implement a factory with the FF decision logic
  - Create a factory having the toggle router injected
  - Add a factory method that uses the toggle router to instantiate an object with the old or new logic implemented
  - The returned business object is used in the original code
  - Switch out is simply updating the factory

___

### REMOVE Idea: side panel on engineering practices

- **Head**
  - what are these
  - why do I care
- **Heart**
  - what a difference this can make
- **You**
  - code quality (readable, maintainable, extensible)
  - professionalism, pride in craft

#### Problem Statement

FF implementations are not a license to ignore software engineering practices.

#### Arc

- **Observe:** Do we know what good software engineering practices are?
    - Do we understand whit good code quality is?
    - Are we concerned about it?
- **Orient:** Care is not always taken when developing code
    - SEs are sometimes lax in implementations, not taking care to manage code quality
- **Decide:** We should be more concerned with code quality
- **Act:**
    - Make sure code readable, maintainable, and extensible
    - Low coupling & high cohesion
        - Gregor Hohpe’s definition of coupling describes it as the “independent variability of connected systems.” In simpler terms, if a change in system A impacts system B, then we have a coupling issue.
    - Adopt good naming practices and be consistent
    - Refactor as a matter of routine

___
