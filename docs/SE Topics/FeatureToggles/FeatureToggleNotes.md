<!-- omit in toc -->
# Feature Toggle Notes

[[_TOC_]]

## Use-Cases (Scenarios)

> Assuming we are practicing continuous deployment - separating release from deployment

### Release Toggles

- When using trunk based development, allow in-progress features to be checked into a shared integration branch (e.g. Mater) while still allowing that branch to be deployed to production.
- Prevent incomplete feature from being exposed to end users.
- Deploy a service's feature that may be complete but should not be available (marketing, waiting on partner, etc.)
- Deploy a feature before it is fully tested
- They are transitionary and should be short lived

### Experiment Toggles

- To perform multivariate (A/B) testing
- Split users into cohorts and route each down a code path base on the cohort
- Track the aggregate behavior for each cohort and compare the effect of each code path
- Used to make data driven optimizations
- Need to remain in place long enough to generate statistically significant results. How long this period depends upon the traffic patterns
- Experiments that run too long may be influenced by other changes that invalidates the results

### Ops Toggles

- Control operational aspects of system behavior
- Use when performance characteristics are uncertain
- If performance is degraded, must be able to disable the feature. AKA "Kill Switch"
- These switches will not be short lived
- Also used to disable non-vital functionality when under load
- These switches may be long lived
- Best that this type of toggle is quickly applied as there may not be enough time to complete any kind of deployment

### Permission Toggles

- Used to change a feature or product experience certain users receive
- Cases:
  - Premium (subscription)
  - Special offer to select users
  - beta opt-in
- May be long lived (premium)

## Types of Feature Toggles

![Toggle Types](TypesOfToggles.png)

### Static

- Simple static routing decision requiring only an on/off flag
- Toggle router simply relays flag to toggle point

### Dynamic

- Require more sophisticated toggle router
- Uses flag and cohort information to determine toggle status

### Long Lived

- When long lived, don't sprinkle code with if/else statements
- Must be more maintainable

### Transient

- Typically for short lived concerns like feature release
- Simple

## Implementation Techniques

- Avoid
  - Feature flags can be messy at toggle points
  - Toggle points have a tendency to proliferate throughout a codebase

```cs
// Inline toggle router / toggle point
public class InvoiceEmailer
{
    private FeatureFlagConfig _featureFlags;

    InvoiceEmailer()
    {
        _featureFlags = LoadFeatureConfigs();
    }

    public GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        if (features.IsEnables("new_feature_name"))
        {
            return AddOrderCancellation(baseEmail);
        }

        return baseEmail;
    }
}
```

### De-couple Decision Point from Decision Logic

```javascript
// invoiceEmailer.js

const features = fetchFeatureTogglesFromSomewhere();

  function generateInvoiceEmail(){
    const baseEmail = buildEmailForInvoice(this.invoice);
    if( features.isEnabled("next-gen-ecomm") ){ 
      return addOrderCancellationContentToEmail(baseEmail);
    }else{
      return baseEmail;
    }
  }
```

```cs
// Decouple Toggle Router and Toggle Point
public class CreateFeatureDecisions
{
    private FeatureConfigs features;

    CreateFeatureDecisions(FeatureConfigs features)
    {
        Features features;
    }

    public bool IncludeOrderCancellationInEmail()
    {
        return features.IsEnables("new_feature_name");
    }
}

public class InvoiceEmailer
{
    private CreateFeatureDecisions _featureDecisions;

    InvoiceEmailer()
    {
        var features = LoadFeatureConfigs();
        _featureDecisions = new CreateFeatureDecisions(features);
    }

    public Email GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        if (_featureDecisions.IncludeOrderCancellationInEmail)
        {
            return AddOrderCancellation(baseEmail);
        }

        return baseEmail;
    }
}
```

The example may seem reasonable, but:

- A magic string is used in the _if_ statement. This can be brittle
- The decision code **is** the _if_ statement, embedded in the toggle point
- Proliferation of this type of implementation gets messy
- Place toggle decision in a toggle router, not at the toggle point

```javascript
//featureDecisions.js

  function createFeatureDecisions(features){
    return {
      includeOrderCancellationInEmail(){
        return features.isEnabled("next-gen-ecomm");
      }
      // ... additional decision functions also live here ...
    };
  }

//invoiceEmailer.js

  const features = fetchFeatureTogglesFromSomewhere();
  const featureDecisions = createFeatureDecisions(features);

  function generateInvoiceEmail(){
    const baseEmail = buildEmailForInvoice(this.invoice);
    if( featureDecisions.includeOrderCancellationInEmail() ){
      return addOrderCancellationContentToEmail(baseEmail);
    }else{
      return baseEmail;
    }
  }
```

In this example, the decision was moved to _featureDecision.js_

- _createFeatureDecision_ may be simple, but leaves option for expanding the decision in a central location
- Toggle pint is cleaner, more readable
- Requires the function is aware of feature flagging and has an extra module coupled to it
- This coupling make it difficult to control toggle state while under tests

### Inversion of Decision

Can solve the coupling issue through Inversion of Control.

```javascript
//invoiceEmailer.js
  function createInvoiceEmailer(config){
    return {
      generateInvoiceEmail(){
        const baseEmail = buildEmailForInvoice(this.invoice);
        if( config.includeOrderCancellationInEmail ){
          return addOrderCancellationContentToEmail(baseEmail);
        }else{
          return baseEmail;
        }
      },
  
      // ... other invoice emailer methods ...
    };
  }

//featureAwareFactory.js
  function createFeatureAwareFactoryBasedOn(featureDecisions){
    return {
      invoiceEmailler(){
        return createInvoiceEmailler({
          includeOrderCancellationInEmail: featureDecisions.includeOrderCancellationInEmail()
        });
      },
  
      // ... other factory methods ...
    };
  }
```

```cs
// Inversion of Control
public class InvoiceEmailer
{
    private CreateFeatureDecisions _featureDecisions;

    InvoiceEmailer(CreateFeatureDecisions featureDecisions)
    {
        _featureDecisions = featureDecisions;
    }

    public Email GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        if (_featureDecisions.IncludeOrderCancellationInEmail)
        {
            return AddOrderCancellation(baseEmail);
        }

        return baseEmail;
    }
}
```

- Rather than _InvoiceEmailer_ reaching out to _featureDecisions_, the decision is injected at construction
- _InvoiceEmailer now has no knowledge of feature toggles
- The _featureAwareFactory_ centralizes creation of the decision-aware injected object.
- A DI system could potentially be used to inject the decision

### Avoiding Conditionals

So far

- The toggle point has been implemented using an _if_ statement
- Possibly makes sense for a short-lived toggle
- Point conditionals are not advised anywhere where
  - A feature will require several toggle points or
  - It is expected to long lived

A more maintainable option is to implement alternative code paths using the strategy pattern

```javascript
//invoiceEmailler.js

  function createInvoiceEmailer(additionalContentEnhancer){
    return {
      generateInvoiceEmail(){
        const baseEmail = buildEmailForInvoice(this.invoice);
        return additionalContentEnhancer(baseEmail);
      },
      // ... other invoice emailer methods ...
  
    };
  }

//featureAwareFactory.js
  function identityFn(x){ return x; }
  
  function createFeatureAwareFactoryBasedOn(featureDecisions){
    return {
      invoiceEmailer(){
        if( featureDecisions.includeOrderCancellationInEmail() ){
          return createInvoiceEmailer(addOrderCancellationContentToEmail);
        }else{
          return createInvoiceEmailer(identityFn);
        }
      },
  
      // ... other factory methods ...
    };
  }
```

```cs
// Avoid Conditionals
public class InvoiceEmailFactory
{
    private CreateFeatureDecisions _featureDecisions;

    InvoiceEmailFactory(CreateFeatureDecisions featureDecisions)
    {
        _featureDecisions featureDecisions;
    }

    public Email GenerateInvoiceEmail()
    {
        if (_featureDecisions.IncludeOrderCancellationInEmail)
        {
            return new InvoiceEmailer(AddOrderCancellation);
        }
        else
        {
            return new InvoiceEmailer(identityFn);
        }
    }
}

public class InvoiceEmailer
{
    FUNC<Email,Email> EmailEnhancer;

    InvoiceEmailer(FUNC<Email,Email> emailEnhancer)
    {
        EmailEnhancer = emailEnhancer;
    }

    public Email GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        
        return EmailEnhancer(baseEmail);
    }
}
```

- In the example, the invoice emailer is configured (injected) with a content enhancement function
- No conditional is required at the toggle point

## Toggle Configuration

### Dynamic Routing versus Dynamic Configuration

- Dynamic routing is where the routing may be turned on/off without any configuration change. The configuration is not static
- Dynamic configuration is a static configuration with routing rules applied at runtime based on some criteria (cohort, permissions, etc.)

### Prefer Static Configuration

- Feature toggle configurations managed through source control and re-deployments are preferred
  - Source control provides the usual SCM benefits
    - History is reviewable
    - Lives next to code
    - Flag state can be reviewed
    - Flows through CI/CD pipeline -repeatable, consistent
  - IaC

### Approaches for Managing Toggle Configuration

- Sometimes a more dynamic approach is needed
- Need a toolkit of configuration techniques

#### Hardcoded Toggle Configuration

- The most basic technique
- Simply comment / uncomment code
- Or surround it _if def_ compiler instructions
- Does not allow for dynamic re-configuration
- Requires deployment to make effective changes
- Best suited for WIP

#### Parameterized Toggle Configuration

- Apply at runtime using command line parameters
- May optionally specify in environment variables
- Has limitations
  - Becomes difficult to coordinate across a number of processes and environments
  - Requires application redeployment or restart
  - And possibly privileged access to environments

#### Toggle Configuration File

- Store toggle configuration in a structured file
- Re-configure toggles by updating the file
- Usually will require a re-deployment in order to become effective
- Must be coordinated across a fleet

#### Toggle Configuration in App DB

- Place configuration in a centralized store
- Feature toggle configurations are no longer tied to a deployment
- Usually accompanied by a feature toggle dashboard

#### Distributed Toggle Configuration

- Special purpose hierarchial key-value stores are a better fit for managing application configuration. E.g. Zookeeper, etcd, or Consul
- Cloud services are also available, e.g. AWS AppConfig, AWS Evidently, or LaunchDarkly
- Typically formed on a distributed cluster where configurations are shared across all nodes
- Toggle router is on each node making decisions based on toggle configuration which is coordinated across entire fleet
- Configurations may be made dynamically
- Some have admin UI or a custom app

## Overriding Configuration

- Overriding configurations per environment is counter to CD ethos of delivering the exact same bits
- Ideally, all environment should be configured the same (??? don't get why you would assume that)

### Per-request Overrides

- Allow environment-specific configuration overrides based on a per-request basis via special cookie, query parameter, or HTTP header
- Can override settings without affecting other users
- Client can configure own overrides when using persistent cookies (useful for testing)
- Creates a greater attack surface
- Can add encryption, but makes things more complex

## Working with Feature-Flagged Systems

There are a few techniques to make working with feature flagged systems easier.

### Expose Current Feature Toggle Configuration

- Any developer, tester, or operator should be able to find the specific configuration in a given environment
- May be done using an API

### Take Advantage of Structured Toggle Configuration Files

- Store toggle configuration in a structured, human-readable format (like YAML, JSON, etc.)
- Add attributes that would be useful to individuals other than the development team, like Ops
- Can add additional meta-data to toggles, such as owner, date created, or expiration date

### Manage Different Toggle Differently

- Different categories of feature toggles with different characteristics can be handled differently. Even if the underlying technical machinery is the same
- When transitioning a toggle from one scenario to another, the way it is configured will change

### Feature Toggles Introduce Validation Complexity

- The addition of feature toggles makes the system more complex to test
- There are multiple code paths in the artifacts moving through the CD pipeline
- To validate all code paths, we must test with the toggle in both states
- With multiple feature toggles, we get a combinatorial explosion of possible toggles states
- If feature flags do not interact, not every combination requires a test run
- Which to test? Test the combinations most likely to be used in production
- If the feature toggle system does not support runtime configuration, may have to restart / redeploy for different configurations
  - When this is the case, consider an endpoint that allows the feature toggle configuration to be changed while running
  - This tool should not be used in production (disable / remove the endpoint)
- Best to use a real distributed feature toggling system

## Where to Place Toggles

### Toggles at the Edge

- Toggles requiring per-request context (experiment or permission toggles), place them at the edge of service boundaries
- Extra benefit of keeping fiddly feature toggle logic out of the application core
- Placing at the edge also make sense when controlling access to new user-facing features which aren't ready to launch
- Interesting note is that some of the unreleased feature functionality may actually be publicly exposed, but sitting at an undiscoverable URL

### Toggles at the Core

- Some types of lower-level features may be placed deeper within the architecture
- These are usually technical in nature and controls some internal implementation
- Localizing these toggles is the only the only option that makes sense

## Managing the Carrying Cost of Feature Toggles

- Feature flags have a tendency to multiply rapidly
- They do come with a carrying cost
  - Requires the introduction of abstractions or conditional logic
  - Introduce significant testing burden
- Keep the number of feature toggles as low as possible
  - Remove when no longer needed
  - Always add a removal task when introducing a feature toggle
  - Set expiration dates that will fail tests when tripped
  - Place a limit on the number of feature flags allowed in a service at any given time

## Original Article

- Toggle tests should only appear at the minimum amount of toggle points to hide the feature
- Don't try to protect every code path, focus on entry points

## When Feature Flags Go Wrong

### Flag Names

- Flags should have clear, unambiguous names
- Do not reuse flag names our use the same flag in two services
- Flag name should indicate if it is long/short-lived
- Name should inform if it is for cohorts or everyone

### Visibility to Technical (and non-technical) Users

- Flags create system states
- Ensure tha all engineering groups (SE, QE, Ops, SRE, Product) know which flags exist
- Make dev teams know or can find out the flags for other services
- Place flags in a centralized user interface for visibility
- If flag can be changed through one interface, it can be flipped easier and without a deployment
- Support must know about flags necessary for customer access

### Flags ar for Business too

- Can be used by marketing to engage existing and potential customers
- Business partners likely are not technical. Don't have access to config files or may not understand them
- They should be able to turn flags for business functionality on and off

### Control Access to Flags

- Flags can have different use-cases
- Should be visible and controllable by different groups based on the use-cases
- Control who can see and control flags accordingly

### Logging

- Log when a flag is toggled and who made the change

## Feature Flag Best Practices

A report on O'Reilly Learning

### Maintain Flag Consistency

- User should see the same toggle variation no matter how many times it is evaluated
- If the system does not have the notion of  "memory" to remember prior flag allocation, little can be done to maintain a user's flag consistency

### Bridge the "Anonymous" to "Logged-In" Transition

- When user transitions from anonymous to logged-in, maintaining flag consistency can be challenge
- Should anonymous user maintain flag consistency
- Is flag consistency from session to session important?
  - If so, the technique to achieve consistency is to track the user with a cookie

### Make Flagging Decision on the Server

#### Performance

- Gain perceived performance by moving flagging decisions on the server

#### Configuration Lag

- Network latency will impact where the flag decision is made
- When a kill switch is triggered, how does client-side feature-flagging system find out?
- User data is on server side, another reason to do server-side flag decisions

#### Security

- Client side flagging decisions poses a security threat
  - Can be seen and manipulated by bad actors
  - May require and expose user information

#### Implementation Complexity

- When making feature toggle decisions on the client side
  - Increases the complexity of managing feature toggles
  - Create parallel implementations
- Parallel implementations need to be synchronized
  - Client-side caching can be problematic and complicated

#### Incremental, Backward-Compatible Database Changes

- Database schema in place needs to be compatible with the expectations of any newly deployed code
- Code First - new version of our code is backward-compatible with the existing database schema
- Data First - ensure that the new schema is backward-compatible with the existing code
- Big Bang - update data and code simultaneously in a lockstep deployment (mostly will only work with simple systems)
- Expand-Contract Migrations (Parallel Change) - a series of changes that consists of where
  - The schema is expanded to allow for old and new cases
  - During this time, do "Dark Reads" the reads from both old and new, comparing the result and ensuring they are the same
  - Followed by a Code-First change that contracts the schema
  - Remove the old schema when no longer needed

#### Implement Flagging Decisions Close to Business Logic

- Rule of thumb for placing flagging decisions
  - Make flagging decision as close to the business logic powering the flag while retaining sufficient runtime (user) context to make the flagging decision
  - We need to make the flagging decision near the edge of the system where a request first arrives (this is where the user context is available) and then pass that flagging decision on to core services

### Scope Each Flag to a Single Team

- Some feature-flagging systems allow you to "link" different flags together, making one flag as a dependency of the other.
- Using one flag to control more than one feature is an anti-pattern. Creates a dependency between services
- Aim to have each flag owned by a clearly identified team
  - Some features will always require changes across multiple teams. When that happens, consider bending the rule about sharing a flag

### Consider Testability

- Feature flags in an application create conditional alternative execution paths
- High level system testing should ensure the expected behavior is produced when the feature is enabled
- When pursuing lower-level unit testing, isolate the functionality the flag is gating and write tests to target both behaviors
- Reduce the scope of test components to span only a handful of flags or isolate the test, allowing a tester to exercise the main target flag with the rest of the flags turned on

### Have a Plan for Working with Flags at Scale

#### Naming Your Flags

- Define a pattern or naming convention for naming feature flags to keep them organized

#### Managing Your Flags

- Have a plan in place that will keep the number of flags in check and help manage existing flags

#### Establish a Retirement Plan

- Have a plan in place to ensure flags are eventually retired
- When a flag is created, add a retirement task
- Assign an expiry date to every flag
  - Should have a way to communicate when expired
  - Can go as far as creating "time bombs"
  - Less extreme, system complains loudly
- Place a limit on the number of flags active at a given time

#### Finding Zombie Flags

- Have a system for showing
  - Flags that are 100% rolled out or 0% rolled out
  - Flags the have not had their configuration modified in a long time

#### Ownership and Change Control

- Have the ability to assign ownership of a flag, either team or individual
- Flag configurations should have the same type of change control as production code deployments
  - With ownership, allows the application of change control policies

#### Flagging Metadata

- System should have the means to associate metadata to flags
- A tagging system may also be used
- Use tags to track the creation date, expiration data, to indicate life-cycle status, what type of flag, ownership, who can modify, and which domain it affects.

### Build a Feedback Loop

- Operate within a feedback loop
  - Make changes, observe the effects, and use those observations to decide what change to make next
- Unlocks maximum value of a feature-flagging practice
  - Making a change without being able to see the effect of that change easily is like driving a car with a fogged-up windshield
- Many implementations start life with no or very limited integration to the analytics and instrumentation systems that exist that provide a rich mechanism for feedback and iteration

## Managing Feature Flags

### Succeeding with Feature Flags

#### Implementation Techniques 2

##### Keep Decision Point Abstracted from Decision Reason

- Keep toggle config abstracted behind the toggle router for two key reasons
  - Easier to increase the complexity in a toggle config without affecting the rest of the code base.
  - Helps with testing - allows for mocking the toggle router

##### Avoid Multiple Layers of Flags

- Not referring to nested flags
- Move feature toggle to the highest common access point

##### Retire Features

- Makes debugging and testing difficult
- Have a process of retiring and removing feature flags from code.
- Best enforced via code - automate process

#### Testing Flagged Systems

- When there are many flags, it is an unreasonable expectation for the QA team to certify all possible configurations of features before the release

##### Test Individual Features, Not Combinations

- Test each feature in isolation for all states of the feature
- It is assumed that the flags are independent
- If you cannot assume independence, test all possible combinations of the dependent flags

##### How to Automate Testing

- Abstraction of the toggle router is key
- For unit testing, can mock and control the state of the feature
- When regression testing, a toggle router can be backed by a configuration file that hardcodes the state of every feature for a specific series of regressions tests. Initialize the router with the appropriate configuration file.

#### From Continuous Delivery to Continuous Experimentation

- The convergence of continuous delivery and continuous experimentation requires additional tooling to support this paradigm

##### Capabilities Your Flagging System Needs

###### Statistical Analysis of KPIs

- Tie feature flags to KPIs for experiment
- Use feature flags to create control groups for experiments

###### Multivariate Flags

- Multivariate experiments will compare multiple treatments against a control group
- Use a flagging systems define flags with attributes used to create the cohorts

###### Targeting

- Use flag to define a segment of users for whom a feature will be enabled
- Flag may be evolved to take context object for details needed by the toggle router
- Can be used to create a limited release

###### Randomized Sampling

- To infer causality between a feature experiment and changes in KPIs, a control group is needed
- Treatment is the group exposed to a new feature
- Control is the group seeing the baseline behavior, needed to control for biases
- Use feature flag to add to randomly assign users to a control group

###### Version History

- Version history is important for experiments
- Keep a versioned history of changes to the configuration of a flag
- Used to perform statistical analysis

## Effective Feature Management

### Feature Management

#### Deploy != Release

- Feature flags allow a team to create an agreed-upon process for deployment with success criteria that may be defined at each stage
  - How will the feature be released?
  - Should anyone see it now?
  - Who will be getting this feature first?
  - Who will beta test it?
  - Will it be rolled out progressively?
  - Do I need to compare it against the old behavior?
  - Do I need to hit a particular date for an external event?
  - What do I do if something doesn't go right?
- Flags provide the ability to move forward or roll back based on the success criteria
- Progressive delivery: the ability to strategically control when individual users or user segments gain access to new features

#### Percentage Deployments

- Percentage-based rollouts are useful when there is little variation in your targeted user base or  you are more concerned with operational impact of your change

### Advice from the Front Lines

#### Flags and Libraries

- Should flags be included in libraries? No
- Flags are an application-level concern, not a library concern
- Creates additional dependencies
- Ties application's data model (feature toggle context) into the library
- Add parameter to the library indicating which operations to use

#### Feature Flags Versus Configuration Management

- A service's configuration is a set of parameters that is likely to change between deployment environments
- Do not recommend moving configuration data into feature flags
  - Feature flags are dynamic and context sensitive

### Selecting a Feature Management Platform

- A robust feature management platform will solve problems like the following:
  - Distributing information globally and propagating flag rule updates quickly
  - Guaranteeing system redundancy and being able to survive failures with a predictable outcome
  - Ensuring that the appropriate set of people have access to manage flags, and maintaining an audit log of changes
  - Separating different teams, projects, and environments and supporting multiple programming languages and frameworks
  - Targeting specific users or segments with customizable rules

#### Feature Management is Mission Critical

##### Design for Scale

- How to maintain the source of truth for flags
- How information is delivered quickly to where it is needed (servers/end user's devices scattered around the world)
- How that information is updated when state changes
- Critical that whenever the application is evaluating a flag that it always receives the same answer regardless of the server, datacenter, or even the continent where the application resides

##### Polling Versus Streaming

| Distribution | Pros                                              | Cons                                                                                                                                                    |
| :----------- | :------------------------------------------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Polling      |                                                   |                                                                                                                                                         |
|              | Simple                                            | Inefficient.                                                                                                                                            |
|              |                                                   | All clients need to connect momentarily, regardless of whether there is a change                                                                        |
|              | Easily cached                                     | Changes require roughly twice the polling interval to propagate to all clients                                                                          |
|              |                                                   | Because of long polling intervals, the system could create a "split brain" situation, in which both new flag and old flag states exist at the same time |
| Streaming    |                                                   |                                                                                                                                                         |
|              | Efficient at scale                                | Requires the central service to maintain connections for every client                                                                                   |
|              | Each client receives messages only when necessary |                                                                                                                                                         |
|              | Fast propagation                                  | Assumes a reliable network                                                                                                                              |
|              | Changes can be pushed out to clients in real time |                                                                                                                                                         |

##### Design for Failure

- Ensure that you have a default option and that your defaults are safe and sane
- While the system is running, it should be:
  - Resilient to momentary interruptions
  - Able to reestablish a connection to the platform
  - Resynchronize to the true state of the world

##### Design for Collaboration

- Separate flags into different projects and help the developers avoid the mental overhead of considering hundreds of flags that they don''t need to care about
- Owner/maintainer should be assigned when created
  - That developer is responsible for the life cycle of the flag, including cleaning it up when it's no longer needed
- Use role-based access to ensure that the people have appropriate access to the right flags, or even limit permissions per environment
- Keep an audit trail of change made to each flag

##### Adoptable

- Most modern SaaS applications are composed of many different programming technologies, and multiple languages
- Important that feature flags work consistently across all applications
- The feature management platform should support all components of the application, with simple SDKs that present a similar API

## Feature Toggles: the Good, the Bad, and the Ugly

### Rules

- Never reuse a toggle
- Name toggles well
- Keep lifespan short
- Architecture matters
- Monitor toggles
  - Alerts
    - Toggle isn't queried
    - Toggle stat hasn't changed

### Additional Points

- TDD - Need fast feedback loop
  - speed of release matters
- Feature toggles adds complexity
- Branch by abstraction

## Terms

| **Term**             | **Definition**                                                                                             |
| :------------------- | :--------------------------------------------------------------------------------------------------------- |
| toggle point         | point in the code base where a code path is chosen based on a feature flag                                 |
| toggle router        | method implementing the toggle logic - the code making the flagging decision                               |
| toggle configuration | the set of rules that the toggle router uses to decide the treatment of a give feature                     |
| toggle context       | contextual information that is passed from the toggle point to a toggle router                             |
| cohort               | group of users who consistently experience a feature as always being On or Off                             |
| Release Toggle       | decouples deployment from release                                                                          |
| Ops Toggle           | Toggles that modify a system's operational aspects that can be used to degrade the service when under load |
| Experiment Toggle    | Short-lived toggles that can be used for A/B testing                                                       |
| Permission Toggles   | Mostly long-lived optional feature switches that can be applied for implementing pricing strategies        |
| dark launch          |                                                                                                            |
| canary rollout       |                                                                                                            |
| gate                 |                                                                                                            |
| kill switch          |                                                                                                            |
| ramping up a feature | Gradual (rolling) release of a feature                                                                     |

## References

- [Feature Toggles (aka Feature Flags)](https://www.martinfowler.com/articles/feature-toggles.html)
- [FeatureToggle](https://martinfowler.com/bliki/FeatureToggle.html)
- [When Feature Flags Go Wrong](https://www.infoq.com/articles/feature-flags-gone-wrong)
- [Feature Flag Best Practices](https://learning.oreilly.com/library/view/feature-flag-best/9781492050452/ch01.html)
- [Managing Feature Flags](https://learning.oreilly.com/library/view/managing-feature-flags/9781492028598/preface01.html)
- [Effective Feature Management](https://learning.oreilly.com/library/view/effective-feature-management/9781492038177/introduction01.html)
- [Feature Toggles: the Good, the Bad, and the Ugly](https://www.infoq.com/presentations/feature-toggles/?itm_source=presentations_about_feature_toggle&itm_medium=link&itm_campaign=feature_toggle)
