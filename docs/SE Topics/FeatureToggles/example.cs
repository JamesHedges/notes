// Inline toggle router / toggle point
public class InvoiceEmailer
{
    private FeatureFlagConfig _featureFlags;

    InvoiceEmailer()
    {
        _featureFlags = LoadFeatureConfigs();
    }

    public GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        if (features.IsEnables("new_feature_name"))
        {
            return AddOrderCancellation(baseEmail);
        }

        return baseEmail;
    }
}

// Decouple Toggle Router and Toggle Point
public class CreateFeatureDecisions
{
    private FeatureConfigs features;

    CreateFeatureDecisions(FeatureConfigs features)
    {
        Features features;
    }

    public bool IncludeOrderCancellationInEmail()
    {
        return features.IsEnables("new_feature_name");
    }
}

public class InvoiceEmailer
{
    private CreateFeatureDecisions _featureDecisions;

    InvoiceEmailer()
    {
        var features = LoadFeatureConfigs();
        _featureDecisions = new CreateFeatureDecisions(features);
    }

    public Email GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        if (_featureDecisions.IncludeOrderCancellationInEmail)
        {
            return AddOrderCancellation(baseEmail);
        }

        return baseEmail;
    }
}

// Inversion of Control
public class InvoiceEmailer
{
    private CreateFeatureDecisions _featureDecisions;

    InvoiceEmailer(CreateFeatureDecisions featureDecisions)
    {
        _featureDecisions = featureDecisions;
    }

    public Email GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        if (_featureDecisions.IncludeOrderCancellationInEmail)
        {
            return AddOrderCancellation(baseEmail);
        }

        return baseEmail;
    }
}

// Avoid Conditionals
public class InvoiceEmailFactory
{
    private CreateFeatureDecisions _featureDecisions;

    InvoiceEmailFactory(CreateFeatureDecisions featureDecisions)
    {
        _featureDecisions featureDecisions;
    }

    public Email GenerateInvoiceEmail()
    {
        if (_featureDecisions.IncludeOrderCancellationInEmail)
        {
            return new InvoiceEmailer(AddOrderCancellation);
        }
        else
        {
            return new InvoiceEmailer(identityFn);
        }
    }
}

public class InvoiceEmailer
{
    FUNC<Email,Email> EmailEnhancer;

    InvoiceEmailer(FUNC<Email,Email> emailEnhancer)
    {
        EmailEnhancer = emailEnhancer;
    }

    public Email GenerateInvoiceEmail(Invoice invoice)
    {
        var baseEmail = BuildInvoiceEmail(invoice);
        
        return EmailEnhancer(baseEmail);
    }
}
