import json

toggle_config = {
        "tiers_feature": '{ \"enabled\": true, \"tiers\": [\"one\", \"three\", \"five\"] }'
    }

def my_toggle_router(feature, tier) -> bool:
  if feature in toggle_config:
    config = json.loads(toggle_config[feature])
    return config["enabled"] and tier in config["tiers"]
  else:
    return False

def old_path(name, tier):
  print(f"Taking old path for {name}")

def new_path(name, tier, status):
  print(f"Taking new path for {name} with status: {status}")

def main ():
   name = "Joe"
   tier = "five"
   status = "awsome"

   if my_toggle_router("tiers_feature", tier) == True:
     new_path(name, tier, status)
   else:
      old_path(name, tier)

main()