<!-- markdownlint-disable MD024 -->

# House of Data Cards

Premise:

Dealing with heterogeneous data through various transformations and reshaping is an expensive proposition. The structures built on top of this data is inelastic and not robust.

What if we could become more nimble by dealing with heterogenous data without reshaping it. Deep Learning (DL) is a technique for analyzing large heterogeneous data sets

Heterogeneous data is a category of unstructured data with an unknown pace.

## Type of Writing

Pitch

## Who is the audience

(what do they value)
Architects: Robustness, efficiency (cost / value)
Product: Getting their questions answered, being self-sufficient

## Ideas

### Aggregate data from multiple sources for analysis and reporting

(Problem Statement)

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
- **You:** (Character/Ethics)

#### OODA and Tentpole

- **Observe:** (aim - obvious / agree / true)
  - one or two tentpole sentences

>~1 paragraph

- **Orient:** (problem,  relationship between you and the thing)
  - one tentpole sentence

>~1 paragraph

- **Decide:** (topic)
  - one tentpole sentence
- **Act:** (details)
  - up to ~17 tentpole sentences

>As many paragraphs as needed, not more than 5 pages / 10 minutes

### Looking for insights

(Problem Statement)

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
- **You:** (Character/Ethics)

#### OODA and Tentpole

- **Observe:** (aim - obvious / agree / true)
  - one or two tentpole sentences

>~1 paragraph

- **Orient:** (problem,  relationship between you and the thing)
  - one tentpole sentence

>~1 paragraph

- **Decide:** (topic)
  - one tentpole sentence
- **Act:** (details)
  - up to ~17 tentpole sentences

>As many paragraphs as needed, not more than 5 pages / 10 minutes

Traditionally, we build structures, schemas, that take disparate data and forms it into a model we can query and report

When data sources change, the schema is broken

What is the alternative? Paradigm shift

Graph Databases?

Tools like Elasticsearch?

Deep Learning?

Relational database is ACID, services prefer availability

Option: Change Data Capture (Db emits a data change event - Data Pump)

Option: Capture system events (event pump)

### Problem

(Problem Statement)

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
- **You:** (Character/Ethics)

#### OODA and Tentpole

- **Observe:** (aim - obvious / agree / true)
  - one or two tentpole sentences

>~1 paragraph

- **Orient:** (problem,  relationship between you and the thing)
  - one tentpole sentence

>~1 paragraph

- **Decide:** (topic)
  - one tentpole sentence
- **Act:** (details)
  - up to ~17 tentpole sentences

>As many paragraphs as needed, not more than 5 pages / 10 minutes

---
---

Aggregating data in microservices

Anti-pattern

Pumps - Data / Event

Change Data Capture

AWS DynamoDb streams - Kinesis
