# Ideas for SE Blogs an Presentations

- CAP Theorem and Eventual Consistency - Why we favor availability
- TDD
- SE Practices / Behaviors
  - measure
  - test
  - deliver incrementally
  - work iteratively
- AI
- Modern Data Thinking
