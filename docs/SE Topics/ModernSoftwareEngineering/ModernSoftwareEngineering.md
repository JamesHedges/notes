<!-- omit in toc -->
# Modern Software Engineering: Doing What Works to Build Better Software Faster

![Modern Software Enginerring](ModernSoftwareEngineering.png)

## Fundamentals

- Scientific Process
  - Characterize: Make an observation of the current state.
  - Hypothesize: Create a description, a theory that may explain your observation.
  - Predict: Make a prediction based on your hypothesis.
  - Experiment: Test your prediction.

- Optimize for Learning
  - Working Iteratively
  - Feedback
  - Incrementalism
  - Empiricism
  - Being Experimental

- Optimize for Managing Complexity
  - Modularity
  - Cohesion
  - Separation of Concern
  - Information Hiding and Abstraction
  - Managing Coupling

- Tools
  - Testability
  - Deployability
  - Controlling the variables
  - Continuous delivery

## What is Engineering

- Production Engineering: creation of physical things with certain levels of precision and efficiency
- Design Engineering: concern with discovery, learning. Where all ideas, solutions, and designs are treated with skepticism and are proven using empiricism and exploration.

>**Software engineering** is the application of an empirical, scientific approach to finding efficient, economic solutions to practical problems in software.

### Engineering Not Equal to Code

Code or designs are the output of engineering, they are only one aspect.

### Craft

- A craft is not precise and has its limits.
- Computers process billions of cycles per second.
- Difference between craft and engineering is
  - precision
  - scalability
  - manages complexity (compartmentalized)
  - Repeatability
  - Accuracy of measurement
- Software craftsmanship is not enough
  - Skill
  - Creativity
  - Freedom to innovate
  - Apprentice schemes
- Skill, creativity, and innovation are exhibited by design engineers too

### Trade-Offs

- All engineering is a game of optimizations and trade-offs
- Must understand the trade-offs

## Fundamental of an Engineering Approach

- Goal is to define a collection of long-lasting thoughts, ideas, practices, and behaviors under the name of software engineering. They must be fundamental to the reality of software development and robust in the face of change.
- The industry struggles to learn and make progress. Much of the change is ephemeral. Very few are impactful
- We find it difficult to learn new ideas and find it almost impssible to discard old ideas, however discredited they may have become.

### Importance of Measurement

- We don't really measure the performance of software development effectively
- Most metrics are irrelevant (velocity) or harmful (lines of code, test coverage)
- We don't have a defensible measure of productivity
- Need measures to make evidence-based decisions
- Instead of measuring productivity, measure effectiveness (see Accelerate). The measures are
  - stability, tracked by
    - Change failure rate
    - Recover failure time
  - throughput, tracked by
    - Lead time (time from idea to working software)
    - Frequency (how often changes are deployed)
  - These don't say anything about if the right thing was developed

### Foundations of a Software Engineering Discipline

- Experts at learning
  - Five linked behaviors
    - Working iteratively
    - Employing fast, high-quality feedback
    - Working incrementally
    - Being experimental
    - Being empirical
  - Software development is an exercise in exploration and discovery
  - Learning is at the heart of everything engineers do
- Experts at managing complexity
  - Manage the complexity of systems we create aw we create them
  - Symptoms that teams have lost control of their system's complexity
    - big-ball-of-mud
    - out of control technical debt
    - crippling bug counts
    - organization afraid to make changes to systems they own
  - Most of us overestimate our abilities to solve a problem in code
  - Structured way to manage complexity
    - Modularity
    - Cohesion
    - Separation of concerns
    - Information hiding/abstraction
    - Coupling

## Optimize for Learning

### Working Iteratively

- Iteration: a procedure in which repetition of a sequence of operations yields results successively closer to a desired result.
- Allows us to (OODA?)
  - learn
  - react
  - adapt
- Agile teams work in a way that, quite intentionally, mitigates the cost of mistakes
- Predictability (waterfall) versus exploration (Agile)
- Any truly agile process is an exercise in "empirical process control"
  - TDD vs skilled developers thinking hard
  - CI vs feature branching
- Working iteratively is different in some fundamental ways than working in a more planned, sequential approach. It is significantly more effective
- Iteration is at the heart of all exploratory learning

#### Practical Advantages of Working Iteratively

- Narrows focus
- Encourages us to think in smaller batches, taking modularity and separation of concerns more seriously
- Can see and measure when smaller features are finished
- Every small change is releasable
- Iterating on products can be steered, based on good feedback, toward higher value outcomes
- Encourages the reduction of size and complexity of the task
- Increase the rate at which we learn

#### Iteration as a Defensive Strategy

- Waterfall thinking as promulgated on the assumption that change gets more expensive as time goes on
  - It assumes the model is correct
  - Decisions must be made early
  - We have perfect information

![Classic Cost of Change](ClassicCostOfChange.png)

- Agile does not make these assumptions
  - Early in the project, we know the least
  - Apply what we know and learn from it (adapt)
  - Discards ideas that don't work (in waterfall, they live on)
- Agile flattens the cost model

![Agile Cost of Change](AgileCostOfChange.png)

- This is the idea at the heart of continuous delivery

![Iteration in Continuous Deliver](IterationInContinuousDelivery.png)

#### Lure of the Plan

- Waterfall thinking was well intentioned, thought it was best way forward
  - The approach sounds very sensible
    - Think carefully
    - Plan carefully
    - Execute the plan diligently
  - Relates well to production engineering
    - Even that is changing as more flexible manufacturing capabilities evolve
    - Difficult intellectual leap to recognize a paradigm is fundamentally wrong

>Process Wars
>
>- There are no 10x improvements available from language, formalism, or diagraming
>- Early days attempted to replicate how production engineering approached efficiency at scale. This was misguided
>- Software creating is multifaceted and complex. Process of its creation has no relationship to a traditional production problem.
>- Attempts to treat software development like a production problem lead to slow and inefficient creation, delivered the wrong thing, and was difficult to maintain
>- As the discipline exploded, the complexity of processes applied increased
>- The problem was well understood early by leading thinkers
>- By the late 1990s, people began trying new strategies. Those resulted in the Agile Manifesto.

- The lure of the plan is a false one
- In software, it took the Agile revolution to overthrow this thinking
- Organizations still remain plan/waterfall driven
  - Assumes (falsely) they can
    - Correctly identify users' needs
    - Accurately assess the value to the organization if those needs were met
    - Accurately estimate how much it would cost fo fulfill those needs
    - Make a rational decision on whether the benefit outweighs the cost
    - Make an accurate plan
    - Execute the plan without deviation
    - Count the money at the end
- Software does not work like this
- It is not a technology problem; this is a human limitation problem
- To make progress, we must organize ourselves to that our guesses won't destroy us
  - We can only solve the problems that we can understand up front
- Iterative approach is different
  - Does not require perfect information up front. Beginning work when we don't know all the answers and when we don't know how much work is involved **will be disquieting** for some.
  - Relies on exploration and learning
  - Can be guided by a fitness function
  - learn, react, and adapt - Adapting to an emerging reality --> refactoring
  - Only effective strategy for a changing situation

#### Practicalities of Working Iteratively

- How to work iteratively
  - Work in smaller batches
    - Reduces scope of change
    - Limits time-horizon
      - Things less likely to change
      - When things change or there is a misunderstanding, less work is lost
    - At sprint scale, work to completed production-ready code with a small fixed time
    - Provides learning opportunities
    - At development scale, consistent with TDD
      - Iterative approach at a fine resolution
      - Code remains correct (passes tests) with each short iteration
      - More opportunities to experiment and evaluate (must keep open to alternatives)

#### Summary

- Iteration is a controlled approach to learning, discovery, and better software
- We must change the way we work to facilitate it. That can be difficult.

### Feedback

"The transmission of evaluative or corrective information about an action, event, or process to the original, or controlling, source"  
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\- _Merriam Webster Dictionary_

- Unless we can know and understand the results of our choices and actions, we cannot tell if we are making progress
- In practice, organizations are more likely to use guesswork, hierarchy, and tradition are more widely used arbiters for decision making
- Feedback allows us to establish a source of evidence for our decisions
- A planned predictive approach
  - Completely understand all variables
  - Nothing will change the understanding of the plan
  - There is one correct solution, either the problem is simple or you can predict the future
  - Basis of waterfall
- A feedback driven approach
  - All about outcomes
  - Test, measure, adjust, and repeat
  - A more effective and more stable approach
- Feedback is essential component or software that operates in a changing, dynamic, environment

>Among other things, why we need observability

#### Feedback in Coding

- Feedback needs to be fast in order to be effective while coding (waiting extended amount of time for QA not acceptable)
- Employ feedback at multiple levels all the time
- TDD is an fast feedback technique
  - A series of tiny steps that get feedback from tests
  - Feedback cycles are very short
  - Second level feedback when all tests are run - confirms change did not result in other tests failing
- Feedback cycles are short, slow tests will not be effective
- Organizing code changes in a series of tiny steps allows opportunities to reflect on
  - Progress
  - The emerging design
  - Steers developer to better outcomes

#### Feedback in Integration

- Another, higher level of feedback
- How changes will integrate with others
- Automatic through CI mechanisms triggered on commit
- CI vs Feature Branching (FB)
  - The CI practice is to merge all developers' working copies to a shared main branch several times per day (at least once per day)
    - About exposing changes in small increments to evaluation
    - Require feature flagging
    - Requires committing to main branch frequently in order to be effective
    - Feature not required to be "complete" prior to merge
    - Get frequent fine-grained feedback
  - Feature branching
    - About isolating change
    - Defers exposing change to other's work
    - Work on sections of code until they are "complete"
    - Tools augment the merge process - make it easier?
    - Defers feedback

#### Feedback in Design

- If tests are hard to write, that tells you something about the quality of your code
- Attributes of quality code:
  - Modularity
  - Separation of Concerns
  - High Cohesion
  - Information hiding (abstraction)
  - Appropriate coupling (minimized)
- Traditional testing has traditionally been left to the end of the development cycle
- The delays feedback
- Extreme programming placed testing front and center, speeding up the feedback loop
  - This pushed the design particular directions
  - Drive modular design
- TDD first proposed in 1968, Kent Beck reintroduced the concept in the late 1990s
- With TDD, there is pressure applied to write code that is more testable

#### Feedback in Architecture

- Architecture needs to take system testability and deployability seriously
- Some tests will need to test the deployment and configuration of the system
- If tests or deployment take too long, it won't be feasible to run in CI
- This adds constraints on the system (architectural characteristics)
- Two approaches - both adopt continuous CI/CD:
  - Monolithic
    - Must still make small changes
    - In order to work together, protections that good design an CI are necessary
  - Distributed
  - Allows team to develop, test, and deploy services independently
  - Services are not tested together

#### Prefer Early Feedback

- IDE highlight errors as developers type (immediate)
- Unit tests are run frequentyl (TDD) (seconds)
- Full test suite run on commit (minutes)
- CI testing
  - Inclueds unit tests
  - Acceptance tests
  - Security and compliance tests
  - Performance tests
- Allows us to fail fast and avert production bugs
- This is termed ~"shift-left"~ for testing

#### Feedback in Product Design

- Software developers are not paid to make nicely designed, easily testable software. They are paid to create value for the business
- Focus on the continuous delivery of useful ideas into production
- How do we know the ideas that we have are good ones?
- We don't know until we get feedback from consumers of the ideas
- Closing the feedback loop around the creation of product ideas and delivering value:
  - Adding telemetry that allows teams to gather data about which features are used
  - Observe user and the behaviors they exhibit
  - Identify what the customer needs to help define the next generation of products

#### Feedback in Organization and Culture

- Traditional measures of software success included:
  - Lines of code
  - Developer days
  - Test coverage
- Required making subjective judgments (guessing) on the basis of intuition
- None of this correlated with actual software success
  - More lines of code probably means the code is worse
  - Amount of effort put into software in not related to its value
  - Test coverage is meaningless unless the tests are testing something useful
- How do we measure success
  - Create feedback loops
  - Frequent, small deployments
  - Measure and compare current statue and compare to target state
  - Repeat
- This feedback is a fitness function
- See Google DORA group's work and the Accelerate book's findings

### Incrementalism

- Working iteratively is about refining and improving something of a series of iterations
- Incrementalism is about building a system piece by piece
- Both a necessary
  - Develop iteratively
  - Deliver incrementally

#### Importance of Modularity

- Divide problems into smallest possible pieces and solve a single part
  - Each component is simpler and easier to test
  - Easier to deploy and can be done independently
- Increases flexibility
- Forces teams to consider boundaries between modules
  - They represent key points of coupling
  - Focusing on protocols of information exchange make it easy to isolate work
- This is one way to achieve incremental delivery

### Organizational Incrementalism

- The most common approach to implementing change is to standardize processes across an organization
- Problems:
  - Dependent on human creativity - if you could standardize it, you could automate it
- Process and policy needs to leave room for creativity
- Modular approach frees teams to work more independently
- One of the defining characteristics of high-performing teams is their ability to  make progress and to change their minds, without asking permission from any person or group outside their small team
- Small team is ~8 engineers
- Distributed, incremental change is the key
- Modular organizations are more
  - Flexible
  - Scalable
  - Efficient

#### Tools of Incrementalism

- Principles for learning are deeply interlinked with managing complexity
- Most profound tools
  - Feedback
  - Experimentation
  - Modularity
  - Separation of concerns
- Architect the system to limit the scope of change -> modularity
- Refactoring
  - Part of TDD (red, green, refactor)
  - Modern IDEs have tools that support refactoring
  - Results in more stable changes
- Version control
  - Makes it easy to back-out small changes
  - Never far from a "safe place"
- Automated testing
  - Gives protection to move forward with more confidence
  - Impacts design, specifically separation of concerns
  - Testable code is modular (modular in every sense)

#### Limiting the Impact of Change



### Empiricism

### Being Experimental

## Optimize for Managing Complexity

### Modularity

### Cohesion

### Separation of Concerns

### Information Hiding and Abstraction

### Managing Coupling

!!! Quote "what's coupling?"

    Gregor Hohpe’s definition of coupling describes it as the “independent variability of connected systems.” In simpler terms, if a change in system A impacts system B, then we have a coupling issue.

## Tools to Support Engineering Software

### The Tools of an Engineering Discipline

### The Modern Software Engineer

## Other Things

### EWD-1305-1

A programmer has to be able to demonstrate that his program has the required properties. If this comes as an afterthought, it is all but certain that he won't be able to meet this obligation: only if he allows this obligation to influence his design, there ais hope he can meet it. Pure a posteriori verification denies you that wholesome influence and is therefore putting the cart before the horse, but that is exactly what happens in the software houses where "programming" and "quality assurance" are done by different groups. [Needless to say, those housed deliver without warranty.]

>Sounds like a case for TDD
### Separation of Concerns

- [NIST](https://csrc.nist.gov/glossary/term/separation_of_concerns): A design principle for breaking down an application into modules, layers, and encapsulations, the roles of which are independent of one another.
- [Wikipeida](https://en.wikipedia.org/wiki/Separation_of_concerns):a design principle for separating a computer program into distinct sections. Each section addresses a separate concern, a set of information that affects the code of a computer program. A concern can be as general as "the details of the hardware for an application", or as specific as "the name of which class to instantiate". A program that embodies SoC well is called a modular[1] program. Modularity, and hence separation of concerns, is achieved by encapsulating information inside a section of code that has a well-defined interface. Encapsulation is a means of information hiding.[2] Layered designs in information systems are another embodiment of separation of concerns (e.g., presentation layer, business logic layer, data access layer, persistence layer).
- [Software Architect's Handbook by Joseph Ingeno](https://learning.oreilly.com/library/view/software-architects-handbook/9781788624060/8ff905c2-217a-47f0-85c2-789296d42e8d.xhtml): a design principle that manages complexity by partitioning the software system so that each partition is responsible for a separate concern, minimizing the overlap of concerns as much as possible.
