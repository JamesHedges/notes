# Platforms

## What is a Platform

- What is a platform? My working definition
  - A cohesive set of capabilities exposed through synchronous and asynchronous events
    - API, both synchronous and asynchronous
    - Pub/Sub, preferable using an event bus
  - A platform offers no solution, just services
  - You think about them differently

### From Thoughtworks

[The enterprise guide to platform thinking](https://www.thoughtworks.com/content/dam/thoughtworks/documents/guide/tw_guide_enterprise_guide_to_platform_thinking.pdf)

- **Platforms** are technology foundations that accelerate an enterprise’s ability to deliver value for its people and its customers. Practically, a platform could be a suite of applications, a business model foundation that anyone can develop on, or any digital foundation that enables easy access to business and technical capabilities.
- **Platform thinking** is the organizational and operational evolution required to both bring platforms to life successfully and get maximum value from them. It’s the fundamental thinking behind making assets and capabilities as easy to access, iterate on, experiment with and convert into customer value as possible.

![Three Types of Platforms](ThreeTypesOfPlatforms.png)

- **Developer-focused infrastructure platforms** that provide a ’paved road‘ to production, increasing technical quality, improving time to market and mitigating risk through a common, validated approach to security and compliance.
- **Business capability platforms** that accelerate new product development by providing a set of Application Programming Interfaces (APIs) that capture existing business capabilities. These platforms make it easier to mobilize and combine capabilities to create new products in response to customer demands, helping organizations directly target outcomes like customer engagement and satisfaction, and ultimately revenue.
- **Platform business models** where a company creates value by facilitating interactions among consumers, peers and service providers, with all participants benefiting from a ‘flywheel’ effect as the platform grows and scales.

## Value Creation

Reference: [From Pipes to Platforms](https://www.digitalfluency.guide/platforms/from-pipes-to-platforms)

>Platform thinking means focusing on enabling others to co-create value, not just delivering value for them to consume.

### Pipes

- A pipe is a communication channel
- Value flows one way, one-to-many
- Monetize the pipes
- Originates from the company to the customer
- Goals are:
  - efficiency
  - maximizing value through the pipes
- Example: Sales funnel

### Platform

- Relies on network effect
  - Non-linear many-to-many interaction
  - Interactions ripple out
  - Value increases exponentially
  - Example: Telephone network
    - One person with a phone, no value
    - Two people with phones, some value
    - Everyone with a phone, very high value
- Communication is multi-directional
  - Company to customer
  - Customer to customer
  - Customer to company
- Changes the relations ship form producer/consumer to co-creator
- Goal is co-creation of value - a shared purpose
- Example:
  - App store
  - Farmers Market (not digital!)

### What type should you use?

- Both types of platforms are valid
- Pipe thinking can co-exist with platform thinking
- Must be clear on which type you are creating

### Apply Platform Thinking

See [Apply Platform Thinking](https://www.digitalfluency.guide/platforms/apply-platform-thinking)

#### Connecting Resources

- About connecting resources; things, people, idead, connections
- Each should correspond to a value model
- Networking resources generates the network effects
  - Exponential value realization
  - Different business strategies
- Idenify resources that may be connected, or connected better to create network effects in the ecosystem
- Example worksheet:

|**Things**|**People**|**Ideas**|**Connections**|
|:-----|:-----|:----|:----------|
|Equipment|Customers|Copyrights & Patents|Data|
|Money|Employees|Software & Hardware|Transactions|
|Products|Partners|Algorithms|Behaviors|
|Facilities||Brand|Conversations|
|Real estate||Content|Reputation|
|||Data|Social networks|
||||Community|

#### Fundamental Elements

- Shared Purpose:  
What is the goal shared by everyone connected by this platform?
- Platform:  
What is the vehicle that creates exponential value through network effects?
- Mindshift:  
What is the shift from one mental model to another required to use this platform?
- Available Resources:  
What resources (things, ideas, connections) are connected to create network effects:
- Social Currency:  
What is the medium of exchange used to strengthen relationships in this platform's community?
- Network Effects:  
How is value increased as each new member joins and participates?
- 10x Results:  
What exponential outcomes will this platform generate?

|**Shared Purpose**|**Platform**|**Mindshift**|**Available Resources**|**Social Currency**|**Network Effects**|**10x Results**|
|:---|:---|:---|:---|:---|:---|:---|
|Wikipedia|||||||
|"unlock the world's knowledge|Online encyclopedia|From reader to editor|Internet, users, knowledge|Articles, changes, comments|Value from other readers sharing their knowledge as editors|Quantity, quality, and cost (free)|
|Sephora|||||||
|"Be fearless"|Beauty Board|From buyer to expert|Makeup lovers, expertise, purchase history, mobile cameras|Looks, cosmetics, tips|Value from how others are using products|Product usage, customer confidence, loyalty|
|John Deere|||||||
|"Healthier and more productive farming"|Internet of Things system with artificial intelligence|From equipment to "smart farming"|Farmers, machinery, crops, data|Data as currency between networked machines|Value increases as system gets smarter with each new piece of data|Higher crop yields, lower costs, fewer chemicals|

## Network Effect

The network effect is a business principle that illustrates the idea that when more people use a product or service, its value increased. [What is the Network Effect](https://online.wharton.upenn.edu/blog/what-is-the-network-effect/)

Note that network externality has a distinct difference. It is an economics term that refers to how the demand for a product is dependent on the demand of others buying that product. [Network Effect](https://www.investopedia.com/terms/n/network-effect.asp). Positive network externality may lead to a network effect.

### Types of Network Effects

- One-sided (Direct): Relates to the growth of a single groups of user of the platform. More users → more market share (value), e.g. telephone network
- Two-sided (Indirect): Relates to platform that benefit from the growth of two or more groups. More of group A → more of group B, e.g. a marketplace like Amazon
  - Complementary: Occurrences in which separate companies mutually benefit from the growth of related companies. e.g. app builder benefits from increased phone adoption

### How to Influence the Network

- Negative Network Externalities: Those effects that have cause negative feedback and reduce growth. May also be caused by positive feedback resulting in network decay (congestion). Negative network externalities is not the same as negative feedback (a pull toward equilibrium).
- Interoperability: has the effect of making the network bigger, which increases its value. Interoperability increases the number of potential connections and thus attracting new participants. Also influences reduced uncertainty, reduce lock-in, commoditization, and price competition.
- Compatibility: Refers to how two systems can be operated together without changing. Get the benefits for both networks. Strong companies will are less inclined to have compatibility. Smaller, weaker, networks will be more inclined to have compatibility
- Open Versus Closed Standards: standards and interfaces developed through the participation of multiple parties to provide mutual benefit will increase the negative effect. Closed standards, controlled by one party, will decrease network effects and may lead to monopolization

## Data

- To get value from data, put it in motion.
  - Data at rest is just an expense.
  - If it does not have kinetic energy, dump it.
  - If it has no potential energy, ice it.

## Digital ecosystem

A **digital ecosystem** is a complex network of interconnected digital tools, technologies, and services that work together to create a cohesive and integrated environment. Here are some key characteristics:

1. **Interconnected Components**: It includes various digital platforms, applications, and services that interact seamlessly[1](https://en.wikipedia.org/wiki/Digital_ecosystem).
2. **Collaboration and Innovation**: Businesses, partners, and users collaborate within the ecosystem, fostering innovation and co-creation[2](https://www.techopedia.com/definition/digital-ecosystem).
3. **Scalability and Flexibility**: Digital ecosystems are designed to be scalable and adaptable, allowing them to grow and evolve with changing needs[1](https://en.wikipedia.org/wiki/Digital_ecosystem).
4. **Customer-Centric**: They focus on providing a seamless and integrated experience for users, often prioritizing customer needs and feedback[2](https://www.techopedia.com/definition/digital-ecosystem).

Examples of digital ecosystems include tech giants like Apple and Google, which integrate various products and services to provide a unified user experience[2](https://www.techopedia.com/definition/digital-ecosystem).

Does this help clarify what a digital ecosystem is? If you have more questions or need further details, feel free to ask!

### Versus software platform

Certainly! Here’s a comparison between a **digital ecosystem** and a **software platform**:

#### Digital Ecosystem
1. **Definition**: A digital ecosystem is a network of interconnected digital tools, technologies, and services that work together to create a cohesive and integrated environment[1](https://knowledge.insead.edu/strategy/dont-confuse-platforms-ecosystems).
2. **Interconnected Components**: It includes various applications, services, and platforms that interact seamlessly to provide a comprehensive solution[1](https://knowledge.insead.edu/strategy/dont-confuse-platforms-ecosystems).
3. **Collaboration and Innovation**: Encourages collaboration among different stakeholders, fostering innovation and co-creation[2](https://www.forbes.com/councils/forbesbusinesscouncil/2023/05/04/choosing-between-the-platform-and-ecosystem-business-models/).
4. **Customer-Centric**: Focuses on providing a seamless and integrated experience for users, often prioritizing customer needs and feedback[2](https://www.forbes.com/councils/forbesbusinesscouncil/2023/05/04/choosing-between-the-platform-and-ecosystem-business-models/).
5. **Scalability and Flexibility**: Designed to be scalable and adaptable, allowing it to grow and evolve with changing needs[1](https://knowledge.insead.edu/strategy/dont-confuse-platforms-ecosystems).

#### Software Platform
1. **Definition**: A software platform is a foundational environment that supports the development, deployment, and management of software applications[3](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/).
2. **Core Functionality**: Provides core services and capabilities that can be used by various applications and services[3](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/).
3. **Facilitation of Transactions**: Primarily facilitates interactions and transactions between different users or services[4](https://www.signal-2-noise.org/blog/platforms-vs-ecosystems).
4. **Standardization**: Ensures consistency and standardization across different applications and services built on the platform[4](https://www.signal-2-noise.org/blog/platforms-vs-ecosystems).
5. **Developer Focused**: Often designed with developers in mind, providing tools and frameworks to streamline the development process[3](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/).

#### Key Differences
- **Scope**: A digital ecosystem encompasses a broader range of interconnected services and technologies, while a software platform focuses on providing a specific set of core functionalities[1](https://knowledge.insead.edu/strategy/dont-confuse-platforms-ecosystems)[3](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/).
- **Integration**: Digital ecosystems emphasize seamless integration and collaboration among various components, whereas software platforms provide a standardized environment for application development[2](https://www.forbes.com/councils/forbesbusinesscouncil/2023/05/04/choosing-between-the-platform-and-ecosystem-business-models/)[4](https://www.signal-2-noise.org/blog/platforms-vs-ecosystems).
- **User Experience**: Digital ecosystems are highly customer-centric, aiming to provide a holistic user experience, while software platforms are more developer-focused, providing the necessary tools and frameworks for building applications[2](https://www.forbes.com/councils/forbesbusinesscouncil/2023/05/04/choosing-between-the-platform-and-ecosystem-business-models/)[3](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/).

In summary, while both digital ecosystems and software platforms play crucial roles in modern technology landscapes, they serve different purposes and offer distinct advantages.

Does this comparison help clarify the differences? If you have more specific questions or need further details, feel free to ask!

[1](https://knowledge.insead.edu/strategy/dont-confuse-platforms-ecosystems): [INSEAD Knowledge](https://knowledge.insead.edu/strategy/dont-confuse-platforms-ecosystems)
[2](https://www.forbes.com/councils/forbesbusinesscouncil/2023/05/04/choosing-between-the-platform-and-ecosystem-business-models/): [Forbes](https://www.forbes.com/councils/forbesbusinesscouncil/2023/05/04/choosing-between-the-platform-and-ecosystem-business-models/)
[3](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/): [Signal to Noise](https://www.signal-2-noise.org/blog/platforms-vs-ecosystems)
[4](https://www.signal-2-noise.org/blog/platforms-vs-ecosystems): [Carson Group](https://www.carsongroup.com/insights/blog/platforms-vs-ecosystems-the-difference-between-a-solid-digital-client-experience-and-a-so-so-one/)

### AI in the digital ecosystem

AI can play a crucial role in a digital ecosystem by enhancing various aspects of the ecosystem's functionality and efficiency. Here are some key ways AI can be integrated:

1. **Data Analysis and Insights**: AI can process and analyze vast amounts of data generated within the ecosystem, providing valuable insights and enabling data-driven decision-making[1](https://oecd.ai/en/dashboards/ai-principles/P11).

2. **Automation**: AI can automate routine and repetitive tasks, improving efficiency and freeing up human resources for more strategic activities[2](https://linvelo.com/the-role-of-ai-in-digital-transformation-a-comprehensive-guide/).

3. **Personalization**: AI algorithms can tailor experiences and recommendations to individual users, enhancing user satisfaction and engagement[3](https://www.mckinsey.com/capabilities/mckinsey-digital/our-insights/ecosystem-2-point-0-climbing-to-the-next-level).

4. **Predictive Maintenance**: In industrial and IoT applications, AI can predict equipment failures and maintenance needs, reducing downtime and operational costs[4](https://online.hbs.edu/blog/post/ai-digital-transformation).

5. **Enhanced Security**: AI can detect and respond to security threats in real-time, protecting the ecosystem from cyberattacks and ensuring data integrity[5](https://www.techfinitive.com/opinions/breaking-down-the-ai-ecosystem/).

6. **Innovation and Development**: AI can accelerate innovation by enabling rapid prototyping and testing of new ideas, as well as optimizing existing processes[2](https://linvelo.com/the-role-of-ai-in-digital-transformation-a-comprehensive-guide/).

By integrating AI into a digital ecosystem, businesses can leverage these capabilities to create more efficient, secure, and user-friendly environments.

Does this help clarify how AI can be part of a digital ecosystem? If you have more specific questions or need further details, feel free to ask!

## API First

The term "API-first" has gained prominence in recent years as the importance of APIs in software development has grown. While APIs themselves have been around for decades, the concept of prioritizing APIs at the beginning of the development process is more recent.

The idea of an API-first approach emerged as organizations recognized the need for consistent, reusable, and well-documented APIs to support modern software development practices. This approach ensures that APIs are treated as first-class citizens, meaning they are designed and developed before other parts of the application[1](https://swagger.io/resources/articles/adopting-an-api-first-approach/)[2](https://www.postman.com/api-first/).

The term "API-first" became more widely used as companies like Amazon, Netflix, and others began to emphasize the importance of APIs in their development processes. These companies demonstrated how an API-first strategy could lead to more scalable, flexible, and efficient software development[2](https://www.postman.com/api-first/).

In summary, while the exact origin of the term "API-first" is not pinpointed to a specific individual or date, it has evolved alongside the increasing recognition of APIs' critical role in modern software architecture.

Does this help clarify the origins of the term for you? If you have more questions or need further details, feel free to ask!

### Couplings

An API-first approach generally **reduces coupling** rather than increasing it. Here’s why:

1. **Loose Coupling**: APIs act as well-defined interfaces between different components of a system. This means that changes in one component (e.g., the backend) do not necessarily require changes in another (e.g., the frontend), as long as the API contract remains the same[1](https://www.postman.com/api-first/).

2. **Parallel Development**: By defining APIs upfront, different teams can work independently on their respective components. This reduces dependencies and allows for parallel development, which can speed up the overall development process[2](https://www.infoq.com/articles/api-mocking-break-dependencies/).

3. **Flexibility and Scalability**: APIs provide a layer of abstraction that allows for greater flexibility and scalability. Components can be updated, replaced, or scaled independently without affecting the entire system[3](https://visionx.io/blog/api-first-approach/).

4. **Standardization**: An API-first approach encourages standardization across the system, making it easier to integrate new services and components. This standardization helps maintain a consistent and predictable interaction model[4](https://www.astera.com/type/blog/api-first/).

In summary, an API-first approach helps in creating a more modular and decoupled architecture, which can lead to better maintainability and scalability of the platform.

Does this address your concern, or is there a specific scenario you’re thinking about?

## Reference

- [Digital ecosystem](https://en.wikipedia.org/wiki/Digital_ecosystem)
- [Guide to API-first](https://www.postman.com/api-first/)
- [Understanding  the API-First Approach to Building Products](https://swagger.io/resources/articles/adopting-an-api-first-approach/)