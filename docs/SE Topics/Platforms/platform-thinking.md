# Platforms

- Audience: me? sr. engineers, engineering managers
- Type: Reference

## What is a business capability platform?

- Idea:
  - Head:
    - how is a business capability platform defined
    - when to use a platform
    - what type of platforms are there
    - why would a platform be created
    - what are the characteristics of a platform
  - Heart:
    - Platforms are an important tool
  - Character:
    - Platforms should be well thought out
- Problem: The term "plaform" is overused and often mis-applied because we do not have a good working definition of what constitutes a platform.
- Arcs:
  - Observe:
    - The designation of a platform is often used for a solution
    - There are different types of platforms
    - The concept of creating a platform is not thought out very well
  - Orient:
    - Create a definition for a platform and identify their characteristics
  - Decide:
    - Define the platform concept as it applies to infrastructure and applications
  - Act
    - Infrastucture platforms are developer-oriented and focus on technical capabilities.
    - Business model platforms are a product sold to external customers as a service.
    - Business capability platforms expose a service to the enterprise as a product.
    - Platforms can be identified by their characterstics.
    - Platforms are a product and not a solution.
    - Enterprise business platforms capture existing business capabilities
    - Platforms expose their capabilities through an APIs
    - Platforms are focused on a business domain
    - Platform operations are owned by the platform's engineering team
    - Platforms are thoughoughly validated and secure
    - Platforms implement and expose full observability
  
## Platform versus Solution

- Idea:
  - Head:
  - Heart:
  - Character:
- Problem:
- Arcs:
  - Observe
  - Orient
  - Decide
  - Act
