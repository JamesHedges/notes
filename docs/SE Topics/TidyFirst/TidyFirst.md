<!-- omit in toc -->
# Tidy First?

## Tidyings

- Refactor: make changes to code structure that do not change behavior
- Tidying: Subset of refactorings that are small changes that results in easier to reason code
- Always tidy in tiny steps
- Commit tidyings before making behavior changes

### Guard Clauses

- Condition required before a function's logic may be applied
- Don't nest them, each stands on their own

```csharp
if (condition)
    if (not other condition)
        // ...some code...

// Tidy into

if (not condition) return;
if (other condition) return;

// ...some code...
```

- Move decision, when more complex, to its own function

```csharp
if (CheckCondition(a,b)) return
// ...some code...

CheckCondition(a,b)
{
if (not condition) return false;
if (other condition) return false;

return true;
}
```

### Dead Code

- Delete it
- If not sure code is unused, log its use and remove when sure
- Can always retrieve deleted code from source control

### Normalize Symmetries

- Solve common patterns in the same manner. Pick a way and be consistent in the code

```csharp
foo(x)
{
    if (x is not null)
        return x
    x = something;

    return x;
}

// versus

foo(x)
{
    return x is not null 
        ? x
        : something;
}
```

### New Interface, Old Implementation

- Replace difficult / complicated / confusing / tedious interface with the one you would like
- Implement it as a pass-through to the original implementation

### Reading Order

- Reorder code file in the order in which the reader would prefer to encounter it.
- Be sure that language will support the reordering of functions

### Cohesion Order

- When making a change with widely dispersed locations, reorder the code so the elements you need to change are adjacent
- If two files contain coupled code, put them in the same directory

### Move Declaration and Initialization Together

- Variable declaration and its initialization may drift apart
- Easier to reason if they are next to each other
- Must respect any data dependencies

```csharp
fn()
{
    int a;
    int b;

    CodeNotUsingA();
    a = x();
    CodeMaybeUsingA_ButNotB(a);

    b = f(a);
    CodeThatUsesB(b)
}

// tidy like this...

fn()
{
    int a = x()

    CodeNotUsingA();
    CodeMaybeUsingA_ButNotB(a);

    int b = f(a);
    CodeThatUsesB(b);
}
```

### Explaining Variables

- Sometimes expression grow complex and a difficult to reason
- Extract subexpressions into a variable with meaningful name

```csharp
fn()
{
    return new Point("Some long expression", "Another long expression");
}

// tidy like this...

fn()
{
    int x = "Some long expression";
    int y = "Another long expression:

    return new Point(x,y);
}
```

### Explaining Constants

- Replace literals with symbolic constants
- Be careful not to have same constant name appear elsewhere with a different meaning

```csharp
fn()
{
    var response = x();
    if (response.code == 404)
    {
        // ...omg...
    }

    // ...blah, blah, blah...
}

// tidy like this

fn()
{
    int PAGE_NOT_FOUND = 404;
    response = x();
    if (response.code == PAGE_NOT_FOUND)
    {
        // ...omg...
    }

    // ...blah, blah, blah...
}

// don't do this

Fn()
{
    int PAGE_NOT_FOUND = "Page not found"; // <-- changed semantics of constant
    MoreCodeHere(PAGE_NOT_FOUND);
}
```

### Explicit Parameters

Make parameters explicit when they are passed in structures like lists. Tidy by adding an internal function that makes the parameters explicit. Ultimately,  refactor all the uses of the function to use the explicit variables.

```csharp
Fn(list<int> params)
{
    return params[0] + params[2];
}

// tidy like this

int Fn(list<int> params)
{
    return FnBody(params[0], param[2]);
}

int FnBody(int explicitA, int explicitB)
{
    return explicitA + explicitB;
}
```

Another case is when using environment variables deep in the code. Make the parameters explicit, then be prepared to push them up the chain of calling functions. This will make code easier to read, test, and analyze.

### Chunk Statements

When a block of code does and does that, separate them into chunks. This will make the code easier to read and prepare it for your next tidying or refactor (smells like a problem with cohesion).

### Extract Helper

#### Obvious Purpose

When you spot a block of code that has an obvious purpose, extract it into a helper function. Name the function after the purpose.

This is a case of refactoring's Extract Method. If supported in your environment, use it.

#### Update Function

When a function update affects more than one line, extract the change into a function. If it makes sense, inline them back into the code.

```csharp
Fn()
{
    x1 = "do some stuff";
    x2 = "do some more stuff";
    x2 += " needs extra help";
    x3 = "finally do the last stuff";
}

// tidy like this

Fn()
{
    x1 = "do some stuff";
    x2 = X2Delta();
    x3 = "finally do the last stuff";
}

X2Delta()
{
    x2 = "do some different stuff";
    x2 += " with a twist";

    return x2;
}
```

#### Temporal Coupling

In a block of code, when one line (or a set of lines) must be called before another, the are temporally coupled. Extract each into a function.

```csharp
Fn()
{
    a = "Prepare for b";
    b = a + " and b requires a to go first";
}

//tidy like this

Fn()
{
    a = DoA();
    b = DoB(a);
}

DoA()
{
    return "Prepares for b";
}

DoB(string a)
{
    return a + " and b requires a to go first";
}
```

The helper may or may not be useful elsewhere. Keep it around to help with future tidyings.

### One Pile

Tidying code has a bias toward many small pieces of code, both theoretically, to increase cohesion as a path to reducing coupling and to reduce the amount of detail held in memory while trying to reason code. The biggest cost of code is reading it. Tidying gone too fare can hinder understanding it. When this occurs, gather the pieces into on pile and move forward from there.

Symptoms of too many small pieces of code are:

- Long, repeated argument lists
- Repeated code, especially repeated conditionals
- Poor naming of helper routines
- Shared mutable data structures

### Explaining Comments

When reading code and you suddenly gain an understanding that was not apparent, insert a comment immediately. Place the note in the header comment. If the comment is relevant to the problem space, write the note in the context of the domain expert. If the comment is relative to a code defect, leave the comment if it is not appropriate to make the fix a that time.

### Delete Redundant Comments

If you see a comment saying exactly what the code is doing, remove it. Any comment left in code should only add context that is not apparent. For example, give insight to a complex financial formula.

## Managing

- Tidyings are for you
- They are geek self-care
- Get used to designing software a little at a time
- Tidyings are gateway refactorings
- Just because you can tidy doesn't mean you should

### Separate Tidyings

- Tidyings belong in their own PR (not included with behavior changes, i.e. stories)
  - Allows them to be rolled-back without affecting any other work
  - Faster code reviews (faster feedback)

### Chaining

- Tidying often begets tidying. When do you stop (see Part III, Theory)
- How tidying leads to additional tidyings:
  - Guard Clause: Once added, may be tidied into a helper
  - Dead Code: When dead code is removed, other tidyings may be apparent
  - Normalize Symmetries: Once identical code is made identical and different coded different, you may be able to group precisely parallel code into reading order
  - New Interface, Old Implementation: When you have a new interface, you will want to use it. Without a refactor tool, will have to do manually. Fans out to create many tidyings
  - Reading Order: After establishing reading order, you may see opportunities to normalize symmetries
  - Cohesion Order: Elements grouped together for cohesion order are candidates to be extracted into a sub-element
  - Explaining Variables: The righthand side of the assignement to an explaining variable is a candidate for an explaining helper
  - Explaining Constants: Extracting an explaining constant leads to cohesion order. Decide on a way to arrange them and be consistent
  - Explicit Parameters: After making parameters explicit, you may be able to move them to and object and their associated code.
  - Chunk Statements: Precede each chunk with a comment. May extract a chunk as an explaining helper
  - Extract Helper: After extracting a helper you may introduce a guard clause, extract explaining constants and variables, or delete redundant comments.
  - One Pile: After making a big, obvious mess, expect to tidy by chunking statements, adding explaining comments, and extracting helpers.
  - Explaining Comments: After adding explaining comments, move the information in the comment into code, if possible, by introducing an explaining variable, explaining constants, or explaining helper.
  - Delete Redundant Comments: After removing noise, may be able to see better reading order or see the chance for explicit parameters.

### Batch Sizes

- How much tidying before integrating and deploying?
  - Must solve immediate need to support a behavioral change
  - Don't look too far ahead
  - Will the tidyings be easy to integrate and deploy?
  - As more tidyings are added to a batch
    - Collisions: Greater change that there is a conflict with existing code when merged
    - Interactions: Possible interactions when merged the alter behavior
    - Speculation: Too many tidyings tend to get speculative and start looking into a crystal ball
  - Reviews get large and more difficult
  - Keep batch size small

### Rhythm
