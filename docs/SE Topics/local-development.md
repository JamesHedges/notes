# Local Development

Address concerns with how to do microservices development on local machines

## Containers

Idea:

- Build runtime container with all external tools installed
  - Update this container using a scheduled build (no update will be preformed when building app container)
- Build application outside of container
- Can either run the tests in or out of the container
- Container build only copies the application into the container
- When run, all configurations will need to set with `docker run`
- All external dependencies must be stubbed locally

> Won't be able to access AWS services! How are we going to handle that?
