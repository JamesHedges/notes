<!-- omit in toc -->
# TDD Cheat Sheet

- [Three Laws](#three-laws)
- [Fear](#fear)
- [Writing Tests](#writing-tests)
  - [Clean Tests](#clean-tests)
  - [Test Doubles](#test-doubles)
    - [Test Dummy](#test-dummy)
    - [Test Stub](#test-stub)
    - [Test Spy](#test-spy)
    - [Test Mock](#test-mock)
    - [Test Fake](#test-fake)
  - [What is Under Test](#what-is-under-test)
    - [Testing State (Classical Testing)](#testing-state-classical-testing)
    - [Testing Behavior (Mocking)](#testing-behavior-mocking)
    - [TDD Uncertainty Principle](#tdd-uncertainty-principle)
  - [F.I.R.S.T](#first)
    - [Fast](#fast)
    - [Isolated](#isolated)
    - [Repeatable](#repeatable)
    - [Self-verifying](#self-verifying)
    - [Timely](#timely)

## Three Laws

1. You are not allowed to write any production code until you have written a unit test that fails due to its absence.
2. You are not allowed to write more of a unit test than i sufficient to fail. Failing to compile is failing.
3. You are not allowed to write more production code than is sufficient to cause the currently failing test to pass.

The three laws of TDD result in:

- Reduced debug time
- A stream fo "perfect" documentation
- Decoupled design
- Testable code

## Fear

Trusted tests mean reduced fear! Fear of

- Changing the code
- Cleaning the code

## Writing Tests

### Clean Tests

- More than one logical assert
- Accidental complexity
- Too slow
- Wide scope
- Colloquialisms

### Test Doubles

#### Test Dummy

#### Test Stub

#### Test Spy

#### Test Mock

#### Test Fake

### What is Under Test

#### Testing State (Classical Testing)

#### Testing Behavior (Mocking)

#### TDD Uncertainty Principle

- Testing State leaves you uncertain that all answers will be correct
- Mocking provides certainty but leads to Fragile Tests.
- Fragility increases with certainty.

### F.I.R.S.T

#### Fast

#### Isolated

#### Repeatable

#### Self-verifying

#### Timely