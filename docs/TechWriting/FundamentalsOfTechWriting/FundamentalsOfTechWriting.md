# Fundamentals of Technical Writing

O'Reilly Learning Online Seminar
2023/02/02
David Griffiths

[Fundamentals of Tech Writing][file:///C:/Users/jhedges/Documents/Training/Fundamentals%20of%20Tech%20Writing/fundamentaltechwriting1674600409243.pdf]

## Purpose

4 Types of writing:

- Reference: transferring information
- Tutorial: transferring skill
- Pitch: cause action
- Story: transferring experience

![Four Type of Writing](FourTypesOfWriting.png)

## Audience

How to Win Friends and Influence People

Look at things from the audience's perspective
Is the value they are going to receive greater then the time spend reading it.

## Getting Ideas

### Body Metaphor

1. Bones == Ideas
   - Purpose/Type
     - Reference, Tutorial, Pitch, Story
   - Aristotle
     - Head (data & logic), Hear (emotion), 'You' (character)
2. Muscles == Motivation

#### Bones == Ideas

##### Purpose / Type

Reference, Tutorial, Pitch, Story

Aristotle

book: Art of Rhetoric

Where do you get ideas:

- Head (data & logic)
    - 7 W's: who, what when, where, why, how, and how much
- Heart (emotion)
    - people interacting w/ people --> there will be emotion
    - 6 emotions - see two graphics  
![Six Emotions](6%20emotions.png)  
![Effects of Emotions](Effects%20of%20Emotions.png)
- 'You' (character)
    - ethos, ethics
    - People being ethical/unethical
    - People having /lacking experience
    - e.g. courage / cowardice

Capture with a Mind Map

- not in any particular order
- see connections between ideas
- draw line for each _problem? ideas_, at end add head, heart, you ideas

#### Muscles == Motivation

##### The Brain

- Getting past the brain's filter
    - Working memory - what your are currently thinking about
    - Info is filtered into the working memory
    - Filter is to focus on survival
    - Need way to get tech info through the filter
    - Something like a "trojan horse"
    - That would be wrapping info in the form of a problem
    - to motivate your writing, the key way of doing it is by associating it with a problem

##### Find Problems

That are solved/caused

##### Write OODA Arcs

Go from problems to order

- Observe (aim)
- Orient (problem, relationship between you and the thing)
- Decide (topic)
- Act (details)

In writing, instead of calling them OODA loops, the call them arcs

Wraps the problem

Easiest way to write with an OODA loop, write everything backwards

1. What you want to write about
2. Let's ...
3. Get a problem
4. something that is obvious/agree/true

observer ~ 1 paragraph
orient ~ 1 paragraph
decide - as little as a sentence

#### Fat --> Refine

too much material than needed, need to refine it down

In story writing, arcs overlap (e.g. character arcs, story arc). In tech writing they do not. Each arch is centered on a problem. The arcs won't all be the same length.

##### 10 Minute Rule

No arc longer than 10 minutes / 5 pages (up to first 1/2 page is observe, orient, decide). Shorter is OK.

10 minutes is the amount of time people can concentrate (was longer in past). At the end of the 10 minutes, the filter starts to block out information

##### Triage arcs

Mark each as

- OK
- Demote
- Remove

demoted material is something that may not be interesting enough for the main text, possibly place in them appendices

#### Skin == Write-Up

Writing part of the writing process. Turn the arcs into body text

Tent pole sentences - key thoughts, act phase will have many. Turn each one into a paragraph

Then add structure

##### Make Skimmable

- Navigate by headings, structure text
- Look at the headings in isolation
- Headings are worth spending time on
- This includes visuals

don't use too many subtitles

##### Visuals / Code

Build in stages. Find metaphors in Google Images.

Be consistent in the use of visuals (style, metaphors, etc)

Show vs tell - code and diagrams are the show (the car chase). Worth thinking about

Use side panels

Info graphics is a way to break things up to consume via skimming in multiple passes. This is a magazine style. Feels like it's easier to read.

Pages shouldn't look the same (print and squint at pages, do they all look the same)

## Writers Block

Typical causes

- Apathy
    - You haven't found the problem
    - Stare into space
- Fear of failure
  - Deliberately write a bad version
      - Impossible to write a version as bad as it can be
      - You now have something to "fix"
- Getting started
    - Write the start at the end

## Tools

- Blank paper + 4 color pen
- Legal pads
- Outlining software (e.g. org-mode or Omnioutliner)
- Basic text editor
- Site blocker
- SEO tool (e.g. <https://seoscout.com/tools/text-analyzer>)
- "Swipe file" <https://swipted.co>
- Grammarly

## Summary

- Identify a purpose
- Match to type (reference, tutorial, pitch, or story)
- Structure writing around problems
- Use an "OODA" sequence
- Use the 10 minute rules
- Make writing "skimmable"

## References

- [The Browser API Worked Example](https://gist.github.com/dogriffiths/6a45d54481c754863b16ab12cd8adc93)
- [THE COMPLETE GUIDE ON HOW TO MIND MAP FOR BEGINNERS](https://blog.iqmatrix.com/how-to-mind-map)
- [Drawing a Mind Map from Start to Finish](https://www.mindmapinspiration.com/drawing-a-mind-map-from-start-to-finish/)
- [The Browser API Worked Example](https://gist.github.com/dogriffiths/6a45d54481c754863b16ab12cd8adc93)


[file:///C:/Users/jhedges/Documents/Training/Fundamentals%20of%20Tech%20Writing/fundamentaltechwriting1674600409243.pdf]: file:///C:/Users/jhedges/Documents/Training/Fundamentals%20of%20Tech%20Writing/fundamentaltechwriting1674600409243.pdf