# Writing Process

## What Type of Writing

- Reference --> Info
- Tutorial --> Skill
- Pitch --> cause action
- Story --> Experience

## Who is the Audience

- What do they value

## Ideas

Iterate through these quickly and make multiple passes. Don't get blocked!

- Head (Data & Logic): 7ws - Who, What, When, Where, Why, How, How Much
- Heart (Emotions): anger, disgust, fear, joy, sadness, surprise
- 'You' (Character)
- Capture ideas with a Mind Map

## Motivation - (Problem)

Write a problem statement for each idea.

- Get past the brain's filter by wrapping ideas with a problem
- Find problems that are solved / caused

## Create the Arcs (OODA Loops)

- Observer: aim - obvious / agree / true
    - one or two tentpole sentences
    - ~1 paragraph
- Orient: problem,  relationship between you and the thing
    - one tentpole sentence
    - ~1 paragraph
- Decide: topic
    - one tentpole sentence
    - one sentence
- Act: details
    - up to ~17 tentpole sentences

Work through these backwards, start with Act, then decide, then orient, and finally observe

## Refine (Reduce)

- 10 minute rule: no arc greater than 10 minutes / 5 pages
- Triage arcs by classifying as:
    - OK
    - Demote
    - Remove

## Write

Work the OODA loop from bottom to top

- Tentpole sentence for each arc
- Add paragraphs

## Add Structure

- Headings
  - Don't nest too deep
- Title

## Add Visuals

- Show vs. Tell
- consistent metaphors
- consistent style
- Use:
    - Graphics
    - Code
    - Side panels
    - Info graphic (magazine style)
- Pages' "form" shouldn't all look the same (break things up)

## Make Sure Skimmable

- Navigate by headings, structure text
- Look at headings and visuals in isolation

## Template for each idea

### Problem

```markdown
(Problem Statement)

- **Head:** (Data & Logic: 7ws - Who, What, When, Where, Why, How, How Much)
- **Heart:** (Emotions: anger, disgust, fear, joy, sadness, surprise)
- **You:** (Character/Ethics)
```

#### OODA and Tentpole

```markdown
- **Observe:** (aim - obvious / agree / true)
    - one or two tentpole sentences

>~1 paragraph

- **Orient:** (problem,  relationship between you and the thing)
    - one tentpole sentence

>~1 paragraph

- **Decide:** (topic)
    - one tentpole sentence
- **Act:** (details)
    - up to ~17 tentpole sentences

>As many paragraphs as needed, not more than 5 pages / 10 minutes
```

```markdown
### Template: abbreviated version

- **Head**
- **Heart**
- **You**

**Problem Statement:**

- **Observe**
- **Orient**
- **Decide**
- **Act**
```