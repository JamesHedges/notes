# Understanding different types of documentation in software development

Chapter 2 - [Understanding different types of documentation in software development](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_02.xhtml)

## Getting started and onboarding

## Templates

- Use templates to get started
- Templates resource: [The Good Docs Project](https://thegooddocsproject.dev)

### Templates for a getting started guide

Getting Started guide should:

- It should highlight what makes your product great and why someone should use it
- The guide should follow an opinionated path that showcases the first point but can offer links to where to find alternative paths and more information

## Types of documentation

### Tutorials

- How-tos or guides taking reader to next steps
- If project has identifiable sections or sub-projects, they need tutorial sections
- Organize by function or use case
- Start with "what something does"
- Later, may add "what someone wants to do with it"
- API on need tutorials when they're complex

#### Templates for tutorials

Ideally, a Getting Started guide should have content under the following headings:

- **Overview:** What the reader can expect from the guide.
- **Prerequisites:** What steps the reader needs to take before starting the guide.
- **Installation:** How to install components.
- **Setup:** How to set up components.
- **Parts:** A series of tasks that form the guide. These are individual headings that should actively describe what the reader will learn and accomplish:
    - **Steps:** Each step is under the task. These are individual headings that should actively describe what the reader will learn and accomplish.
- **Summary:** What did the reader learn in this guide, and why is it useful and relevant?
- **Next steps:** Where to go next to build upon the knowledge learned.

### Reference

A reference section could include the following:

- SDK function details
- API endpoint details
- Architecture and design details
- Security and privacy details
- White papers and academic papers

#### SDK references

- Most SDK methods help end users perform certain functions.
- Methods are typically building blocks of applications, so you only need to explain how each building block works.

#### API references

- API endpoints consist of inputs with parameters and outputs with return values.
- Explain how they fit together
- May be generated (OpenAPI)

#### GraphQL references

- Defines a series of types and functions you query instead of endpoints.
- Organizes around data structures, queries, and methods.
- Some claim GraphQL is self-documenting. This is often not enough and consider guides and additional reference content.

#### Architecture and design details

- Not common
- Useful for complex projects with numerous combinations and optional components.
    - Distributed systems
- Doesn't replace essential onboarding documentation.
- Explains the theoretical underpinnings of the components.

#### Security and privacy details

- Project's internal algorithms and precautions

#### Whitepapers and academic papers

- Least common
- Used for technically novel project
- Explains the theory behind the project
- Often interesting and inspirational

### Technical blog posts

- An end-to-end, self-contained piece of content
- Normally outside of documentation
- Teaches a practical technical lesson
- Much of the advice on structure and language for documentation applies to technical blog posts.
- Differences:
    - Documentation is typically scanned and consumed piecemeal, blogs read start to finish
        - Uses a cohesive narrative
    - Documentation is dry, using a neutral personality, blogs can show more personality
        - Personality and humor are difficult to get right, often easier to leave out
- Be careful with marketing-heavy content, may lead to distrust

## References

- [The Good Docs Project](https://thegooddocsproject.dev)
- [The Good Docs Project - GitLab](https://gitlab.com/tgdp/templates/-/blob/main/README.md)
- [Google developer documentation style guide](https://developers.google.com/style)
- [Microsoft Writing Style Guide](https://learn.microsoft.com/en-us/style-guide/welcome/)
- [Vale](https://vale.sh/)
- [Vale + VS Code](https://github.com/chrischinchilla/vale-vscode?tab=readme-ov-file#vale--vs-code)
