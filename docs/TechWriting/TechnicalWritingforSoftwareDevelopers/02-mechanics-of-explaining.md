# Language and the fundamental mechanics of explaining

Chapter 3 - [Language and the Fundamental Mechanics of Explaining](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_03.xhtml)

!!! Tip "Objective"
    Ensure that all the words you fill that documentation with are clear, confident, and trusted by the people you want to read it.

## Common reasons for not writing confidently

- Not a native speaker
    - Using unnecessary words make reading more difficult for non-native speakers
- Intentionally vague
    - Lack of knowledge?
        - Stuffed with unnecessary words
        - Clever words != clever writing
    - Showing off
- Marketing and product reasons
    - Facts only
    - Marketing material leads to distrust
        - Sometimes used to hide product inadequacies
- Reducing cognitive load
    - Everything that can be done to reduce cognitive load is a positive
    - As small issues accumulate, confusion and frustration increases
- Inclusive language
    - Words affect understandability
    - Non-inclusive language includes
        - Patronizing
        - Rude
        - Overly negative
        - Biased
        - Outdated
        - Unhelpful
    - See [**Inclusive language: in more detail**](#inclusive-language-in-more-detail)

## How to improve your writing

### Consistency

- Terminology
    - Describe terms
    - Define them
    - Stick to them
- Determine documents general tone of voice and stick to it
    - Helps set the mood of the writing piece and influences the reader interpretation
    - Stick to it
- It's a good practice to expand acronyms before using them
- Terminology
    - Be careful with non-standard terms
    - Verify standard use of terms
    - Start with a terminology guide

!!! tip "Identifying tone of voice"
    Identifying a document's tone of voice involves analyzing several key elements:

    1. **Word Choice**: Look at the vocabulary used. Formal documents often use sophisticated language, while informal ones might use slang or colloquial expressions.

    2. **Sentence Structure**: Short, simple sentences can indicate a casual or straightforward tone, while longer, complex sentences might suggest a more formal or academic tone.

    3. **Punctuation**: The use of exclamation marks, question marks, and ellipses can convey excitement, curiosity, or hesitation, respectively.

    4. **Emotional Overtones**: Pay attention to the emotions conveyed through the text. Is it optimistic, sarcastic, serious, or humorous?

    5. **Audience and Purpose**: Consider who the intended audience is and what the document aims to achieve. A marketing brochure might have a persuasive and enthusiastic tone, while a legal document will be more neutral and precise¹(https://www.semrush.com/blog/how-to-define-your-tone-of-voice/)²(https://www.contenthero.co.uk/how-to-create-a-tone-of-voice-document/).

    By examining these aspects, you can get a clearer sense of the document's tone of voice. If you have a specific document in mind, feel free to share more details, and I can help you analyze it further!

    Source: Conversation with Copilot, 10/12/2024

    1. [How to Define Your Brand’s Tone of Voice (+ Template) - Semrush](https://www.semrush.com/blog/how-to-define-your-tone-of-voice/)
    2. [How to create a tone of voice document for your brand](https://www.contenthero.co.uk/how-to-create-a-tone-of-voice-document/)

### Involving the user

- Passive voice
    - Subject is often clear, but not the actor (generally)
    - Can obscure details and make the presentation less clear
    - Something is being done to the sentence's subject (from Stephen King)
    - Is non-committal, cold
    - Some perceive as passive-aggressive
- Active voice
    - Actor and subject is clear and specific
    - The subject is what the reader is most interested in
    - Is technical writing, use the active voice as much as possible
        - More confident
        - Interactions are clear
    - Subject is doing something
- It isn't always possible to make the actor and subject clear (from Stephen King)
- It's easier to write in passive voice
    - Ok to start with passive voice and switch to active voice in later drafts

!!! Tip "Identifying passive voice"

    - Form of "to be" is sometime, but not always, passive voice
    - Reversed the position of the subject (actor) and the object
        - Active - subject doing something
            - Subject (actor) → Verb → Object (receiver)
        - Passive - focuses on the object, making it the subject, and demphasizes the verb
            - Subject (receiver) → Verb → Object (doer)
      - Doer comes after verb
      - Verb is past participle
      - Can look for forms of "to be": was, is, am, are, have been, has, will be, being, and will have been
    - Look for past participle and forms of "to be"
  
    Then you have passive voice when:

    - The actor is either omitted or occurs after the thing that happened
    - **And** a past participle is placed straight after the form of "to be"

### Keeping it short

- Because what you are explaining is complex doesn't mean you need to write verbose and rambling text
- Writing short, concise text is harder than writing longer text
- As an exercise, write the elevator pitch. Start there

!!! Quote
    There is a famous and often mis-accredited (Pascal, Locke, Franklin, Thoreau, Cicero, Wilson?) quote:

    “If I Had More Time, I Would Have Written a Shorter Letter”

### Removing unnecessary words

Common candidate for "weasel words" are:

- Beginning a sentence with "so"
- Using words such as "simply," "easily," and "just". Makes the reader feel inferior, is patronizing, and is not inclusive
- Using the word "very", it accentuates something you have already said. May come off as disingenuous
- Adverbs and adjectives:
    - Verb - word that describes an action, state, or something that happened
    - Noun - a person, place, or thing
    - Adjective - a word that describes a noun
    - Adverb a word or phrase that modifies or add to an adjective or verb

### Using shorter phrases and words

- Often, there are entire phrases you can replace with one word
    - Lose some nuance, but little meaning
    - Technical writing isn't about nuance
- Classic examples:
    - "In order to" → "to"
    - "Almost all" → "most"
    - "At the present time" → "now"
    - "In many cases" → "often"
    - "In some cases" → "sometimes"

### Don't show off - Let the product speak for itself

- Documentation isn't the place to show off your English skills
- Don't overuse cultural references and colloquialisms
- Documentation isn't the place to show off product or programming skills, place this in a blog post
- Will come across as rude, negative, arrogant, or dismissive

### Don't repeat yourself

- Don't repeat something just said to make a point
- Exception is a summarization section

## Inclusive language: In more detail

### Overly negative language

- Documentation isn't a place for opinions on your product or others'
- What the reader can and can't do with what you are documenting
    - Can easily slide into a negative viewpoint

### Biased language

- We are full of biases
- Don't let them find their way into documentation
- Remove any potentially offensive language

### Gender

- English is mostly non-gendered and isn't necessary
- When referring to individuals, use "they/them". This is now accepted
- When referring to a collective, use a job or role
    - Careful when they are gendered, like "middleman"
- Is "guys" engendered? Better alternatives like:
    - everyone
    - folks
    - all

### Out-of-date language

- Watch for common out-of-date language
    - May be considered offensive
    - It evolves, and the writer must adapt
- Changing terminology might be out of your direct control if it's what the product uses
    - Demonstrate alternative language where possible
- Problematic words and phrases:
    - Master / slave
    - Black list or white list

## References


- [43 Words You Should Cut From Your Writing Immediately](https://dianaurban.com/words-you-should-cut-from-your-writing-immediately)
- [Active vs. Passive Voice: What’s The Difference?](https://word-counter.com/active-vs-passive-voice-whats-the-difference/)
- [Avoid Fillers and Unnecessary Words in Writing](https://www.brandeis.edu/writing-program/resources/faculty/handouts/four-types-unnecessary-words-phrases.html)
- [Chapter 3: Language and the Fundamental Mechanics of Explaining](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_03.xhtml)
- [Four type of unnecessary words and phrases](https://www.brandeis.edu/writing-program/resources/faculty/handouts/four-types-unnecessary-words-phrases.html)
- [​​How do you choose the right tone for your writing piece?](https://thewritepractice.com/tone-in-writing/)
- [How to identify passive voice](https://wordcounter.net/blog/2016/09/14/102274_how-to-identify-passive-voice.html)
- [The Inclusive Naming Word Lists v1.0 ](https://inclusivenaming.org/word-lists/)
- [Tone in writing](https://thewritepractice.com/tone-in-writing/)
