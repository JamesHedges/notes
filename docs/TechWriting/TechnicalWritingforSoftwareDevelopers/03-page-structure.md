# Page structure and how it aids reading

Chapter 4 - [Page Structure and How It Aids Reading](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_04.xhtml)


- Avoid the "wall of text," it's difficult to:
    - scan
    - parse out critical information
- Good layout will help solve these issues
- There are standard layout techniques
    - Use templates

## Humans aren't your only readers

Non-human document reader include:

- Search engine crawlers
- Large Language Models (LLM)
    - feeds for training
    - analysis (for summarizing)
- Screen readers (accessibility)

## The principles of good layout

- Layout rules similar to those for good HTML
- Rooted in centuries of best practices for print layout
- Many tools, site builders, and sites don't conform
- Principles begin with semantics
    - Correct heading and sub-heading levels
    - Paragraph breaks
    - data presentations (tables & graphics)
- Modern web "semantic" markup means more

## A quick primer on the makeup language of the web

- Browsers use Hyper Text Markup Language (HTML)
    - Each browser may display the same markup differently
- Markup is a series of _tags_ that surround page elements
- Not necessary to understand what's happening behind the scenes, but need to understand HTML
- Older style tags don't convey semantic meaning well
- Modern markup is better with semantic meaning, for example
    - `<main>` is the dominant area of a page
    - `<aside>` a portion of the page related to content in `<main>`
    - `<footer>` information about the page content, such as author and copyright
- Tools know how to handle the markup
- Think semantically with markup

## Thinking about pages semantically

- Headings
    - Should be withing heading tags
    - Should cascade
        - Each page may only have on first level heading
        - Browsers can handle at least 5-level deep nesting
            - Any deeper than 5-levels indicates content needs re-organized
- Paragraphs
    - Break them up as much as possible by a group of topics or a few sentences
    - Don't fear whitespace - it guides readers eyes
    - Reduces the "wall of text" problem
- Reader frequently don't read top-to-bottom
    - Use whitespace
    - Draw attention to important information
    - Must have balance

### Lists

- Lists are a common way to break up blocks of text
- Summarizes small pieces of information
- Two types
    - Unordered, aka bullet points
        - Use when ordering isn't important
        - Keep the number of points and sub-points reasonable
    - Ordered, aka numbered
        - Use for a short series of steps
        - May nest, but too many levels get confusing

### Paragraph breaks

- Lists can help break up wall of text, but may be overused
- Avoid links that tell you nothing
- Too many short paragraphs serves no purpose. Typically, start a new paragraph
    - Every 100 to 200 words
    - Or, after 5 or 6 sentences

### Tables

- Tables are easy to misuse or overuse
- Don't use them for layout
- Tables are meant to display:
    - Tabular data
    - Comparisons
    - Relationships

### Admonitions

- Admonitions are a small, marked area of text denoting information relevant to the topic
- Typical admonitions

!!! Info
    additional information of interest but not essential

    MkDocs Example:

    ```markdown
    !!! Info

        content here
    ```

!!! Note
    additional information that's useful to some people in situations

    MkDocs Example:
    
    ```markdown
    !!! Note

        content here
    ```

!!! Tip
    helpful information useful to most, but not essential

    MkDocs Example:
    
    ```markdown
    !!! Tip

        content here
    ```

!!! Caution "Caution or warning"

    somewhat important for most readers

    MkDocs Example:
    
    ```markdown
    !!! Caution "Caution or warning"

        content here
    ```

!!! Danger "Error or danger"

    information that's essential for readers to notice and read

    MkDocs Example:
    
    ```markdown
    !!! Danger "Error or danger"

        content here
    ```

- Some tools provide additional admonition types
- Use sparingly, or they won't stand out

### Tabs

- Classic layout component which let you group related information that may not be relevant to all users
- Not all tools support them
- Example:

=== "C"

    ``` c
    #include <stdio.h>

    int main(void) {
      printf("Hello world!\n");
      return 0;
    }
    ```

=== "C++"

    ``` c++
    #include <iostream>

    int main(void) {
      std::cout << "Hello world!" << std::endl;
      return 0;
    }
    ```

!!! Example "Tabbed layout with MkDocs example"

    ```text
    === "C"

        ``` c
        #include <stdio.h>

        int main(void) {
        printf("Hello world!\n");
        return 0;
        }
        ```

    === "C++"

        ``` c++
        #include <iostream>

        int main(void) {
        std::cout << "Hello world!" << std::endl;
        return 0;
        }
        ```
    ```

## An example of a well-structured page

![Example of a well-structured page](well-structured-page.png)

## Creating document menus and navigation

- Organizing a collection of content pages
- Most tools allow content to be moved and restructured
- Most users prefer to have an internal search function
- Navigation can be a work in progress built around standard patterns and compromises

### Following menu patterns

- Readers don't often arrive at pages sequentially
- They find pages through the use of search engines and third-party links
- Attempt to show an ideal path
- Navigation and structure should follow the same pattern
- Look to achieve balance

!!! Note
    Many tools, like mkdocs-material, build navigation for you through configuration and page structure

### Adding internal search

- Fills in gap where it isn't obvious where to start
- Documentation tools may provide search functionality

### Keeping links working

- Over time, links tend to fall into entropy
- Broken links cause search engines to down rank pages
- Difficult to do, consider a tool that will validate links
- Links should use descriptive text, not just the link's address
    - 3-5 words is sufficient

## References

- [Chapter 4: Page Structure and How It Aids Reading](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_04.xhtml)
- [Mkdocs-material admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)
- [Mkdocs-material content tabs](https://squidfunk.github.io/mkdocs-material/reference/content-tabs/)
- [Mkdocs-material data tables](https://squidfunk.github.io/mkdocs-material/reference/data-tables/)
- [Mkdocs-material footnotes](https://squidfunk.github.io/mkdocs-material/reference/footnotes/)
- [Mkdocs-material grids](https://squidfunk.github.io/mkdocs-material/reference/grids/)
- [Mkdocs-material navigation](https://squidfunk.github.io/mkdocs-material/setup/setting-up-navigation/)
- [Mkdocs-material search](https://squidfunk.github.io/mkdocs-material/setup/setting-up-site-search/)
