# The technical writing process

Chapter 5 - [The Technical Writing Process](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_05.xhtml)

Stages of the writing process:

<!-- no toc -->
- [Scoping and requirements gathering](#scoping-and-requirements-gathering)
- [What to document](#what-to-document)
- [Research and product testing](#research-and-product-testing)
- [Drafting and re-drafting](#drafting-and-re-drafting)
- [Feedback, testing, and maintenance](#feedback-testing-and-maintenance)

## Scoping and requirements gathering

- Documentation requests come from internal and external sources:
    - From a product owner about a new feature in progress
    - A completed feature
    - A feature gap reported others
    - A content gap identified by others
    - A logged issue
- First step is to "define done"
    - The initial effort may not be final, just enough for the current needs
    - There may be predictable changes expected
    - The documentarian may need to save the work for the future
    - Most never considered documentation "done," rather it's a work in progress
- Stakeholders have varying expectations
    - Clarify their requirements
    - Ask followup questions for more details
    - Stakeholder will be clearer about the requirements than their assumptions
        - They're usually making assumptions without realizing it
        - Ask directly

## What to document

- Documentation is an iterative process
    - Typically impossible to document everything at once
    - Exception is for sectors where they require completeness. Such as:
        - Medical
        - Sensitive industrial
        - Contractual requirements
- Where to start?
    - Is there a product roadmap? Use it as a guide
    - Start at the beginning and then the end. Proceed by filling in the gaps over time
        - First, complete a "getting started" guide or two
        - Follow up with reference documentation

## Research and product testing

- If the product is technical, get your hands dirty
- You typically can't learn everything about a system
    - Learn how the components fit
    - Determine information sources and output
    - What's the workflow
- Learn how to probe and "ask the right questions."
- Watch demos and speak to team members
- Product users:
    - Assume nothing
    - Be aware of biases
- Understand any dependencies. For example, the libraries required for a code module
- Learn about:
    - How the application starts
    - How to reset it
    - How to break it
- Use any testing/debugging tools at your disposal
- Review code

## Drafting and re-drafting

- One draft is typically not enough
- Get internal feedback early and often
- If not receiving feedback, ask questions like:
    - What did you think about what you read?
    - If you could change anything about what you read, what would it be?
    - What was easy or difficult to understand and why?
    - How would you explain this content to someone else?
    - Does this address your needs? If not, why not?
- It's not necessary to respond to all feedback. Especially on word choice, grammar, or spelling
    - Some may be mistakes
    - Others where chosen for a reason
- Some feedback is opinions - just say "thank you"
- The documentarian is the expert on how to explain
- Some feedback cycles can get heated
    - Make communications as nuance-free and inclusive as possible
    - Don't write the first thing that comes to mind
    - Be patient, open-minded, and cordial
- When is the feedback cycle compete?
    - For high-risk sectors, there likely will be a review and sign off
    - Other sectors, there's more leeway. Refer to the "definition of done"
    - Recognize when there is too much attachment or need for perfection

## Feedback, testing, and maintenance

- External feedback comes from the product's user community
    - If documentation is open source, users may us a repository's issues function
    - Add a feedback widget
    - When available, get feedback from the support groups
- Can be difficult to handle:
    - May receive spam content, need to filter it out
    - Consider who it is from
    - How many are reporting the same or similar issues?
- Use metrics to help determine prioritize

### Metrics and measuring success

- Track metrics like:
    - Page views
    - Bounce rates
    - Spending excessive time on a page. Or short amounts of time
    - Are the times with spikes of activity
    - How are people consuming the pages
        - Scrolling behavior
        - Mouse resting
        - Look for patterns
    - Do changes in documentation alter metrics?
- Determine most popular pages and keep them updated
- When to update a page?
    - Any change to the company or project style guide
    - Software dependency updates
    - Technical flow no longer working
    - Render in errors found
    - Screenshot updates when output changes

## References

- [Chapter 5: The Technical Writing Process](https://learning.oreilly.com/library/view/technical-writing-for/9781835080405/B20936_05.xhtml)
