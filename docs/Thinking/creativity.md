# Creativity

## Five stages of creative process

[The 5 Stages of Creativity](https://duncanstevens.com/creative-process-five-stages)

- Preparation (some refer this stage to idea generation)
    - Learning - explore a specific topic, or many, whichever suits your intent
    - Be curious - put your mind in play mode and use your imagination
    - Experience - creativity draws on your experiences, have a mentor to draw experience from
- Incubation
    - Just let it go
    - Let ideas marinate
    - This can be fast or slow, it doesn't matter
    - Do something that doesn't take full brain power like walk, take a shower, garden, etc.
    - Share the idea and let it incubate in others' mind
- Illumination (or Insight)
    - Creativity is something that happens, not something that you do
    - The 'Aha' moment
        - Surround yourself with creative stimulus
        - Don't force it
        - Enter an activity (like the activities previously mentioned) with intent and let the subconscious mind focus. The activity can't take too much attention. Can't involve absorbing information
    - "The idea is sitting there, all you have to do is arrive"
        - Don't look for if it isn't there
- Evaluation
    - Reflect on the idea. Does it align with you original concept
    - Have it evaluated by others. They can be from different fields of expertise
    - Use this feed back to decide
        - There may be flaws in the final idea, return to incubation phase
        - Don't be afraid to fail. Discard ideas that don't work. Like those that fail to meet requirements or align with other criteria
- Verification / Elaboration / Execution
    - Perspiration phase, where the real work occurs
    - Shape, develop, fine-tune - make "production ready"
    - Validate the idea is correct
