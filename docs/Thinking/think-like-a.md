# Think Like a Lawyer, a Scientist, or an Engineer

## Types

### Legal

In law, rules are the primary foundation for decision making. Years of study enable lawyers to understand rules and how they should be applied. Much of law regards analyzing an issue and concluding whether something complies or does not comply with these rules. These conclusions can then drive behavior. Conclusions guide people to act appropriately and legally, and guide the state in punishing those who violate laws.

### Scientific

Scientific thinking is an entirely different form of logical analysis. The challenge in science is not to follow the rules or define the rules; the challenge is to discover them. In any truly scientific investigation, we do not know the rules in advance. To discover the rules, we use observation and inference.

### Engineering

The final approach to learning is through engineering. Unlike science, where you build systems to help you learn, in engineering you learn underlying rules so you can build new systems. Engineering is the practice of using science or technology to create new systems. You learn basic rules of nature, software, etc, and on that basis build systems that accomplish the results you want. Software engineering is a form of engineering where you create your own rules (programs). You craft logic for manipulating information to accomplish the output you desire. Software engineering has parallels to legal systems. Legislation is a type of engineering whereby you design laws or rules with a desired output on social behavior.

## Cynefin Framework

![Cynefin Framework](./assets/CynefinFramework.png)

We can make sense of a situation using an approach known as the Cynefin framework. If we cannot understand a situation, it appears disordered. As our understanding grows, we can discern four types of situation: clear, complicated, complex, and chaotic. A clear system (also called "simple" or "obvious") is one in which simple rules can be applied, and there is a well-understood best practice we can employ. If you understand those best practices and follow them, you will achieve reliable results. For a simple situation, all you need to do is know the rule and follow the rule reliably to get good results.

## OODA Loop

![OODA Loop](./assets/ooda-loop.png)

The OODA Framework helps knit these approaches together into a framework for action. OODA stands for Observe, Orient, Decide, Act. The framework was developed by Colonel John Boyd of the US Air Force. He was a legendary fighter pilot and developed this framework to describe how to outmaneuver opponents by understanding their biases and acting more quickly than they can respond to. His principles of energy and maneuverability guided the design of the F-16 fighter jet. This framework has been adopted across many branches of the military, and is widely influential in the business and technology world.

## References

[Do You Think Like a Lawyer, a Scientist, or an Engineer?](https://www.infoq.com/articles/think-lawyer-scientist-engineer/)
