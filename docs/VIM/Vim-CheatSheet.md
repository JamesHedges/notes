<!-- omit in toc -->
# VIM Cheat Sheet

## Basics

| Command     | Keys           | Description                                                            |
| :---------- | :------------- | :--------------------------------------------------------------------- |
| Help        | :help [keyword | Performs a search of help documentation for whatever keyword you enter |
| Open File   | :e [file]      | Opens a file, where [file] is the name of the file you want opened     |
| Save        | :w             | Saves the file you are working on                                      |
| Save As     | :w [filename]  | Allows you to save your file with the name you've defined              |
| Save & Quit | :wq            | Save your file and close Vim                                           |
| Quit        | :q!            | Quit without first saving the file you were working on                 |

## Movement

| Command             | Keys     | Description                                                                   |
| :------------------ | :------- | :---------------------------------------------------------------------------- |
| Left                | h        | Moves the cursor to the left                                                  |
| Right               | l        | Moves the cursor to the right                                                 |
| Down                | j        | Moves the cursor down one line                                                |
| Up                  | k        | Moves the cursor up one line                                                  |
| Top                 | H        | Puts the cursor at the top of the screen                                      |
| Middle              | M        | Puts the cursor in the middle of the screen                                   |
| Bottom              | L        | Puts the cursor at the bottom of the screen                                   |
| Start Next          | w        | Puts the cursor at the start of the next word                                 |
| Start Prev          | b        | Puts the cursor at the start of the previous word                             |
| End                 | e        | Puts the cursor at the end of a word                                          |
| Begin Line          | 0        | Places the cursor at the beginning of a line                                  |
| End Line            | $        | Places the cursor at the end of a line                                        |
| Stat Next Sentence  | )        | Takes you to the start of the next sentence                                   |
| Start Prev Sentence | (        | Takes you to the start of the previous sentence                               |
| Next Block          | }        | Takes you to the start of the next paragraph or block of text                 |
| Prev Block          | {        | Takes you to the start of the previous paragraph or block of text             |
| Page Down           | Ctrl + f | Takes you one page forward                                                    |
| Page Up             | Ctrl + b | Takes you one page back                                                       |
| Start File          | gg or \[\[ | Places the cursor at the start of the file                                    |
| End File            | G or \]\]  | Places the cursor at the end of the file                                      |
| To Line             | #G       | Where # is the number of a line, this command takes you to the line specified |

## Editing

| Command              | Keys     | Description                                                                        |
| :------------------- | :------- | :--------------------------------------------------------------------------------- |
| Copy Line            | yy       | Copies a line                                                                      |
| Copy Word            | yw       | Copies a word                                                                      |
| Copy From Cursor     | y$       | Copies from where your cursor is to the end of a line                              |
| Highlight Char       | v        | Highlight one character at a time using arrow buttons or the h, k, j, l buttons    |
| Highlight Line       | V        | Highlights one line, and movement keys can allow you to highlight additional lines |
| Paste                | p        | Paste whatever has been copied to the unnamed register                             |
| Delete Highlighted   | d        | Deletes highlighted text                                                           |
| Delete Line          | dd       | Deletes a line of text                                                             |
| Delete Word          | dw       | Deletes a word                                                                     |
| Delete to End        | D        | Deletes everything from where your cursor is to the end of the line                |
| Delete to Begin      | d0       | Deletes everything from where your cursor is to the beginning of the line          |
| Delete to Begin File | dgg      | Deletes everything from where your cursor is to the beginning of the file          |
| Delete to End File   | dG       | Deletes everything from where your cursor is to the end of the file                |
| Del Char             | x        | Deletes a single character                                                         |
| Undo                 | u        | Undo the last operation; u# allows you to undo multiple actions                    |
| Redo                 | Ctrl + r | Redo the last undo                                                                 |
| Repeat               | .        | Repeats the last action                                                            |

## Searching

| Command                       | Keys                           | Description                                                                                                            |
| :---------------------------- | :----------------------------- | :--------------------------------------------------------------------------------------------------------------------- |
| Search Next                   | /[keyword]                     | Searches for text in the document where keyword is whatever keyword, phrase or string of characters you're looking for |
| Search Prev                   | ?[keyword]                     | Searches previous text for your keyword, phrase or character string                                                    |
| Repeat Search                 | n                              | Searches your text again in whatever direction your last search was                                                    |
| Repeat Search Other Direction | N                              | Searches your text again in the opposite direction                                                                     |
| Replace All                   | :%s/[pattern]/[replacement]/g  | This replaces all occurrences of a pattern without confirming each one                                                 |
| Replace w/ Confirm            | :%s/[pattern]/[replacement]/gc | Replaces all occurrences of a pattern and confirms each one                                                            |

## Multiple Files

| Command           | Keys            | Description                                                                       |
| :---------------- | :-------------- | :-------------------------------------------------------------------------------- |
| Next Buffer       | :bn             | Switch to next buffer                                                             |
| Prev Buffer       | :bp             | Switch to previous buffer                                                         |
| Close Buffer      | :bd             | Close a buffer                                                                    |
| Split File Horz   | :sp [filename]  | Opens a new file and splits your screen horizontally to show more than one buffer |
| Split File Vert   | :vsp [filename] | Opens a new file and splits your screen vertically to show more than one buffer   |
| List Buffers      | :ls             | Lists all open buffers                                                            |
| Split Window Horz | Ctrl + ws       | Split windows horizontally                                                        |
| Split Window Vert | Ctrl + wv       | Split windows vertically                                                          |
| Switch Window     | Ctrl + ww       | Switch between windows                                                            |
| Quit Window       | Ctrl + wq       | Quit a window                                                                     |
| Window Left       | Ctrl + wh       | Moves your cursor to the window to the left                                       |
| Window Right      | Ctrl + wl       | Moves your cursor to the window to the right                                      |
| Window Down       | Ctrl + wj       | Moves your cursor to the window below the one you're in                           |
| Window Up         | Ctrl + wk       | Moves your cursor to the window above the one you're in                           |

## Marking

| Command        | Keys     | Description                                                                                                     |
| :------------- | :------- | :-------------------------------------------------------------------------------------------------------------- |
| Visual Mode    | v        | starts visual mode, you can then select a range of text, and run a command                                      |
| Line Vis Mode  | V        | starts linewise visual mode (selects entire lines)                                                              |
| Block Vis Mode | Ctrl + v | starts visual block mode (selects columns)                                                                      |
| Block ()       | ab       | a block with ()                                                                                                 |
| Block {}       | aB       | a block with {}                                                                                                 |
| Inner Block () | ib       | inner block with ()                                                                                             |
| Inner Block {} | iB       | inner block with {}                                                                                             |
| Mark Workd     | aw       | mark a word                                                                                                     |
| Exit Vis Mode  | Esc      | exit visual mode                                                                                                |
| --             | --       | Once you've selected a particular range of text, you can then run a command on that text such as the following: |
| Del Marked     | d        | delete marked text                                                                                              |
| Yank Marked    | y        | yank (copy) marked text                                                                                         |
| Shift Right    | >        | shift text right                                                                                                |
| Shift Left     | <        | shift text left                                                                                                 |
| Swap Case      | ~        | swap case (upper or lower)                                                                                      |

## Tab

| Command       | Keys          | Description                                                          |
| :------------ | :------------ | :------------------------------------------------------------------- |
| Open Tab      | :tabedit file | opens a new tab and will take you to edit "file"                     |
| Move Next Tab | gt            | move to the next tab                                                 |
| Move Prev Tab | gT            | move to the previous tab                                             |
| Move to Tab   | #gt           | move to a specific tab number (e.g. 2gt takes you to the second tab) |
| Open Tabs     | :tabs         | list all open tabs                                                   |
| Close Tab     | :tabclose     | close a single tab                                                   |

## Install

```powershell
# VIM
choco install vim

# NeoVim
choco install neovim
```

## Configuration

- Configurations
    - Local: _~/.vimrc_

!!! Example "Local VIM Config"
    ```bash
    set autoindent expandtab tabstop=2 shiftwidth=2 
    augroup filetype_yaml
    autocmd FileType yaml setlocal  tabsop=2 shiftwidth=2
    augroup END

    set number
    set title
    ```

## References

- [Choco Vim](https://community.chocolatey.org/packages/vim)
- [Choco NewVim](https://community.chocolatey.org/packages/neovim)
- [How to Show Line Numbers in VIM](https://linuxize.com/post/how-to-show-line-numbers-in-vim/)
- [VIM Commands](https://www.keycdn.com/blog/vim-commands)
