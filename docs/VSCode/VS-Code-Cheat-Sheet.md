# VS Code Cheat Sheet

## Keyboard Shortcuts

|Purpose|Win Keys|Mac Keys|Notes|
|:---|:---|:---|:---|
|Select Columns|Shift+Alt|Shift+Option|Use click and drag mouse to select|
||Ctrl+Shift+Alt|Ctrl+Shift+Option|Use arrow keys to select|

## Using Git Repository

- [Git Editor](./Git-Cheat-Sheet#git-editor) to configure the git editor for VS Code
- [Generate SSH Keys](./Powershell/Powershell-Cheat-Sheet#ssh-keys) to create SSH keys on local system
- [Git Repository SSH Keys](./Git-Cheat-Sheet#ssh-keys) to set remote repository SSH key

## C-Sharp

Use `dotnet` commands to build solutions and projects. See [.NET CLI overview](https://docs.microsoft.com/en-us/dotnet/core/tools/) for more details.

### Add Solution

```powershell
mkdir MySlnFolder
cd MySlnFolder
dotnet new sln

# OR

dotnet new sln -o MySlnFolder
```

### Add Assembly

```powershell
# If does not exist create src folder for solution
mkdir src
cd src
dotnet new classlib -o MyClassLib
```

### Add Assembly to Solution

```powershell
# from folder with sln file
dotnet sln add ./src/MyClassLib/MyClassLib.csproj
```

### Add xUnit Tests

#### Add Test Assembly Manually

##### Add Test Project

```powershell
# from solution's src folder
mkdir MyTests
cd MyTests
dotnet new classlib

# Add Packages
dotnet add package Microsoft.NET.Test.Sdk
dotnet add package xunit
dotnet add package xunit.runner.visualstudio
dotnet add package shouldly
```

#### Add Test Assembly with xunit Command

```powershell
# from solution's src folder
dotnet new xunit -o MyXunitTests
cd MyXunitTests
dotnet add package shouldly
```

## Setting Up Debugging

To initialize, open a project and select _Run_->_Add Configuration_ and select the **.Net Core** option. This will generate a _.vscode_ folder at the solution level with two files, _launch.json_ and _tasks.json_

### Add Launch File

### Add Build File

## Extensions

## Drawio

#### Enable Plugins

> This will need to be done using the desktop version

- Configure to start with plugins enabled
    - Create shortcut to the app
    - Open properties on the shortcut
    - Add `--enable-plugins` to the command
  - Open Drawio using the shortcut
      - Navigate to `Extras --> Plugins`
      - Click on the `Add` button to install plugin

See [External plugins disabled on diagrams.net 19.0.3 about drawio-desktop](https://giter.vip/jgraph/drawio-desktop/issues/1049)

## References

- [External plugins disabled on diagrams.net 19.0.3 about drawio-desktop](https://giter.vip/jgraph/drawio-desktop/issues/1049)
- [Generate SSH Keys](./Powershell/Powershell-Cheat-Sheet#ssh-keys) to create SSH keys on local system
- [Git Editor](./Git-Cheat-Sheet#git-editor) to configure the git editor for VS Code
- [Git Repository SSH Keys](./Git-Cheat-Sheet#ssh-keys) to set remote repository SSH key
- [Vale](https://vale.sh/)
- [Vale + VS Code](https://github.com/chrischinchilla/vale-vscode?tab=readme-ov-file#vale--vs-code)
