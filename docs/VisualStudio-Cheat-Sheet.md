<!-- omit in toc -->
# Visual Studio Cheat Sheet

- [My Extensions Used](#my-extensions-used)
- [My Options](#my-options)
- [Source Link](#source-link)
- [.NET Core Project Templates](#net-core-project-templates)
  - [References](#references)
  - [Folder Structure](#folder-structure)
  - [Process Overview](#process-overview)
  - [Template Definition](#template-definition)
    - [Parameters](#parameters)
  - [Nuspec File](#nuspec-file)
  - [Commands to Know](#commands-to-know)

## My Extensions Used

## My Options

## Source Link

[Configure Source Link](https://devblogs.microsoft.com/dotnet/improving-debug-time-productivity-with-source-link/)

## .NET Core Project Templates

### References

Articles:

- [Tutorial: Create a project template](https://docs.microsoft.com/en-us/dotnet/core/tutorials/cli-templates-create-project-template)

Videos:

- [Create a .NET Core Template](https://channel9.msdn.com/Shows/Visual-Studio-Toolbox/Create-a-NET-Core-Project-Template?term=.net%20core%20template&lang-en=true)
- [Add a Parameter to .NET Core Project Template](https://channel9.msdn.com/Shows/Visual-Studio-Toolbox/Add-a-Parameter-to-a-NET-Core-Project-Template)
- [Troubleshooting .NET Core Project Templates](https://channel9.msdn.com/Shows/Visual-Studio-Toolbox/Troubleshooting-NET-Core-Project-Templates)

Examples:

- [Sayed Hashimi](https://github.com/sayedihashimi/template-sample)

### Folder Structure

- Solution : base folder containing the _.sln_ and _.nuspec_ files (named working in article)
  - Templates : contents of the project template (named content in video)
    - Project : Folder for the project the template will be based upon
      - .template.config : Folder holding the template definition file _template.json_

### Process Overview

1. Create the working solution folder
2. In solution folder, add a _templates_ folder
3. In the _templates_ folder, add base projects to be templated
4. In each project folder, add the folder _.template.config_
5. In _.template.config_ folder, add the _template.json_ file
6. In the solution folder, add a solution file
7. Add the template project files to the solution
8. Add a NuGet nuspec file in the solution folder
9. Ensure all project build
10. Execute _nuget pack_ command in the solution folder

### Template Definition

To view the schema [template.json](http://json.schemastore.org/template)

```json
{
  "$schema": "http://json.schemastore.org/template",
  "author": "jhedges",
  "classifications": ["Console"],
  "name": "jhedges.console.demo.01",
  "shortName": "jhedgesconsoledemo",
  "defaultName": "MyConsole",
  "identity": "jhedges.demo.CSharp",
  "tags": {
    "language": "C#",
    "type": "project"
  },
  "sourceName":  "MyConsole01",
  "symbols": {
      "Framework": {
        "type": "parameter",
        "description": "The target framework for the project.",
        "datatype": "choice",
        "choices": [
          {
          "choice": "netcoreapp3.1",
          "description": "Target netcoreapp3.1"
          }
        ],
        "replaces": "netcoreapp3.1",
        "defaultValue": "netcoreapp3.1"
      },
      "AuthorName": {
      "type": "parameter",
      "defaultValue": "(insert author name)",
      "replaces": "AuthorName",
      "datatype": "text",
      "description": "Author name"
    },
    "Description": {
      "type": "parameter",
      "description": "Description text that will be shown for this nuget package on nuget.org",
      "defaultValue": "(insert description)",
      "replaces": "DescriptionContent"
    }
  }
}
```

| Property        | Required | Description                                                                                                  | Values                               |
| :-------------- | :------: | :----------------------------------------------------------------------------------------------------------- | :----------------------------------- |
| $schema         |    No    | pulls in intellisense                                                                                        | http://json.schemastore.org/template |
| author          |   Yes    | The name of the person or organization publishing the template                                               | string                               |
| classifications |   Yes    | Zero or more characteristics of the template that a user might search for it by                              | array of string                      |
| name            |   Yes    | The name for the template that users should see                                                              | string                               |
| identity        |   Yes    | A unique name for this template                                                                              | string                               |
| shortname       |   Yes    | A shorthand name for selecting the template                                                                  | string                               |
| tags            |   Yes    | Common information about templates, these are effectively interchangeable with choice type parameter symbols | object                               |
| tags.language   |   Yes    | The programming language the template primarily contains or is intended for use with                         | string                               |
| tags.type       |   Yes    | The type of template: project or item                                                                        | [ "project", "item" ]                |
| sourceName      |   Yes    | The name in the source tree to replace with the name the user specifies                                      | string                               |
| defaultName     |    No    | The name to use during creation                                                                              | string                               |
| description     |    No    | The name for the template that users should see                                                              | string                               |
| symbols         |    No    | Variables used in template                                                                                   | object                               |

#### Parameters

The symbols section defines variables and their values, the values may be the defined in terms of other symbols. When a defined symbol name is encountered anywhere in the template definition, it is replaced by the value defined in this configuration. The symbols configuration is a collection of key-value pairs. The keys are the symbol names, and the value contains key-value-pair configuration information on how to assign the symbol a value.

| Variable                  | Recommended | Description                                    | Value                        |
| :------------------------ | :---------: | :--------------------------------------------- | :--------------------------- |
| symbols                   |             | Variables used in template                     | object                       |
| symbols.Framework         |      X      | All values to concatenate                      |                              |
| symbols.Framework.choices |      X      | Frameworks available. Should have at least one | use official framework names |

### Nuspec File

```xml
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2012/06/nuspec.xsd">
  <metadata>
    <id>jhedges.console.01</id>
    <version>0.0.1</version>
    <description>
      Demo project template for creating a .NET Core console.
    </description>
    <authors>JHedges</authors>
    <license type="expression">Apache-2.0</license>
    <packageTypes>
      <packageType name="Template" />
    </packageTypes>
  </metadata>
  <files>
    <file src="templates\**\*.*" exclude="templates\**\bin\**\*.*;templates\**\obj\**\*.*" target="templates" />
  </files>  
</package>
```

### Commands to Know

_dotnet_ commands used when developing templates

| Command                  | Description                        |
| :----------------------- | :--------------------------------- |
| new -l                   | List all installed templates       |
| new -i path-to-nuget-pkg | Install a template templates       |
| new -u path-to-nuget-pkg | Uninstall a template templates     |
| new template-short-name  | Create a project based on template |
