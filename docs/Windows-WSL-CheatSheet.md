<!-- omit in toc -->
# Windows WSL Cheat Sheet

- [wsl Commands](#wsl-commands)
  - [List installed Linux Distros](#list-installed-linux-distros)
  - [Accessing a WSL Distro](#accessing-a-wsl-distro)
  - [Leave wsl Session](#leave-wsl-session)
- [WSL Setup Using PowerShell](#wsl-setup-using-powershell)
  - [Enable WSL](#enable-wsl)
  - [Install Linux Distro](#install-linux-distro)
- [Network Cannot Access Internet](#network-cannot-access-internet)
- [Mounted SSH Key File Permissions](#mounted-ssh-key-file-permissions)

## wsl Commands

For more detail, see [Managing WSL](https://docs.microsoft.com/en-us/windows/wsl/wsl-config) or [Windows Subsystem for Linux Installation Guide for Windows 10](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

### List installed Linux Distros

From a PowerShell command prompt, enter `wsl -l`

### Accessing a WSL Distro

You can access your Linux distro using the following methods:

- From the Windows Start menu
  - Find and select your distro by name.
  - The session will be open in a new console window.
- From PowerShell prompt,
  - Enter `wsl` or `bash` for default distro.
  - If multiple distros are installed, use `wsl <distro name>` for a specific Linux distro.
  - The session will be open in the current PowerShell console.

### Leave wsl Session

To leave a wsl session, enter `exit` at the pormpt

## WSL Setup Using PowerShell

>See this article, [WSL Install](https://www.windowscentral.com/install-windows-subsystem-linux-windows-10#install_linux_subsystem_powershell_windows10), for more details and options

### Enable WSL

> This will require a restart

1. Open PowerShell console as administrator
1. Enter `Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux`
1. When prompted to restart, type `T` and press enter

### Install Linux Distro

You will need to download and install the Linux Distro of choice. See the [Distros](https://docs.microsoft.com/en-us/windows/wsl/install-manual#downloading-distros) article for a list of distros.

1. Start PowerShell as administrator
1. Change to your download directory
1. Create a folder for your Linux distro and navigate to that folder
1. Download your distro by executing `Invoke-WebRequest -Uri https://<your distro file name>.appx -OutFile Ubuntu.appx -UseBasicParsing`
1. Install distro `Add-AppxPackage .\<your distro file name>.appx` e.g. `Add-AppxPackage .\Ubuntu.appx`
1. Initialize your install
    1. Open Window's start
    1. Search for your distro name and click the result
    1. A console is launched with the message _Installing, this may take a few minutes..._
    1. When prompted, enter your new UNIX user name
    1. When prompted, enter your new UNIX password
    1. When prompted, confirm your new UNIX password

## Network Cannot Access Internet

Open windows cmd in admin mode and type these commands

1. netsh winsock reset
1. netsh int ip reset all
1. netsh winhttp reset proxy
1. ipconfig /flushdns
1. reboot

From Stack Overflow [No internet connection on WSL Ubuntu (Windows Subsystem for Linux)](https://stackoverflow.com/questions/62314789/no-internet-connection-on-wsl-ubuntu-windows-subsystem-for-linux)

Tried this (did not work):

```powershell
Get-NetIPInterface -InterfaceAlias "vEthernet (WSL)" | Set-NetIPInterface -InterfaceMetric 1
$VPNInterfaceName = "Ethernet 3"
Get-NetIPInterface -InterfaceAlias $VPNInterfaceName | Set-NetIPInterface -InterfaceMetric 5001
```

From Stack Overflow [unable to access network from WSL2](https://stackoverflow.com/questions/57633406/unable-to-access-network-from-wsl2)

See powershell script c:\src\wsl\FixWslAdapter.ps1

## Mounted SSH Key File Permissions

[Volume Mounting SSH Keys into a Docker Container](https://nickjanetakis.com/blog/docker-tip-56-volume-mounting-ssh-keys-into-a-docker-container)
