<!-- omit in toc -->
# [Windows Terminal](https://docs.microsoft.com/en-us/windows/terminal/) Cheat Sheet

- [Manual Install](#manual-install)
- [Configure Terminal](#configure-terminal)
  - [Visual Studio Terminal](#visual-studio-terminal)
    - [Visual Studio Instance ID](#visual-studio-instance-id)
  - [Windows PowerShell](#windows-powershell)
  - [Add the Visual Studio 2019 Terminal](#add-the-visual-studio-2019-terminal)
  - [Add the Visual Studio 2022 Terminal](#add-the-visual-studio-2022-terminal)
  - [Python](#python)
- [Invoke RDP](#invoke-rdp)
- [Keyboard Short-Cuts](#keyboard-short-cuts)
- [References](#references)

## Manual Install

- Download msix bundle from [Windows Terminal Releases](https://github.com/microsoft/terminal/releases)
- Open the msix with archive app (7-Zip)  
![Archive](./Archive.png)
- Copy the version bundle msix you wish to install (CascadiaPackage_M.m.b_x64.msix)
- Extract files from the version bundle
- Extract files for the version to install into a folder
- Copy the files fom the version folder to C:/Programs Files x86/Windows Terminal

## Configure Terminal

### Visual Studio Terminal

The Visual Studio Terminal includes all of the environment settings. Get the Visual Studio instance ID and add the terminal.

#### Visual Studio Instance ID

To get the Visual Studio instance ID, run this command:

```powershell
C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere -property instanceId

# if have more than one version, execute this to see full version info
C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere
```

> NOTE: The strategy for opening Visual Studio developer shells has changed. Prioer to this version of the notes, the shell was opened using the dev shell directly. That approach has given way to using start-up scripts. The same shell commands are being called from PowerShell start-up scripts.

### Windows PowerShell

- General
  - Name: `Windows PowerShell`
  - Command Line: `PowerShell.exe -noe -File "%USERPROFILE%/Documents/WindowsPowerShell/posh.ps1"`
  - Starting Directory: %USERPROFILE%
  - Use parent process directory: Unchecked
  - Icon: `C:\Users\jhedges\Downloads\Images\OIP.jpg`
  - Tab title: `Admin: PS`
  - Hide profile from dropdown: Off
- Appearance
  - Text
    - Color Scheme: Campbell Powershell
    - FontFace: MesloLGS Nerd Font
    - Font size: 12
    - Font weight: Normal
    - Retro terminal effects: Off
  - Cursor
    - Cursor shape: Vintage
    - Cursor height: 25
  - Background Image
    - Background image path: (none)
    - Use desktop wallpaper: Unchecked
  - Test Formatting
    - Bold font with bright colors
  - Acrylic
    - Enable acrylic: Off
  - Window
    - Padding: 8
    - Scrollbar visibility: Visible
- Advanced
  - Suppress title changes: On
  - Text antialiasing: Grayscale
  - AltGr aliasing: On
  - Scroll to input when typing: On
  - History size: 9001
  - Profile termination behavior: Close only when process exits successfully
  - Bell Notification Style: Audible

### Add the Visual Studio 2019 Terminal

Add a new profile with the following settings:

- General
  - Name: `VS2019`
  - Command Line: `PowerShell.exe -noe -File "%USERPROFILE%\Documents\WindowsPowerShell/VS2019.ps1"`
  - Starting Directory: (leave blank to use Visual Studio's default directory)
  - Use parent process directory: Checked
  - Icon: `C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\Common7\IDE\Assets\VisualStudio.70x70.contrast-standard_scale-80.png`
  - Tab title: `Admin: VS`
  - Hide profile from dropdown: Off
- Appearance
  - Text
    - Color Scheme: One Half Light
    - FontFace: MesloLGS Nerd Font
    - Font size: 12
    - Font weight: Normal
    - Retro terminal effects: Off
  - Cursor
    - Cursor shape: Vintage
    - Cursor height: 25
  - Background Image
    - Background image path: (none)
    - Use desktop wallpaper: Unchecked
  - Test Formatting
    - Bold Font
  - Acrylic
    - Enable acrylic: Off
  - Window
    - Padding: 8
    - Scrollbar visibility: Visible
- Advanced
  - Suppress title changes: On
  - Text antialiasing: Grayscale
  - AltGr aliasing: On
  - Scroll to input when typing: On
  - History size: 9001
  - Profile termination behavior: Close only when process exits successfully
  - Bell Notification Style: Audible

### Add the Visual Studio 2022 Terminal

Add a new profile with the following settings:

- General
  - Name: `VS2022`
  - Command Line: `PowerShell.exe -noe -File "%USERPROFILE%\Documents\WindowsPowerShell/VS2022.ps1"`
  - Starting Directory: (leave blank to use Visual Studio's default directory)
  - Use parent process directory: Checked
  - Icon: `C:\Program Files\Microsoft Visual Studio\2022\Enterprise\Common7\IDE\Assets\VisualStudio.70x70.contrast-standard_scale-80.png`
  - Tab title: `Admin: VS 2022`
  - Hide profile from dropdown: Off
- Appearance
  - Text
    - Color Scheme: Solarized Light
    - FontFace: MesloLGS Nerd Font
    - Font size: 12
    - Font weight: Normal
    - Retro terminal effects: Off
  - Cursor
    - Cursor shape: Vintage
    - Cursor height: 25
  - Background Image
    - Background image path: (none)
    - Use desktop wallpaper: Unchecked
  - Test Formatting
    - Bold Font
  - Acrylic
    - Enable acrylic: Off
  - Window
    - Padding: 8
    - Scrollbar visibility: Visible
- Advanced
  - Suppress title changes: On
  - Text antialiasing: Grayscale
  - AltGr aliasing: On
  - Scroll to input when typing: On
  - History size: 9001
  - Profile termination behavior: Close only when process exits successfully
  - Bell Notification Style: Audible

### Python

- General
  - Name: `Python`
  - Command Line: `PowerShell.exe -noe -File "%USERPROFILE%/Documents/WindowsPowerShell/py.ps1"`
  - Starting Directory: 
  - Use parent process directory: Checked
  - Icon: `C:\Python39\DLLs\py.ico`
  - Tab title: `Admin: Python`
  - Hide profile from dropdown: Off
- Appearance
  - Text
    - Color Scheme: Frost
    - FontFace: MesloLGS Nerd Font
    - Font size: 12
    - Font weight: Normal
    - Retro terminal effects: Off
  - Cursor
    - Cursor shape: Vintage
    - Cursor height: 25
  - Background Image
    - Background image path: (none)
    - Use desktop wallpaper: Unchecked
  - Test Formatting
    - Bold font with bright colors
  - Acrylic
    - Enable acrylic: Off
  - Window
    - Padding: 8
    - Scrollbar visibility: Visible
- Advanced
  - Suppress title changes: On
  - Text antialiasing: Grayscale
  - AltGr aliasing: On
  - Scroll to input when typing: On
  - History size: 9001
  - Profile termination behavior: Close only when process exits successfully
  - Bell Notification Style: Audible

## Invoke RDP

User [mstsc](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/mstsc) to start an RDP session

```powershell
# open using RDP file
mstsc myremote.rdp
# open by IP
mstsc /v:10.220.89.194
# open by IP with admin (use when RDP licenses are not available)
mstsc /v:10.220.89.194 /admin
```

## Keyboard Short-Cuts

>Can now navigate to Settings-->Actions to see all keyboard mappings

| Command             | Short-Cut       |
| :------------------ | :-------------- |
| Command Palette     | Ctrl+Shift+P    |
| New Vertical Pane   | Alt+Shift+=     |
| New Horizontal Pane | Alt+Shift+-     |
| Switch Panes        | Alt+arrow       |
| Resize Panes        | Alt+Shift+arrow |
| Close Pane          | Ctrl+Shift+W    |

## References

- [What is Windows Terminal?](https://docs.microsoft.com/en-us/windows/terminal/)
- [Windows Terminal 4 Heros](https://www.eshlomo.us/windows-terminal-4-heros/)
- [The Windows Terminal made better with the Command Palette plus Multiple Actions in one Command](https://www.hanselman.com/blog/the-windows-terminal-made-better-with-the-command-palette-plus-multiple-actions-in-one-command?utm_source=feedly&utm_medium=webfeeds)
- [How to make Command Prompt, PowerShell, or any Shell launch from the Start Menu directly into Windows Terminal](https://www.hanselman.com/blog/how-to-make-command-prompt-powershell-or-any-shell-launch-from-the-start-menu-directly-into-windows-terminal?utm_source=feedly&utm_medium=webfeeds)