<!-- omit in toc -->
# How to Make Smaller User Stories

## Splitting User Stories

> Why do we want smaller user stories
>
> - Smaller, more frequent commits
> - Get the dialog going
> - Easier to reason and test
> - Creates momentum

### Good User Stories

A user stories describes a change in system behavior for the perspective of a user. It lives in the solution space and is a description of an actor wanting to accomplish a task. It is a Job to be Done (JTBD). It can be great for translating customer empathy into a series of changes to a software system, while maintaining the actor's perspective.

#### Format

Generally written in the role - action / feature - value / goal format. Most importantly, it answers the following question.

- Who is it for?
- What do they want to do that is not possible today?
- Why do they want this?

#### INVEST

- Independent - Self contained, not relying on other stories.
- Negotiable - Leaves room for collaboration, where greater detail is captured.
- Valuable - Adds direct incremental value. Each story may not be worth shipping and may need to accumulate stories to reach MVP.
- Estimable - Defined well enough that the team can estimate how big it is, relative to other stories in the backlog.
- Small - Small enough that 6-10 stories will fit into a sprint. This encourages frequent feedback.
- Testable - Answers the question "when is the story done?" Cannot be vague and should be a concrete change in system behavior. A story should have only one test criterion.

There may be tension between these attributes, especially as stories get smaller. Different attributes matter at different times. It is more important that the stories be small, estimable, and testable. As we look ingot the future, the opposite is true. Then we focus more on independence, negotiability, and value.

#### Vertical Slices

A good story delivers value. Value can only delivered if the story cuts vertically through the architectural layers. When teams work on vertical slices, they

- Make value explicit in their backlog
- Have more conversations about value
- Tend not to accidentally build low-value changes
- Get value sooner
- Get earlier, high-quality feedback
- Can see constraints and inventory more easily and response accordingly
- Become more predictable in delivering value

### Steps to Splitting Stories

[![How to Split A UserStory](./HW-Story-Splitting-Flowchart-Thumbnail.png)](./Agile/HW-Story-Splitting-Flowchart.pdf)

#### Step 1: Getting the input story (or feature) ready

- Does the story/feature satisfy INVEST?
  - Most often, Valuable is the issue.
  - "Unsplittable" stories may be tasks masquerading as stories
- Is it too big? Then split it

#### Step 2: Applying the patterns

##### Pattern #1: Workflow Steps

When a story has a workflow, can the steps be broken down into stories? A story can be split where a step, or set of steps, can provide value. Can then stand alone?

If the steps do not stand alone, can consider hard coding more complex steps and then splitting out the more complex steps. Allows for greater exploration of the complex step.

Note that simply splitting one step at a time from beginning to end may not be the right way to go.

##### Pattern #2: Operations (e.g. CRUD)

A word like "manage" is often a queue that a story covers multiple operations. Split the story along the operations seams.

##### Pattern #3: Business Rule Variations

Look for equally complex stories hidden within a story. These will accomplish the same thing using different business rules. Split the story along the business rules seams.

##### Pattern #4: Variations in Data

When a story offers variation based on data and their groupings, split along these seams. Tackle the simplest first as it may shed light on the following stories. The data variations may not always be apparent at first and will not be discovered until the team starts developing the story. Also, look at the acceptance criteria for these variations too.

##### Pattern #5: Data Entry Methods

For a complex UI, create story with the simplest possible and the enhanced UI. These stories will not be independent. Consider splits on field functionality groupings or components.

##### Pattern #6: Major Efforts

When a story has related workflows that share infrastructure, split into a story for each workflow. The first story will take longe since in includes the infrastructure. The stories will not be independent, but those dependencies are clear.

##### Pattern #7: Simple/Complex

When a story is made complex by its variations, split them on each variation. This is about finding the core of the story and tackling it first.

##### Pattern #8: Defer Performance

When a story has performance, or any other non-functional, requirements, split out the "make it work" and the "make it fast" stories. This split aligns with the avoid early optimizations trap. The "make it work" may be fast enough.

##### Pattern #9: Break Out a Spike

When a story is not well understood or has a high level of uncertainty, do a time-boxed spike first. Note that the uncertainty is not constrained to technological issues. It could be around other issues, like process or business rules. The acceptance criteria should be a set of questions to be answered. Be sure to make a stopping point and not get carried away with doing research.

Spike stories should be a last resort and only done when you don't know enough to build something. Try applying the other patterns first to find a way to create value while doing a less complex story.

##### Meta-Pattern: Find the Complexity & Reduce the Variations

The patterns for splitting stories reveals a meta-pattern: focus on the complexity and reduce the variations.

1. **Find the core complexity.** What's the part that's most likely to surprise you or have something emerge?
2. **Identify the variations.** What are there many of?
3. **Reduce all the variations to one.** Find a single, complete slice through the complex part or one of the variations.

#### Step 3: Evaluating the Split

When a story has more than one pattern to split by, how do you choose between them? Use these two rules of thumb:

1. **Choose the split that lets you de-prioritize or throw away a story.** When a split reveals low-value functionality, you may be able to defer or limit that split story.
2. **Choose the split that gets you more equally sized small stories.** An equal split will give the Product Owner more freedom to prioritize part of the functionality separately.

### Cynefin and Story Splitting

![Cynefin](Cynefin.png)

> Cynefin is  a Welsh word for _habitat_ and pronounced kuh-NEV-in.

Cynefin is a decision strategy based on a problems complexity. It divides the decision-making into contexts called domains. They are obvious, complicated, complex, chaotic, and disordered. Story splitting looks different for each domain.

- **Obvious** Just build it. If is is too big, find all the stories, and do the most valuable ones first.
- **Complicated** Find all the stories, and do the most valuable and/or most risky ones first.
- **Complex** Don't try to find all the stories. Find one or two that will provide some value and teach you something about the problem and solution. Build those and use what you learn to find the remaining stories.
- **Chaotic** Put out the fire; splitting stories probably isn't important right now.
- **Disordered** Figure out which domain you're in before splitting to avoid taking the wrong approach.

When using Cynefin for story splitting, the complex domain leads to a set of stories that don't feel complete. Using this approach is about discovering the complexity. Trying to tackle stories in this domain without allowing for this investigation may give the illusion of predictability, but the actual stories are likely to change as you get int the work. It is better to be transparent about the uncertainty inherent in complex work.

### Getting Good at Story Splitting

Working in thin vertical slices is a vital behavior in Agile development. Applying these story splitting techniques is critical in finding them. That skill is quite learnable through practice. Try splitting stories that you are familiar with, likely to have already completed.  Once comfortable with applying the story splitting, move on to your backlog. After time, it will become automatic and much simpler.

## Estimating Stories

### Size

### Complexity

### Familiarity

## Recommendations

### Sprint Demo

- Sprint demo is for each team and the product owners have stories in that sprint. This is a time for dialog!
- Release demo is for all teams and involves a wider audience. This is more show & tell

## Presentation

> recommend we do a brown bag / series of brown bags and blog posts

- Differentiate Epic/Feature/Story/Spike
- What is a good user story?
  - INVEST
- Splitting User Stories
  - Patterns
  - Case Study
- Story Mapping? (this may be more of a product owner activity)
- Estimating (may be an additional presentation)
  - What's a unit?
  - NO dev talk
  - Size & Complexity
  - Familiarity

## References

- [The Humanizing Work Guide to Splitting User Stories](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/)
- [What You Need to Know About Cynefin](https://www.humanizingwork.com/what-you-need-to-know-about-cynefin/)
- [Cynefin 101 – An Introduction](https://www.infoq.com/articles/cynefin-introduction/)
- [Cynefin framework](https://en.wikipedia.org/wiki/Cynefin_framework)
