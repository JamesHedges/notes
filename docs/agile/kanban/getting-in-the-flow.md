# Getting in the Flow

## What the Process

- **Head** (who, what, when, where, why, and how)
  - What has evolved with Scrum and modern software engineering practices?
  - Why have they diverged?
  - What issues have surfaced?
  - How is the affecting development teams?
  - What is the alternative?
  - When does the current process fall short?
  - Isn't Scrum agile, what has happened to Scrum?
  - Are we Scrum teams or engineering teams?
- **Heart**
  - While it is technically agile, feels like we are trending back to an iterative waterfall pattern that has the disadvantages of:
    - Inflexibility
    - Lack of customer involvement
    - Risk of failure
    - Poor communication - feedback loops are too slow
    - High costs
- **You**
  - Scrum practices are no longer consistent with modern software engineering
    - CI/CD
    - Experimental and Iterative
    - Incremental

**Problem Statement:**

The Scrum methodology as it is practiced today is not melding well with current software engineering practices.

- **Observe**
  - Scrum practices are losing their value and becoming iterative waterfall (thank you SAFe)
  - Modern software engineering struggles with communications and delivery in a Scrum environment
  - The agile practice of ongoing conversation has faded
- **Orient**
  - Align with modern software engineering practices
  - Engage in conversations when most relevant
- **Decide**
  - Adopt a development process that emphasizes delivery and is compatible with modern software engineering practices
- **Act**
  - Scrum, as we practice it, has devolved into iterative waterfall
  - Current process, scrum, delivers each sprint, which is not consistent with CI / CD
  - There are too many ceremonies that require many attendees. Often, they don't attend
  - Big emphasis on story points and estimating, waste too much time on it
  - Story points are too misunderstood and do not translate well to value delivered
  - Teams are spending too much time up front in refinement as this should only be that start of a conversation
  - We identify as software engineering teams, not Scrum teams, and prefer to follow modern practices
  - Engineering must insulate themselves from the lure of the plan

>Modern Software Engineering Practices:
>
> - Experimental
> - Data driven
>   - Measure effectiveness
>   - Empirical
> - Incremental
> - Work iteratively
> - Manage complexity
>   - Modularity
>   - Cohesion
>   - Separation of concerns
>   - Information hiding / abstractions
>   - Coupling

## What is Kanban

- **Head** (who, what, when, where, why, and how)
  - What does the engineering team value
  - Who are we responsible to?
  - What we do, solve problems
  - How can engineering work more effectively
  - Where does engineering's responsibilities start and end?
- **Heart**
  - Accelerate delivery
  - Work within modern software engineering practices
- **You**
  - Should align with Dora metrics

**Problem Statement:**
The engineering team needs to identify a development process that best aligns with its practices.

- **Observe**
  - Engineering must always be delivering value
  - Estimates are generally poor and priorities shift
  - Value does not always flow and gets stuck in the process
- **Orient**
  - Delivering value is our yardstick
  - We need flexibility in how work is put in progress
  - The value stream must be visible
- **Decide**
  - Adopt the Kanban approach
- **Act**
  - The development process needs to align with how we think and work
  - The development process needs to align with how we measure
  - Need to keep value flowing while avoiding activities that provide little or no enhancements
  - Define Kanban
  - The principles of Kanban align better with engineering's practices
  - Kanban evolved from lean and is less reliant on ceremonies, preferring collaboration through faster feedback loops

## Kanban Flow

- **Head** (who, what, when, where, why, and how)
  - How do we visualize the flow
  - How is the Kanban board organized
  - How do stories originate and become available
  - When to transition state
  - What regulates the amount of work in progress
  - Why do we mitigate constraints
- **Heart**
  - We don't want to overcome the flow and cause work delays
- **You**
  - Ultimate goal is delivery of value

**Problem Statement:**

- **Observe**
  - We need a way to visualize and map the flow of value
  - Without mechanisms to regulate the flow, work can get delayed or lost (waste)
  - 
- **Orient**
  - We need a process the allows the team to maximize their value creation
- **Decide**
  - Apply Kanban flow practices
- **Act**
  - Use a Kanban board to visualize the flow of value
  - WIP limits the amount of work in progress
  - Don't pass defects on: definition of done and make the rules explicit
  - Use the strategy of working in small increments
  - Place work ready for the next state into queues
  - We will not be spending too much time on estimates as they tend to be inaccurate and of low value.
  - Flow states represent a value-add phase
  - Queues are a bin for stories ready for the next value-add state.
  - The definition of done for each flow state is set by the team
  - The beauty of Kanban is its clarity

>Kanban Flow States
>
> - Place story ready for refinement in the ready queue
> - Refine the story
>   - Don't much if any time on estimates
>   - Make sure it satisfies the definition of done
> - Moves to Defined queue where it may be prioritized
> - Stories are then pulled into the in-progress states
>   - In development (definition of done)
>   - Ready for test (queue)
>   - In testing (definition of done)
> - Completed
>   - Ready for UAT (queue)
>   - In UAT
> - Accepted
>   - Ready for release (queue)
>   - In deployment (or just release, depending on the use of feature flags)
>   - Accepted (queue) at this point, final done

## Kanban Metrics

- **Head** (who, what, when, where, why, and how)
- **Heart**
- **You**

**Problem Statement:**

- **Observe**
- **Orient**
- **Decide**
- **Act**

>Kanban Metrics
>
> - Four Main Metrics
>   - Throughput
>   - Cycle-time
>   - Aging
>   - Work in Progress - tracks work in progress for a time box
> - Flow Metrics
>   - Flow Velocity
>   - Flow Time
>   - Flow Efficiency
>   - Flow Load
>   - Flow Distribution
>   - Flow Predictability (Rally)
>   - Cumulative Flow Diagram (Rally)