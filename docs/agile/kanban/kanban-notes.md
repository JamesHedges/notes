# Kanban
  
## What is Kanban?

- Agile/Lean project management methodology
- Kanban is Japanese for "visual sign"
- Emphasizes deliver and the elimination of waste

### Five Core Properties

Original 5 core properties from David Anderson

- Visualize the workflow
- Limit Work in Progress (WIP)
- Manage Flow
- Make process policies explicit
- Improve collaboratively

### Principles

- Start with what you do now  
Start with the existing process and review the workflow, roles, responsibilities, titles, communications, and more. Focus on improving them.
- Agree to pursue incremental, evolutionary change  
Make changes to the processes in small, steady increments to avoid disruptions. Significant changes may be met with resistance.
- Respect the current process, roles, responsibilities, titles  
Reinforce that Kanban will not be disruptive or completely change established operating models.
- Encourage acts of leadership at all levels in your organization  
Empower team members to own issues, take the lead, take action backed by solid data, and act transparently.

### Practices

- Visualize (the work, the workflow, and the business risks)  
Visibility is achieved using a Kanban board (similar to a dashboard) where work items are displayed in process (flow state). Helps to understand the process and where adjustments may be required.
- Limit Work in Progress  
To be more efficient and cut-out wasted time and resources, work in progress is limited until available capacity to 'pull' it in. WIP limits should be displayed on the Kanban board.
- Manage Flow  
The Kanban board allows the team to observer work moving through the process. Allows them to identify potential bottlenecks and smooth interruptions. E.g. Could highlight a flow disruption and where additional resources may be needed.
- Make policies explicit  
As a team, define, publish, and share processes and policies. Keeps everyone moving in the same direction and help keep discussions more dispassionate and objective.
- Implement feedback loops  
To facilitate small, continuous, incremental, and evolutionary change, collect feedback. Feedback may be found during meetings, at delivery, operations & risk reviews, or any other project related activities. Do what works best for the team and relevant to the nature of the project.
- Improve collaboratively, evolve experimentally  
Adopt a scientific model of experimentation, analysis, and improvement. Do this in a collaborative way and share the knowledge gained. Suggests considering the use of:
  - The Theory of Constraints
  - The Theory of Profound Knowledge
  - The Lean Economic Model

### Concepts

- System Thinking
- Learning
- Flow
- Pull  
Contrasts with Scrum, no strict time-boxes, pulls work in when other work completed.
- Slack
- Swarm

## Flow State

- Value Stream - map the development process
- Queues
  - In Rally, all will map to
    - Refining
    - Defined
    - In-Progress
    - Complete
- WIP limit
- Exit Agreement
- Work rules
- Age

## Stories

- Create a backlog
- Estimates are not necessary
  - Can use story points or t-shirt sizes
- Groom/Refine stories before putting in the ready/defined queue

## Metrics

- Four Main Metrics
  - Throughput:  
Number of work items completed for a time box. Indicates how much work the team is delivering.
  - Cycle-time:  
Average time over a given time box it takes for work items to flow from the Start State to the End State. Measures how long, on average, for a team to deliver value. Typically, start time is a when story is set to in-progress. End state is when it is set in the accepted state. However, cycle-time may be computed for stage or sequence of stages.
  - Lead Time:  
Time to complete a work item starting when it is placed on the board until it is completed.
  - Aging:  
The amount of time an item is in the in-progress since the team started working on it. Helps team identify a bottleneck that may require mitigation.
  - Work in Progress:  
Tracks the number of work in progress (started but not complete) at a given time. Should be kept low over a given time box so that a team executes effectively. Should not be confused with WIP limits.
- Flow Metrics
  - Flow Velocity (Throughput):  
Measures how many items are being delivered by the team. Helps see if the team delivers value quickly enough.
  - Flow Time (Cycle-Time):  
Measure how long it take for each individual item to be completed from start to finish. Helps predict delivery speed and estimate item due dates.
  - Flow Efficiency (Work item age):  
Measures the waste within your process and whether it is increasing or decreasing. Helps identify if the process is becoming more effective and identify where flow items are waiting.
  - Flow Load:  
Measures the number of items that are in the value stream (in-progress) and how it affects efficiency.  Helps the team to hone in on how many items should be in progress at any time.
  - Flow Distribution:  
Measures what percentage of each flow item types is being delivered during each cycle.  Helps align business objectives with the work that is being delivered by the team.
  - Flow Predictability (Rally):  
Assigns cycle-times to percentiles. Provides insight into the team's cycle-times and their predictability. Ideally, the percentile lines will be close together. Answers how well a team plans and how good it is at meeting its objectives.
  - Cumulative Flow Diagram (Rally):  
Graph that depicts the time spent for each state over time.
- Leaked defects  
Number of defects found in production

## Team Meetings

- Daily Standup
- Stakeholder Planning Meeting

## Rally Flow Based Tracking

## Kanban Verrsus Scrum

|Kanban|Scrum|
|:---|:---|
|Continuous delivery|Short, time-boxed sprints|
|Minimal process and overhead|Prescribed sprint events and roles|
|Completing individual items quickly|Completing a batch of work quickly|
|Measures cycle time|Measures sprint velocity|
|Focuses on efficient flow|Focuses on predictability|
|Limits WIP for individual items|Limits WIP at a sprint level|
|Individual work items are pulled|Work is pulled in batches at sprint planning|
|No prescribed roles|Has prescribed roles (Scrum master, product owner, developer)|Kanban board can be organized around a single cross-functional team or multiple specialized teams|
|Scrum board is organized around a single cross-functional teamChanges can be made at any time so flow is more flexible|Changes are only allowed in the product backlog, never within a sprint|
|Requires minimal training|Requires more training|
|Good for teams where only incremental improvements are needed|Good for teams where fundamental changes are needed|

From [Today’s Project Management Blueprint: A Comparison of Lean, Agile, Scrum, and Kanban](https://www.toptal.com/project-managers/agile/project-management-blueprint-part-1-agile-scrum-kanban-lean)

## References

- [Rally Kanban Principles](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/kanban-principles.html)
- [Rally Kanban Metrics](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/kanban-metrics.html)
- [Rally Cycle Time](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/flow-based-tracking/team-board-page/view-charts-on-the-team-board/cycle-time-on-the-team-board/cycle-time-and-software-flow-efficiency--sfe--on-the-team-board.html)
- [Rally CFD](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/flow-based-tracking/team-board-page/view-charts-on-the-team-board/cumulative-flow-for-the-team-board.html)
- [Rally Team Board](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/flow-based-tracking/team-board-page.html)
- [Kanban Principles](//https/teamhood.com/?url=https%3A%2F%2Fteamhood.com%2Fkanban-resources%2Fkanban-principles%2F)
- [Flow Metrics Charts](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/flow-based-tracking/team-board-page/view-charts-on-the-team-board/flow-metrics-charts.html)
- [Cumulative Flow Diagram](https://techdocs.broadcom.com/us/en/ca-enterprise-software/valueops/rally/rally-help/using/use-flow-based-boards-kanban/flow-based-tracking/team-board-page/view-charts-on-the-team-board/cumulative-flow-for-the-team-board.html)
- [Today’s Project Management Blueprint: A Comparison of Lean, Agile, Scrum, and Kanban](https://www.toptal.com/project-managers/agile/project-management-blueprint-part-1-agile-scrum-kanban-lean)
