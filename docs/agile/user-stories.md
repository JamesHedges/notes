# User Stories

## What is a Good Story?

- Relevant Agile Principles at work here:
    - Continuous attention to technical excellence and good design enhances agility.
    - Simplicity--the art of maximizing the amount of work not done--is essential.
    - Working software is the primary measure of progress.
- Running software is value delivered!
    - Small commits allow for
        - Faster feedback loop - early course corrections are cheaper than later corrections
        - Helps avoid unnecessary work - add small value slices until it good enough
        - Detect issue sooner - architecture, design, conflicting requirements
    - Smaller stories are easier to reason and validate
    - Easier to communicate
- A clear description, user acceptance criteria and test plan ensure everyone understands what is expected of the story
    - Stories are collaboration-dependent; goal = keep everyone informed and on the same page
    - Collaborative conversations that allow for creativity
    - Team is focused on outcomes to create overall better products
- What a User Story Is Not
    - _Is not_ a requirements list
    - _Is not_ coding instructions
    - _Is not_ focused on "how to build"

## BDD

- Describes business outcomes
- Drills into the feature set
- Each feature is captured in a story that defines:
    - Scope
    - Acceptance Criteria
- Business requirements may be too course grained to be useful
    - Define requirements at an intermediate level
- BDD position is you can turn a requirement into implemented, tested, production-ready code simply and effectively
    - Requirements must be specific enough to inform and create a common understanding of the scope
    - Agree on "done"
- A story is
    - Description of requirements and business benefit
    - Acceptance criteria that defines "done"
- Contract to agile story that is a
    - Promise to a conversation
    - Or a description of a feature

### Structure

BDD provides a structure for a story that looks like the following:

```text
Title (one line describing the story)

Narrative:
As a [role]
I want [feature]
So that [benefit]

Acceptance Criteria: (presented as Scenarios)

Scenario 1: Title
Given [context]
  And [some more context]...
When  [event]
Then  [outcome]
  And [another outcome]...

Scenario 2: ...

```

#### Process

- BA talks with stakeholder and frames the feature or requirement
- Tester then helps define the scope of the story using scenarios and acceptance criteria
- Technical representative provides
    - Ballpark estimate on amount of work
    - Offer alternative approaches
- This is an iterative process
- Sometimes the development team will not know enough to make an estimate and create a spike

#### Characteristics of a Good Story

- Title should describe an activity
- The narrative should include a role, a feature, and a benefit
- The Scenario title should say what's different
- Describe the scenario in terms of _Givens_, _Events_, and _Outcomes_ (Given/When/Then)
    - A given should define all of, and no more than, the required context
    - The event should describe the feature
        - Only a single event, typically scoped to a single call into a system
- Should fit into an iteration

#### BDD Example

```text
Story: Account Holder withdraws cash

As an Account Holder
I want to withdraw cash from an ATM
So that I can get money when the bank is closed

Scenario 1: Account has sufficient funds
Given the account balance is $100
 And the card is valid
 And the machine contains enough money
When the Account Holder requests $20
Then the ATM should dispense $20
 And the account balance should be $80
 And the card should be returned

Scenario 2: Account has insufficient funds
Given the account balance is $10
 And the card is valid
 And the machine contains enough money
When the Account Holder requests $20
Then the ATM should not dispense any money
 And the ATM should say there are insufficient funds
 And the account balance should be $20
 And the card should be returned

Scenario 3: Card has been disabled
Given the card is disabled
When the Account Holder requests $20
Then the ATM should retain the card
And the ATM should say the card has been retained

Scenario 4: The ATM has insufficient funds
...
```

## INVEST

- Independent
    - Self-contained and can be prioritized by something other than a technical dependency
    - Ability to develop in any sequence (even if it means building in temporary "scaffolding" so it can be tested independently
- Negotiable
    - It is an invitation to a conversation - captures the essence of what is desired
    - Not a contract
    - Leaves room on the details of what and how
- Valuable
    - Should add some incremental value for the user
    - May need to accumulate several stories to achieve that value (MVP)
- Estimable
    - Small and negotiable
    - Volume, complexity, knowledge, uncertainty
- Small
    - Frequent feedback and managing risk
    - Fit into an iteration
    - focused on doing the simplest thing that works - then stop
- Testable
    - Understood enough to know how to test it
    - Definition of done

## Splitting Stories

> add notes here

## References

- [_What's in a Story?_](https://dannorth.net/whats-in-a-story/), Dan North
- [_The Humanizing Work Guide to Splitting User Stories_](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/), Richard Lawrence, Peter Green
