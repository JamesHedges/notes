<!-- omit in toc -->
# AngularJS Cheat Sheet

>This cheat sheet if for the first version of AngularJS

- [Modules](#modules)
  - [App](#app)
  - [Controller](#controller)
  - [Service](#service)
  - [Controller Versus Service](#controller-versus-service)
- [Wait Cursor](#wait-cursor)
- [References](#references)
  - [Version 1.2.16](#version-1216)
  - [John Papa Style Guide](#john-papa-style-guide)

## Modules

### App

AngularJS controllers, which create state and functions that our view (HTML) then uses. They are stateful and great for

- Which model and data fields to fetch and show in the HTML
- User interaction, as in what needs to happen when a user clicks something
- Presentation logic, such as how a particular UI element should be styled, or whether it should be hidden

### Controller

>Starting with AngularJS 1.2, you can now use the controller as syntax.  
>This does not require injecting $scope, use 'this' instead (but don't forget your js concepts about closure)  
>Scope will go out of scope when the controller is not active whereas $scope does not.  
>When does the controller get destroyed?

### Service

### Controller Versus Service

How to decide what goes into a controller versus a service

| Controllers                                                                                                                          | Services                                                                                                            |
| :----------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------ |
| Presentation Logic                                                                                                                   | Business Logic                                                                                                      |
| Directly linked to a view                                                                                                            | Independent of views                                                                                                |
| Drives the UI                                                                                                                        | Drives the application                                                                                              |
| One-off, specific                                                                                                                    | Reusable                                                                                                            |
| Responsible for decisions like what data to fetch, what data to show, how to handle user interactions, and styling and display of UI | Responsible for making server calls, common validation logic, application-level stores, and reusable business logic |

## Wait Cursor

Here is a simple way to start and stop the wait cursor.

```javascript
        function toggleWait() {
            var body = angular.element(document).find('body');
            if (body.hasClass('waiting'))
                body.removeClass('waiting');
            else
                body.addClass('waiting');
        }
```
... and the CSS

```css
.waiting { cursor: wait;}
```

## References

### Version 1.2.16

- [Angular Developer Guide](https://code.angularjs.org/1.2.16/docs/guide)
- [Angular Forms](https://code.angularjs.org/1.2.16/docs/guide/forms)
- [Angular UI Bootstrap](https://code.angularjs.org/1.2.16/docs/guide/forms)
- [AngularJS Tutorial - Jenkov.com](http://tutorials.jenkov.com/angularjs/index.html)
- [Introduction & Installation - FormGet.com](https://www.formget.com/what-is-angular-js/)

### John Papa Style Guide

- [Angular 1 Style Guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md)
- [Reference Code](https://github.com/johnpapa/ng-demos/tree/master/modular/src/client)
