# Bash Notes

## Variables

### Naming

- First character → a letter or underscore
- Allowed → letters, numbers, & underscore
- Don't use pre-defined varialbe name
  - Bash pre-defined varialbes - HOME, PATH, PWD, USER, etc.
- Uppercase / Lowercase
  - User variables -> use lowercase
  - Nothing is stopping you from using uppercase for your variables

### Assignment

- No spaces around `=`, e.g. `$num=100`
- Curly braces `{}`, are for string interpolation

### set & unset

- **set** allows you to change the values of shell options and set the positional parameters, or to display the names and values of shell variables.
    - **-u option** Treat unset variables and parameters other than the special parameters `@` or `*` as an _error_ when performing parameter expansion. An error message will be written to the standard error, and a non-interactive shell will exit.
- **unset** deletes the variable

### types

- **Local** Accessible in the current instance of a shell.
- **Evnironmental** Accessible in the current instance of the shell & in any child process.
- **Shell** Special variables defined by the shell.

## Functions

- Use `function` keyword to create a function
    - Access params in sequence, e.g. ${1}, ${2}...
- Include function in another file with `source <func-file>`

!!! Example "Script with a function"

    ```bash
    #!/bin/bash

    source ./call-me.sh

    function Greetings {
        echo "Greetings, $1, from a function"
    }

    echo "Calling the greetings function..."
    Greetings "Jim"
    CallMe
    ```

!!! Example "Function in a file included using source <my_file>"

    ```bash
    #!/bin/bash

    function CallMe {
        echo "You have called the CallMe function"
    }
    ```

## Using jq

### Filtering JSON

Use `jq` to filter JSON

```bash
kt get services -n oic-curo -o json | jq -r .items[].metadata.name
```

### Reading JSON File and Using Objects

!!! Example "JSON file to read with bash"

    ```json
    [
      {"english": "one", "espanol": "uno", "value": 1},
      {"english": "two", "espanol": "dos", "value": 2},
      {"english": "three", "espanol": "tres", "value": 3}
    ]
    ```

!!! Example "Bash script to read the JSON file"

    ```bash
    #!/bin/bash

    for row in $(jq -r '.[] | @base64' $1);
    do
      _jq() {
        echo ${row} | base64 --decode | jq -r ${1};
      }
      echo "English: $(_jq '.english'), Value: $(_jq '.value')";
    done;
    ```

## Copy & paste

!!! Example "Copy and Paste from bash"

    ```bash
    # copy
    cat some-file | pbcopy

    # paste
    pbpaste > some-other-file
    ```

## Reference

- [gnu.org](https://gnu.org)
- [jq Commands in Linux | Code Examples for JSON Parsing](https://ioflood.com/blog/jq-linux-command/)