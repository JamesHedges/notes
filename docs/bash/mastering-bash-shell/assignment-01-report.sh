#!/bin/bash

# echo "10 \"Bash\" L100 \$99 \$12"
# echo "105 \"Linux\" L200 \$1599 \$15.0"

# Format report to look like:

#    ID     COURSE NAME       LEVEL    ACTUAL PRICE     DISC. PRICE
#---------------------------------------------------------------------
#    10       Bash            L100            99.00           12.00
#   105      Linux            L200          1599.00           15.00

function print_heading {
    heading1="ID COURSE NAME LEVEL ACTUAL PRICE DISC. PRICE"
    heading2="---------------------------------------------------------------------"

    printf "%6s%11s%5s%12s%10s%6s%10s%5s\n" $heading1
    printf "%s\n" $heading2
}

function print_courses {
    course1="10 "Bash" "L100" 99 12"
    course2="105 "Linux" "L200" 1599 15.0"
    printf "%6d     %6s            %-5s      %10.2f     %10.2f\n" $course1
    printf "%6d     %6s            %-5s      %10.2f     %10.2f\n" $course2
    printf '\n'
}

print_heading
print_courses