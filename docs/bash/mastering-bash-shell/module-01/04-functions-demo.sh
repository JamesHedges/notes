#!/bin/bash

echo "Welcome to my script"

# Functions may  be declared in multiple ways.

# Declared using function keyword where braces and parameters are optional
function welcome_message_1 {
    echo "Welcome to my function function"
}

# Declared without function keyword with braces required
welcome_message_2() {
    echo "Welcome to my implied function"
}

welcome_message_1
welcome_message_2

echo "Goodbye!"