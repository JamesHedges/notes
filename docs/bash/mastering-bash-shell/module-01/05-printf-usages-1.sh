#!/bin/bash

echo "Hello echo - cursor moved to next line"
printf "Hello Printf - cursor on same line"
printf "Hello Printf\n Use line end to place cursor on\nnext line...\n"
printf "%s\n" "Hello printf formatter" "Each param fed into the formatter" "each will be on their own line"
printf "%sABC " "One-" "Two-" "Three-"
printf "\n"

# The 10 preceding the s creates a field witdth that is right justified"
printf "%10s %d " "John" 25 "Mary" 26

printf '\n'
# Add '-' before 10 to left justify
printf "%-10s: %5d\n" "John" 25 "Mary" 26