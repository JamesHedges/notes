
#!/bin/bash

#"A" "B" "C" "D" "E"

#tabbed
printf "%s\t" "A" "B" "C" "D" "E"
printf '\n'

# sequence of formatters
printf "%s\t %s\n" "A" "B" "C" "D" "E"

# Format floating point numbers - total field width 10 = 7 + 1 +2
printf "%10.2f\n" 125.125 125.126 500 3.1456