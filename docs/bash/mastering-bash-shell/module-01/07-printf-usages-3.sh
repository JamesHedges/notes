
#!/bin/bash

n1=10
n2=20

# expression declared using back-ticks
sum=`expr $n1 + $n2`

printf "Sum of two numbers is %d\n" $sum
printf "Sum of two numbers is %.2f\n" $sum