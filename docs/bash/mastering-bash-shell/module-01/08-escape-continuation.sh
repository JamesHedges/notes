#!/bin/bash

# Escape with back-slash
echo "The name of eh book is \"Linux\""

echo "The cost of the book is \$12.50"

# Line continuation with space-back-slash
echo "This is a sample line that is very long \
......................................  \
It is really long!"
