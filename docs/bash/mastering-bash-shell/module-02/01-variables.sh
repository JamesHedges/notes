#!/bin/bash

# Demo: Variables - Basics

# Can't start with number
# Fails with Bash Shell Scripting: command not found
#1course_name1="Bash Shell Scripting"

# Fails w/ bad substitution
#echo ${1course_name1}

# Correct
course_name1="Bash Shell Scripting"

echo "${course_name1}"

# Assign again
course_name1="Linux Command Line"

echo "1: Welcome to the course - ${course_name1}"

# Read only variable

readonly course_name2="Mastering Linux Command Line"
# will fail
#course_name2="Python Scripting"
echo "2: Welcome to the course - ${course_name2}"

# set & unset

course_name3="AWS VPC"
unset course_name3
echo "3: Welcome to the course - ${course_name3}"

# will cause scripting engine to error on undefined vars
set -u

course_name4="Mastering SSH"
# Undefine the va
unset course_name4
# Fails with unbound variable error
echo "4: Welcome to the course - ${course_name4}"
