# Java

## Configuration

### Artifactory

Required file: \~/users/[msid]/.m2/settings.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <profiles>
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>central</id>
                    <name>libs-releases</name>
                    <url>https://repo1.uhc.com:443/artifactory/libs-releases</url>
                </repository>
                <repository>
                    <id>snapshots</id>
                    <name>libs-snapshots</name>
                    <url>https://repo1.uhc.com:443/artifactory/libs-snapshots</url>
                </repository>
            </repositories>
            <pluginRepositories>

                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>central</id>
                    <name>libs-releases</name>
                    <url>https://repo1.uhc.com:443/artifactory/libs-releases</url>
                </pluginRepository>
                <pluginRepository>
                    <id>snapshots</id>
                    <name>libs-snapshots</name>
                    <url>https://repo1.uhc.com:443/artifactory/libs-snapshots</url>
                </pluginRepository>
            </pluginRepositories>
            <id>artifactory</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>artifactory</activeProfile>
    </activeProfiles>
</settings>
```
