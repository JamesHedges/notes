<!-- omit in toc -->
# nUnit Cheat Sheet

- [Testing for Exceptions](#testing-for-exceptions)
- [Test Setup for DelegatingHandlers](#test-setup-for-delegatinghandlers)
  - [HttpContextWrapper and DelegatingHandler References](#httpcontextwrapper-and-delegatinghandler-references)
- [Alternative Strategy for Dealing with HttpContext](#alternative-strategy-for-dealing-with-httpcontext)

## Testing for Exceptions

This technique allows the tester to keep the canonical AAA pattern. It also affords the opportunity to inspect the caught exception further.

```csharp
        [Test]
        public void UpdateNpi_UpdateInvalid_ThrowsAnException()
        {
            // Arrange
            var sut = SystemUnderText();

            // Act

            TestDelegate func = async () => await sut.SomeMethodAsync();
            var except = Assert.Catch(func);

            // Assert
            Assert.IsInstanceOf<Exception>(except);
        }

        // async delegate
        [Test]
        public void UpdateNpi_UpdateInvalid_ThrowsAnException()
        {
            // Arrange
            var sut = SystemUnderText();

            // Act

            AsyncTestDelegate func = async () => await sut.SomeMethodAsync();
            var except = Assert.CatchAsync(func);

            // Assert
            Assert.IsInstanceOf<Exception>(except);
        }
```

## Test Setup for DelegatingHandlers

This is for setting-up tests that have the System.Net.HtpContextMessage injected. In this case, the test is for an HttpMessageHandler derived from DelegatingHandler.

1. Create a test HttpContextWrapper, deriving from HttpContextBase and add test arrangement for the context
1. Create an inner handler used by the message handler, arranging the desired response
1. Arrange an HttpRequestMessage
1. Set the wrapper as a HttpRequestMessage property "MS_HttpContext" in test setup
1. Arrange the handler under test, setting its inner handler
1. Create an invoker for the handler
1. Invoke the SendAsync method. An HttpResponseMessage will be returned
1. Assert on the response message

```csharp
    // HttpContextWrapper for the test HttpContext
    public class TestHttpContextWrapper : HttpContextBase
    {
        private readonly List<Claim> _claims;

        public TestHttpContextWrapper(string correlationId)
        {
            _claims = new List<Claim>
            {
                new Claim("http://commonwellalliance.org/claims/CorrelationId", correlationId)
            };
        }

        public override IPrincipal User => new ClaimsPrincipal(new ClaimsIdentity(_claims));
    }

    // Inner handler for the handler
    public class TestHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => new HttpResponseMessage(HttpStatusCode.OK), cancellationToken);
        }
    }

    // Arrange the wrapper in HttpRequestMessage
    [Test]
    public async Task WhenMessageHandlerGetFormatParamXxml_ReturnsBadRequest()
    {
        // Arrange
        var url = "https://test";
        var queryString = "_format=xxml";
        var correlationId = Guid.NewGuid().ToString();

        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, new Uri($"{url}?{queryString}"));

        request.Properties.Add("MS_HttpContext", new TestHttpContextWrapper(correlationId));

        var handler = new MyHandlerHandler(_logger)
        {
            InnerHandler = new TestHandler()
        };
        var invoker = new HttpMessageInvoker(handler);

        // Act
        var response = await invoker.SendAsync(request, new CancellationToken());

        // Assert
        Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
    }
```

### HttpContextWrapper and DelegatingHandler References

- [Difference between HttpContext and HttpContextWrapper in terms of Unit Testing and in terms of Web Forms and MVC](https://stackoverflow.com/questions/16767131/difference-between-httpcontext-and-httpcontextwrapper-in-terms-of-unit-testing-a)
- [Unit testing DelegatingHandler](https://stackoverflow.com/questions/31650718/unit-testing-delegatinghandler)
- [HttpContextWrapper Class](https://docs.microsoft.com/en-us/dotnet/api/system.web.httpcontextwrapper?view=netframework-4.5.2)

## Alternative Strategy for Dealing with HttpContext

HttpContext is a very complicated object and is difficult to mock. It even prevents mocking with tools like Moq. To avoid testability issues when using HttpContext, inject an HttpContext helper that abstracts away any HttpContext usage.

See [DON'T MOCK HTTPCONTEXT](https://volaresystems.com/technical-posts/dont-mock-httpcontext)
